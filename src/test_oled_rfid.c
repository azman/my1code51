/*----------------------------------------------------------------------------*/
#include "uart_hexascii.h"
#include "oled_ssd1306.h"
#include "cstr_hexascii.h"
#include "frc522.h"
#define APPTITLE "CODE51 RFID/OLED"
/*----------------------------------------------------------------------------*/
void main(void) {
	__xdata cstr_t buff; /* default: 64-bytes long */
	unsigned char temp, stat, loop, size;
	unsigned char pdat[FRC522_MAX_RXSIZE], reqa[2];
	/** initialize */
	cstr_init(&buff);
	uart_init();
	i2c_init();
	oled1306_init();
	spi_init();
	atqa = reqa;
	/** say something... */
	uart_puts("\n----------------\n");
	uart_puts(APPTITLE);
	uart_puts("\n----------------\n\n");
	oled1306_puts(APPTITLE);
	temp = frc522_init();
	if (!temp||temp==0xff) {
		uart_puts("** Cannot find FRC522 hardware! Aborting!\n");
		hang();
	}
	uart_puts("FRC522 found. Firmware version is 0x");
	uart_send_hexbyte(temp);
	uart_puts(".\n");
	/** main loop */
	while (1) {
		stat = frc522_scan(pdat,&size);
		if (stat==FRC522_OK) {
			uart_puts("## TAG(");
			uart_send_hexbyte(stat);
			uart_puts("|");
			uart_send_hexbyte(reqa[0]);
			uart_puts(",");
			uart_send_hexbyte(reqa[1]);
			uart_puts("):");
			cstr_null(&buff);
			for (loop=0;loop<size-1;loop++) { /** UID is size-1 bytes */
				uart_send('[');
				uart_send_hexbyte(pdat[loop]);
				cstr_append_hexbyte(&buff,pdat[loop]);
				uart_send(']');
			}
			uart_puts("[CSUM:");
			uart_send_hexbyte(pdat[loop]);
			cstr_append_hexbyte(&buff,pdat[loop]);
			uart_puts("]\n");
			oled1306_set_cursor(2,0);
			oled1306_puts("#TAG: ");
			oled1306_puts(buff.buff);
			loop_delay(3000);
			oled1306_clear_row(2);
		}
		else if (stat!=FRC522_ERROR_NO_TAG&&stat!=FRC522_ERROR_REQ_A) {
			uart_puts("** Scan Failed (0x");
			uart_send_hexbyte(stat);
			uart_puts("):");
			for (loop=0;loop<FRC522_MAX_RXSIZE;loop++) {
				uart_send('[');
				uart_send_hexbyte(pdat[loop]);
				uart_send(']');
			}
			uart_send('\n');
			cstr_assign(&buff,"*TAG: [ERROR:");
			cstr_append_hexbyte(&buff,stat);
			cstr_add1(&buff,']');
			oled1306_set_cursor(3,0);
			oled1306_puts(buff.buff);
			loop_delay(3000);
			oled1306_clear_row(3);
		}
	}
}
/*----------------------------------------------------------------------------*/