/*----------------------------------------------------------------------------*/
#include "mcu51.h"
#include "timer_tick.h"
#include "utils_lite.h"
#include "oled_ssd1306.h"
/*----------------------------------------------------------------------------*/
/**
 * Simple test for OLED
 * - display 16-bit counter value on OLED (ssd1306)
 * - a running light on P0 (changes with count value)
 * - P2.0 blinks at same rate IF P1.0 is at logic 1 (default)
**/
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char buff[8];
	unsigned char offs, mask, runl;
	unsigned int loop;
	timer_init();
	timer_tick_exec();
	i2c_init();
	oled1306_init();
	oled1306_puts("-- LOOP:");
	offs = 9*CHAR_WIDTH; mask = 0; loop = 0;
	runl = 1; P0 = runl;
	while (1) {
		mask = P1_0?1:0;
		if (!mask) P2_0 = 0;
		if (timer_ticked()) {
			oled1306_clear_row_from(0,offs);
			oled1306_set_cursor(0,offs);
			uint16_2cstr(buff,loop++);
			oled1306_puts(buff);
			if (mask) P2_0 = !P2_0;
			runl <<= 1; if (!runl) runl = 1;
			P0 = runl;
			timer_tick00();
		}
	}
}
/*----------------------------------------------------------------------------*/
