/*----------------------------------------------------------------------------*/
/* override default tick values */
#define TIMER_TICK_LEN TIMER_VAL50MS
#define TIMER_TICK_CNT TIMER_LOOP_1S
/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "timer_tick.h"
/*----------------------------------------------------------------------------*/
#define TRIGGER_STEP 5
/*----------------------------------------------------------------------------*/
/**
 * Simple test for 4 active low switches at P1.3:P1-0
 * - transfer inverted logic from P1 to P2 (full 8-bits)
 * - if no switch is pushed within 5secs:
 *   > a 1hz running light will be at P2.3:P2.0 (if active high leds connected)
 * - also testing MODE1 (higher logic 1 current drive) for P2
**/
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char runl, push, temp, trig;
	timer_init();
	timer_tick_exec();
	runl = 1;
	push = 0;
	trig = TRIGGER_STEP;
	/* higer current drive for P2 - can be disable by shorting P1.7 to gnd */
	if (P1_7) P2MODE1();
	P2 = ~P1;
	while (1) {
		temp = P1&0x0f;
		if (temp!=0x0f) {
			push=1;
			trig = TRIGGER_STEP;
		}
		if (push) P2 = ~P1;
		if (timer_ticked()) {
			if (!trig) {
				if (!push) {
					P2 = runl;
					runl <<= 1;
					if (!(runl&0x0f)) runl = 1;
				}
				else push = 0;
			}
			else trig--;
			timer_tick00();
		}
	}
}
/*----------------------------------------------------------------------------*/