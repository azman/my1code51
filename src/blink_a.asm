; toggle p3.4 using proper timer - 50ms delay
led0	bit 0B4h ; P3.4
		cseg at 0000h
		jmp init
		cseg at 000bh
		jmp blink
		cseg at 0040h
init:	mov TMOD,#11h
		mov TH0,#4bh
		mov TL0,#0fdh
		setb EA
		setb ET0
		setb TR0
here:	jmp here
blink:	clr TR0
		cpl led0
		mov TH0,#4bh
		mov TL0,#0fdh
		setb TR0
		reti
		end
