; toggle p3.4 using max dual-nested loop delay
led0	bit 0B4h ; P3.4
		cseg at 0000h
		jmp init ; skip interrupt vector table!
		cseg at 0040h
init:	cpl led0
		mov r1,#255
loop:	mov r0,#255
		djnz r0,$
		djnz r1,loop
		jmp init
		end
