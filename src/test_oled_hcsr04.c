/*----------------------------------------------------------------------------*/
#include "timer.h"
#define FPSIZE 2
#include "utils_float.h"
#include "oled_ssd1306.h"
#define INTRO_MESG "TESTING HC-SR04"
#ifdef __MY1DEBUG__
#include "uart.h"
#endif
/*----------------------------------------------------------------------------*/
my1sbit(TRIG,PIN16);
my1sbit(ECHO,PIN17);
/*----------------------------------------------------------------------------*/
#define OLED_DISPLAY_ROW 3
#define OLED_DISPLAY_OFFS (5*CHAR_WIDTH)
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char wait;
	unsigned int tval,loop;
	float fval, dist;
	char buff[16];
	timer_init();
#ifdef __MY1UART_H__
	uart_init();
	uart_puts("\n"INTRO_MESG"\n");
#endif
	i2c_init();
	oled1306_init();
	oled1306_puts(INTRO_MESG);
	oled1306_set_cursor(OLED_DISPLAY_ROW,0);
	oled1306_puts("Dist:");
	while (1) {
		timer_prep(0);
		TRIG = 1;
		wait=10; while(--wait); // around 10us?
		TRIG = 0;
		while (!ECHO); timer_exec();
		while (ECHO); timer_stop();
		tval = timer_read();
		fval = (float)tval * 1.085; // in us
		dist = fval / 58.0; // in cm
		float2str(buff,dist);
		oled1306_clear_row_from(OLED_DISPLAY_ROW,OLED_DISPLAY_OFFS);
		oled1306_set_cursor(OLED_DISPLAY_ROW,OLED_DISPLAY_OFFS);
		oled1306_puts(buff);
		oled1306_puts("cm");
#ifdef __MY1UART_H__
		uart_puts("-- Distance=");
		uart_puts(buff);
		uart_puts(" cm\n");
#endif
		loop=60000; while(--loop);
	}
}
/*----------------------------------------------------------------------------*/