;-------------------------------------------------------------------------------
; CLOCK PROGRAM
; - for display designed by HAL
; - using rtc8583 on nxp's p89v51rx2
; - rewrite from old program
;-------------------------------------------------------------------------------

				$nomod51 ; switch off built-in definitions/symbols!
p0				data 080h
p2				data 0a0h
b				data 0f0h

; useful constants
;stack_init		equ 3fh ; default is 07h 
;test_size		equ 20h
disp_size		equ 4
s24h_mask		equ 80h
sapm_mask		equ 40h
chk_24h			equ 24h
chk_13h			equ 13h
chk_apm			equ 12h
I2C_SLAVE		equ 0a0h

; internal ram bit-addr locations (byte addr: 0x3E?)
erb				bit 070h ; ack error flag
chg				bit 071h ; change (transition) flag
srf				bit 072h ; temp i2c last byte read flag
tam				bit 073h ; temp am flag
tpm				bit 074h ; temp pm flag

; bit-addressable port-pins
sda				bit 0b2h ; i2c data line (p3.2)
scl				bit 0b3h ; i2c clock line (p3.3)
sw0				bit p0.0
sw1				bit p0.1
sw2				bit p0.2
sw3				bit p0.3
dot				bit p2.0 ; sec-blink
SER				bit p2.1
RCL				bit p2.2
SCK				bit p2.3
dam				bit p2.4
dpm				bit p2.5

; internal ram
tc0				data 070h ; control byte
tss				data 071h ; second byte (only for config?)
tmm				data 072h ; minute byte
thh				data 073h ; hour byte
tmp				data 074h ; temporary storage
tgf				data 075h ; sec toggle byte (subb!)

				;xseg at 0100h ; actually internal 768b-sram
;test_buff:		ds test_size

				;scratchpad direct ram
				dseg at 68h
disp_buff:		ds disp_size ; must NOT be more than 8 bytes

				cseg at 0000h
				sjmp start ; start must be @ <= (0002h+7fh)

;**************************************************
; main routine
;**************************************************

				cseg at 0080h
start:			;mov sp,#stack_init ; want to use bit-addressable rams?
				clr dam ; clear am
				clr dpm ; clear pm
				clr dot ; switch off by default?
				mov tgf,#00h ; toggle byte is zero (init)
				mov dptr,#table ; init segment display table
				; main routine
main:			call do_read
				; now we update display buffer
				call do_buff
				; write out to clock display board
				call do_disp
				; check button 0 - minute change
chkb0:			call do_swi0
				jb chg, chkwr
				; check button 1 - hour change
chkb1:			call do_swi1
				jb chg, chkwr
				; check button 2 - 24h mode toggle
chkb2:			call do_swi2
				jb chg, chkwr
				; check button 3 - soft reset to test value
chkb3:			call do_swi3
				jb chg, main
				; monitor tf flag
chktm:			call do_check
				jb chg, main
				sjmp chkb0
				; write to rtc if needed
chkwr:			call do_write
				sjmp main

;**************************************************
; subroutines
;**************************************************

; routine to check switch 0 - minute change

do_swi0:		clr chg
				jb sw0,do_swi0_end
				jnb sw0,$
				mov a,tmm
				add a,#01h
				da a
				cjne a,#60h,savem
				mov a,#00h
savem:			mov tmm,a
				mov tss,#00h ; make seconds to 0! new minute!
				setb chg
do_swi0_end:	ret

; routine to check switch 1 - hour change

do_swi1:		clr chg
				jb sw1,do_swi1_end
				jnb sw1,$
				mov a,thh
				anl a,#3fh
				add a,#01h
				da a
				mov r0,a
				mov a,thh
				anl a,#s24h_mask
				jnz chk13
chk24:			cjne r0,#chk_24h,saveh
				; make it 0000 hours
				mov r0,#00h
				jmp saveh
chk13:			cjne r0,#chk_13h,chkxm
				; make it 01:00 am/pm
				mov r0,#01h
				jmp saveh
				; check am/pm toggle?
chkxm:			cjne r0,#chk_apm,saveh
				xrl thh,#sapm_mask ; toggle am/pm flag
saveh:			mov a,r0
				anl thh,#0c0h
				orl thh,a ; cannot simply mov thh,a !
				setb chg
do_swi1_end:	ret

; routine to check switch 2 - 24h mode toggle

do_swi2:		clr chg
				jb sw2,do_swi2_end
				jnb sw2,$
				mov a,thh
				anl a,#s24h_mask
				jnz make24
				; make into 12h time
make12:			mov a,thh
				anl a,#3fh
				mov tmp,a
				clr c
				subb a,#12h
				jnc conv12
				mov a,tmp
				jnz savex
				mov a,#12h
				jmp savex
conv12:			jnz no_add12
				add a,#12h
no_add12:		da a
				orl a,#sapm_mask ; pm!
				jmp savex
				; make into 24h time
make24:			mov a,thh
				anl a,#sapm_mask
				jnz conv24
				mov a,thh
				anl a,#3fh
				cjne a,#12h, savex
				subb a,#12h
				da a
				jmp savex
conv24:			mov a,thh
				anl a,#3fh
				cjne a,#12h, do_add12
				jmp savex
do_add12:		add a,#12h
				da a
savex:			anl thh,#0c0h
				orl thh,a
				xrl thh,#s24h_mask ; toggle 24h mode
				setb chg
do_swi2_end:	ret

; routine to check switch 3 - soft reset to test value

do_swi3:		clr chg
				jb sw3,do_swi3_end
				jnb sw3,$
				; local default setting
				mov tc0,#00h ; mode ext osc,nomask,noalm
				mov tss,#50h ; seconds bcd
				mov tmm,#59h ; minutes bcd
				mov thh,#91h ; 12h,am: hour bcd
				; init rtc
				call do_irtc
				setb chg
do_swi3_end:	ret

; routine to check transition flag on rtc

do_check:		clr chg
				mov a,#I2C_SLAVE
				call i2c_initwr
				mov a,#00h ; word address (00)
				call i2cout
				mov a,#I2C_SLAVE
				call i2c_initrd
				; start read - single byte read
				call i2cin0
				mov tc0,a ; control byte
				call i2csp
				; now we check
				anl a,#01h ; get tf (lsb) only!
				mov tmp, a
				clr c
				subb a,tgf ; look for transition!
				jz dot0 ; no transition!
				mov tgf,tmp
				jnc dot1  ; 0->1 transition!
				clr dot ; 1->0 transition!
dot0:			ret
dot1:			setb chg
do_check_end:	ret

; routine to init rtc 8583 setting & value

do_irtc:		mov a,#I2C_SLAVE
				call i2c_initwr
				mov a,#00h ; word address (control reg)
				call i2cout
				mov a,tc0 ; mode ext osc,nomask,noalm
				call i2cout
				mov a,#00h ; 1/100 seconds bcd
				call i2cout
				mov a,tss ; seconds bcd
				call i2cout
				mov a,tmm ; minutes bcd
				call i2cout
				mov a,thh ; hour bcd
				call i2cout
				call i2csp
				ret

; routine to write data to rtc

do_write:		mov a,#I2C_SLAVE
				call i2c_initwr
				mov a,#01h ; word address (1/100 seconds reg)
				call i2cout
				mov a,#00h ; 1/100 second byte - realign this!
				call i2cout
				mov a, tss ; second byte
				call i2cout
				mov a,tmm ; minutes bcd
				call i2cout
				mov a,thh ; hour bcd
				call i2cout
				call i2csp
				ret

; routine to read data from rtc

do_read:		mov a,#I2C_SLAVE
				call i2c_initwr
				mov a,#02h ; word address (seconds reg)
				call i2cout
				mov a,#I2C_SLAVE
				call i2c_initrd
				; start read
				call i2cin1
				mov tss,a ; second byte
				call i2cin1
				mov tmm,a ; minute byte
				call i2cin0
				mov thh,a ; hour byte
				call i2csp
				ret

; routine to update display from disp_buff

do_buff:		mov a,tmm
				anl a,#0fh
				movc a,@a+dptr
				mov disp_buff,a
				mov a,tmm
				swap a
				anl a,#0fh
				movc a,@a+dptr
				mov disp_buff+1,a
				mov a,thh
				anl a,#0fh
				movc a,@a+dptr
				mov disp_buff+2,a
				mov a,thh
				swap a
				anl a,#03h ; for hour take 2-bits only
				movc a,@a+dptr
				mov disp_buff+3,a
				; update am/pm indicator status
				mov a,thh
				anl a,#s24h_mask
				jz light00 ; 24h - clr all
				mov a,thh
				anl a,#sapm_mask
				jz lightam
lightpm:		setb tpm ; light up pm
				clr tam
				ret
lightam:		setb tam ; light up am
				clr tpm
				ret
light00:		clr tam
				clr tpm
				ret

; routine to update display from disp_buff

do_disp:		clr RCL
				clr SCK
				mov r1,#disp_buff
				mov b,#disp_size
disp_byte:		mov a,@r1
				cpl a
				push b
				mov b,#8
disp_bit:		rlc a
				mov SER,c
				setb SCK
				clr SCK
				djnz b,disp_bit
				inc r1
				pop b
				djnz b,disp_byte
				setb RCL
				clr RCL
				; always light-up dot!
				setb dot
				; update am/pm indicator
				mov c,tam
				mov dam,c
				mov c,tpm
				mov dpm,c
				ret

; routine i2c start slave read (slave addr in a)

i2c_initrd:		call i2cst
				orl a,#01h ; activate read bit
				call i2cout
				jb erb,i2c_initrd ; check acknowledge
				ret

; routine i2c start slave write (slave addr in a)

i2c_initwr:		call i2cst
				call i2cout
				jb erb,i2c_initwr ; check acknowledge
				ret

; routine i2c to write out start marker

i2cst:			setb scl
				setb sda
				nop ; start condition setup time > 4.7 us
				nop ; for 11.0592MHz xtal, 1 mc = 1.085us
				nop ; 1 nop = 1 mach cycle
				nop
				clr sda
				nop ; start condition hold time > 4.0 us
				nop
				nop
				clr scl
				ret

; routine i2c to write out stop marker

i2csp:			clr sda
				nop ; stop condition setup time > 4.0 us
				nop
				nop
				setb scl
				nop
				nop
				nop
				setb sda
				ret

; routine i2c to write 1 byte out

i2cout:			mov b,#08h
i2clo:			rlc a
				jnc i2co0
				setb sda
				sjmp i2co1
i2co0:			clr sda
i2co1:			setb scl
				nop ; scl high time > 4.0 us (low time > 4.7 us)
				nop
				nop
				clr scl
				djnz b,i2clo
				setb sda
				nop
				setb scl
				clr erb
				jnb sda,i2cef ; check acknowledge bit
				setb erb
i2cef:			clr scl
				ret

; routine i2c to read 1 byte in (i2cin1=with ACK, i2cin0=no ACK)

i2cin0:			setb srf
				sjmp i2cinx
i2cin1:			clr srf
i2cinx:			mov b,#08h
				setb sda ; need this to disable int pulldwn source?
i2cli:			clr scl
				nop ; scl low time > 4.7 us (high time > 4.0 us)
				nop
				nop
				nop
				setb scl
				clr c
				jnb sda,i2ci0
				cpl c
i2ci0:			rlc a
				djnz b,i2cli
				clr scl
				jb srf,i2ci1 ; scl low to data valid time < 3.4us??
				clr sda ; send acknowledge bit if not last!
i2ci1:			clr srf ; reset stop read flag
				nop
				nop
				setb scl 
				nop
				nop
				nop
				clr scl
				ret

;**************************************************
; look-up table
;**************************************************

table:			db 40h,73h,09h,21h,32h,24h,04h,71h,00h,30h,7fh

;**************************************************
				end
;**************************************************
