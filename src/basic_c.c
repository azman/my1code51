/* toggle p3.5 using max dual-nested loop delay */
__sbit __at (0xB5) led0;
void main(void) {
	unsigned char r0, r1;
	while(1) {
		led0 = !led0;
#if 1
		r1 = 255;
		do {
			r0 = 255;
			while (--r0); /* sdcc-4.2.0 can still generate djnz from this! */
		} while (--r1);
#else
		/* just to show most compilers do not use djnz for this */
		for (r1=0;r1<(unsigned char)255;r1++) {
			r0=0; while (r0<255) r0++;
			/* the line below triggers a bug in sdcc (r0 treated as char) */
			/*for (r0=0;r0<(unsigned char)255;r0++);*/
		}
#endif
	}
}
