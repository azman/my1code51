/* toggle p3.5 using proper timer - 50ms delay */
__sfr __at (0x89) TMOD;
__sfr __at (0x8A) TL0;
__sfr __at (0x8C) TH0;
__sbit __at (0x8C) TR0;
__sbit __at (0xA9) ET0;
__sbit __at (0xAF) EA;
__sbit __at (0xB5) led0;
void blink(void) __interrupt(1) {
	TR0 = 0; led0 = !led0;
	TH0 = 0x4B; TL0 = 0xFD; TR0 = 1;
}
void main(void) {
	TMOD = 0x11; TH0 = 0x4B; TL0 = 0xFD;
	EA = 1; ET0 = 1; TR0 = 1;
	while(1);
}
