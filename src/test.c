/*----------------------------------------------------------------------------*/
/**
 * test.c
 * - written by azman@my1matrix.org
 * - simple test program (ports, timer, interrupt) for 8051-based MCU
 *   = default system tick is 1Hz (toggle every 0.5s)
 *   = P0/P1/P2/P3.4-P3.7 all blinks at system tick
 *   = P0&P2&P3.5&P3.7 are at 180deg phase shift compared to P1
 *   = P3.4&P3.6 at at the same phase as P1
 *   = P3.2 => set system tick to 1Hz
 *   = P3.3 => set system tick to 10Hz
 * - using sdcc syntax
**/
/*----------------------------------------------------------------------------*/
/* 8051-specific declarations */
__sfr __at (0x80) P0;
__sfr __at (0x90) P1;
__sfr __at (0xA0) P2;
__sfr __at (0xB0) P3;
__sfr __at (0x89) TMOD;
__sfr __at (0x8A) TL0;
__sfr __at (0x8C) TH0;
__sbit __at (0x8C) TR0;
__sbit __at (0xA8) EX0;
__sbit __at (0xAA) EX1;
__sbit __at (0xA9) ET0;
__sbit __at (0xAF) EA;
__sbit __at (0x88) IT0;
__sbit __at (0x8A) IT1;
/*----------------------------------------------------------------------------*/
/* global variable */
unsigned char step, init;
/*----------------------------------------------------------------------------*/
/* external interrupt0 handler */
void isr_ex0(void) __interrupt(0) {
	init = 10;
}
/*----------------------------------------------------------------------------*/
/* external interrupt1 handler */
void isr_ex1(void) __interrupt(2) {
	init = 1;
}
/*----------------------------------------------------------------------------*/
/* timer0 interrupt handler */
void isr_et0(void) __interrupt(1) {
	TH0 = 0x4B; TL0 = 0xFD; /* timer0 - 50ms */
	if (step>0) {
		step--;
		if (!step) {
			P0 = ~P0; /** P0 MAY BE OPEN COLLECTOR! */
			P1 = ~P1;
			P2 = ~P2;
			P3 = P3^0xF0; /** INVERT ONLY P3.4-P3.7 */
			step = init;
		}
	}
}
/*----------------------------------------------------------------------------*/
/* main function */
void main(void) {
	/* initial values for timer0 - 50ms */
	TMOD = 0x11; TH0 = 0x4B; TL0 = 0xFD;
	/* enable interrupt(s) */
	EA = 1; IT0 = 1; IT1 = 1; EX0 = 1; EX1 = 1; ET0 = 1;
	/* FIX P3.0-P3.3 AS INPUT? */
	P0 = 0x55; P2 = 0x55; P1 = 0xAA; P3 = 0x0F;
	/* initialize step & init, start timer */
	init = 10; step = init; TR0 = 1;
	/* just hang around */
	while(1);
}
/*----------------------------------------------------------------------------*/
