;-------------------------------------------------------------------------------
; LED DISPLAY PROGRAM
; - for LED display designed by HAL
; - a rewrite from ARMS' code
; - rewrite for ds89c420
;-------------------------------------------------------------------------------

				$nomod51
p2				data 0a0h
dpl				data 082h
dph				data 083h
ri_0			bit 098h
ti_0			bit 099h
scon0			data 098h
sbuf0			data 099h
sp				data 081h
pmr				data 0c4h
tmod			data 089h
;tcon			data 088h
th1				data 08dh
tr1 			bit 08eh
es0				bit 0ach
ea				bit 0afh
b				data 0f0h
dps				data 086h
psw				data 0f0h
acc				data 0e0h

disp_size		equ 64 ; 16x4
char_size		equ 5  ; 5 columns
buff_size		equ disp_size+char_size-1
zero_size		equ disp_size/char_size
temp_size		equ 200
stack_st		equ 3fh ; default is 07h 

ri				bit ri_0
ti				bit ti_0
scon			data scon0
sbuf			data sbuf0
shclk			bit p2.3
stclk			bit p2.4
data1			bit p2.5
rowport			data p2
dme0_mask		equ 01h
dp1_sel			equ 001h
dp0_sel			equ 0feh
dpx_set			equ 030h ; tog-sel, auto-inc
dpx_not			equ 0cfh

				xseg at 0000h ; actually internal 1k-sram
disp_buff:		ds buff_size
strg_buff:		ds temp_size

				;scratchpad direct ram
				dseg at 30h
count:			ds 1
bitx_mask:		ds 1
row_count:		ds 1

				;org 000h
				cseg at 000h
				jmp start

				cseg at 023h
				ljmp int_serial

				; isr until watchdog @ 063h

				cseg at 080h
start:
				mov sp,#stack_st ; want to use bit-addressable rams
				mov rowport,#00000111b ; off all row
				; setup internal 1k-sram usage
				mov a,pmr
				orl a,#dme0_mask
				mov pmr,a
				; setup timer 1 for serial communications
				mov scon,#50h ;  mode 1, 8-bit uart, enable receiver
				mov tmod,#20h ;  timer 1, mode 2, 8-bit auto-reload
				mov th1,#0fdh ;  reload value for 9600 baud
				setb tr1 ; start timer 1
				; initialize display & default text buffers
				call def_buff
				call clear_disp
disp_mode:
				setb es0 ; enable serial interrupt
				setb ea ; enable global interrupt
run_msg:
				mov dptr,#strg_buff
loop:
				movx a,@dptr
				cjne a,#0,continue
				mov r0,#zero_size
run_zero:		mov a,#' '
				call get_font
				call insert
				djnz r0,run_zero
				jmp run_msg
continue:
				push dph
				push dpl
				call get_font
				call insert
				pop dpl
				pop dph
				inc dptr
				jmp loop
cons_mode:
				call newline
				mov dptr,#cons_menu
				call print
				call cin
				mov b,a
				call cout ; echo back to terminal
				call newline
				call newline
				mov a,b
				cjne a,#'m',cons_chkp
				jmp cons_mode
cons_chkp:		cjne a,#'p',cons_chks
				call put_text
				jmp cons_mode
cons_chks:		cjne a,#'s',cons_chkx
				call get_text
				jmp cons_mode
cons_chkx:		cjne a,#'x',cons_full
				mov dptr,#cons_exe
				call print
				call clear_disp
				ljmp disp_mode
cons_full:		cjne a,#'t',cons_next
				call full_disp
				call newline
				mov dptr,#cons_off
				call print
stat_rep:		call refresh
				jnb ri, stat_rep
				mov a,sbuf
				clr ri
				cjne a,#'t',stat_rep
				mov rowport,#00000111b ; off all row
				jmp cons_mode
cons_next:		mov dptr, #cons_err
				call print
				jmp cons_mode

get_text:
				mov dptr, #cons_wait
				call print
				mov dptr,#strg_buff
next_recv:		call cin
				cjne a,#'\',char_save ; look for \\
				call cin
				cjne a,#'\',char_save ; look for \\
				mov a,#0
char_save:		movx @dptr,a
				inc dptr
				call cout ; echo the char to terminal
				cjne a,#0,next_recv
				call flush_text
				call newline
				mov dptr, #cons_done
				call print
				ret

flush_text:		clr ri
				mov b,#10
flush_1:		push b
				mov b,#200
				djnz b,$
				pop b
				djnz b, flush_1
				jb ri, flush_text
				ret

put_text:
				mov dptr, #cons_text
				call print
				mov dptr,#strg_buff
next_send:		movx a,@dptr
				call cout
				inc dptr
				cjne a,#0,next_send
				call newline
				mov dptr, #cons_done
				call print
				ret

newline:		mov a,#13
				call cout
				mov a,#10
				call cout
				ret

cin:			jnb	ri, cin
				mov	a, sbuf
				clr	ri
				ret

cout:			mov	sbuf, a
				jnb	ti,$
				clr	ti
				ret

print:			clr a
				movc a,@a+dptr
				cjne a,#0,print_1
				jmp print_2
print_1:		call cout
				inc	dptr
				jmp print
print_2:		ret

clear_disp:		mov dptr,#disp_buff
				mov a,#0
				mov b,#buff_size
load_zero:		movx @dptr,a
				inc dptr
				djnz b,load_zero
				ret

full_disp:		mov dptr,#disp_buff
				mov a,#0ffh
				mov b,#buff_size
load_full:		movx @dptr,a
				inc dptr
				djnz b,load_full
				ret

def_buff:		mov dps,#dpx_set ; use dual data pointer
				mov dptr,#strg
				mov dptr,#strg_buff
next_char:		clr a
				movc a,@a+dptr
				movx @dptr,a
				cjne a,#0,next_char
				mov dps,#0  ; reset dual data pointer
				ret

get_font:		subb a,#32 ; ascii char - 32
				mov b,#6
				mul ab ; x6
				mov dptr,#font_table
				add a,dpl
				mov dpl,a
				mov a,b
				addc a,dph
				mov dph,a
				ret

insert:			mov b,#6 ; 6 byte font data
next_font:		clr a
				movc a,@a+dptr
				push b
				push dph
				push dpl
				mov dptr,#disp_buff+buff_size-1 ; store at last location in working disp_buff
				movx @dptr,a
				mov b,#3 ; 3 is optimum
ref_rate:		call refresh
				djnz b,ref_rate
				call shift
				pop dpl
				pop dph
				pop b
				inc dptr
				djnz b,next_font
				ret

shift:			mov dps,#dpx_set ; use dual data pointer
				mov dptr,#disp_buff+1
				mov dptr,#disp_buff
				mov count,#buff_size-1
shift_col:		movx a,@dptr
				movx @dptr,a
				djnz count,shift_col
				mov dps,#0  ; reset dual data pointer
				ret

refresh:		mov row_count,#6 ; 00000110b
				mov bitx_mask,#01000000b
next_row:		mov count,#disp_size
				mov dptr,#disp_buff
repeat:			setb data1
				movx a,@dptr
				inc dptr
				anl a,bitx_mask
				cjne a,#0,bit_1
				jmp bit_0
bit_1:			clr data1
bit_0:			call clk_sh
				djnz count,repeat
				call clk_st
				mov rowport,row_count ; turn on row
				call delay00
				mov rowport,#00000111b ; off all row
				mov a,bitx_mask ; update bit0_mask
				rr a
				mov bitx_mask,a	
				dec row_count
				mov a,row_count
				cjne a,#0ffh,next_row
				ret

clk_sh:			setb shclk
				clr shclk
				ret

clk_st:			setb stclk
				clr stclk
				ret

				; delay of about 1.25ms (800Hz row refresh rate)
delay00:		push b
				mov b,#14
delay00_0:		push b
				mov b,#200
delay00_1:		nop
				djnz b,delay00_1
				nop
				pop b
				djnz b,delay00_0
				nop
				pop b
				ret

				; serial interrupt handler
int_serial:		push psw
				push acc
				clr ea ; disable global interrupt
				clr es0 ; disable serial interrupt
				jnb ri, chk_tx
				mov a, sbuf ; save latest command
				clr ri
				cjne a, #'c', chk_tx
reset_cons:		; no reti if resetting to console
   				mov rowport,#00000111b ; off all row
				mov sp,#stack_st ; reset stack (clear stack)
				mov dptr,#cons_mode
				push dpl
				push dph
				jmp int_sback
chk_tx:			jnb ti, exit_ints
				clr ti
exit_ints:		;call cout
				setb es0
				setb ea
				pop acc
				pop psw
int_sback:		reti

cons_menu:		db 13,10,13,10
				db "LED Display Board Interface Program",13,10
				db "-----------------------------------",13,10
				db "[m] - Show this menu",13,10
				db "[p] - Show current text",13,10
				db "[s] - Send new text",13,10
				db "[t] - Toggle FULL-ON Display",13,10
				db "[x] - Exit and execute",13,10,13,10
				db "Your Choice: ",0
cons_off:		db "Press 't' to get back to menu...",13,10,13,10,0
cons_err:		db "Unknown command!",13,10,13,10,0
cons_exe:		db "Running display...",13,10,13,10,0
cons_wait:		db "Waiting for text file...",13,10,0
cons_done:		db "**DONE**",13,10,0
cons_text:		db "Showing current text:",13,10,0

strg:			db "LED Display Board - developed by Halim Harun",0

font_table:

; space
				db 00000000b
				db 00000000b
				db 00000000b
				db 00000000b
				db 00000000b
				db 00000000b
	
; !
				db 00000000b
				db 00000000b
				db 00000000b
				db 01001111b
				db 00000000b
				db 00000000b

; "
				db 00000000b
				db 00000000b
				db 00000111b
				db 00000000b
				db 00000111b
				db 00000000b

; #
				db 00000000b
				db 00010100b
				db 01111111b
				db 00010100b
				db 01111111b
				db 00010100b

; $
				db 00000000b
				db 00100100b
				db 00101010b
				db 01111111b
				db 00101010b
				db 00010010b

; %
				db 00000000b
				db 00100011b
				db 00010011b
				db 00001000b
				db 01100100b
				db 01100010b

; &
				db 00000000b
				db 00110110b
				db 01001001b
				db 01010101b
				db 00100010b
				db 01010000b

; '
				db 00000000b
				db 00000000b
				db 00000101b
				db 00000011b
				db 00000000b
				db 00000000b

; (
				db 00000000b
				db 00000000b
				db 00011100b
				db 00100010b
				db 01000001b
				db 00000000b

; )
				db 00000000b
				db 00000000b
				db 01000001b
				db 00100010b
				db 00011100b
				db 00000000b

; *
				db 00000000b
				db 00010100b
				db 00001000b
				db 00111110b
				db 00001000b
				db 00010100b

; +
				db 00000000b
				db 00001000b
				db 00001000b
				db 00111110b
				db 00001000b
				db 00001000b

; ,
				db 00000000b
				db 00000000b
				db 00000000b
				db 01010000b
				db 00110000b
				db 00000000b

; -
				db 00000000b
				db 00001000b
				db 00001000b
				db 00001000b
				db 00001000b
				db 00001000b

; .
				db 00000000b
				db 00000000b
				db 00000000b
				db 01100000b
				db 01100000b
				db 00000000b

; /
				db 00000000b
				db 00100000b
				db 00010000b
				db 00001000b
				db 00000100b
				db 00000010b

; 0
				db 00000000b
				db 00111110b
				db 01010001b
				db 01001001b
				db 01000101b
				db 00111110b

; 1
				db 00000000b
				db 00000000b
				db 01000010b
				db 01111111b
				db 01000000b
				db 00000000b

; 2
				db 00000000b
				db 01000010b
				db 01100001b
				db 01010001b
				db 01001001b
				db 01000110b

; 3
				db 00000000b
				db 00100001b
				db 01000001b
				db 01000101b
				db 01001011b
				db 00110001b

; 4
				db 00000000b
				db 00011000b
				db 00010100b
				db 00010010b
				db 01111111b
				db 00010000b

; 5
				db 00000000b
				db 00100111b
				db 01000101b
				db 01000101b
				db 01000101b
				db 00111001b

; 6
				db 00000000b
				db 00111100b
				db 01001010b
				db 01001001b
				db 01001001b
				db 00110000b

; 7
				db 00000000b
				db 00000001b
				db 00000001b
				db 01111001b
				db 00000101b
				db 00000011b

; 8
				db 00000000b
				db 00110110b
				db 01001001b
				db 01001001b
				db 01001001b
				db 00110110b

; 9
				db 00000000b
				db 00000110b
				db 01001001b
				db 01001001b
				db 00101001b
				db 00011110b

; :
				db 00000000b
				db 00000000b
				db 00110110b
				db 00110110b
				db 00000000b
				db 00000000b

; ;
				db 00000000b
				db 00000000b
				db 01010110b
				db 00110110b
				db 00000000b
				db 00000000b

; <
				db 00000000b
				db 00001000b
				db 00010100b
				db 00100010b
				db 01000001b
				db 00000000b

; =
				db 00000000b
				db 00010100b
				db 00010100b
				db 00010100b
				db 00010100b
				db 00010100b

; >
				db 00000000b
				db 00000000b
				db 01000001b
				db 00100010b
				db 00010100b
				db 00001000b

; ?
				db 00000000b
				db 00000010b
				db 00000001b
				db 01010001b
				db 00001001b
				db 00000110b

; @
				db 00000000b
				db 00110010b
				db 01001001b
				db 01001001b
				db 01111001b
				db 00111110b

; A
				db 00000000b
				db 01111110b
				db 00010001b
				db 00010001b
				db 00010001b
				db 01111110b

; B
				db 00000000b
				db 01000001b
				db 01111111b
				db 01001001b
				db 01001001b
				db 00110110b

; C
				db 00000000b
				db 00111110b
				db 01000001b
				db 01000001b
				db 01000001b
				db 00100010b

; D
				db 00000000b
				db 01000001b
				db 01111111b
				db 01000001b
				db 01000001b
				db 00111110b

; E
				db 00000000b
				db 01111111b
				db 01001001b
				db 01001001b
				db 01001001b
				db 01001001b

; F
				db 00000000b
				db 01111111b
				db 00001001b
				db 00001001b
				db 00001001b
				db 00000001b

; G
				db 00000000b
				db 00111110b
				db 01000001b
				db 01000001b
				db 01001001b
				db 01111010b

; H
				db 00000000b
				db 01111111b
				db 00001000b
				db 00001000b
				db 00001000b
				db 01111111b

; I
				db 00000000b
				db 00000000b
				db 01000001b
				db 01111111b
				db 01000001b
				db 00000000b

; J
				db 00000000b
				db 00100000b
				db 01000000b
				db 01000001b
				db 00111111b
				db 00000001b

; K
				db 00000000b
				db 01111111b
				db 00001000b
				db 00010100b
				db 00100010b
				db 01000001b

; L
				db 00000000b
				db 01111111b
				db 01000000b
				db 01000000b
				db 01000000b
				db 01000000b

; M
				db 00000000b
				db 01111111b
				db 00000010b
				db 00001100b
				db 00000010b
				db 01111111b

; N
				db 00000000b
				db 01111111b
				db 00000110b
				db 00001000b
				db 00110000b
				db 01111111b

; O
				db 00000000b
				db 00111110b
				db 01000001b
				db 01000001b
				db 01000001b
				db 00111110b

; P
				db 00000000b
				db 01111111b
				db 00001001b
				db 00001001b
				db 00001001b
				db 00000110b

; Q
				db 00000000b
				db 00111110b
				db 01000001b
				db 01010001b
				db 00100001b
				db 01011110b

; R
				db 00000000b
				db 01111111b
				db 00001001b
				db 00011001b
				db 00101001b
				db 01000110b

; S
				db 00000000b
				db 00100110b
				db 01001001b
				db 01001001b
				db 01001001b
				db 00110010b

; T
				db 00000000b
				db 00000001b
				db 00000001b
				db 01111111b
				db 00000001b
				db 00000001b

; U
				db 00000000b
				db 00111111b
				db 01000000b
				db 01000000b
				db 01000000b
				db 00111111b

; V
				db 00000000b
				db 00011111b
				db 00100000b
				db 01000000b
				db 00100000b
				db 00011111b

; W
				db 00000000b
				db 01111111b
				db 00100000b
				db 00011000b
				db 00100000b
				db 01111111b

; X
				db 00000000b
				db 01100011b
				db 00010100b
				db 00001000b
				db 00010100b
				db 01100011b

; Y
				db 00000000b
				db 00000111b
				db 00001000b
				db 01110000b
				db 00001000b
				db 00000111b

; Z
				db 00000000b
				db 01100001b
				db 01010001b
				db 01001001b
				db 01000101b
				db 01000011b

; [
				db 00000000b
				db 00000000b
				db 01111111b
				db 01000001b
				db 01000001b
				db 00000000b

; \
				db 00000000b
				db 00000010b
				db 00000100b
				db 00001000b
				db 00010000b
				db 00100000b

; ]
				db 00000000b
				db 00000000b
				db 01000001b
				db 01000001b
				db 01111111b
				db 00000000b

; ^
				db 00000000b
				db 00000100b
				db 00000010b
				db 00000001b
				db 00000010b
				db 00000100b

; _
				db 00000000b
				db 01000000b
				db 01000000b
				db 01000000b
				db 01000000b
				db 01000000b

; `
				db 00000000b
				db 00000001b
				db 00000010b
				db 00000100b
				db 00000000b
				db 00000000b

; a 
				db 00000000b
				db 00100000b
				db 01010100b
				db 01010100b
				db 01010100b
				db 01111000b

; b
				db 00000000b
				db 01111111b
				db 01001000b
				db 01000100b
				db 01000100b
				db 00111000b

; c
				db 00000000b
				db 00111000b
				db 01000100b
				db 01000100b
				db 01000100b
				db 00000000b

; d
				db 00000000b
				db 00111000b
				db 01000100b
				db 01000100b
				db 01001000b
				db 01111111b

; e
				db 00000000b
				db 00111000b
				db 01010100b
				db 01010100b
				db 01010100b
				db 00011000b

; f
				db 00000000b
				db 00000000b
				db 00001000b
				db 01111110b
				db 00001001b
				db 00000010b

; g
				db 00000000b
				db 00001100b
				db 01010010b
				db 01010010b
				db 01001100b
				db 00111110b

; h
				db 00000000b
				db 01111111b
				db 00001000b
				db 00000100b
				db 00000100b
				db 01111000b

; i
				db 00000000b
				db 00000000b
				db 01000100b
				db 01111101b
				db 01000000b
				db 00000000b

; j
				db 00000000b
				db 00100000b
				db 01000000b
				db 01000100b
				db 00111101b
				db 00000000b

; k
				db 00000000b
				db 00000000b
				db 01111111b
				db 00010000b
				db 00101000b
				db 01000100b

; l
				db 00000000b
				db 00000000b
				db 01000001b
				db 01111111b
				db 01000000b
				db 00000000b

; m
				db 00000000b
				db 01111100b
				db 00000100b
				db 01111000b
				db 00000100b
				db 01111000b

; n
				db 00000000b
				db 01111100b
				db 00001000b
				db 00000100b
				db 00000100b
				db 01111000b

; o
				db 00000000b
				db 00111000b
				db 01000100b
				db 01000100b
				db 01000100b
				db 00111000b

; p
				db 00000000b
				db 01111110b
				db 00001100b
				db 00010010b
				db 00010010b
				db 00001100b

; q
				db 00000000b
				db 00001100b
				db 00010010b
				db 00010010b
				db 00001100b
				db 01111110b

; r
				db 00000000b
				db 01111100b
				db 00001000b
				db 00000100b
				db 00000100b
				db 00001000b

; s
				db 00000000b
				db 01011000b
				db 01010100b
				db 01010100b
				db 01010100b
				db 01100100b

; t
				db 00000000b
				db 00000100b
				db 00111111b
				db 01000100b
				db 01000000b
				db 00100000b

; u
				db 00000000b
				db 00111100b
				db 01000000b
				db 01000000b
				db 00111100b
				db 01000000b

; v
				db 00000000b
				db 00011100b
				db 00100000b
				db 01000000b
				db 00100000b
				db 00011100b

; w
				db 00000000b
				db 00111100b
				db 01000000b
				db 00110000b
				db 01000000b
				db 00111100b

; x
				db 00000000b
				db 01000100b
				db 00101000b
				db 00010000b
				db 00101000b
				db 01000100b

; y
				db 00000000b
				db 00001100b
				db 01010000b
				db 01010000b
				db 01001000b
				db 00111100b

; z
				db 00000000b
				db 01000100b
				db 01100100b
				db 01010100b
				db 01001100b
				db 01000100b

; {
				db 00000000b
				db 00000000b
				db 00001000b
				db 00110110b
				db 01000001b
				db 00000000b

; |
				db 00000000b
				db 00000000b
				db 00000000b
				db 01110111b
				db 00000000b
				db 00000000b

; }
				db 00000000b
				db 00000000b
				db 01000001b
				db 00110110b
				db 00001000b
				db 00000000b

; ~
				db 00000000b
				db 00000010b
				db 00000001b
				db 00000010b
				db 00000100b
				db 00000010b

				end
