/*----------------------------------------------------------------------------*/
#include "uart2stc.h"
#include "timer_tick.h"
/*----------------------------------------------------------------------------*/
#define MAX_CHAR 32
#define CHK_BEAT 5
/*----------------------------------------------------------------------------*/
unsigned char beat;
char buff[MAX_CHAR];
/*----------------------------------------------------------------------------*/
void strip_newline(char* buff,int size) {
	int loop;
	for (loop=0;loop<size;loop++) {
		if (buff[loop]=='\r'||buff[loop]=='\n') {
			buff[loop] = 0x0;
			break;
		}
	}
	/* insert null if none found */
	if (loop==size) buff[loop-1] = 0x0;
}
/*----------------------------------------------------------------------------*/
void main(void) {
	beat = 0;
	uart2_init();
	timer_tick_exec();
	while(1) {
		if (timer_ticked()) {
			timer_tick00();
			if (beat) beat--;
		}
		if (!beat) {
			uart2_puts("\nPress <ENTER> to start!\n");
			beat = CHK_BEAT;
		}
		if (!uart2_peek()||uart2_read()!='\n')
			continue;
		uart2_puts("\nName: ");
		uart2_gets(buff,MAX_CHAR);
		strip_newline(buff,MAX_CHAR);
		uart2_puts("Hello, ");
		uart2_puts(buff);
		uart2_puts("!\n");
		beat = CHK_BEAT;
	}
}
/*----------------------------------------------------------------------------*/
