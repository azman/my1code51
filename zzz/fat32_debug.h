/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_DEBUG_H__
#define __MY1FAT32_DEBUG_H__
/*----------------------------------------------------------------------------*/
#include "uart_hexascii.h"
#include "utils_int32.h"
/*----------------------------------------------------------------------------*/
typedef struct _f32dbug_temp_t {
	unsigned char loop, iter;
} f32dbug_temp_t;
/*----------------------------------------------------------------------------*/
__xdata f32dbug_temp_t tmpD;
/*----------------------------------------------------------------------------*/
__xdata char conv[16];
/*----------------------------------------------------------------------------*/
#define uart_show_integer(data) { int2str(conv,data); uart_puts(conv); }
/*----------------------------------------------------------------------------*/
unsigned char validate_filename(char* name, unsigned char size) {
	for (tmpD.iter=0;tmpD.iter<size;tmpD.iter++) {
		if ((name[tmpD.iter]>='A'&&name[tmpD.iter]<='Z')||
				(name[tmpD.iter]>='0'&&name[tmpD.iter]<='9')||
				name[tmpD.iter]=='_')
			continue;
		else if (name[tmpD.iter]>='a'&&name[tmpD.iter]<='z')
			name[tmpD.iter] -= 0x20; /* make uppercase */
		else if (name[tmpD.iter]!='.') return 0;
	}
	return tmpD.iter;
}
/*----------------------------------------------------------------------------*/
void fat32_node_show(fat32_node_t *pchk) {
	uart_puts("@@ ");
#ifdef __MY1FAT32_NODE_H__
	uart_puts(" [0x");
	uart_send_hexbyte(pchk->attr);
	uart_puts("] ");
#endif
	if (fat32_node_isvollabel(pchk)) uart_puts("VLBL");
	else if (fat32_node_ispath(pchk)) uart_puts("PATH");
	else uart_puts("FILE");
	uart_puts(" {");
	if (fat32_node_isvollabel(pchk)) {
		for (tmpD.loop=0;tmpD.loop<11;tmpD.loop++) {
			if (pchk->name[tmpD.loop]==0x20) break;
			name4dos[tmpD.loop] = pchk->name[tmpD.loop];
		}
		name4dos[tmpD.loop] = 0x0;
	}
	else fat32_node_name_read((byte08_t*)pchk);
	uart_puts(name4dos);
	uart_puts("} [");
	uart_show_integer(fat32_dateYY_read(pchk->date));
	uart_puts("-");
	tmpD.loop = fat32_dateMM_read(pchk->date);
	if (tmpD.loop<10) uart_send('0');
	uart_show_integer(tmpD.loop);
	uart_puts("-");
	tmpD.loop = fat32_dateDD_read(pchk->date);
	if (tmpD.loop<10) uart_send('0');
	uart_show_integer(tmpD.loop);
	uart_send('T');
	tmpD.loop = fat32_timeHH_read(pchk->time);
	if (tmpD.loop<10) uart_send('0');
	uart_show_integer(tmpD.loop);
	uart_send(':');
	tmpD.loop = fat32_timeMM_read(pchk->time);
	if (tmpD.loop<10) uart_send('0');
	uart_show_integer(tmpD.loop);
	uart_send(':');
	tmpD.loop = fat32_timeSS_read(pchk->time);
	if (tmpD.loop<10) uart_send('0');
	uart_show_integer(tmpD.loop);
	uart_puts("] (Clus:");
	uart_show_integer(fat32_node_cluster(pchk));
	uart_puts(") (Size:");
	uart_show_integer(pchk->size);
	uart_puts(")\n");
}
/*----------------------------------------------------------------------------*/
void fat32_path_list(void) {
	fat32_path_redo();
	fat32_path_load();
	if (fat32_path_error()) {
		fat32_path_noerr();
		uart_puts("** error loading init! **\n");
		return;
	}
	step = 0;
	do {
		path.info = disk.buff;
		for (loop=0;loop<FAT32_NODE_MAXCOUNT;loop++,
				path.info+=FAT32_NODE_SIZE) {
			if (fat32_node_isdeleted(path.info)) continue;
			if (fat32_node_isvollabel(path.info)) {
				if (step||loop) continue;
			}
			if (fat32_node_ends(path.info)) return;
			fat32_node_show(NODEPTR(path.info));
		}
		fat32_path_next();
		if (fat32_path_error()) {
			fat32_path_noerr();
			uart_puts("** error loading next! **\n");
			return;
		}
		step++;
	} while (path.load);
}
/*----------------------------------------------------------------------------*/
#ifdef __MY1FAT32_FILE_H__
/*----------------------------------------------------------------------------*/
void fat32_file_read_all(void) { /* fat32_file should be prepped */
	file.curr = 0;
	while (1) {
		fat32_file_read();
		if (fat32_error()) {
			uart_puts("**error** <");
			uart_send_hexlong(fat32_flag());
			uart_puts(">\n");
			return;
		}
		if (!file.stat) break;
		for (loop=0;loop<file.stat;loop++) {
			if(file.data[loop]>=0x20&&file.data[loop]<=0x7f)
				uart_send(file.data[loop]);
			else {
				switch (file.data[loop]) {
					case 0x0d: break;
					case 0x0a: uart_send('\n'); break;
					default:
						uart_puts("[0x");
						uart_send_hexbyte(file.data[loop]);
						uart_send(']');
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void fat32_file_read_size(void) {
	file.curr = 0; file.tmpx = 0;
	while (1) {
		fat32_file_read();
		if (fat32_error()) return;
		if (!file.stat) break;
		for (loop=0;loop<file.stat;loop++) {
			if ((file.data[loop]>=0x20&&file.data[loop]<=0x7f)||
					file.data[loop]==0x0d||file.data[loop]==0x0a)
				continue;
			file.tmpx++;
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_DEBUG_H__ */
/*----------------------------------------------------------------------------*/
