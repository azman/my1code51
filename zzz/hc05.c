/*----------------------------------------------------------------------------*/
#include "hc05.h"
#include "timer.h"
#include "uart_gets.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
/*----------------------------------------------------------------------------*/
__xdata char buff[BUFFSIZE];
unsigned char test, loop;
/*----------------------------------------------------------------------------*/
my1sbit(MODEPICK,PIN10);
/*----------------------------------------------------------------------------*/
void hc05_setname_full(char *name) {
	atcmd_puts(HC05CMD_ATNAME);
	atcmd_puts(name);
	atcmd_puts(HC05CMD_ATENDS);
	test = atcmd_read_wait(buff,BUFFSIZE);
	if (test==BUFFSIZE) test--;
	buff[test] = 0x0;
}
/*----------------------------------------------------------------------------*/
void hc05_setpin_full(char *pass) {
	atcmd_puts(HC05CMD_ATPASS"\"");
	atcmd_puts(pass);
	atcmd_puts("\""HC05CMD_ATENDS);
	test = atcmd_read_wait(buff,BUFFSIZE);
	if (test==BUFFSIZE) test--;
	buff[test] = 0x0;
}
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n\nHC05 (my1code51)\n\n");
	timer_init();
	timer_delay1s(test,3);
	if (MODEPICK) {
		hc05_init();
		/* try to communicate */
		while (1) {
			uart_puts("-- Sending AT... ");
			hc05_find();
			if (hc05_wait_ok()==HC05_OK) {
				uart_puts("OK.\n");
				break;
			}
			uart_puts("timeout.\n");
			timer_delay1s(test,1);
		}
		while (1) {
			if (uart_peek()) {
				test = uart_read();
				uart_send(test); /* local echo */
				hc05_send(test);
			}
			if (hc05_peek())
				uart_send(hc05_read());
		}
	}
	else {
		uart_puts("@@ AT MODE\n");
		/* prepare atmode */
		hc05_init_atmode();
		/* try to communicate */
		while (1) {
			uart_puts("-- Sending AT... ");
			hc05_find();
			if (hc05_wait_ok()==HC05_OK) {
				uart_puts("OK.\n");
				break;
			}
			uart_puts("timeout.\n");
			timer_delay1s(test,1);
		}
		/* get version */
		uart_puts("-- Get Version  :");
		test = hc05_getvers(buff,BUFFSIZE);
		if (hc05_buff_ok(buff,test)) {
			test -= 4;
			while (buff[test]!='\n')
				test--;
			if (buff[test-1]=='\r') test--;
			buff[test] = 0x0;
			uart_puts("OK. (");
			uart_puts(buff);
			uart_puts(")\n");
		}
		else {
			uart_puts("*error?*. Reply:{");
			uart_puts(buff);
			uart_puts("}\n");
		}
		timer_delay1s(test,1);
		/* chk role */
		uart_puts("-- Check Role   :");
		hc05_wait_send(buff,BUFFSIZE,"AT+ROLE\r\n");
		uart_puts(buff);
		timer_delay1s(test,1);
		/* free flow */
		uart_puts("## FREE FLOW ##");
		while (1) {
			if (uart_peek()) {
				test = uart_read();
				if (test==':') {
					uart_puts("\nName:");
					test = uart_gets(buff,16);
					uart_puts("\n-- Set Name (");
					uart_puts(buff);
					uart_puts("): ");
					hc05_setname_full(buff);
					uart_puts(buff);
					uart_send('\n');
					timer_delay1s(test,1);
				}
				else if (test=='#') {
					uart_puts("\nPin:");
					test = uart_gets(buff,16);
					uart_puts("\n-- Set Pin (");
					uart_puts(buff);
					uart_puts("): ");
					hc05_setpin_full(buff);
					uart_puts(buff);
					uart_send('\n');
					timer_delay1s(test,1);
				}
				else if (test=='~') {
					uart_puts("\nSet default name & pin\n");
					uart_puts("-- Set Name (");
					uart_puts(BTNAME);
					uart_puts("): ");
					hc05_setname(buff,BUFFSIZE);
					uart_puts(buff);
					uart_send('\n');
					timer_delay1s(test,1);
					uart_puts("-- Set Pin (");
					uart_puts(BTPASS);
					uart_puts("): ");
					hc05_setpin(buff,BUFFSIZE);
					uart_puts(buff);
					uart_send('\n');
					timer_delay1s(test,1);
				}
				else {
					uart_send(test); /* local echo */
					hc05_send(test);
				}
			}
			if (hc05_peek())
				uart_send(hc05_read());
		}
	}
}
/*----------------------------------------------------------------------------*/
