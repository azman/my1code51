/*----------------------------------------------------------------------------*/
/* override default tick values to 0.1 sec tick */
#define TIMER_TICK_LEN TIMER_VAL50MS
#define TIMER_TICK_CNT 2
/*----------------------------------------------------------------------------*/
#include "timer_tick.h"
/*----------------------------------------------------------------------------*/
#include "uart_gets.h"
#include "utils_lite.h"
/*----------------------------------------------------------------------------*/
unsigned char mmin, ssec, tcnt, temp;
/*----------------------------------------------------------------------------*/
void show_time(void) {
	/* show positions */
	uart_send(dec2char10(mmin));
	uart_send(dec2char01(mmin));
	uart_send(':');
	uart_send(dec2char10(ssec));
	uart_send(dec2char01(ssec));
	uart_send('.');
	uart_send(dec2char01(tcnt));
}
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_tick_init();
	uart_init();
	uart_print("\n\n-- Stopwatch System\n\n");
	mmin = 0; ssec = 0; tcnt = 0;
	uart_puts("Time> ");
	show_time();
	while (1) {
		if (timer_ticked()) {
			timer_tick00();
			tcnt++; uart_send('\b');
			if (tcnt==10) {
				tcnt = 0; ssec++; uart_puts("\b\b\b");
				if (ssec==60) {
					ssec = 0; mmin++; uart_puts("\b\b\b");
					if (mmin==60) mmin = 0;
					uart_send(dec2char10(mmin));
					uart_send(dec2char01(mmin));
					uart_send('.');
				}
				uart_send(dec2char10(ssec));
				uart_send(dec2char01(ssec));
				uart_send('.');
			}
			uart_send(dec2char01(tcnt));
		}
		if (uart_peek()) {
			temp = uart_read();
			if (temp=='0') {
				timer_stop();
				mmin = 0; ssec = 0; tcnt = 0;
				uart_puts("\b\b\b\b\b\b\b");
				show_time();
			}
			else if (temp==0x20) {
				if (timer_running()) { timer_stop(); }
				else { timer_exec(); }
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
