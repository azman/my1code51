/*----------------------------------------------------------------------------*/
#include "fat32_node.h"
#include "fat32_sd.h"
#include "uart_gets.h"
/*----------------------------------------------------------------------------*/
#define TESTDATA "MY1TEST.DAT"
/*----------------------------------------------------------------------------*/
__xdata word32_t test;
__xdata char buff[16];
__xdata int loop, step;
unsigned char init, what;
/*----------------------------------------------------------------------------*/
#include "fat32_debug.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n-- Testing FAT32 NodeSD\n");
	do {
		uart_puts("\nPress <ENTER> to start initialization.");
		while(uart_read()!='\n');
		uart_puts("\n\n-- Preparing SD card... ");
		fat32_disk_prep();
		if (!fat32_error()) {
			uart_puts("OK.\n");
			break;
		}
		uart_puts("**error** [0x");
		uart_send_hexbyte(card.flag);
		uart_puts("]\n");
	} while (1);
	uart_puts("-- Accessing FAT32 disk... ");
	fat32_disk_init();
	if (fat32_error()) {
		uart_puts("**error** {Flag:0x");
		uart_send_hexlong(fat32_flag());
		uart_puts("}\n");
		hang();
	}
	uart_puts("OK.\n");
	fat32_path_prep();
	fat32_path_list();
	uart_puts("@@ Step = ");
	uart_show_integer(step);
	uart_puts(" Loop = ");
	uart_show_integer(loop);
	uart_puts("\n");
	init = 1;
	while (1) {
		if (init) {
			uart_puts("\n@@ l to list entries");
			uart_puts("\n@@ x to test create/remove entry");
			uart_puts("\n@@ c to find a free cluster");
			uart_puts("\n@@ n<name> to find an entry");
			uart_puts("\n@@ d<name> to cd into a directory\n");
			init = 0;
		}
		if (uart_peek()) {
			what = uart_read();
			if (what=='n') {
				uart_puts("\n-- Name: ");
				init = uart_gets(buff,16);
				validate_filename(buff,init);
				uart_puts("\n-- Looking for '");
				uart_puts(buff);
				uart_puts("'... ");
				if (fat32_node_find_name(buff)) {
					uart_puts("OK.\n");
					fat32_node_show(NODEPTR(path.info));
				}
				else uart_puts("not found!\n");
				init = 1;
			}
			else if (what=='d') {
				uart_puts("\n-- cDir: ");
				init = uart_gets(buff,16);
				validate_filename(buff,init);
				fat32_path_cd(buff);
				if (fat32_link_ismodified(&path)) {
					fat32_path_list();
					fat32_link_updated(&path);
				}
				else uart_puts("** invalid path? **\n");
				init = 1;
			}
			else if (what=='l') {
				uart_puts("\n-- Listing... init.\n");
				fat32_path_list();
				uart_puts("-- Listing... done.\n");
				init = 1;
			}
			else if (what=='x') {
				if (!fat32_node_find_name(TESTDATA)) {
					uart_puts("\nCreating entry for "TESTDATA" ... ");
					fat32_path_make_node(TESTDATA);
					if (fat32_path_OK()) uart_puts("OK.\n");
					else uart_puts("** error **\n");
				}
				else if (!NODEPTR(path.info)->size) { /* only if entry-only */
					uart_puts("\nRemoving entry for "TESTDATA" ... ");
					fat32_path_wipe_node(TESTDATA);
					if (fat32_path_OK()) uart_puts("OK.\n");
					else uart_puts("** error **\n");
				}
				init = 1;
			}
			else if (what=='c') {
				fat32_find_cluster();
				uart_puts("\nFree cluster: ");
				uart_show_integer(path.free);
				path.curr = path.free;
				fat32_link_get(&path);
				uart_puts(" <link:0x");
				if (fat32_error()) uart_puts("**");
				else uart_send_hexlong(path.pu32[0]);
				uart_puts(">\n");
				init = 1;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
