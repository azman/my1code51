/*----------------------------------------------------------------------------*/
/**
 * 8-pin stc15w204s
 * - vcc,gnd
 * - p30,p31,p32,p33,p54,p55
 * - active-hi led @p33
 * - active-lo swi @p32
**/
/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "uart_stc15w.h"
#include "timer_tick.h"
/*----------------------------------------------------------------------------*/
/* main function */
void main(void) {
	P3_2 = 1;
	P3_3 = 0;
	uart_init_stc15w();
	uart_puts("\n-- Test uart stc15w\n\n");
	timer_tick_exec();
	while(1) {
		if (uart_peek()) {
			if (uart_read()=='\n') {
				uart_puts("You got me!\n");
			}
		}
		if (timer_ticked()) {
			timer_tick00();
			P3_3 = !P3_3; /* toggle p33 */
		}
		if (!P3_2) {
			while (!P3_2); /* pause on push */
		}
	}
}
/*----------------------------------------------------------------------------*/
