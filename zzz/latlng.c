/*----------------------------------------------------------------------------*/
/**
*note: add the following line to a makefile.local
latlng: CFLAGS += --stack-auto
*/
#include "uart.h"
#include "latlng.h"
/*----------------------------------------------------------------------------*/
#define FPSIZE 6
#include "utils_float.h"
/*----------------------------------------------------------------------------*/
#define CONV_SIZE 16
/*----------------------------------------------------------------------------*/
__xdata latlng_t keep[2];
__xdata float dist, vdir;
__xdata char conv[CONV_SIZE];
__xdata int that;
/*----------------------------------------------------------------------------*/
#define show_latlng(gps,msg) uart_puts(msg); view_latlng(gps)
/*----------------------------------------------------------------------------*/
void view_latlng(latlng_t* gpos) {
	uart_puts("\n  Lat:");
	float2str(conv,gpos->lat);
	uart_puts(conv);
	uart_puts("\n  Lng:");
	float2str(conv,gpos->lng);
	uart_puts(conv);
	uart_send('\n');
}
/*----------------------------------------------------------------------------*/
void show_calc(void) {
	/* show positions */
	show_latlng(&keep[0],"@@ Pos1");
	show_latlng(&keep[1],"@@ Pos2");
	calc_dist1();
	dist = llcalc.calc;
	that = (int)dist;
	uart_puts("> Dist:");
	float2str(conv,dist);
	uart_puts(conv);
	uart_puts(" (");
	int2str(conv,that);
	uart_puts(conv);
	uart_puts(")\n");
	uart_puts("@ Tmp1:");
	float2str(conv,llcalc.tmp1);
	uart_puts(conv);
	uart_puts(" Tmp2:");
	float2str(conv,llcalc.tmp2);
	uart_puts(conv);
	uart_puts(" Temp:");
	float2str(conv,llcalc.temp);
	uart_puts(conv);
	uart_send('\n');
	calc_dist3();
	dist = llcalc.calc;
	that = (int)dist;
	uart_puts("> Dist:");
	float2str(conv,dist);
	uart_puts(conv);
	uart_puts(" (");
	int2str(conv,that);
	uart_puts(conv);
	uart_puts(")\n");
	uart_puts("@ Tmp1:");
	float2str(conv,llcalc.tmp1);
	uart_puts(conv);
	uart_puts(" Tmp2:");
	float2str(conv,llcalc.tmp2);
	uart_puts(conv);
	uart_puts(" Temp:");
	float2str(conv,llcalc.temp);
	uart_puts(conv);
	uart_send('\n');
	calc_bear1();
	vdir = llcalc.calc;
	that = (int)(rad2deg(vdir));
	if (that<0) that = that + 360;
	uart_puts("> Look:");
	int2str(conv,that);
	uart_puts(conv);
	uart_puts("\n\n");
}
/*----------------------------------------------------------------------------*/
#define get_position(gps,msg) { uart_puts(msg); chk_position(gps); }
/*----------------------------------------------------------------------------*/
void chk_position(latlng_t* gpos) {
	uart_puts("? Lat:");
	uart_gets(conv,CONV_SIZE);
	gpos->lat = str2float(conv);
	uart_puts("? Lng:");
	uart_gets(conv,CONV_SIZE);
	gpos->lng = str2float(conv);
	uart_send('\n');
}
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_print("\n-- Test Lat/Lng Calculation\n\n");
	/* set waypoints */
	keep[0].lat = 6.46187;
	keep[0].lng = 100.35103;
	keep[1].lat = 6.46178;
	keep[1].lng = 100.35127;
	/* default calc */
	llcalc.p1 = &keep[1]; llcalc.p2 = &keep[0];
	show_calc();
	llcalc.p1 = &keep[0]; llcalc.p2 = &keep[1];
	show_calc();
	/* main loop */
	while (1) {
		get_position(&keep[0],"-- Input Pos1\n");
		get_position(&keep[1],"-- Input Pos2\n");
		show_calc();
	}
}
/*----------------------------------------------------------------------------*/
