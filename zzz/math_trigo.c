/*----------------------------------------------------------------------------*/
#include "math_trigo.h"
#include "uart.h"
#include "timer.h"
#include "utils_float.h"
#include "uart_hexascii.h"
/*----------------------------------------------------------------------------*/
char buffer[32];
/*----------------------------------------------------------------------------*/
void uart_send_floatpt(float data)
{
	float2str(buffer,data);
	uart_puts(buffer);
}
/*----------------------------------------------------------------------------*/
void time_delay_s(unsigned char what) {
	what *= 20; /* what must be < 13 */
	do {
		timer_wait(TIMER_VAL50MS);
		what--;
	} while (what);
}
/*----------------------------------------------------------------------------*/
void main(void) {
	float test, calc;
	unsigned long* pu32;
	uart_init();
	timer_init();
	uart_puts("\n-- Settling down...");
	time_delay_s(1);
	uart_puts("\n");
	uart_puts("\nTest cos function\n");
	pu32 = (unsigned long*) &calc;
	test = 0.0;
	while (test<360.0)
	{
		calc = cos(deg2rad(test));
		uart_puts("  > 0x");
		uart_send_hexlong(*pu32);
		uart_puts(" @ ");
		uart_send_floatpt(calc);
		uart_puts("\r\n");
		test += 1.0;
	}
	uart_puts("\nTest sin function\n");
	test = 0.0;
	while (test<360.0)
	{
		calc = sin(deg2rad(test));
		uart_puts("  > 0x");
		uart_send_hexlong(*pu32);
		uart_puts(" @ ");
		uart_send_floatpt(calc);
		uart_puts("\r\n");
		test += 1.0;
	}
	while (1);
}
/*----------------------------------------------------------------------------*/
