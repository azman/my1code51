/*----------------------------------------------------------------------------*/
#include "misc/music.h"
#include "uart.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define PIN_TONE P1_0
#define CHK_AUTO P1_1
/*----------------------------------------------------------------------------*/
unsigned char pick, next;
unsigned int step, wait;
/*----------------------------------------------------------------------------*/
void play_note(void) {
	step = mnotes[pick].tcnt;
	wait = mnotes[pick].tlen>>1; /* half period */
	wait = 0 - wait;
	while (step) {
		PIN_TONE = 0;
		timer_wait(wait);
		PIN_TONE = 1;
		timer_wait(wait);
		step--;
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	next = 0;
	PIN_TONE = 1; // active low!
	pick = NOTE_MIDDLE_C;
	timer_init();
	uart_init();
	uart_purge();
	uart_puts("\n\nMusic note!\n");
	while (1) {
		if (uart_peek()) {
			switch (uart_read()) {
				case 0x31: pick = NOTE_C4; break;
				case 0x32: pick = NOTE_C4+2; break;
				case 0x33: pick = NOTE_C4+4; break;
				case 0x34: pick = NOTE_C4+5; break;
				case 0x35: pick = NOTE_C4+7; break;
				case 0x36: pick = NOTE_C4+9; break;
				case 0x37: pick = NOTE_C4+11; break;
				case 0x38: pick = NOTE_C4+12; break;
				default: pick = 0xff;
			}
			if (pick<0xff) play_note();
		}
		else if (!CHK_AUTO) {
			pick = next;
			play_note();
			next++;
			if (next>12) next = 0;
			timer_delay1s(wait,1);
		}
	}
}
/*----------------------------------------------------------------------------*/
