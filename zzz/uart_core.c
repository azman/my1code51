/*----------------------------------------------------------------------------*/
/**
 * Test program for uart_timeout.h and uart_hexascii.h
 * > includes uart_send.h and uart_read.h
**/
/*----------------------------------------------------------------------------*/
#include "uart_timeout.h"
#include "uart_hexascii.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned int temp;
	uart_init();
	temp = 0;
	while (1) {
		if (!temp||uart_timeout(3000)) {
			uart_puts("\n-- UART Echo Test");
			uart_puts("\n@@ UART BaudCnt:0x");
			uart_send_hexbyte(UART_BAUD_T1);
			uart_puts("\n@@ UART B9600Cnt:0x");
			uart_send_hexbyte(T1CNT_BAUD9600);
			uart_puts("\n@@ T1:0x");
			temp = (unsigned int)(TH1<<8)|TL1;
			uart_send_hexuint(temp);
			uart_puts("\n>> Hello, there!\n");
		}
		else uart_send(uart_read());
	}
}
/*----------------------------------------------------------------------------*/
