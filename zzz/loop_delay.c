/*----------------------------------------------------------------------------*/
/**
 * Test program for loop_delay.h (and LOOP_DELAY_COUNT)
**/
/*----------------------------------------------------------------------------*/
#include "loop_delay.h"
#include "uart.h"
#include "uart_hexascii.h"
#include "utils_lite.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define TLEN_MS 50
/*----------------------------------------------------------------------------*/
void main(void) {
	char buff[16];
	unsigned char step, tlen, test;
	unsigned int curr, calc;
	timer_init();
	uart_init();
	uart_puts("\r\n-- Settling down...");
	timer_delay1s(test,1);
	uart_puts("\r\n");
	curr = LOOP_DELAY_COUNT;
	step = 3; tlen = TLEN_MS;
	while (step>0) {
		uart_puts("\r\n[");
		uint8_2cstr(buff,step);
		uart_puts(buff);
		uart_puts("] Testing loop counter for ");
		uint8_2cstr(buff,tlen);
		uart_puts(buff);
		uart_puts("ms... ");
		timer_null();
		timer_exec();
		loop_delay((unsigned int)tlen);
		timer_stop();
		calc = timer_read();
		uart_puts("done. {calc:");
		uint16_2cstr(buff,calc);
		uart_puts(buff);
		uart_puts("@0x");
		calc = 0-calc;
		uart_send_hexuint(calc);
		uart_puts(", curr:");
		uint16_2cstr(buff,curr);
		uart_puts(buff);
		uart_puts("}\r\n");
		/** delay before next try */
		timer_delay1s(test,2);
		step--;
	}
	uart_puts("\r\n-- Generating squarewave at P1... ");
	P1 = 0xAA;
	while (1) {
		P1 = ~P1;
		loop_delay(500);
	}
}
/*----------------------------------------------------------------------------*/
