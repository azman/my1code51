/*----------------------------------------------------------------------------*/
#include "fat32_file.h"
#include "fat32_sd.h"
#include "uart_hexascii.h"
#include "uart_gets.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 32
#define DUMPTEXT "# I AM NOBODY |"
#define DUMPFULL DUMPTEXT"\n"
#define DUMPSIZE 16
/*----------------------------------------------------------------------------*/
__xdata word32_t test;
__xdata char buff[BUFFSIZE];
__xdata int loop, step;
__xdata unsigned char done;
__xdata fat32_link_t path, pond;
unsigned char init, what;
/*----------------------------------------------------------------------------*/
#include "fat32_debug.h"
/*----------------------------------------------------------------------------*/
void fat32_link_scan(word32_t clus) {
	pond.curr = clus;
	do {
		uart_puts(" >");
		uart_show_integer(pond.curr);
		fat32_link_get(&pond);
		pond.curr = pond.pu32[0];
		if (fat32_is_last_cluster(pond.curr)) {
			uart_puts(" #last.\n");
			break;
		}
		if (fat32_is_invalid_cluster(pond.curr)) {
			uart_puts("** error! **\n");
			break;
		}
	} while (1);
}
/*----------------------------------------------------------------------------*/
void fat32_file_show(void) { /* fat32_file should be prepped */
	uart_puts("-- [INIT] Reading from ");
	uart_puts(file.name);
	uart_puts("\n");
	fat32_file_read_all();
	uart_puts("\n-- [DONE] Reading from ");
	uart_puts(file.name);
	uart_puts(" => {Flag:");
	uart_send_hexlong(disk.core.flag);
	uart_puts("}<");
	uart_show_integer(file.curr);
	uart_puts(">\n");
}
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n-- Testing FAT32 FileSD\n");
	do {
		uart_puts("\nPress <ENTER> to start initialization.");
		while(uart_read()!='\n');
		uart_puts("\n\n-- Preparing SD card... ");
		fat32_disk_prep();
		if (!fat32_error()) {
			uart_puts("OK.\n");
			break;
		}
		uart_puts("**error** [0x");
		uart_send_hexbyte(card.flag);
		uart_puts("]\n");
	} while (1);
	uart_puts("-- Accessing FAT32 disk... ");
	fat32_disk_init();
	if (fat32_error()) {
		uart_puts("**error** {Flag:0x");
		uart_send_hexlong(fat32_flag());
		uart_puts("}\n");
		hang();
	}
	uart_puts("OK.\n");
	/* initialize */
	fat32_path_prep();
	init = 1;
	while (1) {
		if (init) {
			uart_puts("\n@@ l to list entries");
			uart_puts("\n@@ s to scan node cluster chain");
			uart_puts("\n@@ z to check file size & content");
			uart_puts("\n@@ f to find and view a file");
			uart_puts("\n@@ n to nullify a file");
			uart_puts("\n@@ d to cd into a directory");
			uart_puts("\n@@ w|a to write to a file\n");
			uart_puts("\n@@  Path cluster chain: ");
			if (path.cls1==disk.core.root)
				uart_puts("root@");
			fat32_link_scan(path.cls1);
			init = 0;
		}
		if (uart_peek()) {
			what = uart_read();
			if (what=='f') {
				uart_puts("\n-- rd Name: ");
				init = uart_gets(buff,BUFFSIZE);
				init = validate_filename(buff,init);
				if (init) {
					uart_puts("\n-- Looking for '");
					uart_puts(buff);
					uart_puts("'... ");
					if (fat32_node_find_name(buff)) {
						uart_puts("OK.\n");
						if (!fat32_node_ispath(path.info)) {
							fat32_file_prep(buff,buff,BUFFSIZE,0);
							fat32_file_show();
						}
						else {
							uart_puts("** Not a file! => ");
							fat32_node_show((fat32_node_t*)path.info);
						}
					}
					else uart_puts("not found!\n");
				}
				else uart_puts("\n** Invalid filename!\n");
				init = 1;
			}
			else if (what=='w'||what=='a') {
				uart_puts("\n-- ");
				if (what=='a') uart_puts("a+");
				else uart_puts("w+");
				uart_puts(" Name: ");
				init = uart_gets(buff,BUFFSIZE);
				init = validate_filename(buff,init);
				if (init) {
					fat32_file_prep(buff,buff,BUFFSIZE,0);
					uart_puts("@@ NameOK:");
					uart_puts(file.name);
					uart_send('\n');
					if (what=='a') file.opts |= FAT32_OPTS_APPEND;
					uart_puts("\n-- Text: ");
					init = uart_gets(buff,BUFFSIZE-1);
					if (init) {
						buff[init++] = '\n'; /* do not need a null here! */
						file.size = init;
						uart_puts("\n-- ");
						if (what=='a') uart_puts(" Write-append ");
						else uart_puts("Writing ");
						uart_puts("to ");
						uart_puts(file.name);
						uart_puts("... ");
						fat32_file_write();
						if (fat32_error()) {
							uart_puts("**error** [");
							uart_send_hexlong(fat32_flag());
							uart_puts("|");
							uart_send_hexlong(file.self.load);
							uart_puts("|");
							uart_show_integer(file.clus);
							uart_puts("] ");
						}
						else {
							uart_puts("done. <");
							uart_show_integer(file.curr);
							uart_puts(">\n");
						}
					}
					else uart_puts("\n** Nothing to write!\n");
				}
				else uart_puts("\n** Invalid filename!\n");
				init = 1;
			}
			else if (what=='d') {
				uart_puts("\n-- cDir: ");
				init = uart_gets(buff,16);
				validate_filename(buff,init);
				fat32_path_cd(buff);
				if (fat32_link_ismodified(&path)) {
					fat32_path_list();
					fat32_link_updated(&path);
				}
				else uart_puts("** invalid path? **\n");
				init = 1;
			}
			else if (what=='l') {
				uart_puts("\n-- Listing... init.\n");
				fat32_path_list();
				uart_puts("-- Listing... done.\n");
				init = 1;
			}
			else if (what=='s') {
				uart_puts("\n-- Scan: ");
				init = uart_gets(buff,BUFFSIZE);
				init = validate_filename(buff,init);
				if (init&&fat32_node_find_name(buff)) {
					fat32_link_scan(fat32_node_cluster(path.info));
				}
				else uart_puts("\n** Node not found!!\n");
				init = 1;
			}
			else if (what=='n') {
				uart_puts("\n-- Null: ");
				init = uart_gets(buff,BUFFSIZE);
				if (init) {
					validate_filename(buff,init);
					uart_puts("\n@@ Nullifying ");
					uart_puts(buff);
					uart_puts(" ... ");
					fat32_file_name(buff);
					fat32_file_null();
					if (fat32_error()) {
						uart_puts("**error** {Flag:0x");
						uart_send_hexlong(fat32_flag());
						uart_puts("}\n");
						fat32_clearcode();
					}
					else uart_puts(" done.\n");
				}
				else uart_puts("\n** Cancelled!\n");
			}
			else if (what=='z') {
				uart_puts("\n-- Read: ");
				init = uart_gets(buff,BUFFSIZE);
				init = validate_filename(buff,init);
				if (init&&fat32_node_find_name(buff)) {
					if (!fat32_node_ispath(path.info)) {
						fat32_node_show((fat32_node_t*)path.info);
						uart_puts("@@ Reading... ");
						fat32_file_prep(buff,buff,BUFFSIZE,0);
						fat32_file_read_size();
						uart_puts("done. Size:");
						uart_show_integer(file.curr);
						uart_puts(", eCnt:");
						uart_show_integer(file.tmpx);
						uart_puts("\n");
					}
					else uart_puts("\n** Not a file!!\n");
				}
				else uart_puts("\n** File not found!\n");
				init = 1;
			}
			else if (what=='x') {
				uart_puts("\n-- Dump: ");
				init = uart_gets(buff,BUFFSIZE);
				if (init) {
					validate_filename(buff,init);
					uart_puts("\n@@ Dump '"DUMPTEXT"' ... ");
					step = 64; // 64x16 => 1k data!
					fat32_file_name(buff);
					fat32_file_opts(FAT32_OPTS_APPEND);
					do {
						fat32_file_data(DUMPFULL,DUMPSIZE);
						fat32_file_write();
						if (fat32_error()) {
							uart_puts("**error** [0x");
							uart_send_hexlong(fat32_flag());
							uart_puts("]\n");
							break;
						}
						else uart_send('.');
					} while(--step);
					if (fat32_error()) fat32_clearcode();
					else uart_puts(" done.\n");
				}
				else uart_puts("\n** Cancelled!\n");
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
