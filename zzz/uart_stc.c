/*----------------------------------------------------------------------------*/
#include "uart_stc.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
unsigned char wait_timeout1s(unsigned char step) {
	step *= 20; /* step must be < 13 */
	do {
		if (uart_peek()) return 0;
		timer1_wait(0x4bfd);
		step--;
	} while (step);
	return 1;
}
/*----------------------------------------------------------------------------*/
void main(void) {
	/* prepare uart baud=115200 */
	uart_init_brtbaud(BRT_BAUD115200);
	/* just to show timer1 is available */
	timer1_init();
	while (1) {
		if (wait_timeout1s(3))
			uart_puts("\nHello, there!\n");
		else
			uart_send(uart_read());
	}
}
/*----------------------------------------------------------------------------*/
