/*----------------------------------------------------------------------------*/
#include "fat32_link.h"
#include "fat32_sd.h"
#include "uart_gets.h"
#include "uart_timeout.h"
#include "uart_hexascii.h"
#include "utils.h"
/*----------------------------------------------------------------------------*/
__xdata fat32_link_t path; /* main link */
__xdata fat32_link_t tlnk; /* temp link for fat32_link_scan */
__xdata word32_t test, clus;
__xdata int loop, step;
__xdata unsigned char temp, done;
#define BUFFSIZE 16
__xdata char buff[BUFFSIZE];
/*----------------------------------------------------------------------------*/
typedef struct _mine_t {
	byte08_t *pbin;
	word16_t *pu16;
	word16_t *val1, *val2;
	word32_t *chk1;
	char conv[16];
} mine_t;
/*----------------------------------------------------------------------------*/
__xdata mine_t mine;
/*----------------------------------------------------------------------------*/
void uart_show_integer(unsigned long data) {
	uint2str(mine.conv,data);
	uart_puts(mine.conv);
}
/*----------------------------------------------------------------------------*/
void fat32_item_show(unsigned char *pdat) {
	mine.val1 = (word16_t*)&pdat[20];
	mine.val2 = (word16_t*)&pdat[26];
	clus = ((word32_t)(*mine.val1)<<16)+(*mine.val2);
	if (!clus||(pdat[11]&0x06)) /* skip zero cluster, system/hidden entries */
		return;
	if ((step||loop)&&(pdat[11]&0x08)) /* skip volume labels if not first */
		return;
	uart_puts("@@ [0x");
	uart_send_hexbyte(pdat[11]);
	uart_puts("] {");
	if (pdat[0]!=0xe5&&pdat[0]!=0x05) {
		for (temp=0;temp<11;temp++) {
			if (pdat[temp]>=0x20&&pdat[temp]<=0x7e)
				uart_send(pdat[temp]);
			else uart_send('*');
		}
	}
	else {
		uart_puts("0x");
		uart_send_hexbyte(pdat[0]);
		if (pdat[1]>=0x20&&pdat[1]<=0x7e) {
			uart_send(':');
			for (temp=1;temp<11;temp++) {
				if (pdat[temp]>=0x20&&pdat[temp]<=0x7e)
					uart_send(pdat[temp]);
				else uart_send('*');
			}
		}
	}
	uart_puts("} (Time:0x");
	mine.val1 = (word16_t*)&pdat[22];
	uart_send_hexuint(*mine.val1);
	uart_puts(" Date:0x");
	mine.val1 = (word16_t*)&pdat[24];
	uart_send_hexuint(*mine.val1);
	uart_puts(") (Clus:");
	uart_show_integer(clus);
	uart_puts("} (Size:");
	mine.chk1 = (word32_t*)&pdat[28];
	uart_show_integer(*mine.chk1);
	uart_puts(")\n");
}
/*----------------------------------------------------------------------------*/
void fat32_link_list(word32_t clus) {
	done = 0; step = 0;
	fat32_link_prep(&path,clus);
	fat32_link_load(&path);
	if (fat32_link_iserror(&path)) {
		fat32_link_eclear(&path);
		uart_puts("** error loading init! **\n");
		return;
	}
	do {
		uart_puts("@@ Cluster: ");
		uart_show_integer(path.curr);
		uart_puts("|");
		uart_show_integer(path.isec);
		uart_puts(")\n");
		/* browse 32-bytes fat32 entries */
		for (loop=0,mine.pbin=disk.buff;loop<FAT32_ENTRYPSEC;
				loop++,mine.pbin+=FAT32_ENTRYSIZE) {
			if (mine.pbin[0]==0x00) { /** ends here! */
				uart_puts("@@ Done: Loop=");
				uart_show_integer(loop);
				uart_puts(" Step=");
				uart_show_integer(step);
				uart_puts("\n");
				done = 1;
				break;
			}
			fat32_item_show(mine.pbin);
		}
		if (done) break;
		fat32_link_next(&path);
		if (fat32_link_iserror(&path)) {
			fat32_link_eclear(&path);
			uart_puts("** error loading next! **\n");
			return;
		}
		step++;
	} while (path.load);
}
/*----------------------------------------------------------------------------*/
void fat32_link_scan(word32_t clus) {
	tlnk.curr = clus;
	do {
		uart_puts(" >");
		uart_show_integer(tlnk.curr);
		fat32_link_get(&tlnk);
		if (fat32_link_iserror(&path)) {
			fat32_link_eclear(&path);
			uart_puts("** error getting link **\n");
			return;
		}
		tlnk.curr = tlnk.pu32[0];
		if (fat32_is_last_cluster(tlnk.curr)) {
			uart_puts(" #last.\n");
			break;
		}
		if (fat32_is_invalid_cluster(tlnk.curr)) {
			uart_puts("** error! **\n");
			break;
		}
	} while (1);
}
/*----------------------------------------------------------------------------*/
word32_t fat32_link_pick(word32_t clus, int pick) {
	temp = 0;
	fat32_link_prep(&path,clus);
	fat32_link_load(&path);
	if (fat32_link_iserror(&path)) {
		fat32_link_eclear(&path);
		uart_puts("** error loading that! **\n");
		return 0x0;
	}
	do {
		for (loop=0,mine.pbin=card.buff;loop<FAT32_ENTRYPSEC;
				loop++,mine.pbin+=FAT32_ENTRYSIZE) {
			if (mine.pbin[0]==0xe5||mine.pbin[0]==0x05) continue;
			if (mine.pbin[11]&0x08) continue;
			if (mine.pbin[0]==0x00) return 0;
			temp++;
			if (temp==pick) {
				mine.pu16 = (word16_t*)&mine.pbin[20];
				test = (word32_t) (*mine.pu16);
				test <<= 16;
				mine.pu16 = (word16_t*)&mine.pbin[26];
				test =+ (word32_t) (*mine.pu16);
				return test;
			}
		}
		fat32_link_next(&path);
		if (fat32_link_iserror(&path)) {
			fat32_link_eclear(&path);
			uart_puts("** error loading next! **\n");
			return 0;
		}
	} while (path.load);
	return 0;
}
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n-- Testing FAT32 LinkSD\n");
	temp = 1;
	do {
		if (temp||uart_timeout(2000)) {
			uart_puts("\nPress <ENTER> to start.");
			temp = 0;
		}
		else if (uart_read()=='\n') break;
	} while (1);
	uart_puts("\n\n-- Preparing SD card... ");
	fat32_disk_prep();
	if (fat32_error()) {
		uart_puts("**error** [0x");
		uart_send_hexbyte(card.flag);
		uart_puts("]\n");
		hang();
	}
	uart_puts("OK.\n");
	uart_puts("-- Accessing FAT32 disk... ");
	fat32_disk_init();
	if (fat32_error()) {
		uart_puts("**error** {Flag:0x");
		uart_send_hexlong(fat32_flag());
		uart_puts("}\n");
		hang();
	}
	uart_puts("OK.\n");
	uart_puts("\nPress <ENTER> to list root directory.");
	while(uart_read()!='\n');
	uart_puts("\n\n-- [INIT] List\n");
	fat32_link_list(fat32_root());
	uart_puts("-- [DONE] List\n");
	uart_puts("\nPress <ENTER> to view links (cluster chains).");
	while(uart_read()!='\n');
	uart_puts("\n\n## Root:");
	fat32_link_scan(fat32_root());
	loop = 1;
	do {
		test = fat32_link_pick(fat32_root(),loop);
		if (test) {
			uart_puts("   [");
			uart_send_hexuint(loop);
			uart_puts("] ");
			if (mine.pbin[11]&0x10) uart_puts("PATH{");
			else uart_puts("FILE{");
			for (temp=0;temp<11;temp++) {
				if (mine.pbin[temp]>=0x20&&mine.pbin[temp]<=0x7e)
					uart_send(mine.pbin[temp]);
				else uart_send('*');
			}
			uart_puts("}:");
			fat32_link_scan(test);
			loop++;
		}
	} while (test);
	while (1) {
		uart_puts("\n@@ Cluster check\n");
		uart_puts("-- Cluster: ");
		uart_gets(buff,BUFFSIZE);
		test = str2uint(buff);
		if (fat32_is_usable_cluster(test)) {
			tlnk.curr = test;
			fat32_link_get(&tlnk);
			uart_puts("## Cluster ");
			uart_show_integer(test);
			uart_puts(" is ");
			if (fat32_is_usable_cluster(tlnk.pu32[0])||
					fat32_is_last_cluster(tlnk.pu32[0])) {
				uart_puts("used.[0x");
				uart_send_hexlong(tlnk.pu32[0]);
				uart_puts("]\n");
				fat32_link_scan(test);
			}
			else if (fat32_is_free_cluster(tlnk.pu32[0])) {
				uart_puts("free. [0x");
				uart_send_hexlong(tlnk.pu32[0]);
				uart_puts("]\n");
			}
			else {
				uart_puts("unknown. [0x");
				uart_send_hexlong(tlnk.pu32[0]);
				uart_puts("]\n");
			}
		}
		else {
			uart_puts("** Cluster ");
			uart_show_integer(test);
			uart_puts(" not usable!\n");
		}
	}
}
/*----------------------------------------------------------------------------*/
