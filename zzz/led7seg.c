/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "led7seg.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char loop, step, mask, seg7[] = { _7SEGNUM_CC_ };
	timer_init();
	loop = 0; mask = 0x00;
	while (1) {
		mask = (!P1_0)?0xff:0x00;
		P2 = seg7[loop] ^ mask;
		step = TIMER_LOOP_1S;
		do { timer_wait(TIMER_VAL50MS); } while (--step);
		loop++; if (loop==10) loop = 0;
	}
}
/*----------------------------------------------------------------------------*/
