/*----------------------------------------------------------------------------*/
#include "fat32_core.h"
#include "sdcard.h"
#include "uart.h"
#include "uart_hexascii.h"
/*----------------------------------------------------------------------------*/
__xdata fat32_core_t core;
__xdata word32_t ccnt, if2t, fasz, test, step, curr;
word32_t* pu32;
unsigned char what, temp;
int bcnt;
/*----------------------------------------------------------------------------*/
#define FAT32CORE(core) ((fat32_core_t*)core)
#define fat32_has_error(core) (FAT32CORE(core)->flag&FAT32_FLAG_ERROR)
#define fat32_core_fasz(core) ((FAT32CORE(core)->data-FAT32CORE(core)->ifat)>>1)
#define fat32_core_fat2(core) FAT32CORE(core)->ifat + fat32_core_fasz(core)
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n\n-- Testing FAT32 CoreSD\n\n");
	core.buff = card.buff;
	/* prepare sd interface */
	uart_puts("-- Preparing SD card... ");
	do { sdcard_prep(); } while (card.flag!=SDPREP_OK);
	uart_puts("OK.\n");
	/* get mbr */
	uart_puts("-- Reading MBR... ");
	sdcard_read_sector(0);
	if (card.flag!=SDCARD_OK) {
		uart_puts("**error** [0x");
		uart_send_hexbyte(card.resp);
		uart_puts("]\n");
		hang();
	}
	fat32_core_init(&core);
	if (fat32_has_error(&core)) {
		uart_puts("**error** [0x");
		uart_send_hexlong(core.flag);
		uart_puts("]\n");
		hang();
	}
	uart_puts("OK.\n");
	/* get vbr */
	uart_puts("-- Reading VBR... ");
	sdcard_read_sector(core.root);
	if (card.flag!=SDCARD_OK) {
		uart_puts("**error** [0x");
		uart_send_hexbyte(card.resp);
		uart_puts("]\n");
		hang();
	}
	fat32_core_load(&core);
	if (fat32_has_error(&core)) {
		uart_puts("**error** [0x");
		uart_send_hexlong(core.flag);
		uart_puts("]\n");
		hang();
	}
	uart_puts("OK.\n");
	/* estimate # of clusters */
	test = (core.data - core.ifat); /* number of sectors allocated for fat */
	test >>= 1; /* always 2 fats (ensured in fat32_core_load) */
	if2t = core.ifat + test; /* sector for second fat */
	test *= FAT32_SECTOR_SIZE; /* byte count for fat */
	ccnt = test >> 2; /* cluster entry has 4-bytes */
	test = fat32_core_fat2(&core);
	fasz = fat32_core_fasz(&core);
	/* show core info */
	uart_puts("-- FAT32 Flag: 0x");
	uart_send_hexlong(core.flag);
	uart_send('\n');
	uart_puts("## FAT start sector: 0x");
	uart_send_hexlong(core.ifat);
	uart_send('\n');
	uart_puts("## DAT start sector: 0x");
	uart_send_hexlong(core.data);
	uart_send('\n');
	uart_puts("## Sector(s) per cluster: 0x");
	uart_send_hexbyte(core._spc);
	uart_send('\n');
	uart_puts("## Root cluster: 0x");
	uart_send_hexlong(core.root);
	uart_send('\n');
	uart_puts("## FAT2 start sector: 0x");
	uart_send_hexlong(if2t);
	uart_puts(" @ 0x");
	uart_send_hexlong(test);
	uart_send('\n');
	uart_puts("## FAT size (sectors): 0x");
	uart_send_hexlong(((vbr_t*)card.buff)->fasz);
	uart_puts(" @ 0x");
	uart_send_hexuint(fasz);
	uart_send('\n');
	uart_puts("## Cluster count: 0x");
	uart_send_hexlong(ccnt);
	uart_send('\n');
	/* first sector of fat entry */
	uart_puts("\nPress <ENTER> to get stats for first FAT sector.");
	while(uart_read()!='\n');
	uart_puts("\n\n");
	sdcard_read_sector(core.ifat);
	pu32 = (word32_t*) card.buff;

	uart_puts("## Legend: [used] {free} <huh?>\n");
	for (temp=0;temp<(FAT32_SECTOR_SIZE/4);temp++,pu32++) {
		if ((temp%4)==0) uart_puts("\n@@");
		if (fat32_is_usable_cluster(pu32[0])||fat32_is_last_cluster(pu32[0])) {
			uart_puts(" [");
			uart_send_hexbyte(temp);
			uart_puts(":0x");
			uart_send_hexlong(pu32[0]);
			uart_send(']');
		}
		else if (fat32_is_free_cluster(pu32[0])) {
			uart_puts(" {");
			uart_send_hexbyte(temp);
			uart_puts(":0x");
			uart_send_hexlong(pu32[0]);
			uart_send('}');
		}
		else {
			uart_puts(" <");
			uart_send_hexbyte(temp);
			uart_puts(":0x");
			uart_send_hexlong(pu32[0]);
			uart_send('>');
		}
	}
	/* a few view fat entry */
	uart_puts("\nPress <ENTER> to start viewing FAT data.\n");
	while(uart_read()!='\n');
	curr = core.ifat;
	while (1) {
		uart_puts("\n@@ IFAT[0x");
		uart_send_hexlong(curr);
		uart_puts("]\n");
		sdcard_read_sector(curr);
		if (card.resp==SDCARD_RESP_SUCCESS) {
			step = (curr-core.ifat)*512;
			for (bcnt=0;bcnt<FAT32_SECTOR_SIZE;bcnt++) {
				if (bcnt%16==0) {
					uart_send('\n');
					uart_send_hexlong(step);
					uart_send(':');
				}
				if (bcnt%4==0) {
					uart_send(' ');
					step++;
				}
				uart_send(' ');
				uart_send_hexbyte(card.buff[bcnt]);
			}
			uart_send('\n');
		}
		else uart_puts("**error**\n");
		uart_puts("@@ '+' or '-'\n");
		do {
			what = uart_read();
			if (what=='+') {
				curr++;
				if (curr<if2t)
					break;
				curr = if2t-1;
			}
			else if (what=='-') {
				curr--;
				if (curr>=core.ifat)
					break;
				curr = core.ifat;
			}
		} while(1);
	}
}
/*----------------------------------------------------------------------------*/
