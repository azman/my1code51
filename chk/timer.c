/*----------------------------------------------------------------------------*/
#include "timer.h"
/*----------------------------------------------------------------------------*/
unsigned char iter, tcnt;
/*----------------------------------------------------------------------------*/
void timer0_handler(void) __interrupt(TF0_VECTOR) {
	timer0_prep(TIMER_VAL50MS);
	if (iter) iter--;
}
/*----------------------------------------------------------------------------*/
void timer1_handler(void) __interrupt(TF1_VECTOR) {
	timer1_prep(TIMER_VAL10MS);
	if (tcnt) tcnt--;
}
/*----------------------------------------------------------------------------*/
#define STEP1 200
/*----------------------------------------------------------------------------*/
void main(void) {
	P1 = 0xAA; P2 = 0x01;
	iter = TIMER_LOOP_1S;
	tcnt = STEP1;
	timer_init_dual();
	timer0_make(TIMER_VAL50MS);
	timer1_make(TIMER_VAL10MS);
	while(1) {
		if (!iter) {
			P1 = ~P1;
			iter = TIMER_LOOP_1S;
		}
		if (!tcnt) {
			P2 <<= 1;
			if (!P2) P2 = 0x01;
			tcnt = STEP1;
		}
	}
}
/*----------------------------------------------------------------------------*/
