/*----------------------------------------------------------------------------*/
#include "cmps12.h"
#include "uart.h"
#include "timer.h"
#include "utils_lite.h"
/*----------------------------------------------------------------------------*/
void uart_send_hexbyte(unsigned char byte)
{
	unsigned char temp;
	temp = (byte&0xf0)>>4;
	byte = byte&0x0f;
	uart_send(hex2ASCII(temp));
	uart_send(hex2ASCII(byte));
}
/*----------------------------------------------------------------------------*/
void main(void) {
	char buff[16];
	unsigned char test;
	unsigned int temp;
	uart_init();
	i2c_init();
	timer_init();
	timer_delay1s(test,2);
	uart_puts("\nLooking for CMPS12(0x");
	uart_send_hexbyte(CMPS12_I2C_ADDR);
	uart_puts(")... ");
	do {
		test = cmps12_getversion();
		if (test!=0xff) break;
		uart_send('.');
		timer_delay1s(test,2);
	} while (1);
	uart_puts("done.\n\nCMPS12 Interface Test [SW-Vers:0x");
	uart_send_hexbyte(test);
	uart_puts("]\n\n");
	while(1) {
		temp = cmps12_gethead16();
		uart_puts("-- Head:{");
		uint16_2cstr(buff,temp);
		uart_puts(buff);
		uart_puts("}\n");
		timer_delay1s(test,3);
	}
}
/*----------------------------------------------------------------------------*/
