/*----------------------------------------------------------------------------*/
#include "uart.h"
/*----------------------------------------------------------------------------*/
my1sbit(IRMOD,PIN10);
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned int wait;
	uart_init();
	uart_puts("TESTING IR MODULE\r\n");
	P2 = 0;
	while(1) {
		uart_puts("\r\nWaiting... ");
		while (IRMOD); // outputs logic low when obstacle detected
		uart_send('{');
		while (!IRMOD);
		uart_puts("BLOCK}\r\n");
		for (wait=65000;wait;wait--);
		P2++;
	}
}
/*----------------------------------------------------------------------------*/
