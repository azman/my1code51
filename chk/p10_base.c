/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "uart_stc.h"
/*----------------------------------------------------------------------------*/
#define P10_STEP 2
#include "p10_base.h"
/*----------------------------------------------------------------------------*/
#define STEP_SIZE 200
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char curr, rrow, loop;
	unsigned int step;
	uart_init_brt();
	uart_puts("-- I am alive!\n");
	p10_init();
	uart_puts("## Main loop!\n");
	timer_tick_exec();
	curr = 0x80; rrow = 0; step = STEP_SIZE;
	while (1) {
		if (timer_ticked()) {
			if (step) step--;
			else { curr+=4; step = STEP_SIZE; }
/*
			else {
				for (loop=0;loop<16;loop++)
					p10_push(curr);
				curr++;
				step = STEP_SIZE;
			}
			else { p10_shift(P10_PIX_ON); step = STEP_SIZE; }
*/
			for (loop=0;loop<16*P10_STEP;loop++)
				p10_push(curr|rrow);
			p10_off();
			p10_latch();
			switch (rrow) {
				case 0: p10_light0(); break;
				case 1: p10_light1(); break;
				case 2: p10_light2(); break;
				case 3: p10_light3(); break;
			}
			if (++rrow>=4) rrow = 0;
			p10_on();
			timer_tick00();
		}
	}
}
/*----------------------------------------------------------------------------*/