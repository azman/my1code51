/*----------------------------------------------------------------------------*/
#include "uart.h"
#include "timer_tick.h"
/*----------------------------------------------------------------------------*/
#define MAX_CHAR 32
#define CHK_BEAT 5
/*----------------------------------------------------------------------------*/
unsigned char beat;
char buff[MAX_CHAR];
/*----------------------------------------------------------------------------*/
void main(void) {
	beat = 0;
	uart_init();
	timer_tick_exec();
	while(1) {
		if (timer_ticked()) {
			timer_tick00();
			if (beat) beat--;
		}
		if (!beat) {
			uart_puts("\n-- [my1code51] UART Test\n");
			uart_puts("\nPress <ENTER> to start!\n");
			beat = CHK_BEAT;
		}
		if (!uart_peek()||uart_read()!='\n')
			continue;
		uart_puts("\nName: ");
		uart_gets(buff,MAX_CHAR);
		uart_puts("Hello, ");
		uart_puts(buff);
		uart_puts("!\n");
		beat = CHK_BEAT;
	}
}
/*----------------------------------------------------------------------------*/
