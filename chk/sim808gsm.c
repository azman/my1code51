/*----------------------------------------------------------------------------*/
#include "sim808gsm.h"
#include "sim808_uart.h"
/*----------------------------------------------------------------------------*/
#include "uart_gets.h"
#include "uart_hexascii.h"
#include "timer.h"
#include "string.h"
/*----------------------------------------------------------------------------*/
/* active low buttons */
my1sbit(BUTTON_CALL,PIN10);
my1sbit(BUTTON_TEXT,PIN11);
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
__xdata char buff[BUFFSIZE];
char* pbuf;
unsigned char test, step, flag;
/*----------------------------------------------------------------------------*/
#define FLAG_GSM_IDLE 0x00
#define FLAG_GSM_CALL 0x01
#define FLAG_GSM_GREET 0x80
/*----------------------------------------------------------------------------*/
#define buff_len(buf,cnt) { cnt=0; while(buf[cnt]) cnt++; }
/*----------------------------------------------------------------------------*/
void show_buff_byte(char* buff, unsigned char size) {
	unsigned char loop;
	uart_puts("@@ Check:(");
	uart_send_hexbyte(size);
	uart_puts("){");
	for (loop = 0;loop<size&&buff[loop];loop++) {
		if (loop) uart_send(',');
		uart_puts("0x");
		uart_send_hexbyte(buff[loop]);
	}
	uart_puts("}\n");
}
/*----------------------------------------------------------------------------*/
void main(void) {
	BUTTON_CALL = 1;
	BUTTON_TEXT = 1;
	timer_init();
	uart_init();
	uart_puts("\n\nSIM808 GSM\n\n");
	sim808_init();
	uart_puts("-- Looking for device...\n");
	sim808_find_wait();
	uart_puts("-- Disable echo...\n");
	sim808_echo_off();
	sim808_ok2pass();
	uart_puts("-- Get manufacturer info... ");
	sim808_minf();
	sim808_show_if_ok(buff,BUFFSIZE);
	uart_puts("-- Get manufacturer module... ");
	sim808_mmod();
	sim808_show_if_ok(buff,BUFFSIZE);
	uart_puts("-- Get IMEI... ");
	sim808_imei();
	sim808_show_if_ok(buff,BUFFSIZE);
	uart_puts("-- Checking SIM status... ");
	sim808gsm_cpin();
	sim808_show_if_ok(buff,BUFFSIZE);
	do {
		uart_puts("-- Checking line status... ");
		sim808gsm_stat();
		pbuf = sim808_show_if_ok(buff,BUFFSIZE);
		buff_len(pbuf,test);
		if (test==3) {
			if (pbuf[2]=='1') {
				uart_puts("@@ Registered on home network!\n");
				break;
			}
			uart_puts("-- Registering... ");
			sim808gsm_init();
			sim808_show_if_ok(buff,BUFFSIZE);
			uart_puts("-- Allow device to settle down...\n");
			step = 5;
			do {
				if (!sim808_timeout())
					sim808_purge();
			} while (--step);
		}
	} while (1);
	uart_puts("-- Configure text mode... ");
	sim808gsm_conf_text();
	sim808_show_if_ok(buff,BUFFSIZE);
	/* main loop */
	flag = FLAG_GSM_GREET;
	while (1) {
		if (flag&FLAG_GSM_GREET) {
			flag &= ~FLAG_GSM_GREET;
			uart_puts("\n## SIM808 Module Ready for Call/SMS!\n");
		}
		/* any news? */
		if (!sim808_timeout()) {
			while (sim808_peek())
				uart_send(sim808_read());
		}
		if (flag&FLAG_GSM_CALL) {
			/* in call */
			if (BUTTON_CALL==0) {
				uart_puts("\nHanging-up... ");
				sim808gsm_hang();
				pbuf = sim808_show_if_ok(buff,BUFFSIZE);
				if (pbuf) {
					uart_puts("## Call ended!\n");
					flag &= ~FLAG_GSM_CALL;
				}
			}
		}
		else {
			if (BUTTON_CALL==0) {
				while (BUTTON_CALL==0);
				uart_puts("\Number2Call? > ");
				uart_gets(buff,16);
				uart_puts("\nDialling ");
				uart_puts(buff);
				uart_puts("... ");
				sim808gsm_call(buff);
				pbuf = sim808_show_if_ok(buff,BUFFSIZE);
				if (pbuf) {
					uart_puts("## Call in progress...\n");
					flag |= FLAG_GSM_CALL;
				}
			}
			else if (BUTTON_TEXT==0) {
				while (BUTTON_TEXT==0);
				uart_puts("\nNumber2Text? > ");
				uart_gets(buff,16);
				uart_puts("\Saying hello to ");
				uart_puts(buff);
				uart_puts("... ");
				test = sim808gsm_text(buff,"Hello there!");
				if (!test) sim808_show_if_ok(buff,BUFFSIZE);
				else if (test==SIM808GSM_TEXT_ERROR)
					uart_puts("\n** Invalid number?\n");
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
