/*----------------------------------------------------------------------------*/
#include "textlcd.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
unsigned char loop = 0, step;
char fill = '.';
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	lcd_init();
	lcd_goto_line1();
	lcd_puts("MY1CODE51 TXTLCD");
	lcd_goto_line2();
	while (1) {
		lcd_data(fill); loop++;
		if (loop==LCD_MAX_CHAR) {
			loop = 0;
			lcd_goto_line2();
			if(fill=='.') fill = '+';
			else fill = '.';
		}
		timer_delay1s(step,1);
	}
}
/*----------------------------------------------------------------------------*/
