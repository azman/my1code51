/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "uart_stc.h"
/*----------------------------------------------------------------------------*/
#define P10_STEP 2
#include "p10_display.h"
/*----------------------------------------------------------------------------*/
__xdata p10_t p10disp;
/*----------------------------------------------------------------------------*/
#define STEP_SIZE 20
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned int step;
	unsigned char irow, icol;
	uart_init_brt();
	uart_puts("-- I am alive!\n");
	p10_setup(&p10disp);
	p10_clear(&p10disp);
	uart_puts("## Main loop!\n");
	step = STEP_SIZE; irow = 0; icol = 0;
	p10_set_pixel(&p10disp,icol,irow,1);
	timer_tick_exec();
	while (1) {
		if (timer_ticked()) {
			if (step) step--;
			else {
				step = STEP_SIZE;
				p10_set_pixel(&p10disp,icol,irow,0);
				icol++;
				if (icol==(P10_COLS*P10_STEP)) {
					icol = 0;
					irow++;
					if (irow==P10_ROWS) irow = 0;
				}
				p10_set_pixel(&p10disp,icol,irow,1);
			}
			p10_update(&p10disp);
			p10_refresh(&p10disp);
			timer_tick00();
		}
	}
}
/*----------------------------------------------------------------------------*/