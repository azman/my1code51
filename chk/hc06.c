/*----------------------------------------------------------------------------*/
#include "hc06.h"
#include "timer.h"
#include "uart_gets.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
/*----------------------------------------------------------------------------*/
__xdata char buff[BUFFSIZE];
unsigned char test, loop;
/*----------------------------------------------------------------------------*/
void hc06_setname_full(char *name) {
	atcmd_puts(HC06CMD_ATNAME);
	atcmd_puts(name);
	atcmd_puts(HC06CMD_ATENDS);
	test = atcmd_read_wait(buff,BUFFSIZE);
	if (test==BUFFSIZE) test--;
	buff[test] = 0x0;
}
/*----------------------------------------------------------------------------*/
void hc06_setpin_full(char *pass) {
	atcmd_puts(HC06CMD_ATPASS);
	atcmd_puts(pass);
	atcmd_puts(HC06CMD_ATENDS);
	test = atcmd_read_wait(buff,BUFFSIZE);
	if (test==BUFFSIZE) test--;
	buff[test] = 0x0;
}
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	uart_init();
	uart_puts("\n\nHC06 (my1code51)\n\n");
	hc06_init();
	while (1) {
		uart_puts("-- Sending AT... ");
		hc06_find();
		if (hc06_wait_ok()==HC06_OK) {
			uart_puts("OK.\n");
			break;
		}
		uart_puts("timeout.\n");
		timer_delay1s(test,1);
	}
	uart_puts("-- Version: ");
	hc06_getvers(buff,BUFFSIZE);
	/** assume using default: linvorV1.8 */
	if (buff[0]!='O'||buff[1]!='K') {
		uart_puts("**???** ");
		test = 0;
	}
	else test = 2;
	uart_puts(&buff[test]);
	uart_send('\n');
	timer_delay1s(test,1);
	/* free flow */
	while (1) {
		if (uart_peek()) {
			test = uart_read();
			if (test==':') {
				uart_puts("\nName:");
				test = uart_gets(buff,16);
				uart_puts("\n-- Set Name (");
				uart_puts(buff);
				uart_puts("): ");
				hc06_setname_full(buff);
				uart_puts(buff);
				uart_send('\n');
				timer_delay1s(test,1);
			}
			else if (test=='#') {
				uart_puts("\nPin:");
				test = uart_gets(buff,16);
				uart_puts("\n-- Set Pin (");
				uart_puts(buff);
				uart_puts("): ");
				hc06_setpin_full(buff);
				uart_puts(buff);
				uart_send('\n');
				timer_delay1s(test,1);
			}
			else if (test=='~') {
				uart_puts("\nSet default name & pin\n");
				uart_puts("-- Set Name (");
				uart_puts(BTNAME);
				uart_puts("): ");
				hc06_setname(buff,BUFFSIZE);
				uart_puts(buff);
				uart_send('\n');
				timer_delay1s(test,1);
				/* set pass */
				uart_puts("-- Set Pin (");
				uart_puts(BTPASS);
				uart_puts("): ");
				hc06_setpin(buff,BUFFSIZE);
				uart_puts(buff);
				uart_send('\n');
				timer_delay1s(test,1);
			}
			else {
				uart_send(test); /* local echo */
				hc06_send(test);
			}
		}
		if (hc06_peek())
			uart_send(hc06_read());
	}
}
/*----------------------------------------------------------------------------*/
