/*----------------------------------------------------------------------------*/
#include "sim808gps.h"
#include "sim808_uart.h"
/*----------------------------------------------------------------------------*/
#include "uart_hexascii.h"
#include "timer.h"
#include "string.h"
#define MY1CSTR_SIZE 128
#include "cstr.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
__xdata char buff[BUFFSIZE];
__xdata cstr_t json;
unsigned char test,step;
/*----------------------------------------------------------------------------*/
void sim808gps_show_location(void) {
	if (gpsstr.p_ok) {
		uart_puts("OK.\n");
		gpsstr.pstr = gpsstr.pchk; gpsstr.loop = 0;
		while (gpsstr.pstr[0]) {
			if (gpsstr.pstr[0]==',') {
				gpsstr.pstr[0] = 0x0;
				gpsstr.loop++;
			}
			if (gpsstr.loop==4||gpsstr.loop==3||gpsstr.loop==8) {
				/* stop at dot for utc,alt,dir */
				if (gpsstr.pstr[0]=='.')
					gpsstr.pstr[0] = 0x0;
			}
			gpsstr.pstr++;
		}
		cstr_assign(&json,"{\"utc\":");
		cstr_append(&json,gpsstr.putc);
		cstr_append(&json,";\"lng\":");
		cstr_append(&json,gpsstr.plng);
		cstr_append(&json,";\"lat\":");
		cstr_append(&json,gpsstr.plat);
		cstr_append(&json,";\"vel\":");
		cstr_append(&json,gpsstr.pvel);
		cstr_append(&json,";\"dir\":");
		cstr_append(&json,gpsstr.pdir);
		cstr_append(&json,";\"alt\":");
		cstr_append(&json,gpsstr.palt);
		cstr_append(&json,";\"num\":");
		cstr_append(&json,gpsstr.pnum);
		cstr_append(&json,";}");
		uart_puts("## JSON:");
		uart_puts(json.buff);
		uart_puts("\n");
	} 
	else {
		if (gpsstr.temp) {
			uart_puts("???\n@@ Check:{");
			uart_puts(gpsstr.pchk);
			uart_puts("}\n");
		}
		else {
			uart_puts("invalid?\n");
			sim808_show_buff(buff,BUFFSIZE);
		}
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	timer_delay1s(test,1);
	uart_init();
	uart_puts("\n\nSIM808 GPS\n\n");
	sim808_init();
	while (1) {
		uart_puts("-- Looking for SIM808... ");
		sim808_find();
		if (sim808_found()) {
			uart_puts("OK.\n");
			break;
		}
		uart_puts("timeout.\n");
		timer_delay1s(test,1);
	}
	uart_puts("-- Disabling echo...\n");
	sim808_echo_off();
	sim808_ok2pass();
	uart_puts("-- Allow device to settle down...\n");
	step = 5;
	do {
		if (!sim808_timeout())
			sim808_purge();
	} while (--step);
	uart_puts("-- Powerup GPS... ");
	sim808gps_init();
	sim808_must_be_ok();
	uart_puts("-- Checking GPSINF... ");
	sim808gps_opts();
	sim808_show_if_ok(buff,BUFFSIZE);
	/* wait for location fix */
	while (1) {
		uart_puts("-- Checking GPSSTATUS... ");
		sim808gps_fixd();
		gpsstr.p_ok = sim808_show_if_ok(buff,BUFFSIZE);
		if (!strncmp(gpsstr.p_ok,"Location 2D Fix",15)||
				!strncmp(gpsstr.p_ok,"Location 3D Fix",15))
			break;
	}
	sim808gps_inf_init(buff,BUFFSIZE);
	cstr_init(&json);
	/* main loop */
	while (1) {
		uart_puts("-- Getting location... ");
		sim808gps_data();
		sim808gps_wait_data();
		sim808gps_show_location();
		timer_delay1s(test,1);
	}
}
/*----------------------------------------------------------------------------*/
