/*----------------------------------------------------------------------------*/
#include "utils_lite.h"
#include "uart.h"
#include "uart_hexascii.h"
#include "string.h"
/*----------------------------------------------------------------------------*/
#define MAX_CHAR 32
#define PROMPT "\nEnter a string (MAX=" mstr(MAX_CHAR) "): "
/*----------------------------------------------------------------------------*/
int temp;
char buff[MAX_CHAR], conv[MAX_CHAR];
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_purge();
	uart_puts("\nPress <ENTER> to start!\n");
	while (uart_read()!='\n');
	uart_puts(PROMPT);
	uart_gets(buff,MAX_CHAR);
	uart_puts("String is '");
	uart_puts(buff);
	uart_puts("'\n");
	uart_purge();
	while (1) {
		uart_puts("\nEnter an integer (signed,16-bit): ");
		uart_gets(buff,MAX_CHAR);
		uart_puts("Input is ");
		uart_puts(buff);
		cstr_2int16(buff,&temp);
		int16_2cstr(conv,temp);
		if (!strncmp(buff,conv,strlen(buff)+1)) {
			uart_puts("\n-- Integer is ");
			uart_puts(conv);
			uart_puts(" {0x");
			uart_send_hexuint((unsigned int)temp);
			uart_puts("}\n");
		}
		else {
			uart_puts("\n** Invalid integer! {I:");
			uart_puts(buff);
			uart_puts("}{C:");
			uart_puts(conv);
			uart_puts("}{0x");
			uart_send_hexuint((unsigned int)temp);
			uart_puts("}\n");
		}
	}
}
/*----------------------------------------------------------------------------*/
