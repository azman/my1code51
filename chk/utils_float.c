/*----------------------------------------------------------------------------*/
/**
 * Test program for utils_float.h
**/
/*----------------------------------------------------------------------------*/
#include "utils_float.h"
#include "uart_hexascii.h"
#include "uart_gets.h"
/*----------------------------------------------------------------------------*/
#define MAX_CHAR 32
/*----------------------------------------------------------------------------*/
long check;
float value;
char buffer[MAX_CHAR];
unsigned long* pu32;
/*----------------------------------------------------------------------------*/
void uart_send_integer(long word) {
	int2str(buffer,word);
	uart_puts(buffer);
}
/*----------------------------------------------------------------------------*/
void uart_send_floatpt(float data) {
	float2str(buffer,data);
	uart_puts(buffer);
}
/*----------------------------------------------------------------------------*/
void main(void) {
	pu32 = (unsigned long*) &value;
	uart_init();
	uart_puts("\nPress <ENTER> to start!\n");
	while (uart_read()!='\n');
	while (1) {
		uart_puts("\nEnter a string (MAX=");
		uart_send_integer(MAX_CHAR);
		uart_puts("): ");
		uart_gets(buffer,MAX_CHAR);
		uart_puts("String is '");
		uart_puts(buffer);
		uart_puts("'\nEnter an integer: ");
		uart_gets(buffer,MAX_CHAR);
		uart_puts("Input is ");
		uart_puts(buffer);
		uart_puts("\nInteger is ");
		check = str2int(buffer);
		uart_send_integer(check);
		uart_puts(" (0x");
		uart_send_hexlong(check);
		uart_puts(")\nEnter a float: ");
		uart_gets(buffer,MAX_CHAR);
		uart_puts("Input is ");
		uart_puts(buffer);
		uart_puts("\nFloat is ");
		value = str2float(buffer);
		uart_send_floatpt(value);
		uart_puts(" (0x");
		uart_send_hexlong(*pu32);
		uart_puts(")\n");
	}
}
/*----------------------------------------------------------------------------*/
