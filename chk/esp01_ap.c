/*----------------------------------------------------------------------------*/
#include "esp01_ap.h"
#include "esp01_uart.h"
#include "esp01_data.h"
#include "timer_keep.h"
/*----------------------------------------------------------------------------*/
#include "uart_hexascii.h"
#include "uart_read.h"
/*----------------------------------------------------------------------------*/
/**
 * use binary from my1tcpserve repo to test
 * > my1tcpcheck --ip 192.168.4.1 --uuid 0023 --type 1000 --dhex 23 32
*/
/*----------------------------------------------------------------------------*/
#ifndef NODE_UUID
#define NODE_UUID 0x0001
#endif
/*----------------------------------------------------------------------------*/
#define TIMEINT_0755H 0x0737
#define TIMEINT_0800H 0x0800
#define NODE_ACTIVEINIT_DEF TIMEINT_0800H
/*----------------------------------------------------------------------------*/
#define NODE_ACTIVETIME_MAX_S 30*60
#define NODE_ACTIVETIME_DEF_S 15*60
#define NODE_ACTIVE 1
#define NODE_IDLE 0
/*----------------------------------------------------------------------------*/
#define NODE_COUNT 2
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
__xdata char buff[BUFFSIZE];
__xdata char ssid[SSID_BUFFLEN];
__xdata unsigned char repl[32];
/*----------------------------------------------------------------------------*/
my1sbit(SERV1,PIN04); /* server ready */
my1sbit(ERROR,PIN05); /* system error */
/*----------------------------------------------------------------------------*/
/* using P0.0 - P0.1 for node control pins */
my1sbit(NODE0,PIN00);
my1sbit(NODE1,PIN01);
/* active duration time */
__xdata unsigned int dotime[NODE_COUNT];
/* pick time for each node */
__xdata unsigned int dopick[NODE_COUNT];
/* remaining active in seconds */
__xdata unsigned int active[NODE_COUNT];
/*----------------------------------------------------------------------------*/
unsigned int curr, dcnt;
unsigned char test, flag;
my1esp01_data_t *pack;
/*----------------------------------------------------------------------------*/
#define FLAG_NONE 0
#define FLAG_DOSHOW 0x01
#define FLAG_DODIFF 0x02
#define FLAG_DISPLAY (FLAG_DOSHOW|FLAG_DODIFF)
/*----------------------------------------------------------------------------*/
#define THIS_NODE_ID 0x0001
/*----------------------------------------------------------------------------*/
void uart_show_time(void) {
	uart_puts("Time: ");
	uart_send(dec2char10(tkhour));
	uart_send(dec2char01(tkhour));
	uart_send(':');
	uart_send(dec2char10(tkmins));
	uart_send(dec2char01(tkmins));
	uart_send(':');
	uart_send(dec2char10(tksecs));
	uart_send(dec2char01(tksecs));
}
/*----------------------------------------------------------------------------*/
void process_request(void) {
	pack = (my1esp01_data_t*)espd.buff;
	/* check request */
	if (!(pack->type&TCPD_TYPE_TX)) pack->type = 0;
	/* process request */
	if (!pack->type||!pack->uuid) /* unknown packet?! no data to reply! */
		pack->type = TCPD_TYPE_QQ;
/*
	else if (pack->type==TCPD_TYPE_GET_STAT) {
		pack->type = TCPD_TYPE_OK | TCPD_TYPE_RX;
	}
	else if (type==TCPD_TYPE_GET_CTRL) {
		type = TCPD_TYPE_OK | TCPD_TYPE_RX;
	}
	else if (type==TCPD_TYPE_SET_CTRL) {
		type = TCPD_TYPE_OK | TCPD_TYPE_RX;
	}
	else if (type==TCPD_TYPE_GET_TIME_C) {
		type = TCPD_TYPE_OK | TCPD_TYPE_RX;
	}
	else if (type==TCPD_TYPE_SET_TIME_C) {
		type = TCPD_TYPE_OK | TCPD_TYPE_RX;
		if (that<2) type |= TCPD_TYPE_QQ;
	}
*/
	else {
		/* unknown request?! no data to reply! */
		pack->type = TCPD_TYPE_QQ | TCPD_TYPE_RX;
	}
	/* fill header & calc crc */
	espd.loop = 0;
	repl[espd.loop++] = TCPD_DATA_MINSIZE;
	repl[espd.loop++] = 0;
	repl[espd.loop++] = (NODE_UUID&0xff);
	repl[espd.loop++] = ((NODE_UUID>>8)&0xff);
	repl[espd.loop++] = (pack->type&0xff);
	repl[espd.loop++] = ((pack->type>>8)&0xff);
	crc16_calc(repl,espd.loop);
	repl[espd.loop++] = ccrc&0xff;
	repl[espd.loop++] = (ccrc>>8)&0xff;
	espd.fill = espd.loop;
}
/*----------------------------------------------------------------------------*/
void main(void) {
	NODE0 = NODE_IDLE;
	NODE1 = NODE_IDLE;
	SERV1 = 0;
	ERROR = 0;
	dopick[0] = NODE_ACTIVEINIT_DEF;
	dopick[1] = NODE_ACTIVEINIT_DEF;
	dotime[0] = NODE_ACTIVETIME_DEF_S;
	dotime[1] = NODE_ACTIVETIME_DEF_S;
	active[0] = 0;
	active[1] = 0;
	flag = FLAG_NONE;
	uart_init();
	uart_puts("\n\Testing ESP01 AccessPoint Mode\n\n");
	timer_init();
	timer_delay1s(test,3);
	esp01_init();
	esp01_buff(buff,BUFFSIZE);
	uart_puts("-- Looking for ESP01... ");
	esp01_find_wait();
	uart_puts("found.\n");
	uart_puts("-- Disabling echo... ");
	esp01_echo_off();
	esp01_must_be_ok();
	uart_puts("-- Setting AccessPoint mode... ");
	esp01_conf_apmode();
	esp01_must_be_ok();
	uart_puts("## SSID:");
	esp01_ap_ssid(ssid);
	uart_puts(ssid);
	uart_send('\n');
	uart_puts("-- Configure multi-connection (TCP) ... ");
	esp01_ap_multiconn();
	esp01_must_be_ok();
	uart_puts("-- Start server at port "ESP01AP_PORT"... ");
	esp01_ap_serve();
	esp01_must_be_ok();
	/* start serving */
	uart_puts("-- Server ready...\n");
	SERV1 = 1;
	esp01_ap_scan_prep();
	/* start time keeping */
	timer_keep_prep(7,59,45); /* start at 0759 */
	timer_keep_exec();
	while (1) {
		esp01_ap_scan_req();
		if (esp01_ap_conn_ok()) {
			SERV1 = 0;
			esp01_ap_read_req();
			if (esp01_ap_data_ok()) {
				uart_puts("@@ IREQ:");
				uart_send(espd.test); // link ch
				uart_puts(" >>");
				for (espd.loop=0;espd.loop<espd.fill;espd.loop++) {
					uart_puts(" 0x");
					uart_send_hexbyte(espd.buff[espd.loop]);
				}
				uart_puts("\n");
				process_request();
				uart_puts("@@ REPL:");
				uart_send(espd.test);
				uart_puts(" >>");
				for (espd.loop=0;espd.loop<espd.fill;espd.loop++) {
					uart_puts(" 0x");
					uart_send_hexbyte(repl[espd.loop]);
				}
				uart_puts("\n");
				/* reply */
				esp01_data_send(repl,espd.loop);
				if (esp01_data_posted())
					uart_puts("## Data served!\n");
			}
			esp01_data_done();
			esp01_ap_scan_prep();
			SERV1 = 1;
		}
		if (uart_peek()) {
			test = uart_read();
			if (test==' ') {
				if (flag&FLAG_DOSHOW) flag &= ~FLAG_DOSHOW;
				else flag |= FLAG_DOSHOW;
			}
		}
		if (tktick) {
			if (flag&FLAG_DISPLAY) {
				flag &= ~FLAG_DODIFF;
				uart_show_time();
				uart_puts(" NODE0:");
				if (NODE0) uart_puts("ACTIVE");
				else uart_puts("IDLE");
				uart_puts(" NODE1:");
				if (NODE1) uart_puts("ACTIVE");
				else uart_puts("IDLE");
				uart_send('\n');
			}
			/* node control */
			if (active[0]) {
				if (NODE0!=NODE_ACTIVE) {
					flag |= FLAG_DODIFF;
					NODE0 = NODE_ACTIVE;
				}
			}
			else {
				if (NODE0==NODE_ACTIVE) {
					flag |= FLAG_DODIFF;
					NODE0 = NODE_IDLE;
				}
			}
			if (active[1]) {
				if (NODE1!=NODE_ACTIVE) {
					flag |= FLAG_DODIFF;
					NODE1 = NODE_ACTIVE;
				}
			}
			else {
				if (NODE1==NODE_ACTIVE) {
					flag |= FLAG_DODIFF;
					NODE1 = NODE_IDLE;
				}
			}
			/* time update */
			curr = timer_keep_read();
			if (active[0]||active[1]) {
				/* in case multiple ticks occurred */
				while (tktick>0) {
					tktick--;
					if (active[0]) active[0]--;
					if (active[1]) active[1]--;
				}
			}
			else {
				if (!active[0]) {
					if (curr==dopick[0])
						active[0] = dotime[0];
				}
				if (!active[1]) {
					if (curr==dopick[1])
						active[1] = dotime[1];
				}
			}
			tktick = 0;
		}
	}
}
/*----------------------------------------------------------------------------*/
