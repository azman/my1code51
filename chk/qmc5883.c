/*----------------------------------------------------------------------------*/
#include "qmc5883.h"
#include "timer.h"
#include "uart.h"
#include "utils_lite.h"
/*----------------------------------------------------------------------------*/
typedef struct _magcal_t {
	magdata_t calc;
	int xmin, ymin, zmin;
	int xmax, ymax, zmax;
	int xoff, yoff, zoff;
	int xdel, ydel, zdel, tdel;
	float xmul, ymul, zmul, tval;
} magcal_t;
/*----------------------------------------------------------------------------*/
void magcal_init(magcal_t* mcal) {
	mcal->xmin = 0; mcal->xmax = 0;
	mcal->ymin = 0; mcal->ymax = 0;
	mcal->zmin = 0; mcal->zmax = 0;
// xoff:-270|yoff:-661|zoff:-70 (20230707) looks ok!
// xoff:-301|yoff:-602|zoff:187 (20230707) looks ok!
}
/*----------------------------------------------------------------------------*/
void magcal_offset(magcal_t* mcal, magdata_t* praw) {
	mcal->calc.xaxis = (praw->xaxis - mcal->xoff);
	mcal->calc.yaxis = (praw->yaxis - mcal->yoff);
	mcal->calc.zaxis = (praw->zaxis - mcal->zoff);
}
/*----------------------------------------------------------------------------*/
void magcal_scales(magcal_t* mcal) {
	mcal->tval = (float)mcal->calc.xaxis * mcal->xmul;
	mcal->calc.xaxis = (int) mcal->tval;
	mcal->tval = (float)mcal->calc.yaxis * mcal->ymul;
	mcal->calc.yaxis = (int) mcal->tval;
	mcal->tval = (float)mcal->calc.zaxis * mcal->zmul;
	mcal->calc.zaxis = (int) mcal->tval;
}
/*----------------------------------------------------------------------------*/
unsigned char magcal_calibrate(magcal_t* mcal, magdata_t* praw) {
	unsigned char test;
	test = 0;
	/* check min-max */
	if (mcal->xmin>praw->xaxis) { mcal->xmin = praw->xaxis; test++; }
	if (mcal->xmax<praw->xaxis) { mcal->xmax = praw->xaxis; test++; }
	if (mcal->ymin>praw->yaxis) { mcal->ymin = praw->yaxis; test++; }
	if (mcal->ymax<praw->yaxis) { mcal->ymax = praw->yaxis; test++; }
	if (mcal->zmin>praw->zaxis) { mcal->zmin = praw->zaxis; test++; }
	if (mcal->zmax<praw->zaxis) { mcal->zmax = praw->zaxis; test++; }
	/* get offset */
	mcal->xoff = (mcal->xmax + mcal->xmin) / 2;
	mcal->yoff = (mcal->ymax + mcal->ymin) / 2;
	mcal->zoff = (mcal->zmax + mcal->zmin) / 2;
	/* get deltas */
	mcal->xdel = (mcal->xmax - mcal->xmin) / 2;
	mcal->ydel = (mcal->ymax - mcal->ymin) / 2;
	mcal->zdel = (mcal->zmax - mcal->zmin) / 2;
	mcal->tdel = (mcal->xdel + mcal->ydel + mcal->zdel) / 3;
	/* get scaling */
	mcal->xmul = (float) mcal->tdel / (float) mcal->xdel;
	mcal->ymul = (float) mcal->tdel / (float) mcal->ydel;
	mcal->zmul = (float) mcal->tdel / (float) mcal->zdel;
	return test;
}
/*----------------------------------------------------------------------------*/
__xdata magdata_t draw;
__xdata magcal_t dcal;
__xdata char buff[16];
__xdata unsigned long tcnt;
__xdata unsigned char test;
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	i2c_init();
	timer0_init();
	timer0_delay1s(test,2);
	uart_puts("\nLooking for QMC5883L compass... ");
	do {
		if (qmc5883_find()) break;
		uart_send('.');
		timer0_delay1s(test,2);
	} while (1);
	uart_puts(" done.\n\n");
	/** using default configuration */
	uart_puts("-- Initialize QMC5883L\n");
	qmc5883_init();
	uart_puts("-- Set to continuous read\n");
	qmc5883_conf();
	uart_puts("-- Move around the module (looking for min-max)... ");
	magcal_init(&dcal);
	tcnt = 0;
	do {
		qmc5883_data(&draw); test = 0;
		test = magcal_calibrate(&dcal,&draw);
		if (test) tcnt = 0;
		else tcnt++;
	} while (tcnt<1000);
	uart_puts(" done.\n");
	uart_puts("-- Calibration:[xoff:");
	int16_2cstr(buff,dcal.xoff);
	uart_puts(buff);
	uart_puts("|yoff:");
	int16_2cstr(buff,dcal.yoff);
	uart_puts(buff);
	uart_puts("|zoff:");
	int16_2cstr(buff,dcal.zoff);
	uart_puts(buff);
	uart_puts("].\n");
	uart_puts("\n@@ Hit <ENTER> to continue...\n");
	while (uart_read()!='\n');
	while(1) {
		test = qmc5883_data(&draw);
		uart_puts("-- Data:{");
		qmc5883_head(&draw);
		int16_2cstr(buff,draw.xaxis);
		uart_puts(buff);
		uart_puts(",");
		int16_2cstr(buff,draw.yaxis);
		uart_puts(buff);
		uart_puts(",");
		int16_2cstr(buff,draw.zaxis);
		uart_puts(buff);
		uart_puts("|Head:");
		int16_2cstr(buff,draw.xhead);
		uart_puts(buff);
		uart_puts("} ");
		magcal_offset(&dcal,&draw);
		qmc5883_head(&dcal.calc);
		uart_puts("{Offset:");
		int16_2cstr(buff,dcal.calc.xhead);
		uart_puts(buff);
		uart_puts("}");
		magcal_scales(&dcal);
		qmc5883_head(&dcal.calc);
		uart_puts("{Scales:");
		int16_2cstr(buff,dcal.calc.xhead);
		uart_puts(buff);
		uart_puts("}\n");
	}
}
/*----------------------------------------------------------------------------*/
