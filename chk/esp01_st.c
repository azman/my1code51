/*----------------------------------------------------------------------------*/
#include "esp01_st.h"
#include "esp01_uart.h"
/*----------------------------------------------------------------------------*/
#define HOSTADDR "192.168.100.100"
#define HOSTPORT "1337"
#define HOSTPATH "/test/esp01/st/"
/*----------------------------------------------------------------------------*/
/**
 * testing esp01 station mode
 * - compact version of code in esp01.c
 * - use my1apisrvc to test (output when request received)
*/
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
__xdata char buff[BUFFSIZE];
__xdata char ipaddr[16];
__xdata char macadd[18];
__xdata my1cstr_t cstr;
__xdata unsigned char step;
unsigned char test;
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n\Testing ESP01 Station Mode\n\n");
	timer_init();
	timer_delay1s(test,3);
	esp01_init();
	esp01_buff(buff,BUFFSIZE);
	uart_puts("-- Looking for ESP01... ");
	esp01_find_wait();
	uart_puts("found.\n");
	uart_puts("-- Disabling echo... ");
	esp01_echo_off();
	esp01_must_be_ok();
	uart_puts("-- Setting station mode... ");
	esp01_conf_stmode();
	esp01_must_be_ok();
	uart_puts("-- Connecting to "WIFI_SSID"... ");
	esp01_st_connect();
	uart_puts("OK.\n");
	esp01_ipaddr(ipaddr);
	uart_puts("## IP:");
	uart_puts(ipaddr);
	uart_send('\n');
	esp01_mcaddr(macadd);
	uart_puts("## MAC:");
	uart_puts(macadd);
	uart_send('\n');
	step = 0;
	while (1) {
		/* open a socket */
		uart_puts("-- Opening socket ("HOSTADDR":"HOSTPORT")... ");
		esp01_st_socket1(HOSTADDR,HOSTPORT);
		if (!esp01_task_ok()) {
			uart_puts("timeout/failed.\n");
			uart_puts("## Retry in 10 seconds.\n");
			timer_delay1s(test,10);
			continue;
		}
		uart_puts("OK.\n");
		//uart_puts("@@ DEBUG1:");
		//esp01_show_buff(espd.buff,espd.fill);
		/* prepare buffer */
		cstr_assign(&cstr,HOSTPATH);
		uint8_2cstr(buff,step);
		cstr_append(&cstr,buff);
		/* prepare transfer */
		uart_puts("-- HTTP POST: (");
		uart_puts(cstr.buff);
		uart_puts(")... ");
		esp01_st_posthttp(cstr.buff);
		if (esp01_st_posted()) {
			step++;
			uart_puts("OK.\n");
		}
		else {
			uart_puts("???\n");
			esp01_show_buff(espd.buff,espd.fill);
		}
		if (!esp01_st_closed()) {
			/* close socket */
			uart_puts("-- Closing socket... ");
			esp01_st_socket0();
			if (esp01_task_ok()&&esp01_buff_ok())
				uart_puts("OK.\n");
			else {
				uart_puts("???\n");
				esp01_show_buff(espd.buff,espd.fill);
			}
		}
		timer_delay1s(test,1);
	}
}
/*----------------------------------------------------------------------------*/
