/*----------------------------------------------------------------------------*/
#include "sdcard.h"
/*----------------------------------------------------------------------------*/
#include "uart_hexascii.h"
#include "uart_gets.h"
#include "utils.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define SPI_DELAY 0
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 32
/*----------------------------------------------------------------------------*/
__xdata char buff[BUFFSIZE];
__xdata char conv[32];
__xdata unsigned long curr, pick;
unsigned char sav0, sav1, test, init;
/*----------------------------------------------------------------------------*/
void uart_show_integer(long data) {
	int2str(conv,data);
	uart_puts(conv);
}
/*----------------------------------------------------------------------------*/
void show_sector(unsigned long what) {
	int loop;
	for (loop=0;loop<SDCARD_SECTOR_SIZE;loop++) {
		if (!(loop&0xf)) {
			uart_send('\n');
			uart_send_hexlong((what<<9)+loop);
			uart_send(':');
		}
		uart_send(' ');
		uart_send_hexbyte(card.buff[loop]);
	}
	uart_send('\n');
}
/*----------------------------------------------------------------------------*/
void show_response(void) {
	uart_puts("done. (0x");
	uart_send_hexbyte(card.resp);
	uart_puts(":0x");
	uart_send_hexbyte(card.buff[0]);
	uart_puts(":0x");
	uart_send_hexbyte(card.flag);
	uart_puts(")\n");
}
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	uart_init();
	uart_puts("\n-- Testing SDCard Access\n\n");
	uart_puts("Press <ENTER> to start SD interface code.");
	while (uart_read()!='\n');
	/** sdcard_prep:INIT */
	uart_puts("\n\n-- Initialize SD for SPI mode... ");
	sdcard_init();
	uart_puts("done.\n");
	/* software reset */
	uart_puts("-- Software reset... ");
	test = 100;
	do {
		sdcard_idle();
		if (card.resp==SDCARD_RESP_R1_IDLE) {
			show_response();
			break;
		}
		uart_send('.');
		timer_wait(TIMER_VAL50MS);
	} while (--test);
	if (!test) {
		uart_puts("**error**\n");
		hang();
	}
	uart_puts("-- Send IF_COND... ");
	sdcard_cmd8();
	show_response();
	card.type = 0;
	if (card.resp==SDCARD_RESP_ILLEGAL) {
		uart_puts("** Card Version <2 not supported!\n");
		card.type = 1;
	}
	else if (card.resp==SDCARD_RESP_R1_IDLE) {
		if (card.buff[0]||card.buff[1]) {
			uart_puts("** Invalid R7 response!\n");
			hang();
		}
		if (card.buff[2]!=0x01||card.buff[3]!=0xAA) {
			uart_puts("** Invalid voltage/pattern! [0x");
			uart_send_hexbyte(card.buff[2]);
			uart_puts("][0x");
			uart_send_hexbyte(card.buff[3]);
			uart_puts("]\n");
			hang();
		}
		card.type = 2;
	}
	if (card.type==2) {
		uart_puts("-- Setup HCS... ");
		sdcard_acmd41();
		show_response();
		if (card.resp!=SDCARD_RESP_SUCCESS) {
			uart_puts("** Setup ERROR!\n");
			hang();
		}
		uart_puts("-- Checking SDHC... ");
		sdcard_cmd58();
		show_response();
		if ((card.buff[0]&0xc0)==0xc0)
			card.type = 3;
	}
	uart_puts("@@ Card type = ");
	uart_show_integer(card.type);
	uart_send('\n');
	if (card.type<2) {
		uart_puts("** Unsupported version!\n");
		hang();
	}
	/* disable crc */
	uart_puts("-- Disable CRC... ");
	sdcard_disable_crc();
	show_response();
	/* set blocksize */
	uart_puts("-- Set blocksize... ");
	sdcard_block_size();
	show_response();
	/** sdcard_prep:ENDS */
	uart_puts("\n@@ Press <ENTER> to read first sector.\n");
	while (uart_read()!='\n');
	curr = 0;
	uart_puts("\n-- Reading 1st sector (sector 0) on SD... ");
	sdcard_read_sector(curr);
	show_response();
	show_sector(curr);
	init = 1;
	while (1) {
		if (init) {
			uart_puts("\n@@ Press 0-9 to read sectors into buffer.");
			uart_puts("\n@@ Press 'r' to specify sector to read");
			uart_puts("\n@@ Press 'w' to modify and write back to card.\n");
			init = 0;
		}
		if (uart_peek()) {
			sav0 = uart_read();
			if (sav0=='w') {
				sav0 = (unsigned char) curr;
				uart_puts("\n-- Buff for sector ");
				uart_send((sav0+0x30));
				uart_puts("\n## Modify byte 4... ");
				sav0 = card.buff[4];
				uart_send_hexbyte(sav0);
				uart_puts(" > ");
				card.buff[4] = 0x23;
				uart_send_hexbyte(card.buff[4]);
				uart_puts("\n## Modify byte 5... ");
				sav1 = card.buff[5];
				uart_send_hexbyte(sav1);
				uart_puts(" > ");
				card.buff[5] = 0x32;
				uart_send_hexbyte(card.buff[5]);
				uart_puts("\n\n@@ Press <ENTER> to update sector data.\n");
				while (uart_read()!='\n');
				uart_puts("\n-- Writing back to SD... ");
				sdcard_write_sector(curr);
				show_response();
				timer_delay1s(test,3);
				card.buff[4] = 0x0;
				card.buff[5] = 0x0;
				uart_puts("-- Reading back from SD... ");
				sdcard_read_sector(curr);
				show_response();
				show_sector(curr);
				uart_puts("\n@@ Press <ENTER> to restore sector data.\n");
				while (uart_read()!='\n');
				card.buff[4] = sav0;
				card.buff[5] = sav1;
				uart_puts("\n-- Restoring original data... ");
				sdcard_write_sector(curr);
				show_response();
				timer_delay1s(test,3);
				card.buff[4] = 0x0;
				card.buff[5] = 0x0;
				uart_puts("-- Checking sector buffer... ");
				sdcard_read_sector(curr);
				show_response();
				show_sector(curr);
				init = 1;
			}
			else if (sav0=='r') {
				uart_puts("\n-- Sector Number: ");
				uart_gets(buff,BUFFSIZE);
				pick = str2uint(buff);
				uart_puts("\n-- Reading sector ");
				uart_show_integer(pick);
				uart_puts(" @ 0x");
				uart_send_hexlong(pick);
				uart_puts(" on SD... ");
				sdcard_read_sector(pick);
				show_response();
				show_sector(pick);
				init = 1;
			}
			else if (sav0>=0x30&&sav0<=0x39) {
				uart_puts("\n-- Reading sector ");
				uart_send(sav0);
				uart_puts(" on SD... ");
				sav0 -= 0x30;
				sdcard_read_sector(sav0);
				show_response();
				show_sector(sav0);
				curr = sav0;
				init = 1;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
