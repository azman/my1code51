/*----------------------------------------------------------------------------*/
#include "atcmd_read.h"
#include "atcmd_uart.h"
/*----------------------------------------------------------------------------*/
#include "timer.h"
#include "uart_gets.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
/*----------------------------------------------------------------------------*/
__xdata char buff[BUFFSIZE];
char* pchk;
unsigned char test, loop;
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	uart_init();
	uart_puts("\n\nTesting ATCMD\n\n");
	atcmd_init();
	/* change baudrate if P1.0 is low */
	if (!P1_0) brt_load(BRT_BAUD115200);
	while (1) {
		uart_puts("-- Sending AT... ");
		atcmd_find();
		if (atcmd_found()) {
			uart_puts("OK.\n");
			break;
		}
		uart_puts("timeout.\n");
		timer_delay1s(test,1);
	}
	uart_puts("-- Extra AT...");
	test = atcmd_wait_send(buff,BUFFSIZE,ATCMD_CMD_BASE);
	if (!test) uart_puts(" huh?!\n");
	else {
		uart_puts("\n@@ Response[INIT]:");
		uart_puts(buff);
		uart_puts("@@ Response[DONE]\n");
	}
	uart_puts("-- Disabling echo... ");
	atcmd_echo_off();
	if (atcmd_recv_ok()) uart_puts("OK.\n");
	else uart_puts("???\n");
	while (1) {
		uart_purge();
		uart_puts("\nEnter an AT command (full): ");
		test = uart_gets(buff,BUFFSIZE);
		uart_puts("\n-- Sending ");
		uart_puts(buff);
		uart_puts("... ");
		atcmd_puts(buff);
		atcmd_puts("\r\n");
		test = atcmd_read_wait(buff,BUFFSIZE);
		uart_puts("\n@@ Response: ");
		if (!test) uart_puts("timeout?\n");
		else {
			/* in case buffer overflows! */
			if (test==BUFFSIZE) {
				uart_puts("<overflow?>");
				pchk = buff;
			}
			else {
				pchk = atcmd_read_ok(buff,test);
				if (pchk) uart_puts("<OK>");
				else pchk = buff;
			}
			atcmd_show_buff(pchk,test);
		}
	}
}
/*----------------------------------------------------------------------------*/
