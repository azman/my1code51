/*----------------------------------------------------------------------------*/
#include "tm1637.h"
#include "timer.h"
#include "uart_hexascii.h"
/*----------------------------------------------------------------------------*/
void timer_wait_1s(void) {
	unsigned char loop = 20;
	while (loop>0) {
		timer_wait_50ms();
		loop--;
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char bdat[4], pick;
	unsigned long* pchk;
	int step;
	timer_init();
	uart_init();
	tm1637_init();
	if (!P1_0) {
		pchk = (unsigned long*)bdat;
		*pchk = 0x00000001;
		while (1) {
			uart_puts("Data");
			pick = 0;
			if (bdat[0]) { uart_puts("0:"); pick = bdat[0]; }
			if (bdat[1]) { uart_puts("1:"); pick = bdat[1]; }
			if (bdat[2]) { uart_puts("2:"); pick = bdat[2]; }
			if (bdat[3]) { uart_puts("3:"); pick = bdat[3]; }
			uart_send_hexbyte(pick);
			uart_send('\n');
			tm1637_write_data(bdat);
			timer_wait_1s();
			*pchk <<= 1;
			if (!(bdat[0]|bdat[1]|bdat[2]|bdat[3]))
				*pchk = 0x00000001;
		}
	}
	else {
		step = 0;
		while (1) {
			tm1637_display_hex(step,1);
			timer_wait_1s();
			step++;
		}
	}
}
/*----------------------------------------------------------------------------*/
