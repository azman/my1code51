/*----------------------------------------------------------------------------*/
#include "timer_ms.h"
#include "uart_hexascii.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned long curr, prev;
	uart_init();
	uart_puts("\nTimerMS Test\n\n");
	timer_ms_init();
	prev = timer_ms()/1000;
	while (1) {
		curr = timer_ms()/1000;
		if (curr!=prev) {
			uart_puts("\n> 0x");
			uart_send_hexlong(curr);
			uart_puts("\n");
			prev = curr;
		}
	}
}
/*----------------------------------------------------------------------------*/
