/*----------------------------------------------------------------------------*/
#include "esp01_ap.h"
#include "esp01_uart.h"
#include "timer.h"
#include "string.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
__xdata char buff[BUFFSIZE];
__xdata char ssid[SSID_BUFFLEN];
unsigned char test;
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n\Configure ESP01 AccessPoint SSID\n\n");
	timer_init();
	timer_delay1s(test,3);
	esp01_init();
	esp01_buff(buff,BUFFSIZE);
	uart_puts("-- Looking for ESP01... ");
	esp01_find_wait();
	uart_puts("found.\n");
	uart_puts("-- Disabling echo... ");
	esp01_echo_off();
	esp01_must_be_ok();
	uart_puts("-- Setting AccessPoint mode... ");
	esp01_conf_apmode();
	esp01_must_be_ok();
	uart_puts("## SSID:");
	esp01_ap_ssid(ssid);
	uart_puts(ssid);
	uart_send('\n');
	if (strncmp(ssid,ESP01_SSID,espd.loop)) {
		uart_puts("-- Setting SSID to '"ESP01_SSID"'... ");
		esp01_ap_ssid_set();
		if (!esp01_ap_setssid_error()) uart_puts("OK.\n");
		else uart_puts("*error*\n");
		uart_puts("## SSID:");
		esp01_ap_ssid(ssid);
		uart_puts(ssid);
		uart_send('\n');
	}
	else uart_puts("@@ Nothing to do!\n");
	hang();
}
/*----------------------------------------------------------------------------*/
