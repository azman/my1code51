/*----------------------------------------------------------------------------*/
#include "ds18b20.h"
#include "uart_hexascii.h"
#include "utils_lite.h"
/*----------------------------------------------------------------------------*/
unsigned char test, step;
char temp[4];
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	uart_init();
	uart_puts("-- I am ALIVE!\n");
	test = 0;
	do {
		uart_puts("-- Looking for DS18B20... ");
		if (ds18b20_reset()) {
			uart_puts("OK. (");
			uart_send_hexbyte(ds_loop);
			uart_puts(")\n");
			test++;
		}
		else {
			uart_puts("??? (");
			uart_send_hexbyte(ds_loop);
			uart_puts(")\n");
			timer_delay1s(test,3);
		}
	} while (!test);
	step = 0;
	while (1) {
		if (ds18b20_read_temp()) {
			uart_puts("[");
			uart_send_hexbyte(step++);
			uart_puts("] Temp:0x");
			uart_send_hexuint(ds_temp);
			uart_puts("@");
			test = (unsigned char)((ds_temp>>4)&0xff);
			uint8_2cstr(temp,test);
			uart_puts(temp);
			uart_puts("C\n");
			timer_delay1s(test,1);
		}
	}
}
/*----------------------------------------------------------------------------*/
