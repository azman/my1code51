/*----------------------------------------------------------------------------*/
#define SERVO_PIN P1_0
#include "servo.h"
#include "uart.h"
/*----------------------------------------------------------------------------*/
/**
 * Testing TowerPro MG996R servo
 * - note: servo pin (1|orange:PWM)(2|red:vcc)(3|brown:ground)
 * - pwm range is 1.0 - 2.0 ms?
 * Testing TowerPro SG90 servo
 * - pwm range is 0.5 - 2.5 ms?
**/
/*----------------------------------------------------------------------------*/
#define CHECK P1_2
#define GO000 P1_6
#define GO180 P1_7
/*----------------------------------------------------------------------------*/
unsigned char curr, turn, temp, loop, buff[3];
/*----------------------------------------------------------------------------*/
void main(void) {
	CHECK = 0; GO000 = 1; GO180 = 1;
	servo_init();
	uart_init();
	uart_puts("\n\nTesting Servo PWM\n\n");
	timer_init();
	CHECK = 1;
	while (1) {
		if (uart_peek()) {
			curr = uart_read();
			if (curr=='#') {
				temp = uart_read();
				if (temp<0x30||temp>0x39) continue;
				turn = temp - 0x30;
				loop = 0; buff[loop++] = temp;
				do {
					temp = uart_read();
					if (temp=='\r'||temp=='\n') {
						temp = 0;
						buff[loop++] = temp;
					}
					else {
						if (turn>=10||(temp<0x30||temp>0x39)) {
							temp = 0; turn = 0;
						}
						else {
							turn *= 10;
							turn += temp-0x30;
							buff[loop++] = temp;
						}
					}
				} while (temp);
				if (turn>=5&&turn<=25) {
					uart_puts("## Turning to (");
					uart_puts(buff);
					uart_puts(")\r\n");
					servo_turn(turn);
				}
				else {
					uart_puts("** Invalid angle (");
					uart_puts(buff);
					uart_puts(")\r\n");
				}
			}
			else if (curr=='1') {
				uart_puts("## Turning to Position 1 (5)\r\n");
				servo_turn(5);
			}
			else if (curr=='2') {
				uart_puts("## Turning to Position 2 (10)\r\n");
				servo_turn(10);
			}
			else if (curr=='3') {
				uart_puts("## Turning to Position 3 (15)\r\n");
				servo_turn(15);
			}
			else if (curr=='4') {
				uart_puts("## Turning to Position 4 (20)\r\n");
				servo_turn(20);
			}
			else if (curr=='5') {
				uart_puts("## Turning to Position 5 (25)\r\n");
				servo_turn(25);
			}
		}
		if (GO000==0) {
			servo_turn(10); // 1ms pwm
			while (!GO000);
		}
		if (GO180==0) {
			servo_turn(20); // 2ms pwm
			while (!GO180);
		}
	}
}
/*----------------------------------------------------------------------------*/
