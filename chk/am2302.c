/*----------------------------------------------------------------------------*/
#include "am2302.h"
#include "timer.h"
#include "uart.h"
#include "utils_lite.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char test, buff[16];
	unsigned int temp, humi, xtra;
	timer_init();
	uart_init();
	am2302_init();
	timer_delay1s(test,2); /* wait things to settle down */
	uart_puts("\n-- AM2302 Temp/Humi Sensor Interface\n\n");
	while (1) {
		test = am2302_read(&temp,&humi);
		if (!test) {
			uart_puts("-- Temp:");
			xtra = temp % 10;
			temp = temp / 10;
			int16_2cstr(buff,temp);
			uart_puts(buff);
			uart_send('.');
			int16_2cstr(buff,xtra);
			uart_puts(buff);
			uart_puts("°");
			uart_puts("C , Humi:");
			xtra = humi % 10;
			humi = humi / 10;
			int16_2cstr(buff,humi);
			uart_puts(buff);
			uart_send('.');
			int16_2cstr(buff,xtra);
			uart_puts(buff);
			uart_puts("%\n");
		}
		timer_delay1s(test,2); /* 1 read every 2 seconds */
	}
}
/*----------------------------------------------------------------------------*/
