/*----------------------------------------------------------------------------*/
#include "uart_hexascii.h"
#include "frc522.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char temp, stat, loop, size;
	unsigned char pdat[FRC522_MAX_RXSIZE], reqa[2];
	/** initialize */
	uart_init();
	spi_init();
	atqa = reqa;
	/* initialize mf contactless card reader */
	/** say something... */
	uart_puts("\n----------------------\n");
	uart_puts("MF RFID Tag Reader Test!\n");
	uart_puts("----------------------\n\n");
	temp = frc522_init();
	if (!temp||temp==0xff) {
		uart_puts("** Cannot find FRC522 hardware! Aborting!\n");
		hang();
	}
	uart_puts("FRC522 found. Firmware version is 0x");
	uart_send_hexbyte(temp);
	uart_puts(".\n");
	/** main loop */
	while (1) {
		stat = frc522_scan(pdat,&size);
		if (stat==FRC522_OK) {
			uart_puts("## TAG(");
			uart_send_hexbyte(stat);
			uart_puts("|");
			uart_send_hexbyte(reqa[0]);
			uart_puts(",");
			uart_send_hexbyte(reqa[1]);
			uart_puts("):");
			for (loop=0;loop<size;loop++) { /** UID is size-1 bytes */
				uart_send('[');
				uart_send_hexbyte(pdat[loop]);
				uart_send(']');
			}
			uart_send('\n');
			loop_delay(3000);
		}
		else if (stat!=FRC522_ERROR_NO_TAG&&stat!=FRC522_ERROR_REQ_A) {
			uart_puts("** Scan Failed (0x");
			uart_send_hexbyte(stat);
			uart_puts("):");
			for (loop=0;loop<FRC522_MAX_RXSIZE;loop++) {
				uart_send('[');
				uart_send_hexbyte(pdat[loop]);
				uart_send(']');
			}
			uart_send('\n');
		}
	}
}
/*----------------------------------------------------------------------------*/
