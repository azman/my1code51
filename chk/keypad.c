/*----------------------------------------------------------------------------*/
#include "keypad.h"
#include "uart.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char test;
	uart_init();
	uart_puts("MY1CODE51 KEYPAD\r\n");
	while (1) {
		test = key_wait();
		if (test<10)
			uart_send(test+0x30);
		else {
			if(test==0x0F) /** '#' key! */
				uart_puts("#\r\n");
			else if(test==0x0E) /** '*' key! */
				uart_send('*');
			else /** 'A','B','C','D' key! */
				uart_send(test+0x41-10);
		}
	}
}
/*----------------------------------------------------------------------------*/
