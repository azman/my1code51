/*----------------------------------------------------------------------------*/
#include "uart.h"
#include "timer.h"
#define FPSIZE 2
#include "utils_float.h"
/*----------------------------------------------------------------------------*/
my1sbit(TRIG,PIN10);
my1sbit(ECHO,PIN11);
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char wait;
	unsigned int tval,loop;
	float fval, dist;
	char buff[16];
	uart_init();
	uart_puts("TESTING HC-SR04 MODULE\r\n");
	timer_init();
	while(1) {
		timer_prep(0);
		TRIG = 1;
		for (wait=10;wait;wait--); // around 10us...
		TRIG = 0;
		while (!ECHO); timer_exec();
		while (ECHO); timer_stop();
		tval = ((unsigned int)TH0<<8)|TL0;
		fval = (float)tval * 1.085; // in us
		dist = fval / 58.0; // in cm
		uart_puts("-- Distance=");
		float2str(buff,dist);
		uart_puts(buff);
		uart_puts(" cm\r\n");
		for (loop=65000;loop;loop--);
	}
}
/*----------------------------------------------------------------------------*/
