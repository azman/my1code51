/*----------------------------------------------------------------------------*/
#define BRTCNT_USE BRTCNT_B115200
/*----------------------------------------------------------------------------*/
#include "sim808gnss.h"
#include "sim808_uart.h"
/*----------------------------------------------------------------------------*/
#include "uart_hexascii.h"
#include "timer.h"
#include "string.h"
#define MY1CSTR_SIZE 160
#include "cstr.h"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 160
__xdata char buff[BUFFSIZE];
__xdata cstr_t json;
unsigned char test, step;
/*----------------------------------------------------------------------------*/
void sim808gnss_show_location(void) {
	if (sim808gnss_fixed()) {
		gpsstr.pstr = gpsstr.pchk; gpsstr.loop = 0;
		while (gpsstr.pstr[0]) {
			if (gpsstr.pstr[0]==',') {
				gpsstr.pstr[0] = 0x0;
				gpsstr.loop++;
			}
			if (gpsstr.loop==2||gpsstr.loop==5||gpsstr.loop==7) {
				/* stop at dot for utc,alt,dir */
				if (gpsstr.pstr[0]=='.')
					gpsstr.pstr[0] = 0x0;
			}
			gpsstr.pstr++;
		}
		cstr_assign(&json,"{\"utc\":");
		cstr_append(&json,gpsstr.putc); /* note: utc as string? */
		cstr_append(&json,";\"lng\":");
		cstr_append(&json,gpsstr.plng);
		cstr_append(&json,";\"lat\":");
		cstr_append(&json,gpsstr.plat);
		cstr_append(&json,";\"vel\":");
		cstr_append(&json,gpsstr.pvel);
		cstr_append(&json,";\"dir\":");
		cstr_append(&json,gpsstr.pdir);
		cstr_append(&json,";\"alt\":");
		cstr_append(&json,gpsstr.palt);
		cstr_append(&json,";\"hdop\":");
		cstr_append(&json,gpsstr.hdop);
		cstr_append(&json,";\"pdop\":");
		cstr_append(&json,gpsstr.pdop);
		cstr_append(&json,";\"vdop\":");
		cstr_append(&json,gpsstr.vdop);
		cstr_append(&json,";\"num2\":");
		cstr_append(&json,gpsstr.pst2);
		cstr_append(&json,";\"num3\":");
		cstr_append(&json,gpsstr.pst3);
		cstr_append(&json,";}");
		uart_puts("## ");
		uart_puts(json.buff);
		uart_puts("\n");
	} 
	else {
		if (gpsstr.p_ok) {
			uart_puts("@@ {");
			uart_puts(gpsstr.pchk);
			uart_puts("}\n");
		}
		else { sim808_show_buff(buff,BUFFSIZE); }
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	timer_delay1s(test,1);
	uart_init();
	uart_puts("\n\nSIM808 GNSS\n\n");
	sim808_init();
	uart_puts("-- Looking for device...\n");
	sim808_find_wait();
	uart_puts("-- Disable echo...\n");
	sim808_echo_off();
	sim808_ok2pass();
	uart_puts("-- Allow device to settle down...\n");
	step = 5;
	do {
		if (!sim808_timeout())
			sim808_purge();
	} while (--step);
	uart_puts("-- Powerup GNSS... ");
	sim808gnss_init();
	sim808_must_be_ok();
	uart_puts("-- Available GNSSEQ... ");
	sim808gnss_chk0();
	sim808_show_if_ok(buff,BUFFSIZE);
	uart_puts("-- Current GNSSEQ... ");
	sim808gnss_chk1();
	sim808_show_if_ok(buff,BUFFSIZE);
	uart_puts("-- Configure GNSSEQ... ");
	sim808gnss_conf();
	sim808_must_be_ok();
	/* wait loop */
	sim808gnss_inf_init(buff,BUFFSIZE);
	/* main loop */
	while (1) {
		sim808gnss_data();
		sim808gnss_wait_data();
		sim808gnss_show_location();
	}
}
/*----------------------------------------------------------------------------*/
