/*----------------------------------------------------------------------------*/
#include "i2c.h"
#include "uart_hexascii.h"
#include "utils.h"
/*----------------------------------------------------------------------------*/
/** rtc module based on ds1307 */
#define RTC_I2C_ADDR 0x68
#define RTC_ENB_MASK 0x80
/*----------------------------------------------------------------------------*/
unsigned char pdat[8], keep, test, flag, wait, show, step;
unsigned int loop;
/*----------------------------------------------------------------------------*/
/* external interrupt0 handler */
void isr_ex0(void) __interrupt(0) {
	if (!wait) {
		wait = 1;
		flag ^= RTC_ENB_MASK;
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	i2c_init();
	/* prepare stuffs */
	i2c_getb(RTC_I2C_ADDR,0x00,pdat,1);
	flag = pdat[0]&RTC_ENB_MASK;
	keep = pdat[0]&~RTC_ENB_MASK;
	wait = 0; show = 1;
	/* enable interrupt ex0 */
	IT0 = 1; EX0 = 1; EA = 1;
	while (1) {
		i2c_getb(RTC_I2C_ADDR,0x00,pdat,8);
		test = pdat[0]&~RTC_ENB_MASK;
		if (test!=keep||show) {
			uart_send_hexbyte(pdat[6]); // YY
			uart_send('-');
			uart_send_hexbyte(pdat[5]); // mm
			uart_send('-');
			uart_send_hexbyte(pdat[4]); // dd
			uart_send('T');
			uart_send_hexbyte(pdat[2]); // hour
			uart_send(':');
			uart_send_hexbyte(pdat[1]); // minute
			uart_send(':');
			uart_send_hexbyte(test); // second
			uart_puts(" Day:");
			uart_send(nibb_lo2ascii(pdat[4])); // day (1=Mon,...,7=Sun)
			uart_puts(" Cfg:");
			uart_send_hexbyte(pdat[7]); // config
			uart_send('\n');
			keep = test;
			show = 0;
		}
		if ((pdat[0]&RTC_ENB_MASK)!=flag) {
			if (flag) {
				pdat[0] |= RTC_ENB_MASK;
				uart_puts("## Disabled\n");
			}
			else {
				pdat[0] &= ~RTC_ENB_MASK;
				uart_puts("## Enabled\n");
			}
			i2c_putb(RTC_I2C_ADDR,0x00,pdat,1);
			wait_that(step,4,wait_loop(loop,0));
			wait = 0;
		}
	}
}
/*----------------------------------------------------------------------------*/
