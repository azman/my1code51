/*----------------------------------------------------------------------------*/
#include "gps_data.h"
#include "oled_ssd1306.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define BUFF_SIZE 128
/*----------------------------------------------------------------------------*/
__xdata gps_data_t gpsd;
__xdata char buff[BUFF_SIZE];
__xdata char conv[16];
unsigned char step, temp, test;
/*----------------------------------------------------------------------------*/
void trim_buff(unsigned char cmax) {
	if (step>cmax) {
		step = cmax;
		buff[step] = 0x0;
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	timer_init();
	gps_init();
	i2c_init();
	oled1306_init();
	oled1306_puts("GPS TEST");
	timer_delay1s(test,2);
	while (1) {
		step = gps_wait(buff,BUFF_SIZE);
		gps_nmea_gprmc(buff,&gpsd);
		// VK2828U7G5LF latlng in decimal minutes
		if (gpsd.stat&GPSSTAT_VALID) {
			oled1306_set_cursor(2,0);
			oled1306_puts("            ");
			oled1306_set_cursor(2,0);
			oled1306_putchar('[');
			temp = uint2str(conv,gpsd.dutc);
			while (temp<6) {
				oled1306_putchar('0');
				temp++;
			}
			oled1306_puts(conv);
			temp = uint2str(conv,gpsd.tutc);
			while (temp<6) {
				oled1306_putchar('0');
				temp++;
			}
			oled1306_puts(conv);
			oled1306_putchar(']');
			oled1306_set_cursor(4,0);
			oled1306_puts("> Lat:");
			oled1306_puts(&buff[gpsd.ilat]);
			oled1306_set_cursor(5,0);
			oled1306_puts("  @ ");
			float2str(conv,gpsd.dlat);
			oled1306_puts(conv);
			oled1306_set_cursor(6,0);
			oled1306_puts("> Lng:");
			oled1306_puts(&buff[gpsd.ilon]);
			oled1306_set_cursor(7,0);
			oled1306_puts("  @ ");
			float2str(conv,gpsd.dlon);
			oled1306_puts(conv);
		} else {
			trim_buff(13);
			oled1306_set_cursor(2,0);
			oled1306_puts("** Invalid?  ");
			oled1306_set_cursor(4,0);
			oled1306_puts(buff);
		}
	}
}
/*----------------------------------------------------------------------------*/
