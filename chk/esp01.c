/*----------------------------------------------------------------------------*/
#include "esp01.h"
#include "esp01_uart.h"
#include "esp01_util.h"
#include "utils_lite.h"
#include "timer.h"
#define MY1CSTR_SIZE 80
#include "cstr.h"
/*----------------------------------------------------------------------------*/
#define HOSTPATH "/test/only/"
#define HOSTNAME "localhost"
/*----------------------------------------------------------------------------*/
/* host for station mode */
#define HOSTADDR "192.168.100.100"
#define HOSTPORT "1337"
/*----------------------------------------------------------------------------*/
#define SOCKET_REQ0 ESP01CMD_SOCKINIT "=\"TCP\",\""
#define SOCKET_REQ1 "\","
#define SOCKET_REQ2 "\r\n"
/*----------------------------------------------------------------------------*/
#define SOCKET_HOST0 SOCKET_REQ0 HOSTADDR
#define SOCKET_HOST1 SOCKET_REQ1 HOSTPORT SOCKET_REQ2
#define SOCKET_HOST SOCKET_HOST0 SOCKET_HOST1
/*----------------------------------------------------------------------------*/
/**
 * testing esp01 station mode
 * - use my1apisrvc to test (output when request received)
*/
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 128
__xdata char buff[BUFFSIZE];
__xdata char *pbuf;
__xdata my1cstr_t cstr;
__xdata unsigned char chk1, chk2;
unsigned char test, mode;
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n\Debugging ESP01 Module\n\n");
	timer_init();
	timer_delay1s(test,3);
	/* find esp device */
	esp01_init();
	/* the following loop IS esp01_find_wait() */
	while (1) {
		uart_puts("-- Sending AT... ");
		esp01_find();
		/*  esp01_found() */
		if (esp01_wait_ok()==ESP01_OK) {
			uart_puts("OK.\n");
			break;
		}
		uart_puts("timeout.\n");
		timer_delay1s(test,1);
	}
	uart_puts("-- Disabling echo... ");
	esp01_echo_off();
	/* esp01_must_be_ok() */
	if (esp01_ok()) uart_puts("OK.\n");
	else { uart_puts("**error**\n"); hang(); }
	/* prepare buffer */
	esp01_buff(buff,BUFFSIZE);
	/* firmware info */
	uart_puts("-- Getting firmware info... ");
	/* esp01_getfirmware(); */
	esp01_task(ESP01CMD_FWINFO);
	if (esp01_task_ok()) {
		chk1 = 0 ; chk2 = 0;
		if (esp01_buff_ok()) {
			uart_puts("OK.\n");
			if (esp01_seek_atvers()) {
				pbuf = &espd.buff[espd.test];
				uart_puts("## ATVERS:");
				while (chk1<FWINFO_BUFFLEN&&pbuf[chk1]) {
					if (pbuf[chk1]=='(')
						break;
					uart_send(pbuf[chk1]);
					chk1++;
				}
				uart_send('\n');
			}
			if (esp01_seek_sdkver()) {
				pbuf = &espd.buff[espd.test];
				uart_puts("## SDKVER:");
				while (chk2<FWINFO_BUFFLEN&&pbuf[chk2]) {
					if (pbuf[chk2]=='(')
						break;
					uart_send(pbuf[chk2]);
					chk2++;
				}
				uart_send('\n');
			}
		}
		else uart_puts("???\n");
		if (!chk1||!chk2) esp01_show_buff(espd.buff,espd.fill);
	}
	/* this should not happen? */
	else if (esp01_task_timeout()) uart_puts("timeout.\n");
	else {
		if (esp01_task_overflow())
			uart_puts("overflow?\n");
		else uart_puts("???\n");
		esp01_show_buff(espd.buff,espd.fill);
	}
	/* current mode info */
	mode = 0;
	uart_puts("-- Checking current mode... ");
	esp01_getcurrmode();
	if (esp01_task_ok()) {
		/* assume buff ok? */
		if (esp01_seek_cwmode()) {
			uart_puts("OK.\n");
			pbuf = &espd.buff[espd.test];
			if (pbuf[1]=='\r'||pbuf[1]=='\n') {
				if (pbuf[0]>0x30&&pbuf[0]<0x34)
					mode = pbuf[0] - 0x30;
			}
			/* single char info */
			uart_puts("## CWMODE:");
			switch (mode) {
				case ESP01_STMODE: uart_puts("Station\n"); break;
				case ESP01_APMODE: uart_puts("Access Point\n"); break;
				case ESP01_D2MODE: uart_puts("Station/AP\n"); break;
				default:
					uart_puts("Unknown '");
					uart_send(pbuf[0]);
					uart_puts("'\n");
					break;
			}
		}
		else {
			uart_puts("???\n");
			esp01_show_buff(espd.buff,espd.fill);
		}
	}
	else if (esp01_task_timeout()) uart_puts("timeout.\n");
	else {
		if (esp01_task_overflow())
			uart_puts("overflow?\n");
		else uart_puts("???\n");
		esp01_show_buff(espd.buff,espd.fill);
	}
	/* testing ap mode */
	uart_puts("-- Setting AccessPoint mode... ");
	esp01_conf_apmode();
	esp01_must_be_ok();
	/* check current ssid */
	uart_puts("-- Checking current SSID... ");
	esp01_getcurrssid();
	if (esp01_task_ok()) {
		/* assume buff ok? */
		if (esp01_seek_ssid()) {
			uart_puts("OK.\n");
			pbuf = &espd.buff[espd.test+1];
			uart_puts("## SSID:");
			chk1 = 0;
			while (chk1<16&&pbuf[chk1]) {
				if (pbuf[chk1]=='"')
					break;
				uart_send(pbuf[chk1]);
				chk1++;
			}
			uart_send('\n');
		}
		else {
			uart_puts("???\n");
			esp01_show_buff(espd.buff,espd.fill);
		}
	}
	else if (esp01_task_timeout()) uart_puts("timeout.\n");
	else {
		if (esp01_task_overflow())
			uart_puts("overflow?\n");
		else uart_puts("???\n");
		esp01_show_buff(espd.buff,espd.fill);
	}
	uart_puts("-- Getting AP address info... ");
	esp01_getipaddr();
	if (esp01_task_ok()) {
		uart_puts("OK.\n");
		chk1 = 0 ; chk2 = 0;
		if (esp01_seek_ipaddr()) {
			pbuf = &espd.buff[espd.test];
			uart_puts("## IP:");
			while (chk1<IPADDR_BUFFLEN&&pbuf[chk1]) {
				if (pbuf[chk1]=='"') {
					if (esp01_ipnull(pbuf,chk1))
						chk1 = 0;
					break;
				}
				uart_send(pbuf[chk1]);
				chk1++;
			}
			uart_send('\n');
		}
		if (esp01_seek_mcaddr()) {
			pbuf = &espd.buff[espd.test];
			uart_puts("## MAC:");
			while (chk2<IPADDR_BUFFLEN&&pbuf[chk2]) {
				if (pbuf[chk2]=='"')
					break;
				uart_send(pbuf[chk2]);
				chk2++;
			}
			uart_send('\n');
		}
		if (!chk1||!chk2) esp01_show_buff(espd.buff,espd.fill);
	}
	else if (esp01_task_timeout()) uart_puts("timeout.\n");
	else {
		if (esp01_task_overflow())
			uart_puts("overflow?\n");
		else uart_puts("???\n");
		esp01_show_buff(espd.buff,espd.fill);
	}
	/* setting station mode */
	uart_puts("-- Setting station mode... ");
	esp01_conf_stmode();
	esp01_must_be_ok();
	/* connect to access point */
	while (1) {
		uart_puts("-- Connecting to "WIFI_SSID"... ");
		esp01_stconnect();
		if (esp01_task_ok()) {
			if (esp01_buff_ok()) {
				uart_puts("OK.\n");
				break;
			}
			else if (esp01_seek_wifiok()) {
				uart_puts("connected.\n");
				break;
			}
		}
		if (esp01_task_timeout())
			uart_puts("timeout.\n");
		else if (esp01_task_overflow())
			uart_puts("overflow?\n");
		else if (esp01_seek_failmsg()) {
			uart_puts("failed.\n");
		}
		else {
			uart_puts("???\n");
			esp01_show_buff(espd.buff,espd.fill);
		}
		uart_puts("## Retry in 10 seconds.\n");
		timer_delay1s(test,10);
	}
	/* get connection info */
	uart_puts("-- Getting address info... ");
	esp01_getipaddr();
	if (esp01_task_ok()) {
		uart_puts("OK.\n");
		chk1 = 0 ; chk2 = 0;
		if (esp01_seek_ipaddr()) {
			pbuf = &espd.buff[espd.test];
			uart_puts("## IP:");
			while (chk1<IPADDR_BUFFLEN&&pbuf[chk1]) {
				if (pbuf[chk1]=='"') {
					if (esp01_ipnull(pbuf,chk1))
						chk1 = 0;
					break;
				}
				uart_send(pbuf[chk1]);
				chk1++;
			}
			uart_send('\n');
		}
		if (esp01_seek_mcaddr()) {
			pbuf = &espd.buff[espd.test];
			uart_puts("## MAC:");
			while (chk2<IPADDR_BUFFLEN&&pbuf[chk2]) {
				if (pbuf[chk2]=='"')
					break;
				uart_send(pbuf[chk2]);
				chk2++;
			}
			uart_send('\n');
		}
		if (!chk1||!chk2) esp01_show_buff(espd.buff,espd.fill);
	}
	else if (esp01_task_timeout()) uart_puts("timeout.\n");
	else {
		if (esp01_task_overflow())
			uart_puts("overflow?\n");
		else uart_puts("???\n");
		esp01_show_buff(espd.buff,espd.fill);
	}
	/* data connection */
	chk1 = 0;
	while (1) {
		chk2 = 1;
		/* open a socket */
		uart_puts("-- Opening socket ("HOSTADDR":"HOSTPORT")... ");
		esp01_task(SOCKET_HOST);
		if (esp01_task_ok()) {
			if (esp01_seek_connect()) {
				uart_puts("OK.\n## Connected!\n");
				chk2--;
			}
			else {
				if (esp01_seek_errormsg())
					uart_puts("*error* ");
				if (esp01_seek_closed())
					uart_puts("**closed!**\n");
				else {
					uart_puts("???\n");
					esp01_show_buff(espd.buff,espd.fill);
				}
			}
		}
		else if (esp01_task_timeout()) uart_puts("timeout.\n");
		else {
			if (esp01_task_overflow())
				uart_puts("overflow?\n");
			else uart_puts("???\n");
			esp01_show_buff(espd.buff,espd.fill);
		}
		if (chk2) {
			uart_puts("## Retry in 10 seconds.\n");
			timer_delay1s(test,10);
			/* just in case */
			esp01_purge();
			continue;
		}
		/* prepare buffer */
		cstr_assign(&cstr,"POST ");
		cstr_append(&cstr,HOSTPATH);
		uint8_2cstr(buff,chk1);
		cstr_append(&cstr,buff);
		cstr_append(&cstr," HTTP/1.1\r\nHost: ");
		cstr_append(&cstr,HOSTNAME);
		cstr_append(&cstr,"\r\nContent-Length: ");
		cstr_append(&cstr,"0\r\n\r\n");
		/* prepare transfer */
		uart_puts("-- Preparing to post (");
		uart_puts(buff);
		uart_puts(")... ");
		chk2 = 1;
		uint8_2cstr(buff,cstr.blen);
		esp01_puts("AT+CIPSEND=");
		esp01_puts(buff);
		esp01_puts("\r\n");
		test = esp01_read_wait(buff,BUFFSIZE);
		if (buff[test-2]=='>'&&buff[test-1]==' ') {
			uart_puts("OK.\n>> Sending POST data... ");
			esp01_puts(cstr.buff);
			if (esp01_wait_str("SEND OK\r\n",9)==ESP01_OK) {
				test = esp01_read_wait(buff,BUFFSIZE);
				uart_puts("OK.\n-- Status: "); 
				if (!test) uart_puts("timeout.\n");
				else {
					/* look for +IPD? >> api reply!
+IPD,98:HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 27
{"flag":true,"info":"test"} */
					espd.fill = test;
					if (esp01_seek_closed()) {
						uart_puts("**closed!**\n");
						chk2 = 0;
					}
					else esp01_show_buff(espd.buff,test);
				}
				chk1++;
			}
			else uart_puts("*error?*\n");
		}
		else {
			uart_puts("???\n");
			if (test) esp01_show_buff(espd.buff,test);
		}
		if (chk2) {
			/* close socket */
			uart_puts("-- Closing socket... ");
			esp01_task(ESP01CMD_SOCKDONE);
			if (esp01_task_ok()) {
				if (esp01_buff_ok())
					uart_puts("OK!\n");
				else if (esp01_buff_error())
					uart_puts("ERROR!\n");
				else {
					uart_puts("??\n");
					esp01_show_buff(espd.buff,espd.fill);
				}
			}
			else if (esp01_task_timeout()) uart_puts("timeout.\n");
			else {
				if (esp01_task_overflow())
					uart_puts("overflow?\n");
				else uart_puts("???\n");
				esp01_show_buff(espd.buff,espd.fill);
			}
		}
		timer_delay1s(test,1);
	}
}
/*----------------------------------------------------------------------------*/
