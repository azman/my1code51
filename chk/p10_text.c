/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "uart_stc.h"
/*----------------------------------------------------------------------------*/
#include "p10_text.h"
/*----------------------------------------------------------------------------*/
__xdata p10_t p10disp;
/*----------------------------------------------------------------------------*/
#define STEP_SIZE 200
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned int step;
	unsigned char irow, icol, tcnt;
	uart_init_brt();
	uart_puts("-- I am alive!\n");
	timer_init_dual(); /* TODO: modify p10_base to use timer1 intr instead! */
	p10_setup(&p10disp);
	p10_clear(&p10disp);
	uart_puts("## Main loop!\n");
	step = STEP_SIZE; irow = 0; icol = 0; tcnt = 0;
	//p10_putchar8(&p10disp,icol,irow,0x30+tcnt);
	//p10_putchar16(&p10disp,icol,0x30+tcnt);
	p10_putchar8x(&p10disp,icol,0x30+tcnt);
	timer_tick_exec();
	while (1) {
		if (timer_ticked()) {
			if (step) step--;
			else {
				step = STEP_SIZE;
				tcnt++;
				if (tcnt==10) tcnt = 0;
				//p10_putchar8(&p10disp,icol,irow,0x30+tcnt);
				//p10_putchar16(&p10disp,icol,0x30+tcnt);
				p10_putchar8x(&p10disp,icol,0x30+tcnt);
			}
			p10_update(&p10disp);
			p10_refresh(&p10disp);
			timer_tick00();
		}
	}
}
/*----------------------------------------------------------------------------*/