/*----------------------------------------------------------------------------*/
#include "tm1651.h"
#include "timer.h"
#include "uart_hexascii.h"
/*----------------------------------------------------------------------------*/
void timer_wait_1s(void) {
	unsigned char loop = 20;
	while (loop>0) {
		timer_wait_50ms();
		loop--;
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char data[4], pick;
	unsigned long* pchk;
	timer_init();
	uart_init();
	tm1651_init();
	if (!P1_0) {
		pchk = (unsigned long*)data;
		*pchk = 0x00000001;
		while (1) {
			if (!(data[0]&0x80||data[1]&0x80||data[2]&0x80||data[3]&0x80)) {
				uart_puts("Data");
				pick = 0;
				if (data[0]) { uart_puts("0:"); pick = data[0]; }
				if (data[1]) { uart_puts("1:"); pick = data[1]; }
				if (data[2]) { uart_puts("2:"); pick = data[2]; }
				if (data[3]) { uart_puts("3:"); pick = data[3]; }
				uart_send_hexbyte(pick);
				uart_send('\n');
				tm1651_write_data(data);
				timer_wait_1s();
			}
			*pchk <<= 1;
			if (!(data[0]|data[1]|data[2]|data[3]))
				*pchk = 0x00000001;
		}
	}
	else {
		while (1) {
			for (pick=0;pick<6;pick++) {
				uart_puts("Pick:");
				uart_send_hexbyte(pick);
				uart_send('\n');
				tm1651_level(pick,pick<5?0:1);
				timer_wait_1s();
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
