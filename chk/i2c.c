/*----------------------------------------------------------------------------*/
#include "i2c.h"
#include "uart.h"
#include "uart_hexascii.h"
#include "utils_lite.h"
/**
	i2c_test_read(0x3c,0x0a,3);
	i2c_test_send(0x3c,0x02,0);
	i2c_test_send(0x3c,0x01,0x20);
	i2c_test_read(0x3c,0x03,6);
**/
/*----------------------------------------------------------------------------*/
/* all device should respond to addr 0, right? */
#define REG_ADDR 0x00
/*----------------------------------------------------------------------------*/
unsigned char loop, radd, addr, temp, buff[8];
/*----------------------------------------------------------------------------*/
void uart_read_hexd(unsigned char skip) {
	do {
		temp = uart_read();
		if (skip&&(temp==0x0d||temp==0x0a)) {
			temp = 0xff;
			break;
		}
		if (temp>=0x30&&temp<=0x39) {
			uart_send(temp);
			temp -= 0x30;
			break;
		}
		if (temp>=0x41&&temp<=0x46) {
			uart_send(temp);
			temp -= 0x37;
			break;
		}
		if (temp>=0x61&&temp<=0x66) {
			uart_send(temp);
			temp -= 0x57;
			break;
		}
	} while (1);
}
/*----------------------------------------------------------------------------*/
void i2c_test_that(unsigned char send, unsigned char wait) {
	uart_puts("\nSending DevAddr[0x");
	uart_send_hexbyte(addr);
	uart_puts("] RegAddr[0x");
	uart_send_hexbyte(radd);
	uart_puts("] : ");
	i2c_do_start();
	i2c_do_send_byte(addr<<1);
	i2c_do_send_byte(radd);
	if (send) {
		for (loop=0;loop<send;loop++)
			i2c_do_send_byte(buff[loop]);
	}
	if (wait) {
		i2c_do_start();
		i2c_do_send_byte(addr|0x01);
		for (loop=0;loop<wait-1;loop++) {
			buff[loop] = i2c_do_read_byte(0);
		}
		buff[loop] = i2c_do_read_byte(1);
	}
	i2c_do_stop();
	if (wait) {
		uart_puts(" {0x");
		for (loop=0;loop<wait;loop++) {
			if (loop) uart_puts("} {0x");
			uart_send_hexbyte(buff[loop]);
		}
		uart_puts("}");
	}
	uart_send('\n');
}
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	i2c_init();
	radd = REG_ADDR;
	uart_puts("\n\nI2C Test Code\n");
	uart_puts("\nmy1code51 I2C Scanner (0x");
	uart_send_hexbyte(radd);
	uart_puts(")\n");
	for (addr=0;addr<0x80;addr++) {
		if (addr%8==0) uart_send('\n');
		uart_puts("  0x");
		uart_send_hexbyte(addr);
		uart_puts(":");
		if (!i2c_getb(addr,radd,buff,1))
			uart_send_hexbyte(buff[0]);
		else
			uart_puts("--");
	}
	uart_send('\n');
	while (1) {
		uart_puts("\nI2C devAddr: 0x");
		uart_read_hexd(0);
		addr = temp<<4;
		uart_read_hexd(0);
		addr |= temp;
		while (uart_read()!='\n');
		if (addr>=0x80) {
			uart_puts("\nInvalid address! Should be [0x00-0x7f]\n");
			continue;
		}
		uart_puts(" => 0x");
		uart_send_hexbyte(addr);
		uart_puts("\nI2C regAddr: 0x");
		uart_read_hexd(0);
		radd = temp<<4;
		uart_read_hexd(0);
		radd |= temp;
		while (uart_read()!='\n');
		uart_puts(" => 0x");
		uart_send_hexbyte(addr);
		uart_puts("\n\nSimply hit enter to skip this...");
		uart_puts("\nI2C cmdByte: 0x");
		uart_read_hexd(1);
		if (temp!=0xff) {
			buff[0] = temp<<4;
			uart_read_hexd(0);
			buff[0] |= temp;
			i2c_test_that(1,0);
		}
		else {
			uart_puts("\nNumber of byte(s) to read (1-8): ");
			do {
				temp = uart_read();
			} while (temp<0x31||temp>0x38);
			uart_send(temp);
			temp -= 0x30;
			i2c_test_that(0,temp);
		}
	}
}
/*----------------------------------------------------------------------------*/
