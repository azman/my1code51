/*----------------------------------------------------------------------------*/
#include "hmc5883.h"
#include "timer.h"
#include "uart_hexascii.h"
#include "utils_lite.h"
/*----------------------------------------------------------------------------*/
__xdata magdata_t axes;
__xdata char buff[16];
unsigned char test, loop;
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	i2c_init();
	timer0_init();
	timer0_delay1s(test,2);
	uart_puts("\nLooking for HMC5883 compass... ");
	do {
		if (hmc5883_init()) break;
		uart_send('.');
		timer0_delay1s(test,1);
	} while (1);
	uart_puts("done.\n\n");
	while(1) {
		hmc5883_data((unsigned char*)&axes);
		hmc5883_gain(&axes,HMC5883_GAIN_FACTOR);
		uart_puts("-- Data:{");
		hmc5883_head(&axes);
		int16_2cstr(buff,axes.xaxis);
		uart_puts(buff);
		uart_puts(",");
		int16_2cstr(buff,axes.yaxis);
		uart_puts(buff);
		uart_puts(",");
		int16_2cstr(buff,axes.zaxis);
		uart_puts(buff);
		uart_puts("|Head:");
		int16_2cstr(buff,axes.xhead);
		uart_puts(buff);
		uart_puts("}\n");
		test = 10;
		do {
			timer_wait(TIMER_VAL50MS);
		} while (--test);
	}
}
/*----------------------------------------------------------------------------*/
