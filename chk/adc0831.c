/*----------------------------------------------------------------------------*/
#include "adc0831.h"
#include "textlcd.h"
#include "utils_float.h"
/*----------------------------------------------------------------------------*/
char display[LCD_MAX_CHAR];
float value;
unsigned int count;
unsigned char check;
/*----------------------------------------------------------------------------*/
void main(void) {
	TMOD = 0x11;
	adc_init();
	lcd_init();
	lcd_init();
	lcd_goto_line1();
	lcd_puts("MY1CODE51 DO ADC");
	lcd_goto_line2();
	while (1) {
		/** read adc */
		check = adc_get_data();
		value = (float) check * VREF / 255.0;
		float2str(display,value);
		lcd_goto_line2();
		lcd_puts("                ");
		lcd_goto_line2();
		lcd_puts("ADC: ");
		lcd_puts(display);
		lcd_puts(" (");
		int2str(display,check);
		lcd_puts(display);
		lcd_puts(")");
		/** 20 x 50ms delay before next read */
		count = 20;
		do {
			TH0 = 0x4B; TL0 = 0xFD; TR0 = 1;
			while(!TF0); TR0 = 0; TF0 = 0;
			count--;
		} while(count>0);
	}
}
/*----------------------------------------------------------------------------*/
