/*----------------------------------------------------------------------------*/
#include "timer_keep.h"
#include "uart_gets.h"
/*----------------------------------------------------------------------------*/
#define ALARM_TIME 0x0800
/*----------------------------------------------------------------------------*/
unsigned char loop, acmd, buff[6];
unsigned int warn, curr;
/*----------------------------------------------------------------------------*/
void uart_show_time(void) {
	uart_puts("Time: ");
	uart_send(dec2char10(tkhour));
	uart_send(dec2char01(tkhour));
	uart_send(':');
	uart_send(dec2char10(tkmins));
	uart_send(dec2char01(tkmins));
	uart_send(':');
	uart_send(dec2char10(tksecs));
	uart_send(dec2char01(tksecs));
	uart_send('\n');
}
/*----------------------------------------------------------------------------*/
void uart_input4d(void) {
	uart_gets(buff,6);
	for (loop=0;loop<4;loop++) {
		if (buff[loop]<0x30||buff[loop]>0x39) {
			buff[0] = 0; break;
		}
	}
	if (buff[4]) buff[0] = 0;
	if (buff[0]) {
		buff[5] = (buff[2]-0x30)*10;
		buff[5] += (buff[3]-0x30);
		buff[4] = (buff[0]-0x30)*10;
		buff[4] += (buff[1]-0x30);
		if (buff[4]>=24||buff[3]>=60)
			buff[0] = 0;
	}
}
/*----------------------------------------------------------------------------*/
void main(void) {
	warn = ALARM_TIME;
	uart_init();
	timer_keep_prep(7,59,45); /* start at 0759 */
	timer_keep_exec();
	uart_show_time();
	while (1) {
		if (uart_peek()) {
			acmd = uart_read();
			if (acmd=='#') {
				timer_stop();
				uart_puts("## Enter new time (4 digits): ");
				uart_input4d();
				if (buff[0]) {
					tkhour = buff[4];
					tkmins = buff[5];
					tksecs = 0;
					tkiter = TIMER_LOOP_1S;
					tktick = 0;
					uart_puts("## New ");
					uart_show_time();
				}
				else uart_puts("** Invalid input!\n");
				timer_exec();
			}
			else if (acmd=='@') {
				timer_stop();
				uart_puts("@@ Enter alarm time (4 digits): ");
				uart_input4d();
				if (buff[0]) {
					warn = (buff[4]<<8) | buff[5];
					uart_puts("@@ Alarm @");
					uart_send(dec2char10(buff[4]));
					uart_send(dec2char01(buff[4]));
					uart_send(dec2char10(buff[5]));
					uart_send(dec2char01(buff[5]));
					uart_send('\n');
				}
				else uart_puts("** Invalid input!\n");
				timer_exec();
			}
			else if (acmd=='+') {
				timer_stop();
				uart_puts("-- 10secs Increment\n");
				tksecs += 10;
				if (tksecs>=TIMESEC_FULL) tksecs = 59;
				uart_show_time();
				timer_exec();
			}
			else if (acmd=='>') {
				timer_stop();
				uart_puts("-- 30secs Increment\n");
				tksecs += 30;
				if (tksecs>=TIMESEC_FULL) tksecs = 55;
				uart_show_time();
				timer_exec();
			}
		}
		if (tktick) {
			tktick = 0;
			uart_show_time();
			curr = timer_keep_read();
			if (curr==warn) {
				uart_puts("## ALARM!\n");
				warn = 0x0;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
