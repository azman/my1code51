/*----------------------------------------------------------------------------*/
#include "fat32.h"
#include "uart_hexascii.h"
#include "uart_gets.h"
#include "loop_delay.h"
/*----------------------------------------------------------------------------*/
#define DUMPTEXT "# I AM LEGEND #"
#define DUMPFULL DUMPTEXT"\n"
#define DUMPSIZE 16
#define BUFFSIZE 32
/*----------------------------------------------------------------------------*/
__xdata word32_t test;
__xdata char buff[BUFFSIZE];
__xdata int loop, step;
__xdata unsigned char init, what;
/*----------------------------------------------------------------------------*/
#include "fat32_debug.h"
/*----------------------------------------------------------------------------*/
void main(void) {
	uart_init();
	uart_puts("\n\n-- Basic FAT32 Read/Write on SD\n");
	do {
		uart_puts("\n-- Initialize... ");
		fat32_init();
		if (!fat32_error()) {
			uart_puts("OK.\n");
			break;
		}
		uart_puts("**error** [0x");
		uart_send_hexbyte(fat32_flag());
		uart_puts("]\n");
	} while (1);
	init = 1;
	while (1) {
		if (init) {
			uart_puts("\n@@ l to list files");
			uart_puts("\n@@ r to view a file");
			uart_puts("\n@@ z to count/check file content");
			uart_puts("\n@@ x to dump text into given file");
			uart_puts("\n@@ w|a to write to a file\n");
			init = 0;
		}
		if (uart_peek()) {
			what = uart_read();
			if (what=='r') {
				uart_puts("\n-- Name: ");
				init = uart_gets(buff,BUFFSIZE);
				init = validate_filename(buff,init);
				if (init) {
					if (fat32_node_find_name(buff)) {
						if (!fat32_node_ispath(path.info)) {
							fat32_file(buff);
							uart_puts("\nContent:\n");
							fat32_file_data(buff,BUFFSIZE);
							fat32_file_read_all();
							if (fat32_error()) {
								uart_puts("\n** Error reading from ");
								uart_puts(file.name);
								uart_puts("?!\n");
							}
						}
						else uart_puts("** Not a file!\n");
					}
					else uart_puts("\n** Not found!\n");
				}
				else uart_puts("\n** Invalid filename!\n");
				init = 1;
			}
			else if (what=='w'||what=='a') {
				uart_puts("\n-- ");
				if (what=='a') uart_puts("a+");
				else uart_puts("w+");
				uart_puts(" Name: ");
				init = uart_gets(buff,BUFFSIZE);
				init = validate_filename(buff,init);
				if (init) {
					fat32_file(buff);
					if (what=='a') fat32_append_mode();
					else fat32_overwrite_mode();
					uart_puts("\n-- Text: ");
					init = uart_gets(buff,BUFFSIZE-1);
					buff[init++] = '\n'; /* do not need a null here! */
					uart_puts("\n-- ");
					if (what=='a') uart_puts(" Write-append ");
					else uart_puts("Writing ");
					uart_puts("to ");
					uart_puts(file.name);
					uart_puts("... ");
					fat32_write(buff,init);
					if (fat32_error()) {
						uart_puts("**error** [");
						uart_send_hexlong(fat32_flag());
						uart_puts("] ");
					}
					else {
						uart_puts("done. <");
						uart_show_integer(file.curr);
						uart_puts(">\n");
					}
				}
				else uart_puts("\n** Invalid filename!\n");
				init = 1;
			}
			else if (what=='p') {
				uart_puts("\n-- cDir: ");
				init = uart_gets(buff,16);
				validate_filename(buff,init);
				fat32_path_cd(buff);
				if (fat32_link_ismodified(&path)) {
					fat32_path_list();
					fat32_link_updated(&path);
				}
				else uart_puts("** invalid path? **\n");
				init = 1;
			}
			else if (what=='n') {
				uart_puts("\n-- Null: ");
				init = uart_gets(buff,BUFFSIZE);
				if (init) {
					validate_filename(buff,init);
					uart_puts("\n@@ Nullifying ");
					uart_puts(buff);
					uart_puts(" ... ");
					fat32_file_name(buff);
					fat32_file_null();
					if (fat32_error()) {
						uart_puts("**error** {Flag:0x");
						uart_send_hexlong(fat32_flag());
						uart_puts("}\n");
						fat32_clearcode();
					}
					else uart_puts(" done.\n");
				}
				else uart_puts("\n** Cancelled!\n");
			}
			else if (what=='l') {
				uart_puts("\n-- Listing... init.\n");
				fat32_path_list();
				uart_puts("-- Listing... done.\n");
				init = 1;
			}
			else if (what=='z') {
				uart_puts("\n-- Read: ");
				init = uart_gets(buff,BUFFSIZE);
				init = validate_filename(buff,init);
				if (init&&fat32_node_find_name(buff)) {
					if (!fat32_node_ispath(path.info)) {
						fat32_node_show((fat32_node_t*)path.info);
						uart_puts("@@ Reading... ");
						fat32_file(buff);
						fat32_file_data(buff,BUFFSIZE);
						fat32_file_read_size();
						uart_puts("done. Curr:");
						uart_show_integer(file.curr);
						uart_puts(", eCnt:");
						uart_show_integer(file.tmpx);
						uart_puts("\n");
					}
					else uart_puts("\n** Not a file!!\n");
				}
				else uart_puts("\n** File not found!\n");
				init = 1;
			}
			else if (what=='x') {
				uart_puts("\n-- Dump: ");
				init = uart_gets(buff,BUFFSIZE);
				if (init) {
					validate_filename(buff,init);
					fat32_file(buff);
					fat32_append_mode();
					uart_puts("-- Step: ");
					uart_gets(buff,BUFFSIZE);
					step = str2int(buff);
					if (!step) step = 64;
					uart_puts("\n@@ Dumping '"DUMPTEXT"' ");
					uart_show_integer(step);
					uart_puts(" times... ");
					do {
						fat32_write(DUMPFULL,DUMPSIZE);
						if (fat32_error()) {
							uart_puts("**error** [0x");
							uart_send_hexlong(fat32_flag());
							uart_puts("]");
							break;
						}
						else uart_send('.');
					} while(--step);
					if (fat32_error()) fat32_clearcode();
					else uart_puts(" done.");
					uart_puts(" (");
					uart_show_integer(file.curr);
					uart_puts(")\n");
				}
				else uart_puts("\n** Cancelled!\n");
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
