/*----------------------------------------------------------------------------*/
#define MY1CSTR_SIZE 32
#include "cstr_conv.h"
#include "uart.h"
#include "uart_hexascii.h"
#include "string.h"
/*----------------------------------------------------------------------------*/
__xdata my1cstr_t buff, ibuf;
int temp;
unsigned char mode, test;
/*----------------------------------------------------------------------------*/
void uart_send_integer(int word) {
	cstr_null(&ibuf);
	cstr_append_int16(&ibuf,word);
	uart_puts(ibuf.buff);
}
/*----------------------------------------------------------------------------*/
void main(void) {
	mode = 0;
	cstr_init(&ibuf);
	cstr_init(&buff);
	uart_init();
	uart_purge();
	uart_puts("\n\nPress <ENTER> to start!\n");
	while (uart_read()!='\n');
	while (1) {
		if (!mode) {
			uart_puts("\n## Current string = {");
			uart_send_integer(buff.blen);
			uart_send('/');
			uart_send_integer(MY1CSTR_ENDS);
			uart_send(':');
			uart_puts(buff.buff);
			uart_puts("}\n");
			if (buff.blen==MY1CSTR_ENDS) buff.blen = 0;
			uart_puts("\nEnter a string (MAX=");
			uart_send_integer(MY1CSTR_SIZE);
			uart_puts("): ");
			uart_gets(ibuf.buff,MY1CSTR_SIZE);
			if (!ibuf.buff[0]) {
				uart_puts("\nSwitching mode to conversion!\n");
				mode = 1;
				continue;
			}
			uart_puts("Input string is '");
			uart_puts(ibuf.buff);
			uart_puts("'\n");
			cstr_append(&buff,ibuf.buff);
		}
		else {
			uart_puts("\nEnter an integer (signed,16-bit): ");
			uart_gets(ibuf.buff,MY1CSTR_SIZE);
			if (!ibuf.buff[0]) {
				uart_puts("\nSwitching mode to string buffer!\n");
				cstr_null(&buff);
				mode = 0;
				continue;
			}
			uart_puts("\n-- Input is ");
			uart_puts(ibuf.buff);
			cstr_make_int16(&ibuf,&temp);
			cstr_null(&buff);
			cstr_append_int16(&buff,temp);
			if (!strncmp(buff.buff,ibuf.buff,buff.blen+1)) {
				uart_puts("\n## Integer is ");
				uart_puts(buff.buff);
				uart_puts(" {0x");
				uart_send_hexuint((unsigned int)temp);
				uart_puts("}\n");
				/* see if it can be in uint8 */
				cstr_make_uint8(&ibuf,&test);
				cstr_null(&buff);
				cstr_append_uint8(&buff,test);
				if (!strncmp(buff.buff,ibuf.buff,buff.blen+1)) {
					uart_puts("   > HexByte:0x");
					uart_send_hexbyte(test);
					uart_puts("\n");
				}
			} else {
				uart_puts("\n** Invalid integer! {I:");
				uart_puts(ibuf.buff);
				uart_puts("}{C:");
				uart_puts(buff.buff);
				uart_puts("}{0x");
				uart_send_hexuint((unsigned int)temp);
				uart_puts("}\n");
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
