/*----------------------------------------------------------------------------*/
#include "apds9960.h"
#include "uart_hexascii.h"
#include "utils.h"
/*----------------------------------------------------------------------------*/
char buffer[16];
/*----------------------------------------------------------------------------*/
void uart_show_ival(char* text, unsigned char ival) {
	uart_puts(text);
	int2str(buffer,(long)ival);
	uart_puts(buffer);
}
/*----------------------------------------------------------------------------*/
void uart_show_data(unsigned char *pdat) {
	uart_puts(" => {0x");
	uart_send_hexbyte(pdat[0]);
	uart_puts(",0x");
	uart_send_hexbyte(pdat[1]);
	uart_puts(",0x");
	uart_send_hexbyte(pdat[2]);
	uart_puts(",0x");
	uart_send_hexbyte(pdat[3]);
	uart_puts("}\n");
}
/*----------------------------------------------------------------------------*/
unsigned char show_read_gesture(void) {
	unsigned char size, move, step, pdat[4];
	move = 0; step = 0; u1 = 0; u0 = 0;
	while (1) {
		apds9960_wait_fifo();
		i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GFIFO_LVL,&size,1);
		if (!size) {
			if (u1&&u0) {
				/* decode */
				udr1 = ((u1-d1)*100)/(u1+d1);
				lrr1 = ((l1-r1)*100)/(l1+r1);
				udr0 = ((u0-d0)*100)/(u0+d0);
				lrr0 = ((l0-r0)*100)/(l0+r0);
				ydel = udr0 - udr1;
				xdel = lrr0 - lrr1;
				udr0 = iabs(ydel);
				udcc = udr0<APDS9960_GSEN1?0:ydel;
				lrr0 = iabs(xdel);
				lrcc = lrr0<APDS9960_GSEN1?0:xdel;
				if (!udcc) {
					if (lrcc>0) move = MOVE_R;
					else if (lrcc<0) move = MOVE_L;
					/* else, no gesture */
				}
				else if (udcc<0) {
					if (!lrcc||udr0>lrr0) move = MOVE_D;
					else if (lrcc<0) move = MOVE_L;
					else /*if (lrcc>0)*/ move = MOVE_R;
				}
				else {
					/*if (udcc>0)*/
					if (!lrcc||udr0>lrr0) move = MOVE_U;
					else if (lrcc<0) move = MOVE_L;
					else /*if (lrcc>0)*/ move = MOVE_R;
				}
			}
			break;
		}
		i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GFIFO_U,pdat,4);
		if ((pdat[0]>APDS9960_GTH)&&(pdat[1]>APDS9960_GTH)&&
				(pdat[2]>APDS9960_GTH)&&(pdat[3]>APDS9960_GTH)) {
			if (!u1) {
				u1 = pdat[0]; d1 = pdat[1]; l1 = pdat[2]; r1 = pdat[3];
				uart_show_ival("## Size:",size);
				uart_show_ival(", Step:",step);
				uart_show_data(pdat);
			}
			else {
				u0 = pdat[0]; d0 = pdat[1]; l0 = pdat[2]; r0 = pdat[3];
				uart_show_ival("@@ Size:",size);
				uart_show_ival(", Step:",step);
				uart_show_data(pdat);
			}
		}
		step++;
	}
	return move;
}
/*----------------------------------------------------------------------------*/
void main(void) {
	unsigned char temp;
	i2c_init();
	uart_init();
	uart_puts("APDS9960\n");
	i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_ID,&temp,1);
	uart_puts("-- DeviceID:");
	uart_send_hexbyte(temp);
	if (temp==APDS9960_DEV_ID) uart_puts(" => OK");
	uart_send('\n');
	uart_puts("-- Configuring gesture sensor\n");
	apds9960_gesture_init();
	uart_puts("-- Enabling gesture sensor\n");
	apds9960_gesture_mode(1);
	while (1) {
		if (apds9960_gestured()) {
			temp = apds9960_read_gesture();
			/*temp = show_read_gesture();*/
			if (temp) {
				uart_puts("## Gesture:");
				uart_send_hexbyte(temp);
				switch (temp) {
					/* dfrobot module is screwed! */
					case MOVE_U: uart_puts(" => LEFT"); break;
					case MOVE_D: uart_puts(" => RIGHT"); break;
					case MOVE_L: uart_puts(" => UP"); break;
					case MOVE_R: uart_puts(" => DOWN"); break;
				}
				uart_send('\n');
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
