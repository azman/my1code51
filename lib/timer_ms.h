/*----------------------------------------------------------------------------*/
#ifndef __MY1TIMER_MS_H__
#define __MY1TIMER_MS_H__
/*----------------------------------------------------------------------------*/
/**
 * timer_ms.h
 * - extension library for timer
 * - requires exclusive use of timer0
 * - provides millisecond counter
**/
/*----------------------------------------------------------------------------*/
#include "timer.h"
/*----------------------------------------------------------------------------*/
#ifndef TIMER_MSTICK
#define TIMER_MSTICK TIMER_VAL01MS
#endif
#define TIMER_MSFLAG_HOLD 0x01
#define TIMER_MSFLAG_TICK 0x80
/*----------------------------------------------------------------------------*/
unsigned char mstick[4], msflag;
/*----------------------------------------------------------------------------*/
void timerms_handler(void) __interrupt(TF0_VECTOR) {
	timer_prep(TIMER_MSTICK);
	msflag |= TIMER_MSFLAG_HOLD;
	mstick[0]++;
	if (!mstick[0]) {
		mstick[1]++;
		if (!mstick[1]) {
			mstick[2]++;
			if (!mstick[2]) mstick[3]++;
		}
	}
	msflag &= ~TIMER_MSFLAG_HOLD;
	msflag ^= TIMER_MSFLAG_TICK;
}
/*----------------------------------------------------------------------------*/
void timer_ms_init(void) {
	msflag = 0;
	mstick[0] = 0;
	mstick[1] = 0;
	mstick[2] = 0;
	mstick[3] = 0;
	timer_init();
	timer_prep(TIMER_MSTICK);
	timer_evdo();
	timer_exec();
}
/*----------------------------------------------------------------------------*/
unsigned long timer_ms(void) {
	unsigned long read;
	unsigned char flag, loop, *pchk;
	pchk = (unsigned char*)&read;
	do {
		flag = msflag & TIMER_MSFLAG_TICK;
		for (loop=0;loop<4;loop++)
			pchk[loop] = mstick[loop];
	} while (flag!=(msflag&TIMER_MSFLAG_TICK));
	return read;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1TIMER_MS_H__ */
/*----------------------------------------------------------------------------*/
