/*----------------------------------------------------------------------------*/
#ifndef __MY1PID_H__
#define __MY1PID_H__
/*----------------------------------------------------------------------------*/
typedef struct _my1pid_t {
	float pidP, pidI, pidD;
	float errP, errI, errD, prev, calc;
} my1pid_t;
/*----------------------------------------------------------------------------*/
void pid_init(pid_t* ppid) {
	ppid->pidP = 0.0; ppid->pidI = 0.0; ppid->pidD = 0.0;
	ppid->prev = 0.0;
}
/*----------------------------------------------------------------------------*/
float pid_calc(pid_t* ppid, float vdif, float tdif) {
	ppid->errP = vdif;
	ppid->errI += vdif*tdif;
	ppid->errD = (vdif-ppid->prev)/tdif;
	ppid->prev = vdif;
	ppid->calc = ppid->pidP * ppid->errP;
	ppid->calc += ppid->pidI * ppid->errI;
	ppid->calc += ppid->pidD * ppid->errD;
	return ppid->calc;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1PID_H__ */
/*----------------------------------------------------------------------------*/
