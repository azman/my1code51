/*----------------------------------------------------------------------------*/
#ifndef __MY1ATCMD_H__
#define __MY1ATCMD_H__
/*----------------------------------------------------------------------------*/
/**
 * atcmd.h
 * - library for basic at-command
 * - does not define any specific structure
 * - can simply wait for ok
 * - by default uses uart2stc
 *   = define __ATCMD_ON_UART__ to use uart
**/
/*----------------------------------------------------------------------------*/
#ifdef __ATCMD_ON_UART__
#include "uart.h"
#define atcmd_init uart_init
#define atcmd_send uart_send
#define atcmd_read uart_read
#define atcmd_puts uart_puts
#define atcmd_peek uart_peek
#define atcmd_purge uart_purge
#else
#ifndef BRTCNT_USE
#define BRTCNT_USE BRTCNT_B9600
#endif
#include "uart2stc.h"
#define atcmd_init uart2_init
#define atcmd_send uart2_send
#define atcmd_read uart2_read
#define atcmd_puts uart2_puts
#define atcmd_peek uart2_peek
#define atcmd_purge uart2_purge
#endif
/*----------------------------------------------------------------------------*/
#ifndef ATCMD_LOOP_DELAY
#define ATCMD_LOOP_DELAY 950
#endif
#ifndef ATCMD_WAIT_DELAY
#define ATCMD_WAIT_DELAY 1000
#endif
/*----------------------------------------------------------------------------*/
#define ATCMD_OK 0
#define ATCMD_TIMEOUT 0xff
/*----------------------------------------------------------------------------*/
#define ATCMD_CMD_BASE "AT\r\n"
#define ATCMD_CMD_ECHO_OFF "ATE0\r\n"
#define ATCMD_RES_OK "OK\r\n"
/*----------------------------------------------------------------------------*/
#define atcmd_find() atcmd_puts(ATCMD_CMD_BASE)
#define atcmd_echo_off() atcmd_puts(ATCMD_CMD_ECHO_OFF)
/*----------------------------------------------------------------------------*/
#define atcmd_buff_ok(buf,cnt) \
	(cnt>=4&&buf[cnt-1]=='\n'&&buf[cnt-2]=='\r'&& \
	buf[cnt-3]=='K'&&buf[cnt-4]=='O')?1:0
#define atcmd_buff_error(buf,cnt) \
	(cnt>=7&&buf[cnt-1]=='\n'&&buf[cnt-2]=='\r'&& \
	buf[cnt-3]=='R'&&buf[cnt-4]=='O'&&buf[cnt-5]=='R'&& \
	buf[cnt-6]=='R'&&buf[cnt-7]=='E')?1:0
/*----------------------------------------------------------------------------*/
unsigned char atcmd_timeout(unsigned int wait) {
	unsigned int loop;
	do {
		loop = ATCMD_LOOP_DELAY;
		do {
			if (atcmd_peek()) return ATCMD_OK;
		} while (--loop);
	} while (--wait);
	return ATCMD_TIMEOUT;
}
/*----------------------------------------------------------------------------*/
unsigned char atcmd_read_wait(unsigned char* buff, unsigned char size) {
	unsigned char loop;
	loop = 0;
	do {
		if (atcmd_timeout(ATCMD_WAIT_DELAY))
			return loop;
		buff[loop++] = atcmd_read();
	} while (loop<size);
	return loop;
}
/*----------------------------------------------------------------------------*/
#define atcmd_wait_ok() atcmd_wait_str(ATCMD_RES_OK,4)
#define atcmd_recv_ok() (atcmd_wait_ok()==ATCMD_OK)
#define atcmd_found() atcmd_recv_ok()
#define atcmd_ok() atcmd_recv_ok()
/*----------------------------------------------------------------------------*/
unsigned char atcmd_wait_str(unsigned char* pstr, unsigned char size) {
	unsigned char loop;
	loop = 0;
	while (loop<size) {
		if (atcmd_timeout(ATCMD_WAIT_DELAY))
			return ATCMD_TIMEOUT;
		if (atcmd_read()==pstr[loop]) loop++;
		else loop = 0;
	}
	return ATCMD_OK;
}
/*----------------------------------------------------------------------------*/
unsigned char atcmd_wait_send(unsigned char* buff, unsigned char size,
		char* pcmd) {
	unsigned char temp;
	atcmd_puts(pcmd);
	temp = atcmd_read_wait(buff,size);
	if (temp==size) temp--;
	else size = temp;
	buff[temp] = 0x0;
	return size;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1ATCMD_H__ */
/*----------------------------------------------------------------------------*/
