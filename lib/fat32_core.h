/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_CORE_H__
#define __MY1FAT32_CORE_H__
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
/*----------------------------------------------------------------------------*/
/**
 * fat32_core.h : gets basic fat32 info
 * - fat location
 * - cluster size
 * - (cluster) data location
 * - root cluster location
**/
/*----------------------------------------------------------------------------*/
#define FAT32_SECTOR_SIZE 512
/*----------------------------------------------------------------------------*/
/* mbr stuff */
#define MBR_PTABLE_OFFSET 446
#define PTABLE_ENTRY_SIZE 16
#define PTABLE_TYPE_OFFSET 4
#define PARTID_FAT32 0x0b
#define PARTID_FAT32_LBA 0x0c
/*----------------------------------------------------------------------------*/
/* vbr stuff */
#define FAT_BYTES_PSEC_OFFSET 11 // 0x0b secs
#define FAT_SECTS_PCLS_OFFSET 13 // 0x0d spc_
#define FAT_RSVD_SECTS_OFFSET 14 // 0x0e rsvs
#define FAT_TABS_COUNT_OFFSET 16 // 0x10 tabc
#define FAT_ROOT_ENTRY_OFFSET 17 // 0x11 rent
#define FAT32_SEC_PFAT_OFFSET 36 // 0x24 fasz 
#define FAT32_ROOTCLUS_OFFSET 44 // 0x2c rclu
/*----------------------------------------------------------------------------*/
/* fat32 cluster index is actually 28-bits? rationale for FAT32_FAILURE */
#define FAT32_NEXT_CLUSTER_MASK 0x0FFFFFFF
#define FAT32_LAST_CLUSTER FAT32_NEXT_CLUSTER_MASK
/* unused clusters */
#define FAT32_FREE_CLUSTER 0x00000000
#define FAT32_RSVD_CLUSTER 0x00000001
/* some use this? use as mask? */
#define FAT32_LA5T_CLUSTER 0x0FFFFFF8
#define FAT32_LAST_MASK FAT32_LA5T_CLUSTER
/* because this is used as mark for bad cluster */
#define FAT32_BADD_CLUSTER 0x0FFFFFF7
/* practical upper limit? */
#define FAT32_MAXX_CLUSTER 0x0FFFFFF5
/** For 512b sectors: FAT has max 268435446 (0x0ffffff6) clusters */
/** = 4b cluster link size:  128 (0x80) links per sector */
/** = only 0x200000 sectors are needed (sector per fat!) */
#define FAT32_FAT_SECTSIZE 0x00200000
/*----------------------------------------------------------------------------*/
#define FAT32_FAILURE 0xFFFFFFFF
/*----------------------------------------------------------------------------*/
#define fat32_is_last_cluster(clus) ((clus&FAT32_LAST_MASK)==FAT32_LAST_MASK)
#define fat32_is_invalid_cluster(clus) ((clus<2||clus>FAT32_MAXX_CLUSTER))
#define fat32_is_usable_cluster(clus) ((clus>=2&&clus<FAT32_MAXX_CLUSTER))
#define fat32_is_free_cluster(clus) (clus==FAT32_FREE_CLUSTER)
/*----------------------------------------------------------------------------*/
#define FAT32_FLAG_OK 0
#define FAT32_FLAG_ERROR 0x80000000
/*----------------------------------------------------------------------------*/
#define FAT32_MBR_INVALID (FAT32_FLAG_ERROR|0x0001)
#define FAT32_NOT_FOUND (FAT32_FLAG_ERROR|0x0002)
#define FAT32_VBR_INVALID (FAT32_FLAG_ERROR|0x003)
#define FAT32_INVALID_PU16 (FAT32_FLAG_ERROR|0x0004)
#define FAT32_INVALID_FCNT (FAT32_FLAG_ERROR|0x0005)
#define FAT32_INVALID_SSIZE (FAT32_FLAG_ERROR|0x0006)
/*----------------------------------------------------------------------------*/
typedef struct  _tab_t {
	byte08_t flag;
	byte08_t chs0[3];
	byte08_t type;
	byte08_t chs1[3];
	word32_t lba0;
	word32_t nsec;
} tab_t;
/*----------------------------------------------------------------------------*/
typedef struct  _mbr_t {
	byte08_t code[MBR_PTABLE_OFFSET];
	tab_t ptab[4];
	byte08_t id00, id01;
} mbr_t;
/*----------------------------------------------------------------------------*/
typedef struct  _vbr_t {
	byte08_t jmp0[3];
	byte08_t oemn[8];
	word16_t secs; /* 0x0b sector size e.g. 512 => FAT32_SECTOR_SIZE */
	byte08_t spc_; /* 0x0d cluster size @ sector per cluster */
	word16_t rsvs; /* 0x0e reserved sectors */
	byte08_t tabc; /* 0x10 table count - always 2! */
	word16_t rent; /* 0x11 max root entries - 0 for FAT32 */
	byte08_t unk0[17];
	word32_t fasz; /* 0x24 sectors per fat - max 0x200000? */
	byte08_t unk1[4];
	word32_t rclu; /* 0x2c root cluster */
	byte08_t full[462];
	byte08_t id00, id01;
} vbr_t;
/*----------------------------------------------------------------------------*/
typedef struct  _fat32_core_t {
	word32_t flag;
	word32_t ifat; /* starting sector for fat */
	word32_t data; /* starting sector for data cluster */
	word32_t root; /* cluster index for root - 2? */
	/* valid sector per cluster: 2^n (n={0,...,7} */
	byte08_t _spc; /* sector(s) per cluster => cluster size! */
	byte08_t *buff; /* pointer to sector buffer */
} fat32_core_t;
/*----------------------------------------------------------------------------*/
#define PMBR(core) ((mbr_t*)core->buff)
#define PTYPE(buf,num) PMBR(buf)->ptab[num].type
#define PVBR(core) ((vbr_t*)core->buff)
/*----------------------------------------------------------------------------*/
#define part_isfat32(buf,num) (PTYPE(buf,num)==PARTID_FAT32|| \
				PTYPE(buf,num)==PARTID_FAT32_LBA)
#define part_pick(buf,num) buf->root = PMBR(buf)->ptab[num].lba0
/*----------------------------------------------------------------------------*/
void fat32_core_init(fat32_core_t* pfat) {
	/** pfat->buff should be assigned BEFORE calling init! */
	pfat->flag = FAT32_FLAG_OK;
	pfat->_spc = 0;
	pfat->root = 0;
	/* check mbr boot marker */
	if (PMBR(pfat)->id00!=0x55||PMBR(pfat)->id01!=0xaa) {
		pfat->flag |= FAT32_MBR_INVALID;
		return;
	}
	/* check all primary partitions */
	do {
		if (part_isfat32(pfat,0)) { part_pick(pfat,0); break; }
		if (part_isfat32(pfat,1)) { part_pick(pfat,1); break; }
		if (part_isfat32(pfat,2)) { part_pick(pfat,2); break; }
		if (part_isfat32(pfat,3)) { part_pick(pfat,3); break; }
		pfat->flag |= FAT32_NOT_FOUND;
		return;
	} while (0);
}
/*----------------------------------------------------------------------------*/
void fat32_core_load(fat32_core_t* pfat) {
	/* check vbr boot marker */
	if (PVBR(pfat)->id00!=0x55||PVBR(pfat)->id01!=0xaa) {
		pfat->flag |= FAT32_VBR_INVALID;
		return;
	}
	/* 16-bit root entry should be zero for fat32 */
	if (PVBR(pfat)->rent) {
		pfat->flag |= FAT32_INVALID_PU16;
		return;
	}
	/* FAT count always 2! */
	if (PVBR(pfat)->tabc!=2) {
		pfat->flag |= FAT32_INVALID_FCNT;
		return;
	}
	/* only 512-byte sector size is supported */
	if (PVBR(pfat)->secs!=FAT32_SECTOR_SIZE) {
		pfat->flag |= FAT32_INVALID_SSIZE;
		return;
	}
	/* fatst - initial sector + reserved sectors count */
	pfat->ifat = pfat->root + PVBR(pfat)->rsvs;
	/* init sector for data cluster */
	pfat->data = PVBR(pfat)->fasz;
	pfat->data <<= 1; /* because FAT count is always 2! */
	pfat->data += pfat->ifat;
	/* keep sector-per-cluster info */
	pfat->_spc = PVBR(pfat)->spc_;
	/* root cluster */
	pfat->root = PVBR(pfat)->rclu;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_CORE_H__ */
/*----------------------------------------------------------------------------*/
