/*----------------------------------------------------------------------------*/
#ifndef __GPS_DATA_H__
#define __GPS_DATA_H__
/*----------------------------------------------------------------------------*/
/**
 * gps_data.h
 * - extension library for gps
 * - requires utils_float
**/
/*----------------------------------------------------------------------------*/
#include "gps.h"
#ifndef FPSIZE
#define FPSIZE 6
#endif
#include "utils_float.h"
/*----------------------------------------------------------------------------*/
typedef struct _gps_data_t {
	float dlat, dlon, dvel, ddir;
	unsigned long dutc, tutc;
	unsigned char stat, flag;
	unsigned char icnt; /* for gprmc, icnt = 12! */
	/* debug */
	unsigned char ilon, ilat, ivel, idir;
} gps_data_t;
/*----------------------------------------------------------------------------*/
#define GPSSTAT_VALID 0x01
#define GPSSTAT_ERROR 0x10
/*----------------------------------------------------------------------------*/
#define GPSFLAG_LAT_S 0x01
#define GPSFLAG_LON_W 0x02
/*----------------------------------------------------------------------------*/
void swap_byte(unsigned char *p1, unsigned char *p2) {
	unsigned char temp;
	temp = *p1;
	*p1 = *p2;
	*p2 = temp;
}
/*----------------------------------------------------------------------------*/
unsigned char gps_nmea_gprmc(char* buff, gps_data_t* gpsd) {
	unsigned char curr, prev, pdot;
	curr = 0; prev = 0; pdot = 0;
	gpsd->stat = 0; gpsd->icnt = 0;
	gpsd->ilon = 0; gpsd->ilat = 0;
	while (buff[curr]!='\n') {
		if (buff[curr]==',') {
			buff[curr] = 0x0;
			if (curr>prev) { /* in case it is empty */
				if (gpsd->icnt==1) { /* utc time */
					gpsd->tutc = str2uint(&buff[prev]);
				} else if (gpsd->icnt==2) { /* validity */
					if (curr-prev==1&&buff[prev]=='A')
						gpsd->stat |= GPSSTAT_VALID;
				} else if (gpsd->icnt==3) { /* lat */
					gpsd->ilat = prev; /* debug */
					/* gpsd->dlat = str2float(&buff[prev]); */
					gpsd->dlat = str2float(&buff[pdot-2]);
					gpsd->dlat /= 60.0;
					buff[pdot] = buff[pdot-2]; // save that
					buff[pdot-2] = 0x0;
					gpsd->dlat += (float)str2int(&buff[prev]);
					buff[pdot-2] = buff[pdot]; // restore
					buff[pdot] = '.';
				} else if (gpsd->icnt==4) { /* n/s */
					if (curr-prev!=1||
							(buff[prev]!='N'&&buff[prev]!='S')) {
						gpsd->stat |= GPSSTAT_ERROR;
					} else {
						if (buff[prev]=='S')
							gpsd->flag |= GPSFLAG_LAT_S;
						else /** if (buff[prev]=='N') */
							gpsd->flag &= ~GPSFLAG_LAT_S;
					}
				} else if (gpsd->icnt==5) { /* lng */
					gpsd->ilon = prev; /* debug */
					/* gpsd->dlon = str2float(&buff[prev]); */
					gpsd->dlon = str2float(&buff[pdot-2]);
					gpsd->dlon /= 60.0;
					buff[pdot] = buff[pdot-2]; // save that
					buff[pdot-2] = 0x0;
					gpsd->dlon += (float)str2int(&buff[prev]);
					buff[pdot-2] = buff[pdot]; // restore
					buff[pdot] = '.';
				} else if (gpsd->icnt==6) { /* e/w */
					if (curr-prev!=1||
							(buff[prev]!='E'&&buff[prev]!='W')) {
						gpsd->stat |= GPSSTAT_ERROR;
					} else {
						if (buff[prev]=='W')
							gpsd->flag |= GPSFLAG_LON_W;
						else /** if (buff[prev]=='E') */
							gpsd->flag &= ~GPSFLAG_LON_W;
					}
				} else if (gpsd->icnt==7) { /* speed over ground (knots) */
					gpsd->ivel = prev;
				} else if (gpsd->icnt==8) { /* course over ground (degree) */
					gpsd->idir = prev;
				} else if (gpsd->icnt==9) { /* utc date */
					swap_byte(&buff[prev],&buff[prev+4]);
					swap_byte(&buff[prev+1],&buff[prev+5]);
					gpsd->dutc = str2uint(&buff[prev]);
				}
				/* 10:magnetic variation (degrees) */
				/* 11: ??? */
				/* 12:checksum data begins with '*' */
			}
			gpsd->icnt++;
			prev = curr+1;
			pdot = 0;
		} else if (buff[curr]=='.') pdot = curr;
		curr++;
	}
	return (gpsd->stat&GPSSTAT_VALID);
}
/*----------------------------------------------------------------------------*/
#endif /** __GPS_DATA_H__ */
/*----------------------------------------------------------------------------*/
