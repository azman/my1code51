/*----------------------------------------------------------------------------*/
#ifndef __MY1CSTR_CONV_H__
#define __MY1CSTR_CONV_H__
/*----------------------------------------------------------------------------*/
/**
 * cstr_conv.h
 * - converts values to/from string (cstr object)
**/
/*----------------------------------------------------------------------------*/
#include "cstr.h"
/*----------------------------------------------------------------------------*/
void cstr_append_uint8(my1cstr_t* cstr, unsigned char that) {
	unsigned char what, temp;
	what = 0;
	temp = that/100;
	if (temp) {
		that %= 100;
		cstr_add1(cstr,(temp+0x30));
		what++;
	}
	temp = that/10;
	if (temp) {
		that %= 10;
		cstr_add1(cstr,(temp+0x30));
	}
	else if (what) /* need zero if hundreds exists */
		cstr_add1(cstr,0x30);
	cstr_add1(cstr,(that+0x30));
}
/*----------------------------------------------------------------------------*/
void cstr_append_int8(my1cstr_t* cstr, signed char that) {
	if (that<0) {
		cstr_add1(cstr,'-');
		that = -that;
	}
	cstr_append_uint8(cstr,(unsigned char)that);
}
/*----------------------------------------------------------------------------*/
void cstr_append_uint16(my1cstr_t* cstr, unsigned int that) {
	unsigned int test;
	unsigned char what, temp;
	what = 0; test = 10000;
	while (test>=10) {
		temp = (unsigned char)(that/test);
		if (temp) {
			that %= test;
			cstr_add1(cstr,(temp+0x30));
			what++;
		}
		else if (what)
			cstr_add1(cstr,0x30);
		test /= 10;
	}
	cstr_add1(cstr,(that+0x30));
}
/*----------------------------------------------------------------------------*/
void cstr_append_int16(my1cstr_t* cstr, int that) {
	if (that<0) {
		cstr_add1(cstr,'-');
		that = -that;
	}
	cstr_append_uint16(cstr,(unsigned int)that);
}
/*----------------------------------------------------------------------------*/
void cstr_make_uint8(my1cstr_t* cstr, unsigned char* pval) {
	unsigned char loop;
	*pval = 0; loop = cstr->offs;
	while (cstr->buff[loop]) {
		if (cstr->buff[loop]<0x30||cstr->buff[loop]>0x39)
			break;
		*pval = (*pval*10) + (cstr->buff[loop]-0x30);
		loop++;
	}
}
/*----------------------------------------------------------------------------*/
void cstr_make_uint16(my1cstr_t* cstr, unsigned int* pval) {
	unsigned char loop;
	*pval = 0; loop = cstr->offs;
	while (cstr->buff[loop]) {
		if (cstr->buff[loop]<0x30||cstr->buff[loop]>0x39)
			break;
		*pval = (*pval*10) + (cstr->buff[loop]-0x30);
		loop++;
	}
}
/*----------------------------------------------------------------------------*/
void cstr_make_int16(my1cstr_t* cstr, int* pval) {
	unsigned char temp;
	if (cstr->buff[cstr->offs]=='-') {
		cstr->offs++;
		temp = 1;
	}
	else temp = 0;
	cstr_make_uint16(cstr,(unsigned int*)pval);
	if (temp) {
		cstr->offs--;
		*pval = -(*pval);
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1CSTR_CONV_H__ */
/*----------------------------------------------------------------------------*/
