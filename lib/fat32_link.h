/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_LINK_H__
#define __MY1FAT32_LINK_H__
/*----------------------------------------------------------------------------*/
#include "fat32_disk.h"
/*----------------------------------------------------------------------------*/
/**
 * fat32_link.h : fat32 cluster link
 * - data structure and functions to load cluster data and link
 * - call fat32_link_iserror(link) to check if op failed
**/
/*----------------------------------------------------------------------------*/
/* sector_size = 512, link_size = 4 => thus, 128 links per sector */
#define FAT32_LINK_SIZE 4
#define FAT32_LINKCOUNT (FAT32_SECTOR_SIZE/FAT32_LINK_SIZE)
/*----------------------------------------------------------------------------*/
#define FAT32_ENTRYSIZE 32
#define FAT32_ENTRYPSEC (FAT32_SECTOR_SIZE/FAT32_ENTRYSIZE)
/*----------------------------------------------------------------------------*/
#define fat32_cl2sect(pick) (((pick-2)*disk.core._spc)+disk.core.data)
/*----------------------------------------------------------------------------*/
#define FAT32_LINK_ERROR (FAT32_FLAG_ERROR|0x0080)
#define FAT32_LINK_LOAD_ERROR (FAT32_FLAG_ERROR|0x0090)
#define FAT32_LINK_NEXT_ERROR (FAT32_FLAG_ERROR|0x00A0)
#define FAT32_LINK_N3XT_ERROR (FAT32_FLAG_ERROR|0x00B0)
#define FAT32_LINK_GET_ERROR (FAT32_FLAG_ERROR|0x00C0)
#define FAT32_LINK_SET_ERROR (FAT32_FLAG_ERROR|0x00D0)
/*----------------------------------------------------------------------------*/
#define FAT32_LINKFLAG_MODIFIED 0x80
#define FAT32_LINKFLAG_OK 0x40
/* this is intentionally made the same as FAT32_ATTR_SUB_DIR */
#define FAT32_LINKFLAG_MAKEPATH 0x10
#define FAT32_LINKFLAG_ERRORMASK 0x0F
#define FAT32_LINKFLAG_ERROR0 0x01
#define FAT32_LINKFLAG_ERROR1 0x02
/*----------------------------------------------------------------------------*/
typedef struct  _fat32_link_t {
	word32_t cls1; /* first cluster in this cluster chain */
	word32_t curr; /* current cluster for loaded sector */
	word32_t load; /* load/next functions */
	word32_t free, tmp2;
	word32_t tmp1; /* for link_{get|set} */
	word32_t *pu32; /* for link_{get|set} */
	byte08_t *info; /* 32-bytes dir entry */
	byte08_t *pent;
	byte08_t isec; /* currect sector index in current cluster */
	byte08_t flag;
} fat32_link_t;
/*----------------------------------------------------------------------------*/
#define FAT32LINK(link) ((fat32_link_t*)link)
/*----------------------------------------------------------------------------*/
#define fat32_link_iserror(link) \
	(FAT32LINK(link)->flag&FAT32_LINKFLAG_ERRORMASK)
#define fat32_link_ismodified(link) \
	(FAT32LINK(link)->flag&FAT32_LINKFLAG_MODIFIED)
#define fat32_link_modified(link) \
	FAT32LINK(link)->flag |= FAT32_LINKFLAG_MODIFIED
#define fat32_link_updated(link) \
	FAT32LINK(link)->flag &= ~FAT32_LINKFLAG_MODIFIED
#define fat32_link_isOK(link) \
	(FAT32LINK(link)->flag&FAT32_LINKFLAG_OK)
#define fat32_link_OK(link) \
	FAT32LINK(link)->flag |= FAT32_LINKFLAG_OK
#define fat32_link_waitOK(link) \
	FAT32LINK(link)->flag &= ~FAT32_LINKFLAG_OK
#define fat32_link_eclear(link) \
	FAT32LINK(link)->flag &= ~FAT32_LINKFLAG_ERRORMASK
#define fat32_link_error0(link) \
	FAT32LINK(link)->flag |= FAT32_LINKFLAG_ERROR0
#define fat32_link_prep(link,clus) {\
	FAT32LINK(link)->cls1 = clus; \
	FAT32LINK(link)->load = clus; \
	FAT32LINK(link)->curr = 0; \
	FAT32LINK(link)->isec = 0; \
	FAT32LINK(link)->flag = 0; \
	}
#define fat32_link_redo(link) { \
	FAT32LINK(link)->load = FAT32LINK(link)->cls1; \
	FAT32LINK(link)->curr = 0; \
	FAT32LINK(link)->isec = 0; \
	}
/*----------------------------------------------------------------------------*/
void fat32_link_load(fat32_link_t* link) {
	fat32_link_eclear(link);
	link->isec = 0;
	if (disk.load(fat32_cl2sect(link->load))) {
		fat32_errorcode(FAT32_LINK_LOAD_ERROR);
		fat32_link_error0(link);
		return;
	}
	link->curr = link->load;
	link->isec = 1; /* next sector to load */
}
/*----------------------------------------------------------------------------*/
void fat32_link_get(fat32_link_t* link) {
	fat32_link_eclear(link);
	link->tmp1 = link->curr/FAT32_LINKCOUNT; /* fat sector offset */
	link->tmp1 += disk.core.ifat;
	if (disk.load(link->tmp1)) {
		fat32_errorcode(FAT32_LINK_GET_ERROR);
		fat32_link_error0(link);
		return;
	}
	link->tmp1 = link->curr%FAT32_LINKCOUNT; /* link offset */
	link->pu32 = (word32_t*)&disk.buff[link->tmp1<<2];
}
/*----------------------------------------------------------------------------*/
void fat32_link_set(fat32_link_t* link, word32_t mark) {
	/* set fat cluster link table - should call fat32_link_get first! */
	fat32_link_eclear(link);
	link->pu32[0] = mark;
	if (disk.save(disk.csec)) {
		fat32_errorcode(FAT32_LINK_SET_ERROR);
		fat32_link_error0(link);
	}
}
/*----------------------------------------------------------------------------*/
void fat32_link_next(fat32_link_t* link) {
	fat32_link_eclear(link);
	link->load = 0;
	if (link->isec<disk.core._spc) {
		if (disk.load(disk.csec+1)) {
			fat32_errorcode(FAT32_LINK_NEXT_ERROR);
			fat32_link_error0(link);
			return;
		}
		link->isec++;
		link->load = link->curr; /* success flag */
		return; /* still same cluster */
	}
	fat32_link_get(link);
	if (fat32_link_iserror(link)) return;
	if (!fat32_is_last_cluster(link->pu32[0])) {
		/* do we need this check? */
		if (fat32_is_invalid_cluster(link->pu32[0])) {
			fat32_errorcode(FAT32_LINK_N3XT_ERROR);
			fat32_link_error0(link);
			return;
		}
		link->load = link->pu32[0];
		fat32_link_load(link);
		if (fat32_link_iserror(link))
			link->load = 0; /* just in case */
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_LINK_H__ */
/*----------------------------------------------------------------------------*/
