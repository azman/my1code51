/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_SD_H__
#define __MY1FAT32_SD_H__
/*----------------------------------------------------------------------------*/
#include "fat32_disk.h"
#include "sdcard.h"
/*----------------------------------------------------------------------------*/
#define DISKACCESS_OK 0
#define DISKACCESS_ERROR 1
/*----------------------------------------------------------------------------*/
unsigned char sdload_sector(word32_t pick) {
	disk.csec = pick;
	sdcard_read_sector(pick);
	if (card.flag==SDCARD_OK)
		return DISKACCESS_OK;
	fat32_errorcode(FAT32_DISK_RD_ERROR);
	return DISKACCESS_ERROR;
}
/*----------------------------------------------------------------------------*/
unsigned char sdsave_sector(word32_t pick) {
	disk.csec = pick;
	sdcard_write_sector(pick);
	if (card.flag==SDCARD_OK)
		return DISKACCESS_OK;
	fat32_errorcode(FAT32_DISK_WR_ERROR);
	return DISKACCESS_ERROR;
}
/*----------------------------------------------------------------------------*/
void fat32_disk_prep(void) {
	disk.load = sdload_sector;
	disk.save = sdsave_sector;
	disk.buff = card.buff;
	disk.core.flag = 0; /* will be override in fat32_disk_init */
	sdcard_prep();
	if (card.flag!=SDPREP_OK)
		fat32_errorcode(FAT32_DISK_ERROR);
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_SD_H__ */
/*----------------------------------------------------------------------------*/
