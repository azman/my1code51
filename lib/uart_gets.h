/*----------------------------------------------------------------------------*/
#ifndef __UART_GETS_H__
#define __UART_GETS_H__
/*----------------------------------------------------------------------------*/
/**
 * uart_gets.h
 * - extension library for uart_{core,read}.h
 * - uart_gets is not commonly needed, so provided as extension only
 *   = this version stops at newline char or when size is read
**/
/*----------------------------------------------------------------------------*/
#include "uart_send.h"
#include "uart_read.h"
/*----------------------------------------------------------------------------*/
unsigned char uart_gets(char *mesg, unsigned char size) {
	unsigned char temp, loop;
	loop = 0;
	do {
		temp = uart_read();
		uart_send(temp);
		if (temp=='\n') {
			if (mesg[loop-1]=='\r')
				loop--;
			break;
		}
		mesg[loop++] = temp;
		if (loop==size) {
			loop--; break;
		}
	} while (1);
	mesg[loop] = 0x0;
	return loop;
}
/*----------------------------------------------------------------------------*/
#endif /** __UART_GETS_H__ */
/*----------------------------------------------------------------------------*/
