/*----------------------------------------------------------------------------*/
#ifndef __MY1ATCMD_READ_H__
#define __MY1ATCMD_READ_H__
/*----------------------------------------------------------------------------*/
/**
 * atcmd_read.h
 * - extension for atcmd
 * - function(s) to process reply
**/
/*----------------------------------------------------------------------------*/
#include "atcmd.h"
/*----------------------------------------------------------------------------*/
unsigned char* atcmd_read_ok(unsigned char* buff, unsigned char size) {
	unsigned char loop;
	if (atcmd_buff_ok(buff,size)) {
		/* strip ok */
		size -= 4;
		buff[size] = 0x0;
		/* trim newline at the end */
		loop = size - 1;
		while (buff[loop]==0x0d||buff[loop]==0x0a) {
			buff[loop] = 0x0;
			loop--; size--;
		}
		/* trim newline at the beginning */
		while (buff[0]==0x0d||buff[0]==0x0a)
			buff++;
	}
	else buff = 0x0;
	return buff;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1ATCMD_READ_H__ */
/*----------------------------------------------------------------------------*/
