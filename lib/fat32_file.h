/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_FILE_H__
#define __MY1FAT32_FILE_H__
/*----------------------------------------------------------------------------*/
#include "fat32_node.h"
/*----------------------------------------------------------------------------*/
#define FAT32_OPTS_APPEND 0x01
/*----------------------------------------------------------------------------*/
#define FAT32_ENTRY_FULL   (FAT32_FLAG_ERROR|0x00010000)
#define FAT32_ENTRY_FAIL   (FAT32_FLAG_ERROR|0x00020000)
#define FAT32_ENTRY_MISS   (FAT32_FLAG_ERROR|0x00030000)
#define FAT32_CLINK_FAIL   (FAT32_FLAG_ERROR|0x00040000)
#define FAT32_ELINK_FAIL   (FAT32_FLAG_ERROR|0x00050000)
#define FAT32_ALINK_FAIL   (FAT32_FLAG_ERROR|0x00060000)
#define FAT32_CLUSTER_FULL (FAT32_FLAG_ERROR|0x00070000)
#define FAT32_NOT_FILE     (FAT32_FLAG_ERROR|0x00080000)
#define FAT32_CLUSTER_FAIL (FAT32_FLAG_ERROR|0x00090000)
#define FAT32_DOWRITE_FAIL (FAT32_FLAG_ERROR|0x000A0000)
#define FAT32_RMLINK_FAIL  (FAT32_FLAG_ERROR|0x000B0000)
#define FAT32_WRNEXT_FAIL  (FAT32_FLAG_ERROR|0x000C0000)
#define FAT32_WRMORE_FAIL  (FAT32_FLAG_ERROR|0x000D0000)
#define FAT32_ENTRY_WHAT   (FAT32_FLAG_ERROR|0x000E0000)
#define FAT32_EDATE_FAIL   (FAT32_FLAG_ERROR|0x000F0000)
/*----------------------------------------------------------------------------*/
typedef struct _fat32_file_t {
	fat32_link_t self;
	char name[FAT32_NAME_BUFFSIZE];
	word32_t clus;
	word32_t stat; /* number of bytes read/written */
	word32_t curr; /* file read position */
	word32_t full; /* file read size */
	byte08_t opts;
	fat32_node_t *pnod;
	/* temporary stuffs */
	word32_t step, tmpf;
	int loop, size;
	int tmpx; /* not used here... free for use by 'top-level' codes */
	byte08_t* data;
} fat32_file_t;
/*----------------------------------------------------------------------------*/
typedef struct _fat32_tmps_t {
	unsigned char loop, temp;
} fat32_tmps_t;
/*----------------------------------------------------------------------------*/
__xdata fat32_file_t file;
__xdata fat32_tmps_t tmps;
/*----------------------------------------------------------------------------*/
#define fat32_file_reset() file.curr = 0
#define fat32_file_data(buf,cnt) { \
		file.data = (byte08_t*)buf; file.size = cnt; \
	}
#define fat32_file_opts(opt) file.opts = opt
#define fat32_file_prep(lbl,buf,cnt,opt) { \
		fat32_file_name(lbl); \
		fat32_file_data(buf,cnt); \
		fat32_file_opts(opt); \
		fat32_file_reset(); \
	}
/*----------------------------------------------------------------------------*/
void fat32_file_name(char* name) {
	file.loop = 0;
	while (name[file.loop]) {
		file.name[file.loop] = name[file.loop];
		file.loop++;
	}
	file.name[file.loop] = 0x0;
}
/*----------------------------------------------------------------------------*/
/* read from a file in file->path - may be repeatedly called */
void fat32_file_read(void) {
	file.stat = 0;
	if (file.size<=0) return; /* make sure we have buffer */
	file.pnod = fat32_node_find_name(file.name);
	if (!file.pnod) {
		fat32_errorcode(FAT32_ENTRY_MISS);
		return;
	}
	if (fat32_node_ispath(file.pnod)) {
		fat32_errorcode(FAT32_NOT_FILE);
		return;
	}
	file.clus = fat32_node_cluster(file.pnod);
	file.full = file.pnod->size;
	fat32_link_prep(&file.self,file.clus);
	fat32_link_load(&file.self);
	if (fat32_link_iserror(&file.self)) {
		fat32_link_eclear(&file.self);
		fat32_errorcode(FAT32_CLUSTER_FAIL);
		return;
	}
	file.tmpf = file.curr;
	file.curr = 0;
	do {
		file.loop = 0;
		while (file.loop<FAT32_SECTOR_SIZE&&file.curr<file.full) {
			file.curr++;
			if (file.curr>file.tmpf) {
				file.data[file.stat++] = disk.buff[file.loop];
				if (file.stat==file.size)
					return;
			}
			file.loop++;
		}
		if (file.curr==file.full)
			break;
		fat32_link_next(&file.self);
		if (fat32_link_iserror(&file.self)) {
			fat32_link_eclear(&file.self);
			fat32_errorcode(FAT32_CLUSTER_FAIL);
			return;
		}
	} while (file.self.load);
}
/*----------------------------------------------------------------------------*/
void fat32_file_link_cluster(void) {
	/* link file.curr to file.clus */
	fat32_link_get(&file.self);
	if (!fat32_is_last_cluster(file.self.pu32[0])) {
		fat32_errorcode(FAT32_CLINK_FAIL);
		return;
	}
	fat32_link_set(&file.self,file.clus);
	if (fat32_link_iserror(&file.self)) {
		fat32_errorcode(FAT32_CLINK_FAIL);
		return;
	}
	file.self.curr = file.clus;
	fat32_link_get(&file.self);
	if (file.self.pu32[0]!=FAT32_FREE_CLUSTER) {
		fat32_errorcode(FAT32_CLINK_FAIL);
		return;
	}
	fat32_link_set(&file.self,FAT32_LAST_CLUSTER);
	if (fat32_link_iserror(&file.self)) {
		fat32_errorcode(FAT32_CLINK_FAIL);
		return;
	}
}
/*----------------------------------------------------------------------------*/
void fat32_file_new_cluster(void) {
	/* find first empty cluster */
	fat32_find_cluster();
	if (!path.free) {
		fat32_errorcode(FAT32_CLUSTER_FULL);
		return;
	}
	file.clus = path.free;
	/* mark as used! */
	file.self.curr = file.clus;
	fat32_link_get(&file.self);
	if (fat32_link_iserror(&file.self)) {
		fat32_link_eclear(&file.self);
		fat32_errorcode(FAT32_CLUSTER_FAIL|0x08);
		return;
	}
	if (file.self.pu32[0]!=FAT32_FREE_CLUSTER) {
		fat32_errorcode(FAT32_CLUSTER_FAIL|0x09);
		return;
	}
	fat32_link_set(&file.self,FAT32_LAST_CLUSTER);
	if (fat32_link_iserror(&file.self)) {
		fat32_link_eclear(&file.self);
		fat32_errorcode(FAT32_CLINK_FAIL);
		return;
	}
	/* get node entry */
	file.pnod = fat32_node_find_name(file.name);
	if (!file.pnod) {
		fat32_errorcode(FAT32_ENTRY_MISS);
		return;
	}
	/* update cluster info */
	file.pnod->clhi = (file.clus>>16)&0xffff;
	file.pnod->clus = file.clus&0xffff;
	/* update that */
	if (disk.save(disk.csec)) return;
	/* load cluster */
	fat32_link_prep(&file.self,file.clus);
	fat32_link_load(&file.self);
	if (fat32_link_iserror(&file.self)) {
		fat32_link_eclear(&file.self);
		fat32_errorcode(FAT32_CLUSTER_FAIL|0x0A);
	}
}
/*----------------------------------------------------------------------------*/
void fat32_file_null(void) {
	file.pnod = fat32_node_find_name(file.name);
	if (file.pnod) {
		file.clus = fat32_node_cluster(file.pnod);
		if (file.clus) {
			fat32_link_prep(&file.self,file.clus);
			file.pnod->size = 0;
			if (disk.save(disk.csec)) {
				fat32_errorcode(FAT32_RMLINK_FAIL);
				return;
			}
			/* set current as last cluster */
			file.self.curr = file.clus;
			fat32_link_get(&file.self);
			if (!fat32_is_last_cluster(file.self.pu32[0])) {
				file.self.curr = file.self.pu32[0]; /* save this */
				fat32_link_set(&file.self,FAT32_LAST_CLUSTER);
				if (fat32_link_iserror(&file.self)) {
					fat32_errorcode(FAT32_CLINK_FAIL);
					return;
				}
				/* release all other clusters */
				do {
					fat32_link_get(&file.self);
					file.self.curr = file.self.pu32[0];
					fat32_link_set(&file.self,FAT32_FREE_CLUSTER);
					if (fat32_is_last_cluster(file.self.curr))
						break;
				} while (1);
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
/* write to a file in file->path */
void fat32_file_write(void) {
	file.stat = 0;
	file.pnod = fat32_node_find_name(file.name);
	/* create if not existing! */
	if (!file.pnod) {
		fat32_path_make_node(file.name);
		if (!fat32_path_OK()) {
			fat32_errorcode(FAT32_ENTRY_FULL);
			return;
		}
		/* the entry should now be valid */
		if (file.size>0) {
			fat32_file_new_cluster();
			if (fat32_error()) return;
		}
		file.curr = 0; /* current total size */
		file.loop = 0; /* current sector free size */
	}
	/* found entry */
	else {
		if (fat32_node_ispath(file.pnod)) {
			fat32_errorcode(FAT32_NOT_FILE);
			return;
		}
		/* keep current total size AND current sector used size */
		file.curr = file.pnod->size;
		file.loop = file.pnod->size%FAT32_SECTOR_SIZE;
		/* access cluster data */
		file.clus = fat32_node_cluster(file.pnod);
		if (!file.clus) {
			if (file.curr>0) {
				fat32_errorcode(FAT32_CLUSTER_FAIL|0x01);
				return;
			}
			/* file.curr & file.loop should be zero! */
			fat32_file_new_cluster();
			if (fat32_error()) return;
		}
		else {
			fat32_link_prep(&file.self,file.clus);
			/* cannot load now... need to change size if overwrite mode */
			if (file.opts&FAT32_OPTS_APPEND) {
				/* append mode - get last cluster */
				file.step = file.curr;
				file.tmpf = (FAT32_SECTOR_SIZE*disk.core._spc);
				file.self.curr = file.clus;
				do {
					fat32_link_get(&file.self);
					if (fat32_is_last_cluster(file.self.pu32[0]))
						break;
					if (fat32_is_invalid_cluster(file.self.pu32[0])) {
						fat32_errorcode(FAT32_CLINK_FAIL);
						return;
					}
					if (file.step<file.tmpf) {
						fat32_errorcode(FAT32_CLUSTER_FAIL|0x02);
						return;
					}
					file.self.curr = file.self.pu32[0];
					file.step -= file.tmpf;
				} while (1);
				/* should we load it? go to last sector? */
				file.self.load = file.self.curr;
				fat32_link_load(&file.self);
				if (fat32_link_iserror(&file.self)) {
					fat32_errorcode(FAT32_CLUSTER_FAIL|0x03);
					return;
				}
				while (file.step>=FAT32_SECTOR_SIZE) {
					fat32_link_next(&file.self);
					if (fat32_link_iserror(&file.self)) {
						fat32_errorcode(FAT32_CLUSTER_FAIL|0x04);
						return;
					}
					file.step -= FAT32_SECTOR_SIZE;
				}
				/* should get last sector in last cluster by now */
				if (!file.self.load) {
					/* no more space! need new cluster in chain! */
					fat32_find_cluster();
					if (!path.free) {
						fat32_errorcode(FAT32_WRMORE_FAIL);
						return;
					}
					file.clus = path.free;
					fat32_file_link_cluster();
					if (fat32_error()) return;
/*
					fat32_link_get(&file.self);
					if (!fat32_is_last_cluser(file.self.pu32[0])) {
						fat32_errorcode(FAT32_CLINK_FAIL);
						return;
					}
					fat32_link_set(&file.self,file.clus);
					if (fat32_link_iserror(&file.self)) {
						fat32_errorcode(FAT32_CLINK_FAIL);
						return;
					}
					file.self.curr = file.clus;
					fat32_link_get(&file.self);
					if (file.self.pu32[0]!=FAT32_FREE_CLUSTER) {
						fat32_errorcode(FAT32_CLINK_FAIL);
						return;
					}
					fat32_link_set(&file.self,FAT32_LAST_CLUSTER);
					if (fat32_link_iserror(&file.self)) {
						fat32_errorcode(FAT32_CLINK_FAIL);
						return;
					}
*/
					file.self.load = file.clus;
					fat32_link_load(&file.self);
					if (fat32_link_iserror(&file.self)) {
						fat32_errorcode(FAT32_CLUSTER_FAIL|0x05);
						return;
					}
				}
			}
			else {
				/* default mode - overwrite file - delete old content */
				file.curr = 0;
				file.loop = 0;
				file.pnod->size = 0;
				if (disk.save(disk.csec)) {
					fat32_errorcode(FAT32_RMLINK_FAIL);
					return;
				}
				/* set current as last cluster */
				file.self.curr = file.clus;
				fat32_link_get(&file.self);
				if (!fat32_is_last_cluster(file.self.pu32[0])) {
					file.self.curr = file.self.pu32[0]; /* save this */
					fat32_link_set(&file.self,FAT32_LAST_CLUSTER);
					if (fat32_link_iserror(&file.self)) {
						fat32_errorcode(FAT32_CLINK_FAIL);
						return;
					}
					/* release all other clusters */
					do {
						fat32_link_get(&file.self);
						file.self.curr = file.self.pu32[0];
						fat32_link_set(&file.self,FAT32_FREE_CLUSTER);
						if (fat32_is_last_cluster(file.self.curr))
							break;
					} while (1);
				}
				fat32_link_redo(&file.self);
				fat32_link_load(&file.self);
				if (fat32_link_iserror(&file.self)) {
					fat32_errorcode(FAT32_CLUSTER_FAIL|0x06);
					return;
				}
			}
		}
	}
	/* start writing */
	while (file.size>0) {
		while (file.loop<FAT32_SECTOR_SIZE&&file.size>0) {
			disk.buff[file.loop] = file.data[0];
			file.loop++; file.data++;
			file.size--; file.curr++;
			file.stat++;
		}
		if (disk.save(disk.csec)) {
			fat32_errorcode(FAT32_DOWRITE_FAIL);
			return;
		}
		if (!file.size) break;
		/* prepare for next sector - reset sector counter */
		file.loop = 0;
		/* next sector? */
		fat32_link_next(&file.self);
		if (fat32_link_iserror(&file.self)) {
			fat32_errorcode(FAT32_WRNEXT_FAIL);
			return;
		}
		/* is it last cluster? */
		if (!file.self.load) {
			fat32_find_cluster();
			if (!path.free) {
				fat32_errorcode(FAT32_WRMORE_FAIL);
				return;
			}
			file.clus = path.free;
			fat32_file_link_cluster();
			if (fat32_error()) return;
/*
			fat32_link_get(&file.self);
			fat32_link_set(&file.self,file.clus);
			if (fat32_link_iserror(&file.self)) {
				fat32_errorcode(FAT32_CLINK_FAIL);
				return;
			}
			file.self.curr = file.clus;
			fat32_link_get(&file.self);
			fat32_link_set(&file.self,FAT32_LAST_CLUSTER);
			if (fat32_link_iserror(&file.self)) {
				fat32_errorcode(FAT32_CLINK_FAIL);
				return;
			}
*/
			file.self.load = file.clus;
			fat32_link_load(&file.self);
			if (fat32_link_iserror(&file.self)) {
				fat32_errorcode(FAT32_CLUSTER_FAIL|0x07);
				return;
			}
		}
	}
	/* write new file size */
	file.pnod = fat32_node_find_name(file.name);
	if (!file.pnod) {
		fat32_errorcode(FAT32_ENTRY_WHAT);
		return;
	}
	file.pnod->size = file.curr;
	/* update that */
	if (disk.save(disk.csec)) {
		fat32_errorcode(FAT32_EDATE_FAIL);
		return;
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_FILE_H__ */
/*----------------------------------------------------------------------------*/
