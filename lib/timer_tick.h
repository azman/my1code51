/*----------------------------------------------------------------------------*/
#ifndef __MY1TIMER_TICK_H__
#define __MY1TIMER_TICK_H__
/*----------------------------------------------------------------------------*/
/**
 * timer_tick.h
 * - extension library for timer
 * - requires exclusive use of timer0
 * - configurable time ticks (default: 1s)
 *   = define TIMER_TICK_LEN and TIMER_TICK_CNT
**/
/*----------------------------------------------------------------------------*/
#include "timer.h"
/*----------------------------------------------------------------------------*/
#ifndef TIMER_TICK_LEN
#define TIMER_TICK_LEN TIMER_VAL10MS
#define TIMER_TICK_CNT 100
#endif
/*----------------------------------------------------------------------------*/
/* macro to access ticker variable */
#define timer_ticked() ttickd
#define timer_tick00() ttickd = 0
/* macro to initialize */
#define timer_tick_init0() ttickd=0; tticks=TIMER_TICK_CNT; timer_init()
#define timer_tick_init() timer_tick_init0(); timer_make(TIMER_TICK_LEN)
/* macro to do it all */
#define timer_tick_exec() timer_tick_init(); timer_exec()
/*----------------------------------------------------------------------------*/
unsigned char ttickd, tticks;
/*----------------------------------------------------------------------------*/
/* timer0 interrupt handler */
void timer_handler(void) __interrupt(TF0_VECTOR) {
	timer_prep(TIMER_TICK_LEN); /* reset this a.s.a.p. */
	tticks--;
	if (!tticks) {
		tticks = TIMER_TICK_CNT;
		ttickd++;
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1TIMER_TICK_H__ */
/*----------------------------------------------------------------------------*/
