/*----------------------------------------------------------------------------*/
#ifndef __MY1UART_CORE_H__
#define __MY1UART_CORE_H__
/*----------------------------------------------------------------------------*/
/**
 * uart_core.h
 * - library for core uart (rs232) configuration
 * - requires exclusive use of timer 1
 * - baudrate9600 is for FOSC = 11.0592MHz (TH1=253)
 *   > fclk = FOSC/12 = 0.9216MHz = 921600Hz
 *   > pfix = 2^smod/32 = 1/32 (by default smod = 0)
 *   > fcnt = pfix * fclk = 28800
 *   = for baud = 9600
 *   > tcnt = fcnt / baud = 28800 / 9600 = 3
 *   > th1 = 256 - tcnt = 253
 * - define UART_BAUD_T1 to set other values
 * - only init function defined
**/
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
/*----------------------------------------------------------------------------*/
#define T1CNT_VALUE(step) (unsigned char)(256-step)
#define T1CNT_SMOD0(baud) (unsigned char)(((MCU51_FOSC/12)/32)/baud)
/*----------------------------------------------------------------------------*/
#define T1CNT_BAUD9600 T1CNT_VALUE(T1CNT_SMOD0(9600))
/*----------------------------------------------------------------------------*/
#ifndef UART_BAUD_T1
#define UART_BAUD_T1 T1CNT_BAUD9600
#endif
/*----------------------------------------------------------------------------*/
void uart_init(void) {
	/* using T1 for baudrate control */
	TMOD &= 0x0F; TMOD |= 0x20;
	SCON = 0x50; TH1 = UART_BAUD_T1; TR1 = 1;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1UART_CORE_H__ */
/*----------------------------------------------------------------------------*/
