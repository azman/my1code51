/*----------------------------------------------------------------------------*/
#ifndef __MY1MATH_SQRT_H__
#define __MY1MATH_SQRT_H__
/*----------------------------------------------------------------------------*/
/**
 *  math_sqrt.h - provides sqrt
 */
/*----------------------------------------------------------------------------*/
#define SQRT_MAXLOOP 25
#define SQRT_MINDIFF 0.0000001
/*----------------------------------------------------------------------------*/
#define sqrt(x) my1sqrt(x)
/*----------------------------------------------------------------------------*/
#include "vars_math.h"
/*----------------------------------------------------------------------------*/
float my1sqrt(float that) {
	trig_temp = that/2; /* initial estimate */
	for (math_loop=0;math_loop<SQRT_MAXLOOP;math_loop++) {
		trig_full = (trig_temp + that/trig_temp)/2;
		trig_temp = trig_full - trig_temp;
		if (trig_temp<0.0) trig_temp = -trig_temp;
		if (trig_temp<SQRT_MINDIFF) break;
		trig_temp = trig_full;
	}
	return trig_full;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1MATH_SQRT_H__ */
/*----------------------------------------------------------------------------*/
