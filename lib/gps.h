/*----------------------------------------------------------------------------*/
#ifndef __GPS_H__
#define __GPS_H__
/*----------------------------------------------------------------------------*/
/**
 * gps.h
 * - library for gps
 *   = provides gps_wait (waits for nmea gprmc)
 * - depends on uart
 *   = can be set to use uart2stc instead (default baud: 9600)
**/
/*----------------------------------------------------------------------------*/
#ifndef __GPS_USE_UART2STC__
#include "uart.h"
#define gps_init() uart_init()
#define gps_read() uart_read()
#define gps_peek() uart_peek()
#else
#ifndef BRTCNT_USE
#define BRTCNT_USE BRTCNT_B9600
#endif
#include "uart2stc.h"
#define gps_init() uart2_init()
#define gps_read() uart2_read()
#define gps_peek() uart2_peek()
#endif
/*----------------------------------------------------------------------------*/
unsigned char gps_wait(char* buff, unsigned char size) {
	unsigned char curr, loop;
	loop = 0;
	while (1) {
		curr = gps_read();
		if (!loop&&curr!='$') continue;
		buff[loop++] = curr;
		if (loop==size) { /* make sure within buffer size */
			loop = 0;
			continue;
		}
		if (loop==6) { /* look for GPRMC or GNRMC */
			if (buff[1]!='G'||(buff[2]!='P'&&buff[2]!='N')||
					buff[3]!='R'||buff[4]!='M'||buff[5]!='C') {
				loop = 0;
				continue;
			}
		}
		if (curr=='\n') { /* look for newline char */
			buff[loop] = 0x0;
			break;
		}
	}
	return loop;
}
/*----------------------------------------------------------------------------*/
#endif /** __GPS_H__ */
/*----------------------------------------------------------------------------*/
