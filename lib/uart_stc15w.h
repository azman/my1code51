/*----------------------------------------------------------------------------*/
#ifndef __UART_STC15W_H__
#define __UART_STC15W_H__
/*----------------------------------------------------------------------------*/
/**
 * uart_stc15w.h
 * - library for uart1 on stc15w
 *   = maintain same uart feature 9600-8n1
 *   = has different baud generator settings (timer2)
 *   = timer2 seems to have similar settings with brt
 * uart1 using t2 as baudrate gen
 * > assume 12T clock
 * > baud = MCU51_FOSC / 12 / (65536-load) / 4
 *   load = 65536-(MCU51_FOSC/12/4/baud)
 * > assume 11059200 Hz
 *   load = 65536-(11059200/12/4/baud)
 *   load = 65536-(230400/baud)
 * > for baud = 9600
 *   t2 reload = 65536 - 24 = 0xffe8
 * > assume 1T clock
 *   baud = MCU51_FOSC / (65536-load) / 4
 * > assume 11059200 Hz
 *   load = 65536-(2764800/baud)
 * > for baud = 9600
 *   t2 reload = 65536 - 288 = 0xfee0
**/
/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "uart.h"
/*----------------------------------------------------------------------------*/
#define STC15W_BAUD115200 0xffe8
#define STC15W_BAUD57600 0xffd0
#define STC15W_BAUD38400 0xffb8
#define STC15W_BAUD19200 0xff70
#define STC15W_BAUD9600 0xfee0
/*----------------------------------------------------------------------------*/
#define uart_init_stc15w() uart_init_stc15w_baud(STC15W_BAUD9600)
/*----------------------------------------------------------------------------*/
void uart_init_stc15w_baud(unsigned int pick) {
	SCON = 0x50;
	TH2 = (pick>>8)&0xff;
	TL2 = pick&0xff;
	AUXR = T2_4S1; /* using T2 as baurate gen for uart1 */
	AUXR |= T2x12; /* 1T clock */
	AUXR |= TR2; /* run t2 */
}
/*----------------------------------------------------------------------------*/
#endif /** __UART_STC15W_H__ */
/*----------------------------------------------------------------------------*/
