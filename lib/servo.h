/*----------------------------------------------------------------------------*/
#ifndef __MY1SERVO_H__
#define __MY1SERVO_H__
/*----------------------------------------------------------------------------*/
/**
 * servo.h
 * - basic servo pwm library
 * - requires timer.h
**/
/*----------------------------------------------------------------------------*/
#include "timer.h"
/*----------------------------------------------------------------------------*/
/** allow code to override these pins! */
#ifndef SERVO_PIN
#define SERVO_PIN P2_0
#endif
/* pulse count */
#ifndef SERVO_TCNT
#define SERVO_TCNT 50
#endif
/*----------------------------------------------------------------------------*/
#define servo_init()  SERVO_PIN = 0
/*----------------------------------------------------------------------------*/
void servo_step(unsigned char wide) {
	unsigned char full;
	full = 200-wide; /* in 0.1ms counts: 15 <= wide <= 25 */
	SERVO_PIN = 1;
	while (wide) {
		timer_wait(0xffa3); // 0.1ms
		wide--;
	}
	SERVO_PIN = 0;
	while (full) {
		timer_wait(0xffa3); // 0.1ms
		full--;
	}
}
/*----------------------------------------------------------------------------*/
void servo_turn(unsigned char wide) {
	unsigned char tlen;
	tlen = SERVO_TCNT;
	while (tlen>0) {
		servo_step(wide);
		tlen--;
	}
}/*----------------------------------------------------------------------------*/
#endif /** __MY1SERVO_H__ */
/*----------------------------------------------------------------------------*/
