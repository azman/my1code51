/*----------------------------------------------------------------------------*/
#ifndef __MY1SWRESET_H__
#define __MY1SWRESET_H__
/*----------------------------------------------------------------------------*/
/**
 * swreset.h
 * - soft reset for system
 * - useful for system reset due to software trap
**/
/*----------------------------------------------------------------------------*/
void swreset(void) {
#ifdef __USE_KEIL__
/* keil syntax - NOT TESTED! */
#pragma asm
	pop acc
	pop acc
	clr a
	push acc
	push acc
	reti
#pragma endasm
#else
/* sdcc syntax */
#if 1
	__asm
	pop acc
	pop acc
	clr a
	push acc
	push acc
	reti
	__endasm;
#else
/** new format: **/
	__asm__ ("pop acc\n"
		"pop acc\n"
		"clr a\n"
		"push acc\n"
		"push acc\n"
		"reti\n");
#endif
#endif
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SWRESET_H__ */
/*----------------------------------------------------------------------------*/
