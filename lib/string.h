/*----------------------------------------------------------------------------*/
#ifndef __MY1STRING_H__
#define __MY1STRING_H__
/*----------------------------------------------------------------------------*/
/**
 * string.h
 * - basic string processing
**/
/*----------------------------------------------------------------------------*/
#include "vars_string.h"
/*----------------------------------------------------------------------------*/
signed char strncmp(char* src1, char* src2, unsigned char size) {
	/* strncmp has a size limit */
	for (str_loop=0,str_stat=0;str_loop<size;str_loop++) {
		/* check if null comes first */
		if (!(*src1)) {
			if ((*src2)) str_stat = -1; /* src2 longer string */
			/* else? stop at same time => equal! */
			break;
		}
		if (!(*src2)) {
			if ((*src1)) str_stat = 1; /* src1 longer string */
			/* else? stop at same time => equal! */
			break;
		}
		/* actual strcmp! */
		if ((unsigned char)(*src1)>(unsigned char)(*src2)) {
			str_stat = 1; break;
		}
		if ((unsigned char)(*src1)<(unsigned char)(*src2)) {
			str_stat = -1; break;
		}
		src1++; src2++;
	}
	return str_stat;
}
/*----------------------------------------------------------------------------*/
unsigned char strncpy(char* dst, char* src, unsigned char size) {
	for (str_loop=0;str_loop<size;str_loop++) {
		dst[str_loop] = src[str_loop];
		if (!src[str_loop]) break;
	}
	if (str_loop==size) dst[str_loop-1] = 0x0;
	return str_loop;
}
/*----------------------------------------------------------------------------*/
unsigned char strlen(char* pstr) {
	str_loop=0;
	while (pstr[str_loop]) str_loop++;
	return str_loop;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1STRING_H__ */
/*----------------------------------------------------------------------------*/
