/*----------------------------------------------------------------------------*/
#ifndef __MY1MATH_TRIGO_H__
#define __MY1MATH_TRIGO_H__
/*----------------------------------------------------------------------------*/
/**
 *  math_trigo.h - basic trigonometry for 8051
 *  - provides cos/sin functions
 *  - input for cos/sin must 0 -> 2pi
 *     = use limit_radian() if required
*note*: tested on pc against math lib!
  *Error threshold: 5.000000e-04
  *Degree Increment: 0.000100
  *Cycle limit: 1 (-180.000000-180.000000)
Test my1cos function (7|7)
All good from -180.000000-180.000000 degrees
Avg error:3.395452e-06
Max error:1.007780e-04
Test my1sin function (7|7)
All good from -180.000000-180.000000 degrees
Avg error:6.941366e-07
Max error:2.156444e-05
 *  - output here tested against libreoffice calc cos/sin values
 *    = looks ok up until 4 decimal points
 */
/*----------------------------------------------------------------------------*/
#include "vars_math.h"
/*----------------------------------------------------------------------------*/
#define cos(x) my1cos(x)
#define sin(x) my1sin(x)
/*----------------------------------------------------------------------------*/
#define COS_TERMS 7
#define SIN_TERMS 7
/*----------------------------------------------------------------------------*/
#define limit_rad0(rad) while (rad<0) rad+=PERIOD_
#define limit_rad1(rad) while (rad>PERIOD_) rad-=PERIOD_
#define limit_radian(rad) limit_rad0(rad); limit_rad1(rad)
/*----------------------------------------------------------------------------*/
/* TODO: eliminate these! */
/*----------------------------------------------------------------------------*/
float my1cos_core(float radian) {
	trig_next = 1.0, trig_full = 0.0;
	trig_temp = radian*radian;
	trig_temp = -trig_temp;
	math_loop = 0;
	while (math_loop<COS_TERMS) {
		trig_full += trig_next;
		trig_ival = (++math_loop)<<1;
		trig_ival = trig_ival*(trig_ival-1);
		trig_next *= trig_temp/(float)trig_ival;
	}
	return trig_full;
}
/*----------------------------------------------------------------------------*/
float my1sin_core(float radian) {
	trig_next = radian, trig_full = 0.0;
	trig_temp = radian*radian;
	trig_temp = -trig_temp;
	math_loop = 0;
	while (math_loop<SIN_TERMS) {
		trig_full += trig_next;
		trig_ival = (++math_loop)<<1;
		trig_ival = trig_ival*(trig_ival+1);
		trig_next *= trig_temp/(float)trig_ival;
	}
	return trig_full;
}
/*----------------------------------------------------------------------------*/
#define CYCLEQ1 0
#define CYCLEQ2 1
#define CYCLEQ3 2
#define CYCLEQ4 3
/*----------------------------------------------------------------------------*/
float my1cos(float radian) {
	/* find 0 -> pi/2 quadrant */
	if (radian<HALF_PI) math_pick = CYCLEQ1;
	else if (radian<MATH_PI) {
		radian = MATH_PI-radian;
		math_pick = CYCLEQ2;
	}
	else if (radian<(MATH_PI+HALF_PI)) {
		radian = radian-MATH_PI;
		math_pick = CYCLEQ3;
	}
	else {
		radian = PERIOD_-radian;
		math_pick = CYCLEQ4;
	}
	/* get for 0 -> pi/4 , use sin if > pi/4 */
	if (radian<QUAD_PI) trig_temp = my1cos_core(radian);
	else trig_temp = my1sin_core(HALF_PI-radian);
	/* assign sign based on quadrant */
	if ((math_pick==CYCLEQ2)||(math_pick==CYCLEQ3))
		trig_temp = -trig_temp;
	return trig_temp;
}
/*----------------------------------------------------------------------------*/
float my1sin(float radian) {
	/* find 0 -> pi/2 quadrant */
	if (radian<HALF_PI) math_pick = CYCLEQ1;
	else if (radian<MATH_PI) {
		radian = MATH_PI-radian;
		math_pick = CYCLEQ2;
	}
	else if (radian<(MATH_PI+HALF_PI)) {
		radian = radian-MATH_PI;
		math_pick = CYCLEQ3;
	}
	else {
		radian = PERIOD_-radian;
		math_pick = CYCLEQ4;
	}
	/* get for 0 -> pi/4 , use cos if > pi/4 */
	if (radian<QUAD_PI) trig_temp = my1sin_core(radian);
	else trig_temp = my1cos_core(HALF_PI-radian);
	/* assign sign based on quadrant */
	if ((math_pick==CYCLEQ3)||(math_pick==CYCLEQ4))
		trig_temp = -trig_temp;
	return trig_temp;
}
/*----------------------------------------------------------------------------*/
/**
taylor series (maclaurin) for trigo functions

cos x = sum(0,inf) [ pow(-1,n) * pow(x,2n) / (2n)! ]

*note*
- term[0] = 1
- term[i] = pow(-1,i) * pow(x,2i) / (2i)!
- term[i-1] = pow(-1,i-1) * pow(x,2i-2) / (2i-2)!
thus,
- term[i] = (-1*pow(-1,i-1))*(pow(x,2)*pow(x,2i-2)) / 2i*(2i-1)*(2i-2)!
          = (pow(-1,i-1) * pow(x,2i-2) / (2i-2)!) * (-1 * pow(x,2) / 2i*(2i-1))
          = term[i-1] * (-1 * pow(x,2) / 2i*(2i-1))

sin x = sum(0,inf) [ pow(-1,n) * pow(x,2n+1) / (2n+1)! ]

*note*
- term[0] = x
- term[i] = pow(-1,i) * pow(x,2i+1) / (2i+1)!
- term[i-1] = pow(-1,i-1) * pow(x,2i-1) / (2i-1)!
thus,
- term[i] = (-1*pow(-1,i-1))*(pow(x,2)*pow(x,2i-1)) / (2i+1)*2i*(2i-1)!
          = (pow(-1,i-1) * pow(x,2i-1) / (2i-1)!) * (-1 * pow(x,2) / 2i*(2i+1))
          = term[i-1] * (-1 * pow(x,2) / 2i*(2i+1))
**/
/*----------------------------------------------------------------------------*/
#endif /* __MY1MATH_TRIGO_H__ */
/*----------------------------------------------------------------------------*/
