/*----------------------------------------------------------------------------*/
#ifndef __BYTES_H__
#define __BYTES_H__
/*----------------------------------------------------------------------------*/
/**
 * bytes.h
 * - byte data array
 * - provides my1bytes_t
 */
/*----------------------------------------------------------------------------*/
#ifndef MY1BYTES_SIZE
#define MY1BYTES_SIZE 16
#endif
/*----------------------------------------------------------------------------*/
typedef struct _my1bytes_t {
	unsigned char bdat[MY1BYTES_SIZE];
	unsigned char blen, bcur;
} my1bytes_t;
/*----------------------------------------------------------------------------*/
#define BYTES(pb) ((my1bytes_t*)pb)
/*----------------------------------------------------------------------------*/
#define bytes_here(pb,u8) BYTES(pb)->bdat[BYTES(pb)->blen] = u8
#define bytes_next(pb) BYTES(str)->blen++;
#define bytes_full(pb) (CSTR(str)->blen>=MY1CSTR_SIZE)
#define bytes_put1(pb,u8) bytes_here(pb,u8); bytes_next(pb)
#define bytes_add1(pb,u8) if (!bytes_full(pb)) { bytes_put1(pb,u8); }
/*----------------------------------------------------------------------------*/
void bytes_adds(my1bytes_t* pdat, unsigned char* pins, unsigned char size) {
	while (size) {
		if (bytes_full(pdat)) break;
		bytes_put1(pdat,*pins);
		pins++; size--;
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __BYTES_H__ */
/*----------------------------------------------------------------------------*/
