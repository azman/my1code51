/*----------------------------------------------------------------------------*/
#ifndef __CSTR_H__
#define __CSTR_H__
/*----------------------------------------------------------------------------*/
/**
 * cstr.h
 * - library for basic c string
 * - provides my1cstr_t
 */
/*----------------------------------------------------------------------------*/
#ifndef MY1CSTR_SIZE
#define MY1CSTR_SIZE 64
#endif
#define MY1CSTR_ENDS MY1CSTR_SIZE-1
/*----------------------------------------------------------------------------*/
typedef struct _my1cstr_t {
	char buff[MY1CSTR_SIZE];
	unsigned char blen, offs;
} my1cstr_t;
/*----------------------------------------------------------------------------*/
typedef my1cstr_t cstr_t;
/*----------------------------------------------------------------------------*/
#define CSTR(pc) ((my1cstr_t*)pc)
/*----------------------------------------------------------------------------*/
#define cstr_offs(str) CSTR(str)->offs = 0
#define cstr_empt(str) CSTR(str)->blen = 0
#define cstr_stop(str) CSTR(str)->buff[CSTR(str)->blen] = 0
#define cstr_null(str) cstr_empt(str); cstr_stop(str)
#define cstr_init(str) cstr_null(str); cstr_offs(str)
#define cstr_here(str,chr) CSTR(str)->buff[CSTR(str)->blen] = chr
#define cstr_next(str) CSTR(str)->blen++;
#define cstr_chk1(str) if (CSTR(str)->blen==MY1CSTR_SIZE) CSTR(str)->blen--
#define cstr_append(str,txt) cstr_adds(str,txt)
#define cstr_assign(str,txt) cstr_empt(str); cstr_adds(str,txt)
/*----------------------------------------------------------------------------*/
#define cstr_addchr(ps,ch) cstr_here(ps,ch); cstr_next(pstr)
/*----------------------------------------------------------------------------*/
void cstr_adds(my1cstr_t* pstr, char* text) {
	while (*text) {
		cstr_here(pstr,*text);
		cstr_next(pstr);
		if (pstr->blen==MY1CSTR_SIZE) {
			pstr->blen--;
			break;
		}
		text++;
	}
	cstr_stop(pstr);
}
/*----------------------------------------------------------------------------*/
void cstr_add1(my1cstr_t* pstr, char that) {
	cstr_here(pstr,that);
	cstr_next(pstr);
	cstr_chk1(pstr);
	cstr_stop(pstr);
}
/*----------------------------------------------------------------------------*/
#endif /* __CSTR_H__ */
/*----------------------------------------------------------------------------*/
