/*----------------------------------------------------------------------------*/
#ifndef __MY1LNGLAT_H__
#define __MY1LNGLAT_H__
/*----------------------------------------------------------------------------*/
/**
 *  latlng.h - calculations using longitude/latitude points
 */
/*----------------------------------------------------------------------------*/
typedef struct _latlng_t {
	float lat, lng;
} latlng_t;
/*----------------------------------------------------------------------------*/
typedef struct _latlng_calc_t {
	latlng_t *p1, *p2;
	float calc, temp, tmp1, tmp2;
} latlng_calc_t;
/*----------------------------------------------------------------------------*/
__xdata latlng_calc_t llcalc;
/*----------------------------------------------------------------------------*/
#include "math_trigo.h"
#include "math_sqrt.h"
#include "math_atan.h"
/*----------------------------------------------------------------------------*/
/** earth's radius at equatorial (in meters) */
#define EARTH_RADIUS_M 6378137.0
/** average earth's radius (in km) */
#define EARTH_R_AVG_KM 6371.0
/*----------------------------------------------------------------------------*/
#define latlng_copy(pdst,psrc) { \
	((latlng_t*)pdst)->lat = ((latlng_t*)psrc)->lat; \
	((latlng_t*)pdst)->lng = ((latlng_t*)psrc)->lng; \
	}
/*----------------------------------------------------------------------------*/
void calc_dist1(void) {
	/** https://www.movable-type.co.uk/scripts/latlong.html */
	llcalc.tmp1 = (llcalc.p2->lat-llcalc.p1->lat)/2.0;
	llcalc.calc = deg2rad(llcalc.tmp1);
	llcalc.temp = sin(llcalc.calc);
	llcalc.tmp1 = llcalc.temp * llcalc.temp; // 1st term for a
	llcalc.tmp2 = (llcalc.p2->lng-llcalc.p1->lng)/2.0;
	llcalc.calc = deg2rad(llcalc.tmp2);
	llcalc.temp = sin(llcalc.calc);
	llcalc.tmp2 = llcalc.temp * llcalc.temp;
	llcalc.calc = llcalc.tmp2 * cos(deg2rad(llcalc.p2->lat));
	llcalc.tmp2 = llcalc.calc * cos(deg2rad(llcalc.p1->lat)); // 2nd term for a
	llcalc.temp = llcalc.tmp1 + llcalc.tmp2; // a
	llcalc.tmp1 = sqrt(llcalc.temp); // y term for atan2
	llcalc.tmp2 = sqrt(1.0-llcalc.temp); // x term for atan2
	llcalc.calc = atan2(llcalc.tmp1,llcalc.tmp2);
	llcalc.calc *= 2.0; // c
	llcalc.calc *= EARTH_RADIUS_M; // result in m!
}
/*----------------------------------------------------------------------------*/
void calc_dist3(void) {
	/** https://www.movable-type.co.uk/scripts/latlong.html */
	llcalc.tmp1 = llcalc.p2->lng-llcalc.p1->lng;
	llcalc.tmp1 = deg2rad(llcalc.tmp1);
	llcalc.tmp2 = (llcalc.p1->lat+llcalc.p2->lat)/2.0;
	llcalc.tmp2 = deg2rad(llcalc.tmp2);
	llcalc.calc = cos(llcalc.tmp2);
	llcalc.tmp1 = llcalc.tmp1*llcalc.calc; //X
	llcalc.tmp2 = llcalc.p2->lat-llcalc.p1->lat;
	llcalc.tmp2 = deg2rad(llcalc.tmp2); //Y
	llcalc.calc = llcalc.tmp1*llcalc.tmp1;
	llcalc.temp = llcalc.calc + (llcalc.tmp2*llcalc.tmp2);
	llcalc.temp = sqrt(llcalc.temp);
	llcalc.calc = llcalc.temp*EARTH_RADIUS_M;
}
/*----------------------------------------------------------------------------*/
void calc_bear1(void) {
	/** https://www.movable-type.co.uk/scripts/latlong.html */
	/**
	 * validated @ https://www.igismap.com/map-tool/bearing-angle
	 * (6.46187,100.35103) -> (6.46178,100.35127)
	**/
	llcalc.temp = llcalc.p2->lng-llcalc.p1->lng;
	llcalc.temp = deg2rad(llcalc.temp);
	llcalc.tmp1 = cos(deg2rad(llcalc.p2->lat))*sin(llcalc.temp); //Y
	llcalc.calc = sin(deg2rad(llcalc.p1->lat))*cos(deg2rad(llcalc.p2->lat));
	llcalc.temp = llcalc.calc * cos(llcalc.temp);
	llcalc.tmp2 = cos(deg2rad(llcalc.p1->lat))*sin(deg2rad(llcalc.p2->lat));
	llcalc.tmp2 = llcalc.tmp2 - llcalc.temp; //X
	llcalc.calc = atan2(llcalc.tmp1,llcalc.tmp2); // -180 to 180 in radian
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1LNGLAT_H__ */
/*----------------------------------------------------------------------------*/
