/*----------------------------------------------------------------------------*/
#ifndef __MY1TIMER_KEEP_H__
#define __MY1TIMER_KEEP_H__
/*----------------------------------------------------------------------------*/
/**
 * timer_keep.h
 * - extension library for timer
 * - requires exclusive use of timer0
 * - allows time-keeping stuff
**/
/*----------------------------------------------------------------------------*/
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define TIMEHHH_FULL 24
#define TIMEMIN_FULL 60
#define TIMESEC_FULL 60
/*----------------------------------------------------------------------------*/
/* default is 8am */
#define DEFAULT_HOUR 8
#define DEFAULT_MINS 0
/*----------------------------------------------------------------------------*/
unsigned char tkiter, tktick, tksecs;
unsigned char tkmins, tkhour;
/*----------------------------------------------------------------------------*/
#define timer_keep_read() ((unsigned int)tkhour<<8|tkmins)
#define timer_keep_time() tkiter = TIMER_LOOP_1S; timer_make(TIMER_VAL50MS)
#define timer_keep_init() timer_init(); timer_keep_time(); tktick = 0
#define timer_keep_secs(ss) tksecs = ss
#define tkset_mins(mm) tkmins = mm
#define tkset_hour(hh) tkhour = hh
#define timer_keep_make(hm) tkset_mins(hm&0xff); tkset_hour((hm>>8)&0xff)
#define timer_keep_curr(h,m,s) tksecs = s; tkmins = m; tkhour = h
#define timer_keep_prep(h,m,s) timer_keep_init(); timer_keep_curr(h,m,s)
#define timer_keep_exec timer_exec
/*----------------------------------------------------------------------------*/
void timer_keeper(void) __interrupt(TF0_VECTOR) {
	timer_prep(TIMER_VAL50MS); /* reset this a.s.a.p. */
	tkiter--;
	if (!tkiter) {
		tkiter = TIMER_LOOP_1S;
		tktick++; tksecs++;
		if (tksecs==TIMESEC_FULL) {
			tksecs = 0; tkmins++;
			if (tkmins==TIMEMIN_FULL) {
				tkmins = 0; tkhour++;
				if (tkhour==TIMEHHH_FULL)
					tkhour = 0;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1TIMER_KEEP_H__ */
/*----------------------------------------------------------------------------*/
