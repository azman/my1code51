/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_NODE_H__
#define __MY1FAT32_NODE_H__
/*----------------------------------------------------------------------------*/
#include "fat32_link.h"
/*----------------------------------------------------------------------------*/
/**
 * fat32_node.h : fat32 dir/file entry
 * - data structure and functions to process fat32 dir entry
 * - provides char array name4dos
 * - provides path (fat32_link_t)
**/
/*----------------------------------------------------------------------------*/
#define FAT32_NAME_BUFFSIZE 16
/*----------------------------------------------------------------------------*/
#define FAT32_ATTR_READ_ONLY 0x01
#define FAT32_ATTR_HIDDEN 0x02
#define FAT32_ATTR_SYSTEM 0x04
#define FAT32_ATTR_VOLUMELBL 0x08
#define FAT32_ATTR_SUB_DIR 0x10
#define FAT32_ATTR_ARCHIVE 0x20
/**
 * FAT32_ATTR_ARCHIVE should be set on create/modify to indicate 'dirty'
 * - backup software should clear it to indicate 'pure' state
**/
/*----------------------------------------------------------------------------*/
#define FAT32_NODE_SIZE sizeof(fat32_node_t)
#define FAT32_NODE_MAXCOUNT (FAT32_SECTOR_SIZE/FAT32_NODE_SIZE)
/*----------------------------------------------------------------------------*/
typedef struct _fat32_node_t {
	char name[11]; /* 8+3, whitespace (NOT NULL!) padded for 8 */
	byte08_t attr; /* 6-bits actually */
	byte08_t rsvd[8]; /* reserved */
	word16_t clhi; /* first cluster - hi 2 bytes */
	word16_t time; /* 5/6/5 => hh/mm/ss */
	word16_t date; /* 7/4/5 => yy-1980/mm/dd */
	word16_t clus; /* first cluster - low 2 bytes */
	word32_t size;
} fat32_node_t; /* 32-bytes directory (and file) entry */
/*----------------------------------------------------------------------------*/
typedef struct _f32node_temp_t {
	unsigned char loop, temp;
} f32node_temp_t;
/*----------------------------------------------------------------------------*/
__xdata char name4dos[FAT32_NAME_BUFFSIZE];
__xdata fat32_link_t path;
__xdata f32node_temp_t tmpP;
/*----------------------------------------------------------------------------*/
#define fat32_path_error() fat32_link_iserror(&path)
#define fat32_path_noerr() fat32_link_eclear(&path);
#define fat32_path_OK() fat32_link_isOK(&path)
#define fat32_path_redo() fat32_link_redo(&path)
#define fat32_path_load() fat32_link_load(&path)
#define fat32_path_next() fat32_link_next(&path)
#define fat32_path_prep() fat32_link_prep(&path,fat32_root())
/*----------------------------------------------------------------------------*/
#define fat32_timeHH(hh) (word16_t)((hh&0x1f)<<11)
#define fat32_timeMM(mm) (word16_t)((mm&0x3f)<<5)
#define fat32_timeSS(ss) (word16_t)(ss&0x1f)
#define fat32_timeMS(mm,ss) fat32_timeMM(mm)|fat32_timeSS(ss)
#define fat32_time(hh,mm,ss) (fat32_timeHH(hh)|fat32_timeMS(mm,ss))
#define fat32_timeHH_read(from) (from>>11)
#define fat32_timeMM_read(from) ((from>>5)&0x3f)
#define fat32_timeSS_read(from) (from&0x1f)
#define fat32_dateYY(yy) (word16_t)((((yy-1980)%100)&0x7f)<<9)
#define fat32_dateMM(mm) (word16_t)((mm&0x0f)<<5)
#define fat32_dateDD(dd) (word16_t)(dd&0x1f)
#define fat32_dateMD(mm,dd) fat32_dateMM(mm)|fat32_dateDD(dd)
#define fat32_date(yy,mm,dd) (fat32_dateYY(yy)|fat32_dateMD(mm,dd))
#define fat32_dateYY_read(from) ((from>>9)+1980)
#define fat32_dateMM_read(from) ((from>>5)&0x0f)
#define fat32_dateDD_read(from) (from&0x1f)
/*----------------------------------------------------------------------------*/
/** useful macros! */
#define NODEPTR(pnod) ((fat32_node_t*)pnod)
#define fat32_node_reset(pnod) { \
		NODEPTR(pnod)->name[0] = 0x0; \
		NODEPTR(pnod)->attr = 0x00; \
		NODEPTR(pnod)->time = 0; \
		NODEPTR(pnod)->date = fat32_date(2023,1,1); \
		NODEPTR(pnod)->clhi = 0; \
		NODEPTR(pnod)->clus = 0; \
		NODEPTR(pnod)->size = 0; \
	}
#define fat32_node_isvollabel(pnod) \
	(NODEPTR(pnod)->attr&FAT32_ATTR_VOLUMELBL)
#define fat32_node_isdelete1(pnod) (NODEPTR(pnod)->name[0]==0xe5)
#define fat32_node_isdelete2(pnod) (NODEPTR(pnod)->name[0]==0x05)
#define fat32_node_ends(pnod) (NODEPTR(pnod)->name[0]==0x00)
#define fat32_node_isdeleted(pnod) \
	(fat32_node_isdelete1(pnod)||fat32_node_isdelete2(pnod))
#define fat32_node_isignored(pnod) \
	(fat32_node_isvollabel(pnod)||fat32_node_isdeleted(pnod))
#define fat32_node_ispath(pnod) \
	(NODEPTR(pnod)->attr&FAT32_ATTR_SUB_DIR)
#define fat32_node_cluster(pnod) \
	((word32_t)NODEPTR(pnod)->clhi<<16)+NODEPTR(pnod)->clus
#define fat32_node_isfree(pnod) \
	(fat32_node_ends(pnod)||fat32_node_isdeleted(pnod))
/*----------------------------------------------------------------------------*/
/* fills name4dos char array from 8+3 dos name in fat32 entry */
void fat32_node_name_read(unsigned char *pdat) {
	for (tmpP.loop=0;tmpP.loop<8;tmpP.loop++) {
		if (pdat[tmpP.loop]==0x20) break;
		else name4dos[tmpP.loop] = pdat[tmpP.loop];
	}
	tmpP.temp = tmpP.loop;
	for (tmpP.loop=8;tmpP.loop<11;tmpP.loop++,tmpP.temp++) {
		if (!pdat[tmpP.loop]||pdat[tmpP.loop]==0x20)
			break;
		else {
			if (tmpP.loop==8) name4dos[tmpP.temp++] = '.';
			name4dos[tmpP.temp] = pdat[tmpP.loop];
		}
	}
	name4dos[tmpP.temp] = 0x0;
}
/*----------------------------------------------------------------------------*/
/* compare name4dos with given name, returns 1 if the same */
unsigned char fat32_node_name_cmp(char* name) {
	for (tmpP.loop=0;tmpP.loop<FAT32_NAME_BUFFSIZE;tmpP.loop++) {
		if (name[tmpP.loop]!=name4dos[tmpP.loop]) break;
		if (!name[tmpP.loop]) return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
/* finds fat32_node_t entry with given name */
fat32_node_t* fat32_node_find_name(char* name) {
	fat32_link_redo(&path);
	fat32_link_load(&path);
	if (fat32_path_error()) return 0x0;
	do {
		path.info = disk.buff;
		for (tmpP.loop=0;tmpP.loop<FAT32_NODE_MAXCOUNT;
				tmpP.loop++,path.info+=FAT32_NODE_SIZE) {
			if (fat32_node_isignored(path.info)) continue;
			if (fat32_node_ends(path.info)) return 0x0;
			fat32_node_name_read(path.info);
			if (fat32_node_name_cmp(name))
				return (fat32_node_t*)path.info;
		}
		fat32_link_next(&path);
		if (fat32_path_error()) break;
	} while (!fat32_is_last_cluster(path.load));
	return 0x0;
}
/*----------------------------------------------------------------------------*/
void fat32_path_cd(char* name) {
	fat32_link_waitOK(&path);
	if (fat32_node_find_name(name)) {
		if (fat32_node_ispath(path.info)) {
			path.load = path.curr; // save this?
			path.curr = fat32_node_cluster(path.info);
			if (!path.curr) path.curr = disk.core.root;
			if (fat32_is_usable_cluster(path.curr)) {
				fat32_link_prep(&path,path.curr);
				fat32_link_modified(&path);
				fat32_link_OK(&path);
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void fat32_find_cluster(void) {
	path.free = 0;
	path.tmp2 = 2;
	path.tmp1 = disk.core.ifat;
	tmpP.loop = path.tmp2;
	while (path.tmp1<FAT32_FAT_SECTSIZE) {
		if (disk.load(path.tmp1)) break;
		path.pu32 = (word32_t*)disk.buff;
		while (tmpP.loop<FAT32_LINKCOUNT) {
			if (path.pu32[tmpP.loop]==FAT32_FREE_CLUSTER) {
				path.free = path.tmp2;
				return;
			}
			tmpP.loop++;
			path.tmp2++;
		}
		path.tmp1++; /* next sector of fat */
		tmpP.loop = 0;
	}
}
/*----------------------------------------------------------------------------*/
void fat32_find_node(void) {
	/* get a free node in current path */
	path.pent = 0x0;
	fat32_link_redo(&path);
	fat32_link_load(&path);
	if (fat32_path_error()) return;
	do {
		path.info = disk.buff;
		for (tmpP.temp=0;tmpP.temp<FAT32_ENTRYPSEC;tmpP.temp++) {
			if (fat32_node_isfree(path.info)) {
				path.pent = path.info;
				return;
			}
			path.info += FAT32_ENTRYSIZE;
		}
		fat32_link_next(&path);
		if (fat32_path_error()) break;
		if (fat32_is_last_cluster(path.load)) {
			fat32_find_cluster();
			if (!path.free) break;
			fat32_link_get(&path);
			if (fat32_path_error()) break;
			fat32_link_set(&path,path.free);
			if (fat32_path_error()) break;
			path.load = path.free;
			fat32_link_load(&path);
			if (fat32_path_error()) break;
			/* free all entries */
			path.info = disk.buff;
			path.pent = path.info;
			for (tmpP.temp=0;tmpP.temp<FAT32_NODE_MAXCOUNT;tmpP.temp++) {
				path.info[0] = 0x0;
				path.info += FAT32_ENTRYSIZE;
			}
		}
	} while (!path.pent);
}
/*----------------------------------------------------------------------------*/
#define valid_UC(that) (that>='A'&&that<='Z')
#define valid_LC(that) (that>='a'&&that<='z')
#define valid_DD(that) (that>='0'&&that<='9')
#define valid_1(that) (valid_UC(that)||valid_DD(that)||(that=='_'))
#define valid_2(that) (that>='a'&&that<='z')
#define valid_doschar(that) valid_1(that)?that:(valid_2(that)?that-0x20:'_')
/*----------------------------------------------------------------------------*/
/* create 8+3 dos name in fat32 entry from given char array */
void fat32_node_name_make(char* name) {
	for (tmpP.loop=0,tmpP.temp=0;tmpP.loop<8;tmpP.loop++) {
		if (!name[tmpP.temp]||name[tmpP.temp]=='.')
			NODEPTR(path.info)->name[tmpP.loop] = 0x20;
		else {
			NODEPTR(path.info)->name[tmpP.loop] =
				valid_doschar(name[tmpP.temp]);
			tmpP.temp++;
		}
	}
	/* find extension in source */
	while (name[tmpP.temp]) {
		if (name[tmpP.temp]=='.')
			break;
		tmpP.temp++;
	}
	if (!name[tmpP.temp]) tmpP.temp = 0;
	else tmpP.temp++;
	/* copy in - tmpP.loop should be 8 here! */
	for (;tmpP.loop<11;tmpP.loop++) {
		if (!tmpP.temp) NODEPTR(path.info)->name[tmpP.loop] = 0x20;
		else {
			NODEPTR(path.info)->name[tmpP.loop] =
				valid_doschar(name[tmpP.temp]);
			tmpP.temp++;
		}
	}
}
/*----------------------------------------------------------------------------*/
void fat32_path_make_node(char* name) {
	fat32_link_waitOK(&path);
	if (!fat32_node_find_name(name)) {
		fat32_find_node();
		if (path.pent) {
			path.info = path.pent;
			fat32_node_reset(NODEPTR(path.info));
			if (path.flag&FAT32_LINKFLAG_MAKEPATH)
				NODEPTR(path.info)->attr |= FAT32_ATTR_SUB_DIR;
			fat32_node_name_make(name);
			if (disk.save(disk.csec))
				fat32_link_error0(&path);
			else fat32_link_OK(&path);
		}
	}
}
/*----------------------------------------------------------------------------*/
void fat32_path_wipe_node(char* name) {
	fat32_link_waitOK(&path);
	if (fat32_node_find_name(name)) {
		path.info[0] = 0xe5;
		if (disk.save(disk.csec))
			fat32_link_error0(&path);
		else fat32_link_OK(&path);
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_NODE_H__ */
/*----------------------------------------------------------------------------*/
