/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_DISK_H__
#define __MY1FAT32_DISK_H__
/*----------------------------------------------------------------------------*/
#include "fat32_core.h"
/*----------------------------------------------------------------------------*/
/**
 * fat32_disk.h : fat32 disk interface
 * - provides fat32_disk_t data structure
 * - holds fat32_core data
 * - functions to load/store from/to disk storage
 * - provides fat32_disk_init function to initialize fat32_core
 * - actual storage interface code should define fat32_disk_prep function
**/
/*----------------------------------------------------------------------------*/
#define FAT32_DISK_ERROR (FAT32_FLAG_ERROR|0x0010)
#define FAT32_DISK_RD_ERROR (FAT32_FLAG_ERROR|0x0020)
#define FAT32_DISK_WR_ERROR (FAT32_FLAG_ERROR|0x0030)
/*----------------------------------------------------------------------------*/
typedef byte08_t (*fat32_sect_loader_t)(word32_t that);
typedef fat32_sect_loader_t fat32_sect_writer_t;
/*----------------------------------------------------------------------------*/
typedef struct  _fat32_disk_t {
	fat32_core_t core;
	word32_t csec; /* current sector being loaded */
	byte08_t *buff; /* pointer to sector buffer */
	fat32_sect_loader_t load;
	fat32_sect_writer_t save;
} fat32_disk_t;
/*----------------------------------------------------------------------------*/
__xdata fat32_disk_t disk;
/*----------------------------------------------------------------------------*/
#define fat32_flag() disk.core.flag
#define fat32_root() disk.core.root
#define fat32_error() (disk.core.flag&FAT32_FLAG_ERROR)
#define fat32_errorcode(code) disk.core.flag |= code
#define fat32_clearcode(code) disk.core.flag = 0
/*----------------------------------------------------------------------------*/
void fat32_disk_init(void) {
	/** assign these BEFORE calling init */
	/** disk.load = load; */
	/** disk.save = save; */
	/** disk.pdev = (e.g. sdcmdd_t*); */
	/** disk.buff = (loader's sector buffer)*/
	disk.core.buff = disk.buff;
	/* get mbr */
	if (disk.load(0)) return;
	fat32_core_init(&disk.core);
	if (fat32_error()) return;
	/* get vbr */
	if (disk.load(disk.core.root)) return;
	fat32_core_load(&disk.core);
	/**if (fat32_error()) return;*/
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_DISK_H__ */
/*----------------------------------------------------------------------------*/
