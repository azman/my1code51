/*----------------------------------------------------------------------------*/
#ifndef __UART_STC_H__
#define __UART_STC_H__
/*----------------------------------------------------------------------------*/
/**
 * uart_stc.h
 * - extension library for uart on stc51
 *   = maintain same uart feature 9600-8n1
 * - using stc-specific baud generator (shared with uart2)
 *   = timer 1 can be used for other purpose
**/
/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
#include "uart.h"
#include "stc51brt.h"
/*----------------------------------------------------------------------------*/
#define uart_init_brt() uart_init_brtbaud(BRT_BAUD9600)
/*----------------------------------------------------------------------------*/
void uart_init_brtbaud(unsigned char pick) {
	SCON = 0x50;
	brt_serial1(); /* use brt instead of timer 1 */
	brt_load(pick);
	brt_mode1t();
	brt_enable();
}
/*----------------------------------------------------------------------------*/
#endif /** __UART_STC_H__ */
/*----------------------------------------------------------------------------*/
