/*----------------------------------------------------------------------------*/
#ifndef __MY1CSTR_CONV_H__
#define __MY1CSTR_CONV_H__
/*----------------------------------------------------------------------------*/
/**
 * cstr_hexascii.h
 * - converts values to hexascii string (cstr object)
**/
/*----------------------------------------------------------------------------*/
#include "cstr.h"
/*----------------------------------------------------------------------------*/
void cstr_append_hexbyte(my1cstr_t* cstr, unsigned char byte) {
	cstr_add1(cstr,nibb_hi2ascii(byte));
	cstr_add1(cstr,nibb_lo2ascii(byte));
}
/*----------------------------------------------------------------------------*/
void cstr_append_hexuint(my1cstr_t* cstr, unsigned int word) {
	cstr_append_hexbyte(cstr,(unsigned char)(word>>8)&0xff);
	cstr_append_hexbyte(cstr,(unsigned char)word&0xff);
}
/*----------------------------------------------------------------------------*/
void cstr_append_hexlong(my1cstr_t* cstr, unsigned long word) {
	cstr_append_hexuint(cstr,(unsigned int)(word>>16)&0xffff);
	cstr_append_hexuint(cstr,(unsigned int)word&0xffff);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1CSTR_CONV_H__ */
/*----------------------------------------------------------------------------*/
