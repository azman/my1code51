/*----------------------------------------------------------------------------*/
#ifndef __STC51BRT_H__
#define __STC51BRT_H__
/*----------------------------------------------------------------------------*/
/**
 * stc51brt.h
 * - constants for stc51's baudrate timer
 * - used by uart2 and optionally by uart
**/
/*----------------------------------------------------------------------------*/
#define S2FOSC 11059200L
#define S2BAUD 115200
#define BRTCNT (S2FOSC/(32*S2BAUD))
/** the above DOES NOT expand well using sdcc? */
#define BRTCNT_B115200 3
#define BRTCNT_B57600 6
#define BRTCNT_B38400 9
#define BRTCNT_B19200 18
#define BRTCNT_B9600 36
/*----------------------------------------------------------------------------*/
#define BRT_CALC(step) (unsigned char)(256-step)
/*----------------------------------------------------------------------------*/
#define BRT_BAUD115200 BRT_CALC(BRTCNT_B115200)
#define BRT_BAUD57600 BRT_CALC(BRTCNT_B57600)
#define BRT_BAUD38400 BRT_CALC(BRTCNT_B38400)
#define BRT_BAUD19200 BRT_CALC(BRTCNT_B19200)
#define BRT_BAUD9600 BRT_CALC(BRTCNT_B9600)
/*----------------------------------------------------------------------------*/
#ifndef BRTCNT_USE
#define BRTCNT_USE BRTCNT_B115200
#endif
#define BRTVAL BRT_CALC(BRTCNT_USE)
/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
/*----------------------------------------------------------------------------*/
#define brt_load(lval) BRT = (unsigned char)lval
#define brt_smod0() AUXR &= ~S2SMOD
#define brt_noscaling brt_smod0
#define brt_mode1t() AUXR |= BRTx12
#define brt_hispeed brt_mode1t
#define brt_enable() AUXR |= BRT_ENB_BIT
#define brt_disable() AUXR &= ~BRT_ENB_BIT
#define brt_serial1() AUXR |= S1BRS;
/*----------------------------------------------------------------------------*/
#endif /** __STC51BRT_H__ */
/*----------------------------------------------------------------------------*/
