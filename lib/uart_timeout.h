/*----------------------------------------------------------------------------*/
#ifndef __UART_TIMEOUT_H__
#define __UART_TIMEOUT_H__
/*----------------------------------------------------------------------------*/
/**
 * uart_timeout.h
 * - extension library for uart_read.h
 * - provides uart_timeout, wait for rx with a timeout
**/
/*----------------------------------------------------------------------------*/
#include "uart_read.h"
/*----------------------------------------------------------------------------*/
#ifndef UART_TIMEOUT_LOOP
#define UART_TIMEOUT_LOOP MCU51_COUNT_1MS
#endif
/*----------------------------------------------------------------------------*/
unsigned char uart_timeout(unsigned int wait) {
	unsigned int loop;
	do {
		loop = UART_TIMEOUT_LOOP;
		do {
			if (uart_peek()) return 0;
		} while (--loop);
	} while (--wait);
	return 1;
}
/*----------------------------------------------------------------------------*/
#endif /** __UART_TIMEOUT_H__ */
/*----------------------------------------------------------------------------*/
