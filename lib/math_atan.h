/*----------------------------------------------------------------------------*/
#ifndef __MY1MATH_ATAN_H__
#define __MY1MATH_ATAN_H__
/*----------------------------------------------------------------------------*/
/**
 *  math_atan.h - arctan function for 8051
 *  - provides atan/atan2 functions
 *  - tested input range -1 to 1 (normalized values?)
*note*: tested on pc against math lib!
Test my1atan function
  *Error threshold: 5.000000e-03
  *Value Increment: 0.000100
- Avg error:4.391182e-04
- Max error:8.432269e-04
Test my1atan2 function
  *Error threshold: 5.000000e-03
  *Value Increment: 0.050000
- ERR count:0/1600
- Avg error:4.135323e-04
- Max error:8.431077e-04
 *  - output here tested against libreoffice calc atan values (-1 to 1)
 *    = all errors are below 5e-03
 */
/*----------------------------------------------------------------------------*/
#include "vars_math.h"
/*----------------------------------------------------------------------------*/
#define atan(x) my1atan(x)
#define atan2(y,x) my1atan2(y,x)
/*----------------------------------------------------------------------------*/
#define ATAN_CONST1 0.07765096
#define ATAN_CONST2 0.28743448
#define ATAN_CONST3 (QUAD_PI-ATAN_CONST1+ATAN_CONST2)
/*----------------------------------------------------------------------------*/
float my1atan(float radian) {
	trig_next =radian*radian;
	trig_next = (((ATAN_CONST1*trig_next)-ATAN_CONST2)*trig_next)+ATAN_CONST3;
	return trig_next*radian;
}
/*----------------------------------------------------------------------------*/
float my1atan2(float y, float x) {
	trig_temp = fabs(x);
	trig_full = fabs(y);
	if (trig_temp<FLOAT_PRECISION) {
		if (trig_full<FLOAT_PRECISION)
			trig_full = QUAD_PI; // undefined, actually...
		else if (y>0.0f) trig_full = HALF_PI;
		else trig_full = -HALF_PI;
	}
	else {
		if (trig_temp>trig_full) {
			/* atan func proven for -1 to 1! */
			trig_full = my1atan(y/x);
		}
		else {
			/* using identity: atan(y/x) = PI/2 - atan(x/y) */
			trig_temp = x/y;
			trig_full = my1atan(trig_temp);
			if (trig_temp>0.0f) trig_full = HALF_PI-trig_full;
			else trig_full = -HALF_PI-trig_full;
		}
		if (x<0.0f) {
			if (y>=0.0f) trig_full += MATH_PI;
			else trig_full -= MATH_PI;
		}
	}
	return trig_full;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1MATH_ATAN_H__ */
/*----------------------------------------------------------------------------*/
