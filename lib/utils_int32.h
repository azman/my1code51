/*----------------------------------------------------------------------------*/
#ifndef __MY1UTILS_INT32_H__
#define __MY1UTILS_INT32_H__
/*----------------------------------------------------------------------------*/
/**
 * utils_int32.h
 * - library for basic string-integer conversions utility
**/
/*----------------------------------------------------------------------------*/
#include "vars_utils.h"
/*----------------------------------------------------------------------------*/
unsigned long str2uint(char* str) {
	util_ui32 = 0;
	while (*str) {
		if (*str<0x30||*str>0x39)
			break;
		util_ui32 = (util_ui32*10) + ((*str)-0x30);
		str++;
	}
	return util_ui32;
}
/*----------------------------------------------------------------------------*/
long str2int(char* str) {
	util_temp = 0;
	/** check negative value */
	if ((*str)=='-') {
		util_temp++; str++;
	}
	util_si32 = (long) str2uint(str);
	/** check overflow */
	if (util_si32<0) return 0;
	/** check sign */
	if (util_temp) util_si32 = -util_si32;
	return util_si32;
}
/*----------------------------------------------------------------------------*/
unsigned char uint2str(char* str, unsigned long val) {
	/** get number of digits */
	util_ui32 = 1;
	while (util_ui32<val) util_ui32 *= 10;
	/** start convert */
	util_loop = 0; util_test = 0;
	while (util_ui32>0) {
		if (val>=util_ui32||util_test) {
			util_temp = (unsigned char)(val/util_ui32); /** should be 0-9 */
			val = val % util_ui32;
			str[util_loop++] = util_temp + 0x30;
			util_test = 1;
		}
		util_ui32 /= 10;
	}
	/** check zero value */
	if (util_loop==0) str[util_loop++] = '0';
	/** add terminator */
	str[util_loop] = 0x0;
	/** return current position - strlen */
	return util_loop;
}
/*----------------------------------------------------------------------------*/
unsigned char int2str(char* str, long val) {
	util_size = 0;
	/** check negative value */
	if (val<0) {
		str[util_size++] = '-';
		val = -val;
	}
	util_size += uint2str(&str[util_size],val);
	return util_size;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1UTILS_INT32_H__ */
/*----------------------------------------------------------------------------*/
