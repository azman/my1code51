/*----------------------------------------------------------------------------*/
#ifndef __MY1UTILS_FLOAT_H__
#define __MY1UTILS_FLOAT_H__
/*----------------------------------------------------------------------------*/
/**
 * utils_float.h
 * - extension library for utils
 * - provides floating-point conversions
**/
/*----------------------------------------------------------------------------*/
#include "utils_int32.h"
#include "vars_utils.h"
/*----------------------------------------------------------------------------*/
/* floating point resolution */
#ifndef FPSIZE
#define FPSIZE 4
#endif
/*----------------------------------------------------------------------------*/
float str2float(char* str) {
	util_test = 0xff; util_size = 0; util_temp = 0;
	/** check negative value */
	if (str[0]=='-') {
		util_temp = 1; str++;
	}
	/** get dp position */
	while (str[util_size]) {
		if (str[util_size]=='.') util_test = util_size;
		util_size++;
	}
	if (util_test==0xff) util_test = util_size;
	else str[util_test] = 0x0;
	str2uint(str); /* util_ui32 */
	if (util_test<util_size) str[util_test] = '.';
	/** loop decimal points */
	util_ftmp = 0.0; util_divf = 10.0;
	while (++util_test<util_size) {
		util_ftmp += util_ftmp + (float)(str[util_test]-0x30)/util_divf;
		util_divf *= 10.0;
	}
	/** add up and apply sign flag */
	util_ftmp = util_ftmp + (float)util_ui32;
	if (util_temp) util_ftmp = -util_ftmp;
	return util_ftmp;
}
/*----------------------------------------------------------------------------*/
unsigned char float2str(char* str, float val) {
	util_size = 0;
	/** check negative value */
	if (val<0) {
		str[util_size++] = '-';
		val = -val;
	}
	util_utmp = (unsigned long) val;
	val -= (float) util_utmp;
	/** convert integer part! */
	util_size += uint2str(&str[util_size],util_utmp);
	/** put decimal point in */
	str[util_size++] = '.';
	/** show decimal part */
	util_test = FPSIZE;
	while (util_test>0) {
		val *= 10;
		util_temp = (unsigned char) val;
		str[util_size++] = util_temp + 0x30;
		val -= (float) util_temp;
		util_test--;
	}
	/** add terminator */
	str[util_size] = 0x0;
	/** return current position @ strlen */
	return util_size;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1UTILS_FLOAT_H__ */
/*----------------------------------------------------------------------------*/
