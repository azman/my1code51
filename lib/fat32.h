/*----------------------------------------------------------------------------*/
#ifndef __MY1FAT32_H__
#define __MY1FAT32_H__
/*----------------------------------------------------------------------------*/
#include "fat32_file.h"
/* use sdcard interface by default */
#ifndef __FAT32_ONLY__
#include "fat32_sd.h"
#endif
/* simple use, read/write (text files) from/to root path only */
/*----------------------------------------------------------------------------*/
void fat32_init(void) {
	fat32_disk_prep();
	if (fat32_error()) return;
	fat32_disk_init();
	if (fat32_error()) return;
	fat32_path_prep();
}
/*----------------------------------------------------------------------------*/
#define fat32_file(lbl) fat32_file_name(lbl)
#define fat32_write(dat,cnt) fat32_file_data(dat,cnt); fat32_file_write()
#define fat32_write_file(lbl,dat,cnt) fat32_file(lbl); fat32_write(dat,cnt)
#define fat32_append_mode() file.opts |= FAT32_OPTS_APPEND
#define fat32_overwrite_mode() file.opts &= ~FAT32_OPTS_APPEND
/*----------------------------------------------------------------------------*/
#endif /* __MY1FAT32_H__ */
/*----------------------------------------------------------------------------*/
