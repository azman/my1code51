/*----------------------------------------------------------------------------*/
#ifndef __MY1SIM808_UART_H__
#define __MY1SIM808_UART_H__
/*----------------------------------------------------------------------------*/
/**
 * sim808_uart.h
 * - for debugging sim808 code
 * - requires atcmd_uart.h
**/
/*----------------------------------------------------------------------------*/
#include "sim808base.h"
/*----------------------------------------------------------------------------*/
#include "atcmd_uart.h"
/*----------------------------------------------------------------------------*/
#ifndef __ATCMD_ON_UART__
/*----------------------------------------------------------------------------*/
#define sim808_must_be_ok atcmd_must_be_ok
/*----------------------------------------------------------------------------*/
#define sim808_show_buff(ps,sz) uart_puts("@@ Reply:"); atcmd_show_buff(ps,sz)
/*----------------------------------------------------------------------------*/
char* sim808_show_if_ok(char* buff, unsigned char size) {
	char* pchk;
	unsigned char temp;
	temp = sim808_read_wait(buff,size);
	if (!temp) {
		uart_puts("timeout.\n");
		pchk = 0x0;
	}
	else {
		if (temp==size) uart_puts("<overflow> ");
		pchk = sim808_trim_ok(buff,temp);
		if (pchk) {
			uart_puts("OK.\n");
			pchk = sim808_trim_stat(pchk);
		}
		else { pchk = buff; uart_puts("??.\n"); }
		sim808_show_buff(pchk,temp);
	}
	return pchk;
}
/*----------------------------------------------------------------------------*/
#endif /* __ATCMD_ON_UART__ */
/*----------------------------------------------------------------------------*/
#endif /* __MY1SIM808_UART_H__ */
/*----------------------------------------------------------------------------*/
