# makefile for 8051-based projects by azman@my1matrix.org
# - includes libmake_tool and libmake_rule

DOLIST = $(subst .c,,$(subst src/,,$(sort $(wildcard src/*.c))))
DOLIST += $(basename $(notdir $(sort $(wildcard chk/*.c))))
DOLIST += $(basename $(notdir $(sort $(wildcard zzz/*.c))))
DOLIST += $(subst .c,,$(sort $(wildcard *.c)))
XXLIST = $(basename $(notdir $(sort $(wildcard src/*.asm))))
XXLIST += $(subst .asm,,$(sort $(wildcard *.asm)))

DOTHAT = $(word 1,$(MAKECMDGOALS))
ifeq ($(DOTHAT),clean)
DOTHAT = $(word 2,$(MAKECMDGOALS))
endif

ifneq ($(findstring $(DOTHAT),$(DOLIST)),)
DOMAKE = $(DOTHAT)
else
DOMAKE ?=
endif

# optional project settings
PROJECT ?=
MODLIST ?=
OBJECTS = $(PROJECT).rel $(addsuffix .rel,$(MODLIST))
HEXFILE = $(PROJECT).hex
# need to be defined due to multiple sources (*.rel)
OUTFILE = $(PROJECT).ihx

include libmake_tool

ifneq ($(wildcard $(ASM)),)
DOLIST += $(XXLIST)
endif
ALLHEX = $(addsuffix .hex,$(DOLIST))

.PHONY: $(PROJECT) $(DOMAKE) dummy all inc clean sweep

$(PROJECT): $(HEXFILE)

# building single source
$(DOMAKE): $(DOMAKE).hex

dummy:
	@echo
	@echo "Run 'make <mod>' OR 'make PROJECT=<src> MODLIST={<mod>,...}'"
	@echo
	@echo "  <mod> = { $(DOLIST) }"
	@echo
	@echo "SDCC_PATH:{$(SDCC_PATH)}"
	@echo

all: $(ALLHEX)

PACKER=$(shell which packthat 2>/dev/null)
ZIPINC=my1code51inc

inc:
ifeq ($(PACKER),)
	@echo "** Cannot find packer (packthat)!"
else
	@packthat inc/ --zip --date --name $(ZIPINC) || true
endif

# for building project from multiple source files
$(OUTFILE): $(OBJECTS)
	$(CC) $(MMODEL) $(CFLAGS) -o $@ $^ $(LFLAGS) $(MODLIB) $(OFLAGS)

include libmake_rule
