/*----------------------------------------------------------------------------*/
#ifndef __AM2302_H__
#define __AM2302_H__
/*----------------------------------------------------------------------------*/
/**
 * am2302.h
 * - library for AM2302 temperature/humidity sensor module
 * - requires timer module
**/
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#ifndef AM2302_DATA
#define AM2302_DATA P1_0
#endif
#define AM2302_WAIT_30us 0xffe4
/*----------------------------------------------------------------------------*/
#define am2302_init() AM2302_DATA = 1
/*----------------------------------------------------------------------------*/
unsigned char am2302_read(unsigned int* temp, unsigned int* humi) {
	unsigned char loop, csum, tmp1, tmp2;
	/* start protocol! */
	AM2302_DATA = 0;
	timer_wait(TIMER_VAL01MS); /* >= 1ms */
	AM2302_DATA = 1;
	timer_wait(AM2302_WAIT_30us); /* 20-40us */
	/* wait low */
	while(AM2302_DATA); /* max 80us */
	/* wait high */
	while(!AM2302_DATA); /* max 80us */
	/* wait low */
	while(AM2302_DATA); /* prep data */
	/* prepare to calc checksum */
	csum = 0;
	/* get humidity - high byte */
	for (loop=0,tmp1=0;loop<8;loop++) {
		tmp1 <<= 1;
		timer_null();
		while(!AM2302_DATA);
		timer_exec();
		while(AM2302_DATA);
		timer_stop();
		/* check logic '1': 26-28us = 0, 70us = 1 */
		if(timer_read()>45) tmp1 |= 0x01;
	}
	csum += tmp1;
	/* get humidity - low byte */
	for (loop=0,tmp2=0;loop<8;loop++) {
		tmp2 <<= 1;
		timer_null();
		while(!AM2302_DATA);
		timer_exec();
		while(AM2302_DATA);
		timer_stop();
		if(timer_read()>45) tmp2 |= 0x01;
	}
	csum += tmp2;
	if (humi) *humi = ((unsigned int)tmp1<<8)|tmp2;
	/* get temperature - high byte */
	for (loop=0,tmp1=0;loop<8;loop++) {
		tmp1 <<= 1;
		timer_null();
		while(!AM2302_DATA);
		timer_exec();
		while(AM2302_DATA);
		timer_stop();
		if(timer_read()>45) tmp1 |= 0x01;
	}
	csum += tmp1;
	/* get temperature - low byte */
	for (loop=0,tmp2=0;loop<8;loop++) {
		tmp2 <<= 1;
		timer_null();
		while(!AM2302_DATA);
		timer_exec();
		while(AM2302_DATA);
		timer_stop();
		if(timer_read()>45) tmp2 |= 0x01;
	}
	csum += tmp2;
	if (temp) *temp = ((unsigned int)tmp1<<8)|tmp2;
	/* get checksum */
	for (loop=0,tmp1=0;loop<8;loop++)
	{
		tmp1 <<= 1;
		timer_null();
		while(!AM2302_DATA);
		timer_exec();
		while(AM2302_DATA);
		timer_stop();
		if(timer_read()>45) tmp1 |= 0x01;
	}
	AM2302_DATA = 1;
	return (tmp1==csum) ? 0 : csum;
}
/*----------------------------------------------------------------------------*/
#endif /** __AM2302_H__ */
/*----------------------------------------------------------------------------*/
