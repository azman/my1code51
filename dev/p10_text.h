/*----------------------------------------------------------------------------*/
#ifndef __P10_TEXT_H__
#define __P10_TEXT_H__
/*----------------------------------------------------------------------------*/
#include "p10_display.h"
#include "font.h"
/*----------------------------------------------------------------------------*/
void p10_putchar8(p10_t* disp, unsigned char xpos, unsigned char ypos,
		unsigned char pval) {
	unsigned int pick;
	unsigned char irow, icol, temp, mask;
	if (pval<0x20||pval>=0x80) return;
	pick = (pval-0x20)<<3; /* times 8 */
	for (irow=0;irow<8&&ypos<P10_ROWS;irow++,ypos++) {
		temp = xpos; mask = 0x80; pval = fontdata_8x8[pick];
		for (icol=0;icol<8&&temp<(P10_COLS*P10_STEP);icol++) {
			p10_set_pixel(disp,icol,irow,(pval&mask)?1:0);
			temp++; mask>>=1;
		}
		pick++;
	}
}
/*----------------------------------------------------------------------------*/
void p10_putchar8x(p10_t* disp, unsigned char xpos, unsigned char pval) {
	unsigned int pick;
	unsigned char irow, icol, temp, mask;
	if (pval<0x20||pval>=0x80) return;
	pick = (pval-0x20)<<3; /* times 8 */
	for (irow=0;irow<8;irow++) {
		temp = xpos; mask = 0x80; pval = fontdata_8x8[pick];
		for (icol=0;icol<8&&temp<(P10_COLS*P10_STEP);icol++) {
			p10_set_pixel(disp,icol,(irow<<1)+1,(pval&mask)?1:0);
			p10_set_pixel(disp,icol,(irow<<1)+2,(pval&mask)?1:0);
			temp++; mask>>=1;
		}
		pick++;
	}
}
/*----------------------------------------------------------------------------*/
void p10_putchar16(p10_t* disp, unsigned char xpos, unsigned char pval) {
	unsigned int pick;
	unsigned char irow, icol, temp, mask;
	if (pval<0x20||pval>=0x80) return;
	pick = (pval-0x20)<<4; /* times 16 */
	for (irow=0;irow<16;irow++) {
		temp = xpos; mask = 0x80; pval = fontdata_8x16[pick];
		for (icol=0;icol<8&&temp<(P10_COLS*P10_STEP);icol++) {
			p10_set_pixel(disp,icol,irow,(pval&mask)?1:0);
			temp++; mask>>=1;
		}
		pick++;
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __P10_TEXT_H__ */
/*----------------------------------------------------------------------------*/