/*----------------------------------------------------------------------------*/
#ifndef __KEYPAD_922_H__
#define __KEYPAD_922_H__
/*----------------------------------------------------------------------------*/
/**
 * keypad_922.h
 * - extension library for keypad using 922 encoder
**/
/*----------------------------------------------------------------------------*/
#include "keypad.h"
/*----------------------------------------------------------------------------*/
#ifndef _KEY_922I_DEFINED_
/** DA => P2.4 : key-press indicator! */
my1sbit(KEY_INT,0xA4);
#endif
/*----------------------------------------------------------------------------*/
keybyte_t key_scan_922(void) {
	keybyte_t scan;
	if (!KEY_INT) return KEY_NOT_CODE; /** return if no keypress detected */
	while (KEY_INT); /** wait for keyrelease - consider skipping this? */
	scan = KEY_DATA&0x0F;
	if (scan&0x03==0x03)
		scan = (scan >> 2) + 0x0A;
	else {
		switch (scan&0x0c) {
			case 0x00: scan++; break;
			case 0x04: break;
			case 0x08: scan--; break;
			default:
				if (scan==0x0d) scan = 0;
				else if (scan==0x0c) scan = 0x0e;
				else scan = 0x0f;
		}
	}
	return scan;
}
/*----------------------------------------------------------------------------*/
keybyte_t key_wait_922(void) {
	keybyte_t scan;
	while ((scan=key_scan_922())==KEY_NOT_CODE);
	return scan;
}

/*----------------------------------------------------------------------------*/
#endif /** __KEYPAD_922_H__ */
/*----------------------------------------------------------------------------*/
