/*----------------------------------------------------------------------------*/
#ifndef __MY1SDCARD_H__
#define __MY1SDCARD_H__
/*----------------------------------------------------------------------------*/
#define SDPREP_OK 0
#define SDPREP_ERROR 0x80
#define SDPREP_ERROR_IDLE    (SDPREP_ERROR|0x01)
#define SDPREP_ERROR_CMD8    (SDPREP_ERROR|0x02)
#define SDPREP_ERROR_NOTV2   (SDPREP_ERROR|0x03)
#define SDPREP_ERROR_VOLT    (SDPREP_ERROR|0x04)
#define SDPREP_ERROR_RESET   (SDPREP_ERROR|0x05)
#define SDPREP_ERROR_APPCMD  (SDPREP_ERROR|0x06)
#define SDPREP_ERROR_ACMD41  (SDPREP_ERROR|0x07)
#define SDPREP_ERROR_UNVERS  (SDPREP_ERROR|0x08)
#define SDPREP_ERROR_INVALID (SDPREP_ERROR|0x09)
#define SDPREP_ERROR_DIS_CRC (SDPREP_ERROR|0x0A)
#define SDPREP_ERROR_BLKSIZE (SDPREP_ERROR|0x0B)
#define SDPREP_ERROR_CMD58   (SDPREP_ERROR|0x0C)
/*----------------------------------------------------------------------------*/
/** GO_IDLE_STATE */
#define SDCARD_CMD00 0x00
#define SDCARD_CMD00_CRC 0x95
#define SDCARD_CMD01 0x01
#define SDCARD_CMD01_CRC 0xF9
/** SEND_IF_COND */
#define SDCARD_CMD08 0x08
#define SDCARD_CMD08_ARG 0x000001AA
#define SDCARD_CMD08_CRC 0x87
#define SDCARD_CMD16 0x10
#define SDCARD_CMD17 0x11
#define SDCARD_CMD24 0x18
#define SDCARD_CMD41 0x29
/** CMD41 arg/crc assumes HCS (high capacity?)( else 0x00000000 & 0xE5 */
#define SDCARD_CMD41_ARG 0x40000000
#define SDCARD_CMD41_CRC 0x77
#define SDCARD_CMD55 0x37
#define SDCARD_CMD55_CRC 0x65
#define SDCARD_CMD58 0x3A
#define SDCARD_CMD58_CRC 0x01
#define SDCARD_CMD59 0x3B
/*----------------------------------------------------------------------------*/
#define SDCARD_ARG_NONE_ 0x00000000
#define SDCARD_DUMMY_DATA 0xFF
#define SDCARD_DUMMY_CRC SDCARD_DUMMY_DATA
/*----------------------------------------------------------------------------*/
#define SDCARD_MMC_MASK 0xC0
#define SDCARD_MMC_CMD_ 0x40
#define SDCARD_SECTOR_SIZE 512
/*----------------------------------------------------------------------------*/
#define SDCARD_RESP_SUCCESS 0x00
#define SDCARD_RESP_R1_IDLE 0x01
#define SDCARD_RESP_ILLEGAL 0x05
#define SDCARD_RESP_INVALID 0xFF
#define SDCARD_RESP_TIMEOUT 0xff
#define SDCARD_DATA_TIMEOUT 0xffff
/*----------------------------------------------------------------------------*/
#define RESP_R1_IDLE_STATE  0x01
#define RESP_R1_ERASE_RESET 0x02
#define RESP_R1_ILLEGAL_CMD 0x04
#define RESP_R1_CMD_CRC_ERR 0x08
#define RESP_R1_ERASESQ_ERR 0x10
#define RESP_R1_ADDRESS_ERR 0x20
#define RESP_R1_PARAM_ERROR 0x40
/*----------------------------------------------------------------------------*/
#define SDCARD_FLUSH_R1 1
#define SDCARD_FLUSH_R7 4
/*----------------------------------------------------------------------------*/
#define SDCARD_START_TOKEN 0xFE
#define SDCARD_WRITE_CHECK 0x1F
#define SDCARD_WACCEPT_DAT 0x05
#define SDCARD_WREJECT_CRC 0x0B
#define SDCARD_WREJECT_WRI 0x0D
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
#include "loop_delay.h"
/*----------------------------------------------------------------------------*/
#ifndef _SDCARD_PINS_DEFINED_
my1sbit(SD_SCLK,PIN24);
my1sbit(SD_MOSI,PIN25);
my1sbit(SD_MISO,PIN26);
my1sbit(SD_CENB,PIN27);
#endif
/*----------------------------------------------------------------------------*/
typedef struct _sdcmdd_t {
	byte08_t itrx, temp; /* used ONLY in sdcard_txrx */
	byte08_t loop, resp; /* used in sdcard_send_command info */
	byte08_t rcnt, rchk; /* sd response options info */
	byte08_t acmd; /* sd command byte */
	byte08_t acrc; /* sd crc byte */
	byte08_t flag; /* prep flag */
	byte08_t type; /* sd type during prep */
	word16_t icnt; /* sdcard_read/sdcard_write */
	word32_t sect; /* sector index to read/write */
	word32_t args; /* sd command argument */
	byte08_t buff[SDCARD_SECTOR_SIZE];
} sdcmdd_t;
/*----------------------------------------------------------------------------*/
__xdata sdcmdd_t card;
/*----------------------------------------------------------------------------*/
#define SDCARD_OK SDPREP_OK
#define SDCARD_READ_RQ 0x01
#define SDCARD_WRITE_RQ 0x02
#define sdcard_read_sector(pick) card.sect = pick; sdcard_read_base()
#define sdcard_write_sector(pick) card.sect = pick; sdcard_write_base()
/*----------------------------------------------------------------------------*/
unsigned char sdcard_txrx(unsigned char data) {
	for (card.itrx=0,card.temp=0;card.itrx<8;card.itrx++) {
		card.temp <<= 1;
		/* msb first! */
		if(data&0x80) SD_MOSI = 1;
		else SD_MOSI = 0;
		card.temp |= SD_MISO;
		SD_SCLK = 1;
		SD_SCLK = 0;
		data <<= 1;
	}
	return card.temp;
}
/*----------------------------------------------------------------------------*/
void sdcard_init(void) {
	SD_SCLK = 0;
	SD_MOSI = 0;
	SD_MISO = 1;
	SD_CENB = 1;
	/* sdc/mmc init flow (spi) */
	loop_delay(1); /* wait at least 1ms */
	/* send 74 dummy clock cycles? 10 x 8-bit data?*/
	for (card.loop=0;card.loop<10;card.loop++)
		sdcard_txrx(SDCARD_DUMMY_DATA);
}
/*----------------------------------------------------------------------------*/
void sdcard_send_command(void) {
	sdcard_txrx(card.acmd|SDCARD_MMC_CMD_);
	sdcard_txrx((card.args>>24)&0xff);
	sdcard_txrx((card.args>>16)&0xff);
	sdcard_txrx((card.args>>8)&0xff);
	sdcard_txrx((card.args)&0xff);
	sdcard_txrx(card.acrc);
	/* wait card to be ready  */
	for (card.loop=0;card.loop<SDCARD_RESP_TIMEOUT;card.loop++) {
		sdcard_txrx(SDCARD_DUMMY_DATA);
		if (card.temp!=SDCARD_RESP_INVALID)
			break;
	}
	card.resp = card.temp;
}
/*----------------------------------------------------------------------------*/
#define sdcard_prep_response(what,save) { \
	card.rcnt = what; card.rchk = save; }
/*----------------------------------------------------------------------------*/
void sdcard_read_response(void) {
	for (card.loop=0;card.loop<card.rcnt;card.loop++) {
		sdcard_txrx(SDCARD_DUMMY_DATA);
		if (card.rchk) card.buff[card.loop] = card.temp;
	}
}
/*----------------------------------------------------------------------------*/
void sdcard_idle(void) {
	/* GO_IDLE_STATE */
	SD_CENB = 0;
	card.acmd = SDCARD_CMD00;
	card.args = SDCARD_ARG_NONE_;
	card.acrc = SDCARD_CMD00_CRC;
	sdcard_prep_response(SDCARD_FLUSH_R1,0);
	sdcard_send_command();
	sdcard_read_response();
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
void sdcard_cmd8(void) {
	/* SEND_IF_COND */
	SD_CENB = 0;
	card.acmd = SDCARD_CMD08;
	card.args = SDCARD_CMD08_ARG;
	card.acrc = SDCARD_CMD08_CRC;
	sdcard_send_command();
	if (card.resp==SDCARD_RESP_R1_IDLE) {
		sdcard_prep_response(SDCARD_FLUSH_R7,1);
		sdcard_read_response();
	}
	sdcard_prep_response(SDCARD_FLUSH_R1,0);
	sdcard_read_response();
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
void sdcard_acmd41(void) {
	/* SD_SEND_IF_COND */
	SD_CENB = 0;
	do {
		card.acmd = SDCARD_CMD55;
		card.args = SDCARD_ARG_NONE_;
		card.acrc = SDCARD_CMD55_CRC;
		sdcard_prep_response(SDCARD_FLUSH_R1,0);
		sdcard_send_command();
		sdcard_read_response();
		if (card.resp!=SDCARD_RESP_R1_IDLE)
			break;
		card.acmd = SDCARD_CMD41;
		card.args = SDCARD_CMD41_ARG;
		card.acrc = SDCARD_CMD41_CRC;
		sdcard_prep_response(SDCARD_FLUSH_R1,0);
		sdcard_send_command();
		sdcard_read_response();
		if (card.resp==SDCARD_RESP_ILLEGAL)
			break;
	} while (card.resp!=SDCARD_RESP_SUCCESS);
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
void sdcard_cmd58(void) {
	/* READ_OCR */
	SD_CENB = 0;
	card.acmd = SDCARD_CMD58;
	card.args = SDCARD_ARG_NONE_;
	card.acrc = SDCARD_CMD58_CRC;
	sdcard_send_command();
	sdcard_prep_response(SDCARD_FLUSH_R7,1);
	sdcard_read_response();
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
void sdcard_disable_crc(void) {
	/* CRC_ON_OFF */
	SD_CENB = 0;
	card.acmd = SDCARD_CMD59;
	card.args = SDCARD_ARG_NONE_;
	card.acrc = SDCARD_DUMMY_CRC;
	sdcard_prep_response(SDCARD_FLUSH_R1,0);
	sdcard_send_command();
	sdcard_read_response();
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
void sdcard_block_size(void) {
	/* BLOCK_SIZE */
	SD_CENB = 0;
	card.acmd = SDCARD_CMD16;
	card.args = SDCARD_SECTOR_SIZE;
	card.acrc = SDCARD_DUMMY_CRC;
	sdcard_prep_response(SDCARD_FLUSH_R1,0);
	sdcard_send_command();
	sdcard_read_response();
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
void sdcard_prep(void) {
	card.flag = SDPREP_OK;
	card.type = 0;
	sdcard_init();
	sdcard_idle();
	if (card.resp!=SDCARD_RESP_R1_IDLE) {
		card.flag = SDPREP_ERROR_IDLE;
		return;
	}
	sdcard_cmd8();
	if (card.resp==SDCARD_RESP_ILLEGAL) {
		card.type = 1; /* allow this? */
		card.flag = SDPREP_ERROR_NOTV2;
		return;
	}
	if (card.resp!=SDCARD_RESP_R1_IDLE) {
		card.flag = SDPREP_ERROR_CMD8;
		return;
	}
	if (card.buff[0]||card.buff[1]||card.buff[2]!=0x01||card.buff[3]!=0xAA) {
		card.flag = SDPREP_ERROR_VOLT;
		return;
	}
	card.type = 2; /* microsd type/version 2 or later */
	sdcard_acmd41();
	if (card.resp!=SDCARD_RESP_SUCCESS) {
		card.flag = SDPREP_ERROR_ACMD41;
		return;
	}
	sdcard_cmd58();
	if (card.resp!=SDCARD_RESP_SUCCESS) {
		card.flag = SDPREP_ERROR_CMD58;
		return;
	}
	if ((card.buff[0]&0xc0)==0xc0)
		card.type = 3;
	sdcard_disable_crc();
	if (card.resp!=SDCARD_RESP_SUCCESS) {
		card.flag = SDPREP_ERROR_DIS_CRC;
		return;
	}
	sdcard_block_size();
	if (card.resp!=SDCARD_RESP_SUCCESS) {
		card.flag = SDPREP_ERROR_BLKSIZE;
		/*return;*/
	}
}
/*----------------------------------------------------------------------------*/
void sdcard_read_base(void) {
	card.flag = SDCARD_READ_RQ;
	SD_CENB = 0;
	do {
		card.icnt = SDCARD_RESP_TIMEOUT;
		do {
			card.acmd = SDCARD_CMD17;
			card.args = (card.type<3)?card.sect<<9:card.sect;
			card.acrc = SDCARD_DUMMY_CRC;
			sdcard_prep_response(SDCARD_FLUSH_R1,0);
			sdcard_send_command();
			sdcard_read_response();
			if (card.resp==SDCARD_RESP_SUCCESS) break;
		} while (--card.icnt);
		if (!card.icnt) break;
		card.icnt = SDCARD_DATA_TIMEOUT;
		do {
			sdcard_txrx(SDCARD_DUMMY_DATA);
			if (card.temp==SDCARD_START_TOKEN) {
				break;
			}
		} while (--card.icnt);
		if (!card.icnt) break;
		/* get the data */
		for (card.icnt=0;card.icnt<SDCARD_SECTOR_SIZE;card.icnt++)
			card.buff[card.icnt] = sdcard_txrx(SDCARD_DUMMY_DATA);
		/* 16-bit dummy crc? */
		sdcard_txrx(SDCARD_DUMMY_CRC);
		sdcard_txrx(SDCARD_DUMMY_CRC);
		card.flag = SDCARD_OK;
	} while (0);
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
void sdcard_write_base(void) {
	card.flag = SDCARD_WRITE_RQ;
	SD_CENB = 0;
	do {
		card.icnt = SDCARD_RESP_TIMEOUT;
		do {
			card.acmd = SDCARD_CMD24;
			card.args = (card.type<3)?card.sect<<9:card.sect;
			card.acrc = SDCARD_DUMMY_CRC;
			sdcard_prep_response(SDCARD_FLUSH_R1,0);
			sdcard_send_command();
			sdcard_read_response();
			if (card.resp==SDCARD_RESP_SUCCESS) break;
		} while (--card.icnt);
		if (!card.icnt) break;
		sdcard_txrx(SDCARD_START_TOKEN);
		for (card.icnt=0;card.icnt<SDCARD_SECTOR_SIZE;card.icnt++)
			sdcard_txrx(card.buff[card.icnt]);
		sdcard_txrx(SDCARD_DUMMY_CRC);
		sdcard_txrx(SDCARD_DUMMY_CRC);
		card.icnt = SDCARD_DATA_TIMEOUT;
		do {
			sdcard_txrx(SDCARD_DUMMY_DATA);
			if (card.temp!=SDCARD_RESP_INVALID) {
				card.temp &= SDCARD_WRITE_CHECK;
				break;
			}
		} while (--card.icnt);
		if (!card.icnt||card.temp!=SDCARD_WACCEPT_DAT) break;
		/* no timeout? assume ok? */
		while (sdcard_txrx(SDCARD_DUMMY_DATA)==SDCARD_RESP_SUCCESS);
		card.flag = SDCARD_OK;
	} while (0);
	SD_CENB = 1;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SDCARD_H__ */
/*----------------------------------------------------------------------------*/
