/*----------------------------------------------------------------------------*/
#ifndef __DS18B20_H__
#define __DS18B20_H__
/*----------------------------------------------------------------------------*/
/**
 * ds18b20.h
 * - library for DS18B20 Digital Thermometer module
 * - requires timer module
**/
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#ifndef DS18B20_DATA
#define DS18B20_DATA P2_0
#endif
#define TVAL_010US 0xfff7
#define TVAL_100US 0xffa3
#define TVAL_010MS 0xdbff
/*----------------------------------------------------------------------------*/
#define DS18B20_CMD_TCONV 0x44
#define DS18B20_CMD_SKIPROM 0xCC
#define DS18B20_CMD_READPAD 0xBE
#define DS18B20_READOK 0xAA
/*----------------------------------------------------------------------------*/
__xdata unsigned char ds_step, ds_loop, ds_recv;
__xdata unsigned int ds_temp;
/*----------------------------------------------------------------------------*/
unsigned char ds18b20_reset(void) {
	DS18B20_DATA = 1;
	timer_wait(0xfffe); /* not really needed? */
	DS18B20_DATA = 0; /* master reset pulse: min=480us max=960us */
	ds_step = 7; /* around 700us should do? */
	do { timer_wait(TVAL_100US); } while(--ds_step);
	DS18B20_DATA = 1; /* 'release' wire for input, wait: 15-60us */
	ds_loop = 0;
	ds_step = 6;
	do {
		timer_wait(TVAL_010US);
		if (!DS18B20_DATA) break;
	} while(--ds_step);
	if (DS18B20_DATA) /* still high? */
		return 0;
	/* check presence pulse: min=60us max=240us */
	while (!DS18B20_DATA) {
		timer_wait(TVAL_010US);
		ds_loop++;
		if (ds_loop>24) return 0; /* maybe not ds18b20? */
	}
	/* master rx min=480us , ds_loop should be <24 */
	ds_step = 48-ds_loop;
	do {
		timer_wait(TVAL_010US);
	} while(--ds_step);
	return ds_loop;
}
/*----------------------------------------------------------------------------*/
void ds18b20_wr(unsigned char what) {
	DS18B20_DATA = 0; /* initiate write timeslot */
	ds_step++; /* ~1us? */
	if (what) /* write1: line must go high within 15us */
		DS18B20_DATA = 1;
	ds_step=14; while(--ds_step); /* 1st 15us segment - ds18b20 sample init */
	ds_step=15; while(--ds_step); /* 2nd 15us segment - ds18b20 sample typ */
	ds_step = 30; while(--ds_step); /* write0: remain low for 60us */
	DS18B20_DATA = 1;
}
/*----------------------------------------------------------------------------*/
#define DS18B20_READ_WAIT 1
#define DS18B20_READ_NEXT 9
#define DS18B20_READ_REST (60-(DS18B20_READ_WAIT+DS18B20_READ_NEXT))
/*----------------------------------------------------------------------------*/
unsigned char ds18b20_rd(void) {
	unsigned char read;
	DS18B20_DATA = 0; /* initiate read timeslot */
	ds_step = DS18B20_READ_WAIT; while(--ds_step);
	DS18B20_DATA = 1; /* prepare to sample */
	ds_step = DS18B20_READ_NEXT; while(--ds_step);
	read = DS18B20_DATA; /* master sample */
	ds_step = DS18B20_READ_REST; while(--ds_step); /* read timeslots >=60us */
	return read;
}
/*----------------------------------------------------------------------------*/
void ds18b20_send(unsigned char that) {
	for (ds_loop=0;ds_loop<8;ds_loop++) {
		ds18b20_wr(that&0x01); /* lsb first */
		that>>=1; /* whould be enough for 1us recovery time */
	}
}
/*----------------------------------------------------------------------------*/
unsigned char ds18b20_recv(void) {
	ds_recv = 0;
	for (ds_loop=0;ds_loop<8;ds_loop++) {
		ds_recv>>=1;
		if (ds18b20_rd())
			ds_recv |= 0x80;
	}
	return ds_recv;
}
/*----------------------------------------------------------------------------*/
unsigned char ds18b20_read_temp(void) {
	if (!ds18b20_reset()) return 0;
	ds18b20_send(DS18B20_CMD_SKIPROM);
	ds18b20_send(DS18B20_CMD_TCONV);
	DS18B20_DATA = 1; /* just in case */
	/* default 12-bit conversion needs 750ms */
	ds_step = 75; do { timer_wait(TVAL_010MS); } while(--ds_step);
	if (!ds18b20_reset()) return 0;
	ds18b20_send(DS18B20_CMD_SKIPROM);
	ds18b20_send(DS18B20_CMD_READPAD);
	ds_temp = ds18b20_recv();
	ds18b20_recv();
	ds_temp |= ((unsigned int)ds_recv<<8);
	ds18b20_reset(); /* reset to terminate read */
	return DS18B20_READOK;
}
/*----------------------------------------------------------------------------*/
#endif /** __DS18B20_H__ */
/*----------------------------------------------------------------------------*/
