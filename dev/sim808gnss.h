/*----------------------------------------------------------------------------*/
#ifndef __MY1SIM808GNSS_H__
#define __MY1SIM808GNSS_H__
/*----------------------------------------------------------------------------*/
/**
 * sim808gnss.h
 * - sim808 interface for gnss (gps) info
 * - requires sim808base.h
 * - data:(21)
 * run(0/1),fix(0/1)
 * utc (yyyymmddHHMMSS.sssyyy),
 * lat(-90...90),lng(-180...180),alt(meters)
 * vel(kmph),dir(deg)
 * fix_mode(0/1/2),rsvd1
 * hdop,pdop,vdop (???)
 * rsvd2
 * gnss_sat_view,gnss_sat_used,glonass_sat_used
 * rsvd3,cn0max(dbhz)
 * hpa(meters),vda(meters) (???)
**/
/*----------------------------------------------------------------------------*/
#include "sim808base.h"
/*----------------------------------------------------------------------------*/
#define sim808gnss_init() atcmd_puts("AT+CGNSPWR=1\r\n")
#define sim808gnss_conf() atcmd_puts("AT+CGNSSEQ=RMC\r\n");
#define sim808gnss_chk0() atcmd_puts("AT+CGNSSEQ=?\r\n");
#define sim808gnss_chk1() atcmd_puts("AT+CGNSSEQ?\r\n");
#define sim808gnss_data() atcmd_puts("AT+CGNSINF\r\n")
/*----------------------------------------------------------------------------*/
typedef struct _sim808gnss_inf_t {
	char *pfix, *putc, *plat, *plng;
	char *palt, *pvel, *pdir, *pfmd; /* 8 */
	char *hdop, *pdop, *vdop; /* 12 - skip 9 and 13 */
	char *pst1, *pst2, *pst3; /* 16 - skip till 21 */
	char *p_ok, *pchk, *pstr;
	unsigned char temp, loop;
	/* external buffer */
	char *buff;
	unsigned char size;
} sim808gnss_inf_t;
/*----------------------------------------------------------------------------*/
#ifndef SIM808GNSS_INF_TYPE
#define SIM808GNSS_INF_TYPE __xdata
#endif
/*----------------------------------------------------------------------------*/
SIM808GNSS_INF_TYPE sim808gnss_inf_t gpsstr;
/*----------------------------------------------------------------------------*/
#define sim808gnss_inf_init(pb,sz) gpsstr.buff = pb; gpsstr.size = sz
#define sim808gnss_fixed() (gpsstr.p_ok&&gpsstr.pfix[0]=='1')
/*----------------------------------------------------------------------------*/
void sim808gnss_find_data(void) {
	gpsstr.pstr = gpsstr.pchk; gpsstr.loop = 0;
	while (gpsstr.pstr[0]) {
		if (gpsstr.pstr[0]==',') {
			gpsstr.loop++;
			switch (gpsstr.loop) {
				case 1: gpsstr.pfix = &gpsstr.pstr[1]; break;
				case 2: gpsstr.putc = &gpsstr.pstr[1]; break;
				case 3: gpsstr.plat = &gpsstr.pstr[1]; break;
				case 4: gpsstr.plng = &gpsstr.pstr[1]; break;
				case 5: gpsstr.palt = &gpsstr.pstr[1]; break;
				case 6: gpsstr.pvel = &gpsstr.pstr[1]; break;
				case 7: gpsstr.pdir = &gpsstr.pstr[1]; break;
				case 8: gpsstr.pfmd = &gpsstr.pstr[1]; break;
				case 10: gpsstr.hdop = &gpsstr.pstr[1]; break;
				case 11: gpsstr.pdop = &gpsstr.pstr[1]; break;
				case 12: gpsstr.vdop = &gpsstr.pstr[1]; break;
				case 14: gpsstr.pst1 = &gpsstr.pstr[1]; break;
				case 15: gpsstr.pst2 = &gpsstr.pstr[1]; break;
				case 16: gpsstr.pst3 = &gpsstr.pstr[1]; break;
			}
		}
		else if (gpsstr.pstr[0]=='\r'||gpsstr.pstr[0]=='\n') {
			gpsstr.pstr[0] = 0x0; break;
		}
		gpsstr.pstr++;
	}
	if (gpsstr.loop==20)
		gpsstr.p_ok = gpsstr.pchk;
}
/*----------------------------------------------------------------------------*/
void sim808gnss_wait_data(void) {
	gpsstr.p_ok = 0x0;
	gpsstr.temp = sim808_read_wait(gpsstr.buff,gpsstr.size);
	if (!gpsstr.temp) return;
	gpsstr.pchk = sim808_trim_ok(gpsstr.buff,gpsstr.temp);
	if (!gpsstr.pchk) return;
	gpsstr.pchk = sim808_trim_stat(gpsstr.pchk);
	if (gpsstr.pchk) sim808gnss_find_data();
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1SIM808GNSS_H__ */
/*----------------------------------------------------------------------------*/
