/*----------------------------------------------------------------------------*/
#ifndef __MY1ESP01_UTIL_H__
#define __MY1ESP01_UTIL_H__
/*----------------------------------------------------------------------------*/
/**
 * esp01_util.h
 * - extension for esp01.h
 * - provides useful functions
**/
/*----------------------------------------------------------------------------*/
#include "esp01.h"
/*----------------------------------------------------------------------------*/
unsigned char esp01_buff_copy(unsigned char *buff, unsigned char* copy,
		unsigned char size, unsigned char stop) {
	unsigned char loop = 0;
	size--;
	while (buff[loop]) {
		copy[loop] = buff[loop];
		if (copy[loop]==stop||loop==size) {
			copy[loop] = 0x0;
			break;
		}
		loop++;
	}
	size++;
	return buff[loop];
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1ESP01_UTIL_H__ */
/*----------------------------------------------------------------------------*/
