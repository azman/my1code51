/*----------------------------------------------------------------------------*/
#ifndef __MY1ESP01_ST_H__
#define __MY1ESP01_ST_H__
/*----------------------------------------------------------------------------*/
/**
 * esp01_st.h
 * - extension for esp01.h
 * - provides useful function(s) for station mode
 **/
/*----------------------------------------------------------------------------*/
#include "esp01.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define ESP01ST_SOCKREQ0 ESP01CMD_SOCKINIT "=\"TCP\",\""
#define ESP01ST_SOCKREQ1 "\","
#define ESP01ST_SOCKREQ2 "\r\n"
/*----------------------------------------------------------------------------*/
#ifndef ESP01ST_CONNECT_DELAY
#define ESP01ST_CONNECT_DELAY 10
#endif
/*----------------------------------------------------------------------------*/
void esp01_st_connect(void) {
	while (1) {
		esp01_stconnect();
		if (esp01_task_ok()) {
			if (esp01_buff_ok()) break;
			else if (esp01_seek_wifiok()) break;
		}
		timer_delay1s(espd.loop,ESP01ST_CONNECT_DELAY);
	}
}
/*----------------------------------------------------------------------------*/
#define ESP01ST_SOCK_ETASK 0x08
#define ESP01ST_SOCK_ERROR 0x10
#define ESP01ST_SOCK_CLOSED 0x20
#define ESP01ST_SOCK_NOLINK 0x40
/*----------------------------------------------------------------------------*/
void esp01_st_socket1(char *host, char *port) {
	espd.stat = ESP01_STAT_OK;
	esp01_puts(ESP01ST_SOCKREQ0);
	esp01_puts(host);
	esp01_puts(ESP01ST_SOCKREQ1);
	esp01_puts(port);
	esp01_puts(ESP01ST_SOCKREQ2);
	esp01_wait();
	if (esp01_task_ok()) {
		if (!esp01_seek_connect()) {
			if (esp01_seek_errormsg())
				espd.stat |= ESP01ST_SOCK_ERROR;
			if (esp01_seek_closed())
				espd.stat |= ESP01ST_SOCK_CLOSED;
			else espd.stat |= ESP01ST_SOCK_NOLINK;
		}
	}
	else espd.stat |= ESP01ST_SOCK_ETASK;
}
/*----------------------------------------------------------------------------*/
void esp01_st_socket0(void) {
	espd.stat = ESP01_STAT_OK;
	esp01_puts(ESP01CMD_SOCKDONE);
	esp01_wait();
}
/*----------------------------------------------------------------------------*/
/* separate this into esp01_st_http? */
#include "utils_lite.h"
#include "cstr.h"
__xdata my1cstr_t hbuf;
__xdata char tbuf[4];
/*----------------------------------------------------------------------------*/
#define HTTP_REQ_0POST "POST "
#define HTTP_REQ_1POST " HTTP/1.1\r\n"
#define HTTP_REQ_1HOST "Host: localhost\r\n"
#define HTTP_REQ_1CLEN "Content-Length: 0\r\n\r\n"
#define HTTP_REQ_BLEN (5+11+17+21)
#define HTTP_REQ_2POST HTTP_REQ_1POST HTTP_REQ_1HOST HTTP_REQ_1CLEN
/*----------------------------------------------------------------------------*/
#define ESP01ST_POSTED 0x10
#define ESP01ST_CLOSED 0x20
/*----------------------------------------------------------------------------*/
#define esp01_st_posted() (espd.stat&ESP01ST_POSTED)
#define esp01_st_closed() (espd.stat&ESP01ST_CLOSED)
/*----------------------------------------------------------------------------*/
void esp01_st_posthttp(char* purl) {
	espd.stat = ESP01_STAT_OK;
	cstr_assign(&hbuf,HTTP_REQ_0POST);
	cstr_append(&hbuf,purl);
	cstr_append(&hbuf,HTTP_REQ_2POST);
	uint8_2cstr(tbuf,hbuf.blen);
	esp01_puts("AT+CIPSEND=");
	esp01_puts(tbuf);
	esp01_puts("\r\n");
	esp01_wait();
	if (espd.buff[espd.fill-2]=='>'&&espd.buff[espd.fill-1]==' ') {
		esp01_puts(hbuf.buff);
		if (esp01_wait_str("SEND OK\r\n",9)==ESP01_OK) {
			esp01_wait();
			if (espd.fill) {
				if (esp01_seek_closed())
					espd.stat |= ESP01ST_CLOSED;
			}
			espd.stat |= ESP01ST_POSTED;
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1ESP01_ST_H__ */
/*----------------------------------------------------------------------------*/
