/*----------------------------------------------------------------------------*/
#ifndef __P10_DISPLAY_CORE_H__
#define __P10_DISPLAY_CORE_H__
/*----------------------------------------------------------------------------*/
#define TIMER_TICK_LEN TIMER_VAL01MS
#define TIMER_TICK_CNT 4
#include "timer_tick.h"
/*----------------------------------------------------------------------------*/
/* p10 display */
#define P10_A P1_1
#define P10_B P1_0
#define P10_CLK P1_3
#define P10_SCLK P1_5
#define P10_LAT P10_SCLK
#define P10_R P1_4
#define P10_OE P1_7
/*----------------------------------------------------------------------------*/
#define P10_COLS 32
#define P10_ROWS 16
#define P10_PIX_PER_BYTE 8
#ifndef P10_STEP
#define P10_STEP 1
#endif
/*----------------------------------------------------------------------------*/
/* total pixels - 1 unit */
#define P10_SIZEX (P10_COLS*P10_ROWS)
/* bytes per row - 1 unit */
#define P10_SIZE0 (P10_COLS/P10_PIX_PER_BYTE)
/* bytes per row - display */
#define P10_SIZEF (P10_SIZE0*P10_STEP)
/* bytes per unit */
#define P10_SIZE1 (P10_SIZE0*P10_ROWS)
/* bytes per display */
#define P10_SIZE (P10_SIZE1*P10_STEP)
/*----------------------------------------------------------------------------*/
/* bytes per refresh row - 1 unit */
#define P10_SIZER (P10_SIZE0<<2)
/* refresh rows */
#define P10_ROW1 (P10_STEP*P10_SIZER)
#define P10_ROW2 (P10_STEP*P10_SIZER*2)
#define P10_ROW3 (P10_STEP*P10_SIZER*3)
/*----------------------------------------------------------------------------*/
#define P10_PIX_ON 0
#define P10_PIX_OFF 1
/*----------------------------------------------------------------------------*/
#define p10_light0() P10_B = 0; P10_A = 0
#define p10_light1() P10_B = 0; P10_A = 1
#define p10_light2() P10_B = 1; P10_A = 0
#define p10_light3() P10_B = 1; P10_A = 1
#define p10_latch() P10_LAT = 1; P10_LAT = 0
#define p10_off() P10_OE = 0
#define p10_on() P10_OE = 1
#define p10_spulse() P10_CLK = 1; P10_CLK = 0
#define p10_shift(bb) P10_R = bb; p10_spulse()
/*----------------------------------------------------------------------------*/
void p10_init(void) {
	P10_A = 0;
	P10_B = 0;
	P10_CLK = 0;
	P10_LAT = 0;
	P10_R = 1;
	P10_OE = 0;
}
/*----------------------------------------------------------------------------*/
void p10_push(unsigned char sdat) {
	unsigned char loop, mask;
	for (loop=0,mask=0x80;loop<P10_PIX_PER_BYTE;loop++) {
		if (sdat&mask) { p10_shift(P10_PIX_ON); }
		else { p10_shift(P10_PIX_OFF); }
		mask>>=1;
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __P10_DISPLAY_CORE_H__ */
/*----------------------------------------------------------------------------*/