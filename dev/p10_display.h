/*----------------------------------------------------------------------------*/
#ifndef __P10_DISPLAY_H__
#define __P10_DISPLAY_H__
/*----------------------------------------------------------------------------*/
#include "p10_base.h"
/*----------------------------------------------------------------------------*/
typedef struct _p10_t {
	unsigned char bpix[P10_SIZE];
	unsigned char rrow;
} p10_t;
/*----------------------------------------------------------------------------*/
void p10_clear(p10_t* disp) {
	unsigned char loop; /* valid size for P10_STEP<=4 */
	for (loop=0;loop<P10_SIZE;loop++)
		disp->bpix[loop] = 0x00;
}
/*----------------------------------------------------------------------------*/
void p10_setup(p10_t* disp) {
	p10_init();
	disp->rrow = 0;
}
/*----------------------------------------------------------------------------*/
void p10_update(p10_t* disp) {
	unsigned char loop,offs;
	offs = disp->rrow * P10_SIZEF;
	for (loop=0;loop<P10_SIZEF;loop++) {
		p10_push(disp->bpix[offs+loop+P10_ROW3]);
		p10_push(disp->bpix[offs+loop+P10_ROW2]);
		p10_push(disp->bpix[offs+loop+P10_ROW1]);
		p10_push(disp->bpix[offs+loop]);
	}
}
/*----------------------------------------------------------------------------*/
void p10_refresh(p10_t* disp) {
	p10_off();
	p10_latch();
	switch (disp->rrow) {
		case 0: p10_light0(); break;
		case 1: p10_light1(); break;
		case 2: p10_light2(); break;
		case 3: p10_light3(); break;
	}
	disp->rrow++;
	if (disp->rrow>=4) disp->rrow = 0;
	p10_on();
}
/*----------------------------------------------------------------------------*/
void p10_set_pixel(p10_t* disp, unsigned char xpos, unsigned char ypos,
		unsigned char pval) {
	unsigned char pick, cmsk;
	// get byte-row that holds ypos
	pick = ypos * P10_SIZEF;
	// get byte that holds xpos
	pick = pick + (xpos / P10_PIX_PER_BYTE);
	// get bit mask for that xpos (i.e. xpos=0 is at MSB @left-most)
	cmsk = 0x80 >> (xpos % P10_PIX_PER_BYTE);
	// set it
	if (pval) disp->bpix[pick] |= cmsk;
	else disp->bpix[pick] &= ~cmsk;
}
/*----------------------------------------------------------------------------*/
#endif /* __P10_DISPLAY_H__ */
/*----------------------------------------------------------------------------*/