/*----------------------------------------------------------------------------*/
#ifndef __MY1HMC5883_H__
#define __MY1HMC5883_H__
/*----------------------------------------------------------------------------*/
#ifndef I2C_WAIT_TLEN
#define I2C_WAIT_TLEN 2
#endif
#include "i2c_core.h"
#include "math_atan.h"
/*----------------------------------------------------------------------------*/
#ifdef _BN880_MODULE_
#define NS_AXIS yaxis
#define EW_AXIS xaxis
#endif
/*----------------------------------------------------------------------------*/
#ifndef NS_AXIS
#define NS_AXIS xaxis
#endif
#ifndef EW_AXIS
#define EW_AXIS yaxis
#endif
/*----------------------------------------------------------------------------*/
#define HMC5883_REAL_ADDR 0x1e
/* i2c left-shift @ 0x3c */
#define HMC5883_I2C_SHIFT_ADDR (HMC5883_REAL_ADDR<<1)
/*----------------------------------------------------------------------------*/
#ifndef HMC5883_I2C_ADDR
#define HMC5883_I2C_ADDR 0x3c
#endif
/*----------------------------------------------------------------------------*/
/* register addresses */
#define HMC5883_REG_CFGA 0x00
#define HMC5883_REG_CFGB 0x01
#define HMC5883_REG_MODE 0x02
#define HMC5883_REG_XMSB 0x03
#define HMC5883_REG_XLSB 0x04
#define HMC5883_REG_ZMSB 0x05
#define HMC5883_REG_ZLSB 0x06
#define HMC5883_REG_YMSB 0x07
#define HMC5883_REG_YLSB 0x08
#define HMC5883_REG_STAT 0x09
#define HMC5883_REG_IDRA 0x0A
#define HMC5883_REG_IDRB 0x0B
#define HMC5883_REG_IDRC 0x0C
/*----------------------------------------------------------------------------*/
/* bit settings: measurement mode */
#define HMC5883_CFGA_MSMASK 0x03
#define HMC5883_CFGA_MSNORM 0x00
#define HMC5883_CFGA_MSPOSB 0x01
#define HMC5883_CFGA_MSNEGB 0x02
#define HMC5883_CFGA_MSRSVD 0x03
/* bit settings: output data rate */
#define HMC5883_CFGA_DRMASK 0x1C
#define HMC5883_CFGA_07P5HZ 0x0C
#define HMC5883_CFGA_15P0HZ 0x10
#define HMC5883_CFGA_30P0HZ 0x14
#define HMC5883_CFGA_DR_DEF HMC5883_CFGA_15P0HZ
/* bit settings: sample average */
#define HMC5883_CFGA_SAMASK 0x60
#define HMC5883_CFGA_SANONE 0x00
#define HMC5883_CFGA_SA2AVG 0x20
#define HMC5883_CFGA_SA4AVG 0x40
#define HMC5883_CFGA_SA8AVG 0x60
#define HMC5883_CFGA_SA_DEF HMC5883_CFGA_SANONE
/* bit settings: measurement gain */
#define HMC5883_CFGB_GNMASK 0xE0
#define HMC5883_CFGB_GN0P88 0x00
#define HMC5883_CFGB_GN1P30 0x20
#define HMC5883_CFGB_GN1P90 0x40
#define HMC5883_CFGB_GN2P50 0x60
#define HMC5883_CFGB_GN4P00 0x80
#define HMC5883_CFGB_GN4P70 0xA0
#define HMC5883_CFGB_GN5P60 0xC0
#define HMC5883_CFGB_GN8P10 0xE0
#define HMC5883_CFGB_GN_DEF HMC5883_CFGB_GN1P30
/* gain table: 1370, 1090, 820, 660, 440, 390, 330, 230 */
/* digital res: 0.73, 0.92, 1.22, 1.52, 2.27, 2.56, 3.03, 4.35 */
#define HMC5883_CFGB_GAIN0 HMC5883_CFGB_GN0P88
#define HMC5883_CFGB_GAIN1 HMC5883_CFGB_GN1P30
#define HMC5883_CFGB_GAIN2 HMC5883_CFGB_GN1P90
#define HMC5883_CFGB_GAIN3 HMC5883_CFGB_GN2P50
#define HMC5883_CFGB_GAIN4 HMC5883_CFGB_GN4P00
#define HMC5883_CFGB_GAIN5 HMC5883_CFGB_GN4P70
#define HMC5883_CFGB_GAIN6 HMC5883_CFGB_GN5P60
#define HMC5883_CFGB_GAIN7 HMC5883_CFGB_GN8P10
#define HMC5883_CFGB_GFAC0 0.73
#define HMC5883_CFGB_GFAC1 0.92
#define HMC5883_CFGB_GFAC2 1.22
#define HMC5883_CFGB_GFAC3 1.52
#define HMC5883_CFGB_GFAC4 2.27
#define HMC5883_CFGB_GFAC5 2.56
#define HMC5883_CFGB_GFAC6 3.03
#define HMC5883_CFGB_GFAC7 4.35
/* bit settings: operation mode */
#define HMC5883_MODE_MDMASK 0x03
#define HMC5883_MODE_MDCONT 0x00
#define HMC5883_MODE_MDONCE 0x01
#define HMC5883_MODE_MDIDLE 0x02
#define HMC5883_MODE_MDIDL3 0x03
/* bit settings: hs mode */
#define HMC5883_MODE_HS_BIT 0x80
/*----------------------------------------------------------------------------*/
#ifdef _HMC5883_USE_GAIN5_
#define HMC5883_GAIN_FACTOR HMC5883_CFGB_GFAC5
#define HMC5883_GAIN_SELECT HMC5883_CFGB_GAIN5
#else
#define HMC5883_GAIN_FACTOR HMC5883_CFGB_GFAC1
#define HMC5883_GAIN_SELECT HMC5883_CFGB_GAIN1
#endif
/*----------------------------------------------------------------------------*/
#define HMC5883_CFGA_DEF00 (HMC5883_CFGA_SANONE|HMC5883_CFGA_15P0HZ)
#define HMC5883_CFGA_DEF10 (HMC5883_CFGA_SA8AVG|HMC5883_CFGA_15P0HZ)
#define HMC5883_CFGA_DEFAULT (HMC5883_CFGA_MSNORM|HMC5883_CFGA_DEF00)
#define HMC5883_CFGA_CALIB_P (HMC5883_CFGA_MSPOSB|HMC5883_CFGA_DEF10)
#define HMC5883_CFGA_CALIB_N (HMC5883_CFGA_MSNEGB|HMC5883_CFGA_DEF10)
/* should use this! */
#define HMC5883_CFGA_DEFBASE (HMC5883_CFGA_MSNORM|HMC5883_CFGA_15P0HZ)
#define HMC5883_CFGA_DEFAULT0 (HMC5883_CFGA_DEFBASE|HMC5883_CFGA_SANONE)
#define HMC5883_CFGA_DEFAULT1 (HMC5883_CFGA_DEFAULT0|HMC5883_CFGA_SA8AVG)
/*----------------------------------------------------------------------------*/
#define HMC5883_CFGA_PICK HMC5883_CFGA_MSNORM|HMC5883_CFGA_DEF10
#define HMC5883_CFGB_PICK HMC5883_GAIN_SELECT
/*----------------------------------------------------------------------------*/
/** may use this in the future... instead of HMC5883_GAIN_FACTOR
__code const float gainFactor[] =
	{ 0.73, 0.92, 1.22, 1.52, 2.27, 2.56, 3.03, 4.35 };
**/
/*----------------------------------------------------------------------------*/
/** anisotropic magneto-resistive sensor data @ digital compass! */
typedef struct _magdata_t {
	int xaxis, zaxis, yaxis, xhead;
} magdata_t;
/*----------------------------------------------------------------------------*/
void hmc5883_regs(unsigned char addr, unsigned char bval) {
	i2c_do_start();
	i2c_do_send_byte(HMC5883_I2C_ADDR);
	i2c_do_send_byte(addr);
	i2c_do_send_byte(bval);
	i2c_do_stop();
}
/*----------------------------------------------------------------------------*/
unsigned char hmc5883_rval(unsigned char addr) {
	unsigned char test;
	i2c_do_start();
	i2c_do_send_byte(HMC5883_I2C_ADDR);
	i2c_do_send_byte(addr);
	i2c_do_start();
	i2c_do_send_byte(HMC5883_I2C_ADDR|0x01);
	test = i2c_do_read_byte(1);
	i2c_do_stop();
	return test;
}
/*----------------------------------------------------------------------------*/
#define hmc5883_conf(addr,conf) hmc5883_regs(addr,conf)
#define hmc5883_read() hmc5883_regs(HMC5883_REG_MODE,HMC5883_MODE_MDONCE)
#define hmc5883_read_cont() hmc5883_regs(HMC5883_REG_MODE,HMC5883_MODE_MDCONT)
#define hmc5883_done() hmc5883_rval(HMC5883_REG_STAT)
#define hmc5883_conf_avg8() hmc5883_regs(HMC5883_REG_CFGA,HMC5883_CFGA_DEFAULT1)
#define hmc5883_conf_gain() hmc5883_regs(HMC5883_REG_CFGB,HMC5883_GAIN_SELECT)
/*----------------------------------------------------------------------------*/
unsigned char hmc5883_init(void) {
	unsigned char find, test;
	find = 1;
	i2c_do_start();
	i2c_do_send_byte(HMC5883_I2C_ADDR);
	i2c_do_send_byte(HMC5883_REG_IDRA);
	i2c_do_start();
	i2c_do_send_byte(HMC5883_I2C_ADDR|0x01);
	test = i2c_do_read_byte(0);
	if (test!='H') find = 0;
	test = i2c_do_read_byte(0);
	if (test!='4') find = 0;
	test = i2c_do_read_byte(1);
	if (test!='3') find = 0;
	i2c_do_stop();
	if (find) {
		hmc5883_conf_gain();
		hmc5883_conf_avg8();
		hmc5883_read_cont();
/*
	or, skip hmc5883_read_cont and use code below in main loop
	hmc5883_read();
	while (!hmc5883_done());
*/
	}
	return find;
}
/*----------------------------------------------------------------------------*/
void hmc5883_data(unsigned char* pdat) {
	unsigned char loop, swap;
	i2c_do_start();
	i2c_do_send_byte(HMC5883_I2C_ADDR);
	i2c_do_send_byte(HMC5883_REG_XMSB);
	i2c_do_start();
	i2c_do_send_byte(HMC5883_I2C_ADDR|0x01);
	for (loop=0;loop<5;loop++)
		pdat[loop] = i2c_do_read_byte(0);
	pdat[loop] = i2c_do_read_byte(1);
	i2c_do_stop();
	/* hmc5883l i2c reg layout has msb first! */
	for (loop=0;loop<6;loop+=2) {
		swap = pdat[loop];
		pdat[loop] = pdat[loop+1];
		pdat[loop+1] = swap;
	}
}
/*----------------------------------------------------------------------------*/
#define MDAT(mobj) ((magdata_t*)(mobj))
#define hmc5883_gain(mdat,gain) { \
	MDAT(mdat)->xaxis = (int)((float)(MDAT(mdat)->xaxis)*gain); \
	MDAT(mdat)->zaxis = (int)((float)(MDAT(mdat)->zaxis)*gain); \
	MDAT(mdat)->yaxis = (int)((float)(MDAT(mdat)->yaxis)*gain); \
	}
/*----------------------------------------------------------------------------*/
#define hmc5883_calc(fval,xdat,ydat) { \
	fval = atan2((float)ydat,(float)xdat); \
	fval = rad2deg(fval); \
	}
/*----------------------------------------------------------------------------*/
void hmc5883_head(magdata_t *calc) {
	float temp;
	hmc5883_calc(temp,calc->NS_AXIS,calc->EW_AXIS);
	calc->xhead = (int)temp;
	/* convert [-180,180] range to [0,360] */
	if (calc->xhead<0) calc->xhead += 360;
	else if (calc->xhead>=360) calc->xhead -= 360;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1HMC5883_H__ */
/*----------------------------------------------------------------------------*/
