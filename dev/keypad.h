/*----------------------------------------------------------------------------*/
#ifndef __KEYPAD_H__
#define __KEYPAD_H__
/*----------------------------------------------------------------------------*/
/**
 * keypad.h
 *   - library for raw 4x4 keypad interface {R0,R1,R2,R3,C0,C1,C2,C3}
 *     = filter 4x3 keypad manually! {C1,R0,C0,R3,C2,R2,R1}
**/
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
/*----------------------------------------------------------------------------*/
#ifndef _KEY_PINS_DEFINED_
/** Default: R0-R3 => P1.0-P1.3, C0-C3 => P1.4-P1.7 */
my1sfr(KEY_DATA,0x90);
#endif
#ifndef _ROWS_AT_UPPER_
#define ROWS_FLAG 0x01
#define COLS_FLAG 0x10
#else
#define ROWS_FLAG 0x10
#define COLS_FLAG 0x01
#endif
/** no-key indicator for keypad */
#define KEY_NOT_CODE 0x10
/*----------------------------------------------------------------------------*/
/* useful to differentiate type */
typedef unsigned char keybyte_t;
/*----------------------------------------------------------------------------*/
keybyte_t key_scan(void) {
	/** scan for key press */
	unsigned char irow, icol, mask, test;
	mask = ROWS_FLAG;
	for (irow=0;irow<4;irow++) {
		KEY_DATA = ~mask; test = COLS_FLAG;
		for (icol=0;icol<4;icol++) {
			if ((KEY_DATA&test)==0) {
				while ((KEY_DATA&test)==0);
				if (icol==3) return irow+0x0A;
				if (irow==0) return icol+1;
				if (irow==1) return icol+4;
				if (irow==2) return icol+7;
				if (icol==1) return 0;
				return (icol>>1)+0x0e;
			}
			test <<= 1;
		}
		mask <<= 1;
	}
	return KEY_NOT_CODE;
}
/*----------------------------------------------------------------------------*/
keybyte_t key_wait(void) {
	/** wait for key press */
	keybyte_t scan;
	while ((scan=key_scan())==KEY_NOT_CODE);
	return scan;
}
/*----------------------------------------------------------------------------*/
#endif  /** __KEYPAD_H__ */
/*----------------------------------------------------------------------------*/
