/*----------------------------------------------------------------------------*/
#ifndef __MY1ESP01_H__
#define __MY1ESP01_H__
/*----------------------------------------------------------------------------*/
/**
 * esp01.h
 * - library for esp01 wifi module interface
 * - requires atcmd
AT version:1.2.0.0
SDK version:1.5.4.1
- tested ok
AT version:2.2.0.0
SDK version:v3.4-22-g967752e2
- no OK on AT+GMR
- no space after '>' on AT+CIPSEND
**/
/*----------------------------------------------------------------------------*/
#define ESP01_STMODE 0x01
#define ESP01_APMODE 0x02
#define ESP01_D2MODE (ESP01_APMODE|ESP01_STMODE)
/*----------------------------------------------------------------------------*/
#ifndef ESP01_WAIT_TIME
#define ESP01_WAIT_TIME 3000
#endif
/*----------------------------------------------------------------------------*/
#ifndef ESP01_SSID
#define ESP01_SSID "MY1ESP01"
#endif
#ifndef ESP01_PASS
#define ESP01_PASS ""
#endif
#ifndef WIFI_SSID
#define WIFI_SSID "KINGSPLAY00"
#endif
#ifndef WIFI_PASS
#define WIFI_PASS "12345678"
#endif
/*----------------------------------------------------------------------------*/
/* longer timeout for esp01 */
#define ATCMD_WAIT_DELAY ESP01_WAIT_TIME
/* baudrate for esp01 */
#define BRTCNT_USE BRTCNT_B115200
#include "atcmd.h"
/*----------------------------------------------------------------------------*/
/* uart functions */
#define esp01_init atcmd_init
#define esp01_send atcmd_send
#define esp01_read atcmd_read
#define esp01_puts atcmd_puts
#define esp01_peek atcmd_peek
#define esp01_purge atcmd_purge
/*----------------------------------------------------------------------------*/
/* atcmd macros */
#define esp01_find atcmd_find
#define esp01_wait_ok atcmd_wait_ok
#define esp01_recv_ok atcmd_recv_ok
#define esp01_echo_off atcmd_echo_off
/* should be called after esp01_task */
#define esp01_buff_ok() atcmd_buff_ok(espd.buff,espd.fill)
#define esp01_buff_error() atcmd_buff_error(espd.buff,espd.fill)
/* atcmd functions */
#define esp01_timeout() atcmd_timeout(ESP01_WAIT_TIME)
#define esp01_read_wait atcmd_read_wait
#define esp01_wait_str atcmd_wait_str
/*----------------------------------------------------------------------------*/
#define ESP01_OK ATCMD_OK
#define ESP01_TIMEOUT ATCMD_TIMEOUT
/*----------------------------------------------------------------------------*/
/** 'silent-mode' macros */
#define esp01_ok() (esp01_wait_ok()==ESP01_OK)
#define esp01_found() esp01_ok()
#define esp01_find_wait() do { esp01_find(); } while (!esp01_found())
#define esp01_ok2pass() if (!esp01_recv_ok()) hang()
/*----------------------------------------------------------------------------*/
#define ESP01CMD_FWINFO "AT+GMR\r\n"
#define ESP01CMD_IPADDR "AT+CIFSR\r\n"
#define ESP01CMD_SOCKINIT "AT+CIPSTART"
#define ESP01CMD_SOCKENDS "AT+CIPCLOSE"
#define ESP01CMD_SOCKDONE ESP01CMD_SOCKENDS"\r\n"
/*----------------------------------------------------------------------------*/
#define FWINFO_BUFFLEN 12
#define IPADDR_BUFFLEN 16
/*----------------------------------------------------------------------------*/
#define ESP01CMD_CWMODE_AT "AT+CWMODE"
#define ESP01CMD_CWMODE ESP01CMD_CWMODE_AT"?\r\n"
#define ESP01CMD_STMODE ESP01CMD_CWMODE_AT"=1\r\n"
#define ESP01CMD_APMODE ESP01CMD_CWMODE_AT"=2\r\n"
/*----------------------------------------------------------------------------*/
#define ESP01CMD_CWSAP_AT "AT+CWSAP"
#define ESP01CMD_CWSAPQ ESP01CMD_CWSAP_AT"?\r\n"
#define ESP01CMD_CWSAP_INIT ESP01CMD_CWSAP_AT"="
#define ESP01CMD_CWSAP_ARG0 "\""ESP01_SSID"\",\""ESP01_PASS"\""
#define ESP01CMD_CWSAP_ARG1 ",1,0\r\n"
#define ESP01CMD_CWSAP_ARGS  ESP01CMD_CWSAP_ARG0 ESP01CMD_CWSAP_ARG1
#define ESP01CMD_CWSAP ESP01CMD_CWSAP_INIT ESP01CMD_CWSAP_ARGS
/*----------------------------------------------------------------------------*/
#define ESP01CMD_CWJAP "AT+CWJAP"
#define ESP01CMD_CWJAP_ARGS "=\""WIFI_SSID"\",\""WIFI_PASS"\"\r\n"
#define ESP01CMD_CWJAP_DEF ESP01CMD_CWJAP ESP01CMD_CWJAP_ARGS
/*----------------------------------------------------------------------------*/
#define esp01_conf_stmode() atcmd_puts(ESP01CMD_STMODE)
#define esp01_conf_apmode() atcmd_puts(ESP01CMD_APMODE)
/*----------------------------------------------------------------------------*/
//__code char CSSID[] = ESP01_SSID;
#define CWSAP_SIZE 7
__code char CWSAP[] = "+CWSAP:";
#define CWMODE_SIZE 8
__code char CWMODE[] = "+CWMODE:";
#define CIFSR_SIZE 7
__code char CIFSR[] = "+CIFSR:";
#define ATVERS_SIZE 11
__code char ATVERS[] = "AT version:";
#define SDKVER_SIZE 12
__code char SDKVER[] = "SDK version:";
/** +CIFSR:STAIP,"192.168.1.1" */
#define IPFIND_SIZE 4
__code char IPFIND[] = "IP,\"";
/** +CIFSR:STAMAC,"00:00:00:00:00:00" */
#define MACFIND_SIZE 5
__code char MACFIND[] = "MAC,\"";
#define WIFIOK_SIZE 16
__code char WIFIOK[] = "WIFI CONNECTED\r\n";
#define ATCONNECT_SIZE 9
__code char ATCONNECT[] = "CONNECT\r\n";
#define ATCLOSED_SIZE 8
__code char ATCLOSED[] = "CLOSED\r\n";
/*----------------------------------------------------------------------------*/
#define ESP01_ERROR 0x80
#define ESP01_IDLE 0x40
#define ESP01_DATA 0x10
#define ESP01_FOUND 0x08
/*----------------------------------------------------------------------------*/
typedef struct _my1esp01_t {
	unsigned char loop, temp, test;
	unsigned char stat, fill, size;
	unsigned int ui16; /* temp 16-bit value - esp01_ap uses this */
	char tbuf[8];
	char *buff; /* keep buff external so it can be used elsewhere */
	char *pbuf; /* temp pointer useful for text extraction */
} my1esp01_t;
/*----------------------------------------------------------------------------*/
__xdata my1esp01_t espd;
/*----------------------------------------------------------------------------*/
#define esp01_buff(bf,sz) espd.buff=bf; espd.size=sz
/*----------------------------------------------------------------------------*/
#define ESP01_STAT_OK 0
/*----------------------------------------------------------------------------*/
#define ESP01_TASK_OK ESP01_STAT_OK
#define ESP01_TASK_TIMEOUT 0x01
#define ESP01_TASK_BUFFOVERFLOW 0x02
/*----------------------------------------------------------------------------*/
#define esp01_task_ok() (espd.stat==ESP01_TASK_OK)
#define esp01_task_timeout() (espd.stat&ESP01_TASK_TIMEOUT)
#define esp01_task_overflow() (espd.stat&ESP01_TASK_BUFFOVERFLOW)
/*----------------------------------------------------------------------------*/
void esp01_wait(void) {
	espd.fill = esp01_read_wait(espd.buff,espd.size);
	if (espd.fill==espd.size) {
		espd.fill--;
		espd.stat |= ESP01_TASK_BUFFOVERFLOW;
	}
	espd.buff[espd.fill] = 0x0;
	if (!espd.fill) espd.stat |= ESP01_TASK_TIMEOUT;
}
/*----------------------------------------------------------------------------*/
void esp01_task(char* pcmd) {
	espd.stat = ESP01_STAT_OK;
	esp01_puts(pcmd);
	esp01_wait();
}
/*----------------------------------------------------------------------------*/
#define esp01_getfirmware() esp01_task(ESP01CMD_FWINFO)
#define esp01_getcurrmode() esp01_task(ESP01CMD_CWMODE)
#define esp01_getcurrssid() esp01_task(ESP01CMD_CWSAPQ)
/*----------------------------------------------------------------------------*/
#define esp01_stconnect() esp01_task(ESP01CMD_CWJAP_DEF)
#define esp01_getipaddr() esp01_task(ESP01CMD_IPADDR)
/*----------------------------------------------------------------------------*/
#define ESP01_SEEK_OK ESP01_STAT_OK
#define ESP01_SEEK_FAIL 0x01
/*----------------------------------------------------------------------------*/
#define esp01_seek_atvers() esp01_seek_buff(ATVERS,ATVERS_SIZE)
#define esp01_seek_sdkver() esp01_seek_buff(SDKVER,SDKVER_SIZE)
#define esp01_seek_cwmode() esp01_seek_buff(CWMODE,CWMODE_SIZE)
#define esp01_seek_wifiok() esp01_seek_buff(WIFIOK,WIFIOK_SIZE)
#define esp01_seek_ipaddr() esp01_seek_buff(IPFIND,IPFIND_SIZE)
#define esp01_seek_mcaddr() esp01_seek_buff(MACFIND,MACFIND_SIZE)
#define esp01_seek_failmsg() esp01_seek_buff("FAIL\r\n",6)
#define esp01_seek_errormsg() esp01_seek_buff("ERROR\r\n",7)
/*----------------------------------------------------------------------------*/
#define esp01_seek_ssid() esp01_seek_buff(CWSAP,CWSAP_SIZE)
/* when do we use this? */
#define esp01_seek_ipadd2() esp01_seek_buff(CIFSR,CIFSR_SIZE)
/*----------------------------------------------------------------------------*/
#define esp01_seek_connect() esp01_seek_buff(ATCONNECT,ATCONNECT_SIZE)
#define esp01_seek_closed() esp01_seek_buff(ATCLOSED,ATCLOSED_SIZE)
/*----------------------------------------------------------------------------*/
unsigned char esp01_seek_buff(unsigned char* seek, unsigned char slen) {
	espd.stat = ESP01_SEEK_FAIL;
	espd.loop = 0; espd.temp = 0;
	while (espd.buff[espd.loop]&&espd.loop<espd.fill) {
		if (espd.buff[espd.loop]==seek[espd.temp]) {
			espd.temp++;
			if (espd.temp==slen) {
				espd.test = espd.loop+1;
				espd.stat = ESP01_SEEK_OK;
				break;
			}
		}
		else espd.temp = 0;
		espd.loop++;
	}
	return (espd.stat==ESP01_SEEK_OK)?1:0;
}
/*----------------------------------------------------------------------------*/
#define pchr(ptr) ((char*)ptr)
#define esp01_ipnull(ptr,psz) \
	(((psz)==7)&&pchr(ptr)[0]=='0'&&pchr(ptr)[1]=='.'&&pchr(ptr)[2]=='0'&& \
	pchr(ptr)[3]=='.'&&pchr(ptr)[4]=='0'&& \
	pchr(ptr)[5]=='.'&&pchr(ptr)[6]=='0')
/*----------------------------------------------------------------------------*/
/* ipv4 */
#define ESP01_IPADDR_SZ 16
/* 6-bytes hexdecimal */
#define ESP01_MCADDR_SZ 18
/*----------------------------------------------------------------------------*/
void esp01_ipaddr(char* buff) {
	buff[0] = 0x0;
	esp01_getipaddr();
	if (esp01_task_ok()) {
		if (esp01_seek_ipaddr()) {
			espd.pbuf = &espd.buff[espd.test];
			espd.loop = 0;
			while (espd.loop<IPADDR_BUFFLEN&&espd.pbuf[espd.loop]) {
				if (espd.pbuf[espd.loop]=='"')
					break;
				buff[espd.loop] = espd.pbuf[espd.loop++];
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void esp01_mcaddr(char* buff) {
	buff[0] = 0x0;
	if (!esp01_seek_mcaddr()) {
		esp01_getipaddr();
		if (!esp01_task_ok()||!esp01_seek_mcaddr())
			return;
	}
	espd.pbuf = &espd.buff[espd.test];
	espd.loop = 0;
	while (espd.loop<IPADDR_BUFFLEN&&espd.pbuf[espd.loop]) {
		if (espd.pbuf[espd.loop]=='"')
			break;
		buff[espd.loop] = espd.pbuf[espd.loop++];
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1ESP01_H__ */
/*----------------------------------------------------------------------------*/
