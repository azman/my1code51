/*----------------------------------------------------------------------------*/
#ifndef __HC06_H__
#define __HC06_H__
/*----------------------------------------------------------------------------*/
/**
 * hc06.h
 * - library for hc06 bluetooth module
 * - requires atcmd
 *   = atcmd by default uses uart2stc
 *   = define __ATCMD_ON_UART__ to use uart
**/
/*----------------------------------------------------------------------------*/
#include "atcmd.h"
/*----------------------------------------------------------------------------*/
#ifndef BTNAME
#define BTNAME "my1bluez51"
#endif
#ifndef BTPASS
#define BTPASS "1234"
#endif
/*----------------------------------------------------------------------------*/
/** linvorV1.8 */
#define HC06CMD_AT "AT"
#define HC06CMD_ATVERS "AT+VERSION"
#define HC06CMD_ATNAME "AT+NAME"
#define HC06CMD_ATPASS "AT+PIN"
#define HC06CMD_ATENDS ""
#if 0
#define HC06CMD_AT "AT\r\n"
#define HC06CMD_ATVERS "AT+VERSION\r\n"
#define HC06CMD_ATNAME "AT+NAME:"
#define HC06CMD_ATPASS "AT+PSWD:"
#define HC06CMD_ATENDS "\r\n"
#endif
/*----------------------------------------------------------------------------*/
#define HC06_WAIT_DELAY ATCMD_WAIT_DELAY
#define HC06_OK ATCMD_OK
#define HC06_TIMEOUT ATCMD_TIMEOUT
/*----------------------------------------------------------------------------*/
#define hc06_init atcmd_init
#define hc06_send atcmd_send
#define hc06_read atcmd_read
#define hc06_puts atcmd_puts
#define hc06_peek atcmd_peek
/*----------------------------------------------------------------------------*/
#define hc06_find() atcmd_puts(HC06CMD_AT)
#define hc06_wait_ok() atcmd_wait_str("OK",2)
#define hc06_wait(pbuf,size) atcmd_read_wait(pbuf,size)
#define hc06_getvers(pbuf,size) atcmd_wait_send(pbuf,size,HC06CMD_ATVERS)
#define hc06_setname(pbuf,size) \
	atcmd_wait_send(pbuf,size,HC06CMD_ATNAME BTNAME HC06CMD_ATENDS)
#define hc06_setpin(pbuf,size) \
	atcmd_wait_send(pbuf,size,HC06CMD_ATPASS BTPASS HC06CMD_ATENDS)
#define hc06_purge() while (!atcmd_timeout(HC06_WAIT_DELAY)) atcmd_read()
/*----------------------------------------------------------------------------*/
#endif /* __HC06_H__ */
/*----------------------------------------------------------------------------*/
