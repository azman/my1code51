/*----------------------------------------------------------------------------*/
#ifndef __APDS9960_H__
#define __APDS9960_H__
/*----------------------------------------------------------------------------*/
#include "my1stc51.h"
/*----------------------------------------------------------------------------*/
/**
 * uses i2c interface and optionally an interrupt
 * - interrupt should be conf/init by top level code
 * - by default using P3.2 (INTO@EX0)
**/
/*----------------------------------------------------------------------------*/
#include "i2c.h"
/*----------------------------------------------------------------------------*/
#ifndef APDS9960_INT
#define APDS9960_INT INT0
#endif
/*----------------------------------------------------------------------------*/
#ifndef APDS9960_I2C_ADDR
#define APDS9960_I2C_ADDR 0x39
#endif
/*----------------------------------------------------------------------------*/
#ifndef APDS9960_DEV_ID
#define APDS9960_DEV_ID 0xAB
#endif
/*----------------------------------------------------------------------------*/
/** register addresses - general */
#define APDS9960_REG_ENABLE  0x80
#define APDS9960_REG_ATIME   0x81
#define APDS9960_REG_WTIME   0x83
#define APDS9960_REG_IFORCE  0xE4
#define APDS9960_REG_PICLEAR 0xE5
#define APDS9960_REG_CICLEAR 0xE6
#define APDS9960_REG_AICLEAR 0xE7
/** register addresses - gesture */
#define APDS9960_REG_GPENTH  0xA0
#define APDS9960_REG_GEXTH   0xA1
#define APDS9960_REG_GCONF1  0xA2
#define APDS9960_REG_GCONF2  0xA3
#define APDS9960_REG_GCONF3  0xAA
#define APDS9960_REG_GCONF4  0xAB
#define APDS9960_REG_GSTATUS 0xAF
/* offsets are 8-bit sign magnitude values  */
#define APDS9960_REG_GOFFSET_U 0xA4
#define APDS9960_REG_GOFFSET_D 0xA5
#define APDS9960_REG_GOFFSET_L 0xA7
#define APDS9960_REG_GOFFSET_R 0xA9
/* GPLEN <= <B7|B6> { 00=4us , 01=8us(def), 10=16us, 11=32us } */
/* GPULSE <= <B5|B4|B3|B2|B1|B0> { # of pulses - 1 , i.e. max=64 } */
#define APDS9960_REG_GPULSE 0xA6
/* gesture fifo */
#define APDS9960_REG_GFIFO_LVL 0xAE
#define APDS9960_REG_GFIFO_U   0xFC
#define APDS9960_REG_GFIFO_D   0xFD
#define APDS9960_REG_GFIFO_L   0xFE
#define APDS9960_REG_GFIFO_R   0xFF
/** register addresses - proximity */
#define APDS9960_REG_PILT       0x89
#define APDS9960_REG_PIHT       0x8B
#define APDS9960_REG_PERS       0x8C
#define APDS9960_REG_CONFIG1    0x8D
/* PPLEN <= <B7|B6> { 00=4us , 01=8us(def), 10=16us, 11=32us } */
/* PPULSE <= <B5|B4|B3|B2|B1|B0> { # of pulses - 1 , i.e. max=64 } */
#define APDS9960_REG_PPULSE     0x8E
#define APDS9960_REG_CONTROL    0x8F
#define APDS9960_REG_CONFIG2    0x90
#define APDS9960_REG_ID         0x92
#define APDS9960_REG_STATUS     0x93
#define APDS9960_REG_CDATAL     0x94
#define APDS9960_REG_CDATAH     0x95
#define APDS9960_REG_RDATAL     0x96
#define APDS9960_REG_RDATAH     0x97
#define APDS9960_REG_GDATAL     0x98
#define APDS9960_REG_GDATAH     0x99
#define APDS9960_REG_BDATAL     0x9A
#define APDS9960_REG_BDATAH     0x9B
#define APDS9960_REG_PDATA      0x9C
#define APDS9960_REG_POFFSET_UR 0x9D
#define APDS9960_REG_POFFSET_DL 0x9E
#define APDS9960_REG_CONFIG3    0x9F
/** register addresses - color/ambient light sense */
#define APDS9960_REG_AILTL 0x84
#define APDS9960_REG_AILTH 0x85
#define APDS9960_REG_AIHTL 0x86
#define APDS9960_REG_AIHTH 0x87
/*----------------------------------------------------------------------------*/
/* APDS9960_REG_GCONF4 - 0xAB */
#define APDS9960_BIT_GMODE 0x01
/* APDS9960_REG_ENABLE - 0x80 */
#define APDS9960_BIT_PON 0x01
#define APDS9960_BIT_PEN 0x04
#define APDS9960_BIT_WEN 0x08
#define APDS9960_BIT_GEN 0x40
/* APDS9960_REG_GSTATUS - 0xAF */
#define APDS9960_BIT_GVALID 0x01
/* APDS9960_REG_CONFIG2 - 0x90 */
#define APDS9960_LEDBOOST_MASK 0x30
#define APDS9960_LEDBOOST_100P 0x00
#define APDS9960_LEDBOOST_150P 0x10
#define APDS9960_LEDBOOST_200P 0x20
#define APDS9960_LEDBOOST_300P 0x30
/* APDS9960_REG_GCONF1 - 0xA2 */
#define APDS9960_GFIFOTH_MASK 0xC0
#define APDS9960_GFIFOTH_I01 0x00
#define APDS9960_GFIFOTH_I04 0x40
#define APDS9960_GFIFOTH_I08 0x80
#define APDS9960_GFIFOTH_I16 0xC0
#define APDS9960_GEXMSK_MASK 0x3C
#define APDS9960_GEXPERS_MASK 0x03
#define APDS9960_GEXPERS_1GE 0x00
#define APDS9960_GEXPERS_2GE 0x01
#define APDS9960_GEXPERS_4GE 0x02
#define APDS9960_GEXPERS_7GE 0x03
/* APDS9960_REG_GCONF2 - 0xA3 */
#define APDS9960_GGAIN_MASK 0x60
#define APDS9960_GGAIN_1X 0x00
#define APDS9960_GGAIN_2X 0x20
#define APDS9960_GGAIN_4X 0x40
#define APDS9960_GGAIN_8X 0x60
#define APDS9960_GLDRIVE_MASK 0x18
/* full led drive at 100mA */
#define APDS9960_GLDRIVE_1000 0x00
#define APDS9960_GLDRIVE_0500 0x08
#define APDS9960_GLDRIVE_0250 0x10
#define APDS9960_GLDRIVE_0125 0x18
#define APDS9960_GWTIME_MASK 0x07
#define APDS9960_GWTIME_0000 0x00
/* this is for 2.8ms wait */
#define APDS9960_GWTIME_028 0x01
#define APDS9960_GWTIME_056 0x02
#define APDS9960_GWTIME_084 0x03
#define APDS9960_GWTIME_140 0x04
#define APDS9960_GWTIME_224 0x05
#define APDS9960_GWTIME_308 0x06
#define APDS9960_GWTIME_392 0x07
/* APDS9960_REG_GCONF3 - 0xAA */
#define APDS9960_GDIMS_MASK 0x03
#define APDS9960_GDIMS_BOTH 0x00
#define APDS9960_GDIMS_UD_ONLY 0x01
#define APDS9960_GDIMS_LR_ONLY 0x02
#define APDS9960_GDIMS_BOTH1 0x03
/*----------------------------------------------------------------------------*/
#define APDS9960_PLEN_04US 0x00
#define APDS9960_PLEN_08US 0x40
#define APDS9960_PLEN_16US 0x80
#define APDS9960_PLEN_32US 0xC0
#define APDS9960_PULSE(what) ((what-1)&0x3F)
/*----------------------------------------------------------------------------*/
#define bitcopy(that,from,tbit) if (from&tbit) that|=tbit; else that&=~tbit;
#define bitfrom(that,from,tbit) if (from) that|=tbit; else that&=~tbit;
/*----------------------------------------------------------------------------*/
__xdata unsigned char u1,d1,l1,r1;
__xdata unsigned char u0,d0,l0,r0;
__xdata int udr1, lrr1, udr0, lrr0, xdel, ydel, udcc, lrcc;
/*----------------------------------------------------------------------------*/
#define iabs(val) val<0?-val:val
/*----------------------------------------------------------------------------*/
void apds9960_enable(unsigned char pick)
{
	unsigned char temp;
	i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_ENABLE,&temp,1);
	bitcopy(temp,pick,APDS9960_BIT_PEN);
	bitcopy(temp,pick,APDS9960_BIT_WEN);
	bitcopy(temp,pick,APDS9960_BIT_GEN);
	bitcopy(temp,pick,APDS9960_BIT_PON);
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_ENABLE,&temp,1);
}
/*----------------------------------------------------------------------------*/
void apds9960_gesture_init(void)
{
	unsigned char temp;
	/* gesture enter/exit threshold */
	temp = 40;
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GPENTH,&temp,1);
	temp = 30;
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GEXTH,&temp,1);
	/* gesture events - 4ev for int, 1gend to exit */
	temp = APDS9960_GFIFOTH_I04|APDS9960_GEXPERS_1GE;
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GCONF1,&temp,1);
	/* gesture gain & led drive & gesture wait time */
	i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GCONF2,&temp,1);
	temp &= ~APDS9960_GGAIN_MASK; /* clear old */
	temp |= APDS9960_GGAIN_4X; /* set new */
	temp &= ~APDS9960_GLDRIVE_MASK;
	temp |= APDS9960_GLDRIVE_1000;
	temp &= ~APDS9960_GWTIME_MASK;
	temp |= APDS9960_GWTIME_028;
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GCONF2,&temp,1);
	/* gesture offsets - reset all */
	temp = 0;
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GOFFSET_U,&temp,1);
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GOFFSET_D,&temp,1);
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GOFFSET_L,&temp,1);
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GOFFSET_R,&temp,1);
	/* gesture pulse width & count */
	temp = APDS9960_PLEN_32US | APDS9960_PULSE(10);
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GPULSE,&temp,1);
	/* gesture dimensions pick */
	i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GCONF3,&temp,1);
	temp &= ~APDS9960_GDIMS_MASK; /* clear old */
	temp |= APDS9960_GDIMS_BOTH; /* set new */
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GPULSE,&temp,1);
	/* wait time */
	temp = 0xff;
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_WTIME,&temp,1);
	/* proximity pulse width & count */
	temp = APDS9960_PLEN_16US | APDS9960_PULSE(10);
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_PPULSE,&temp,1);
	/* led boost */
	i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_CONFIG2,&temp,1);
	temp &= ~APDS9960_LEDBOOST_MASK; /* clear old */
	temp |= APDS9960_LEDBOOST_300P; /* set new */
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_CONFIG2,&temp,1);
}
/*----------------------------------------------------------------------------*/
#define PICK_ALL_GESTURE (APDS9960_BIT_PEN|APDS9960_BIT_WEN| \
	APDS9960_BIT_GEN|APDS9960_BIT_PON)
/*----------------------------------------------------------------------------*/
void apds9960_gesture_mode(unsigned char on)
{
	unsigned char temp;
	/* gesture mode */
	i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GCONF4,&temp,1);
	bitfrom(temp,on,APDS9960_BIT_GMODE);
	i2c_putb(APDS9960_I2C_ADDR,APDS9960_REG_GCONF4,&temp,1);
	/* main enables */
	apds9960_enable(on?PICK_ALL_GESTURE:0x00);
}
/*----------------------------------------------------------------------------*/
unsigned char apds9960_gestured(void)
{
	unsigned char temp;
	i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GSTATUS,&temp,1);
	return temp&APDS9960_BIT_GVALID;
}
/*----------------------------------------------------------------------------*/
#define APDS9960_GTH 10
#define APDS9960_GSEN1 50
/*----------------------------------------------------------------------------*/
#define MOVE_U 0x01
#define MOVE_D 0x02
#define MOVE_L 0x04
#define MOVE_R 0x08
/*----------------------------------------------------------------------------*/
void apds9960_wait(unsigned char ncnt)
{
	do {
		NOP;
	} while(--ncnt);
}
/*----------------------------------------------------------------------------*/
void apds9960_wait_fifo(void)
{
	apds9960_wait(0xff);
	apds9960_wait(0xff);
	apds9960_wait(0xff);
	apds9960_wait(0xff);
}
/*----------------------------------------------------------------------------*/
unsigned char apds9960_read_gesture(void)
{
	unsigned char size, move, data[4];
	/* assume APDS9960_BIT_GVALID is set */
	move = 0; u1 = 0; u0 = 0;
	while (1)
	{
		apds9960_wait_fifo();
		i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GFIFO_LVL,&size,1);
		if (!size)
		{
			if (u1&&u0)
			{
				/* decode */
				udr1 = ((u1-d1)*100)/(u1+d1);
				lrr1 = ((l1-r1)*100)/(l1+r1);
				udr0 = ((u0-d0)*100)/(u0+d0);
				lrr0 = ((l0-r0)*100)/(l0+r0);
				ydel = udr0 - udr1;
				xdel = lrr0 - lrr1;
				udr0 = iabs(ydel);
				udcc = udr0<APDS9960_GSEN1?0:ydel;
				lrr0 = iabs(xdel);
				lrcc = lrr0<APDS9960_GSEN1?0:xdel;
				if (!udcc)
				{
					if (lrcc>0) move = MOVE_R;
					else if (lrcc<0) move = MOVE_L;
					/* else, no gesture */
				}
				else if (udcc<0)
				{
					if (!lrcc||udr0>lrr0) move = MOVE_D;
					else if (lrcc<0) move = MOVE_L;
					else /*if (lrcc>0)*/ move = MOVE_R;
				}
				else /*if (udcc>0)*/
				{
					if (!lrcc||udr0>lrr0) move = MOVE_U;
					else if (lrcc<0) move = MOVE_L;
					else /*if (lrcc>0)*/ move = MOVE_R;
				}
			}
			break;
		}
		i2c_getb(APDS9960_I2C_ADDR,APDS9960_REG_GFIFO_U,data,4);
		if ((data[0]>APDS9960_GTH)&&(data[1]>APDS9960_GTH)&&
				(data[2]>APDS9960_GTH)&&(data[3]>APDS9960_GTH))
		{
			if (!u1)
			{
				u1 = data[0]; d1 = data[1]; l1 = data[2]; r1 = data[3];
			}
			else
			{
				u0 = data[0]; d0 = data[1]; l0 = data[2]; r0 = data[3];
			}
		}
	}
	return move;
}
/*----------------------------------------------------------------------------*/
#endif /** __APDS9960_H__ */
/*----------------------------------------------------------------------------*/
