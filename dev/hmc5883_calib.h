/*----------------------------------------------------------------------------*/
#ifndef __MY1HMC5883_CALIB_H__
#define __MY1HMC5883_CALIB_H__
/*----------------------------------------------------------------------------*/
#include "hmc5883.h"
/*----------------------------------------------------------------------------*/
/**https://github.com/pganssle/HMC5883L*/
#define HMC5883_BIAS_XY 1160.0
#define HMC5883_BIAS_Z 1080.0
/*----------------------------------------------------------------------------*/
typedef struct _mcalibrate_t {
	int xval, yval, zval;
	float xcal, ycal, zcal;
} mcalibrate_t;
/*----------------------------------------------------------------------------*/
void magdata_copy(magdata_t* mdat, magdata_t* from) {
	mdat->xaxis = from->xaxis;
	mdat->yaxis = from->yaxis;
	mdat->zaxis = from->zaxis;
}
/*----------------------------------------------------------------------------*/
unsigned char hmc5883_biastest(magdata_t* mdat, unsigned char bias) {
	unsigned char savA, savB;
	savA = hmc5883_rval(HMC5883_REG_CFGA);
	savB = hmc5883_rval(HMC5883_REG_CFGB);
	hmc5883_conf(HMC5883_REG_CFGA,HMC5883_CFGA_DEF10|bias);
	hmc5883_conf(HMC5883_REG_CFGB,HMC5883_CFGB_GAIN5);
	hmc5883_read_data(0x0); /* ignore one after changing gain */
	hmc5883_read_data(mdat);
	/* +bias gain5 limits are [243,575] */
	hmc5883_conf(HMC5883_REG_CFGA,savA);
	hmc5883_conf(HMC5883_REG_CFGB,savB);
	hmc5883_read_data(0x0);
	return 0;
}
/*----------------------------------------------------------------------------*/
unsigned char hmc5883_calibrate(mcalibrate_t* mcal) {
	magdata_t tmpP, tmpN;
	hmc5883_biastest(&tmpP,HMC5883_CFGA_MSPOSB);
	hmc5883_biastest(&tmpN,HMC5883_CFGA_MSNEGB);
	mcal->xval = (tmpP.xaxis+tmpN.xaxis)>>1;
	mcal->yval = (tmpP.yaxis+tmpN.yaxis)>>1;
	mcal->zval = (tmpP.zaxis+tmpN.zaxis)>>1;
	/**https://github.com/pganssle/HMC5883L*/
	/* use these values as multiplier for axis data */
	mcal->xcal = mcal->xval/HMC5883_BIAS_XY;
	mcal->ycal = mcal->yval/HMC5883_BIAS_XY;
	mcal->zcal = mcal->zval/HMC5883_BIAS_Z;
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1HMC5883_CALIB_H__ */
/*----------------------------------------------------------------------------*/
