/*----------------------------------------------------------------------------*/
#ifndef __MY1QMC5883_H__
#define __MY1QMC5883_H__
/*----------------------------------------------------------------------------*/
#ifndef I2C_WAIT_TLEN
#define I2C_WAIT_TLEN 2
#endif
#include "i2c_core.h"
#include "math_atan.h"
/*----------------------------------------------------------------------------*/
#ifndef QMC5883_I2C_ADDR
#define QMC5883_I2C_ADDR 0x1a
#endif
/*----------------------------------------------------------------------------*/
#define QMC5883_MODE_CONTINUOUS 0x01
#define QMC5883_ODR_10HZ 0x00
#define QMC5883_ODR_50HZ 0x04
#define QMC5883_ODR_100HZ 0x08
#define QMC5883_ODR_200HZ 0x0C
#define QMC5883_RNG_2G 0x00
#define QMC5883_RNG_8G 0x10
#define QMC5883_OSR_512 0x00
#define QMC5883_OSR_256 0x40
#define QMC5883_OSR_128 0x80
#define QMC5883_OSR_64 0xC0
#define QMC5883_INIT0_STATE (QMC5883_ODR_200HZ|QMC5883_RNG_8G)
#define QMC5883_INIT1_STATE (QMC5883_ODR_100HZ)
#define QMC5883_INIT2_STATE (QMC5883_ODR_50HZ|QMC5883_RNG_8G)
#define QMC5883_INIT_STATE (QMC5883_MODE_CONTINUOUS|QMC5883_INIT0_STATE)
/*----------------------------------------------------------------------------*/
/** anisotropic magneto-resistive sensor data @ digital compass! */
typedef struct _magdata_t { /* qmc5883l i2c reg layout */
	int xaxis, yaxis, zaxis, xhead;
} magdata_t;
/*----------------------------------------------------------------------------*/
unsigned char qmc5883_find(void) {
	unsigned char find, test;
	test = 0;
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR);
	test |= i2c_do_send_byte(0x0d); /* chip id => 0xff? */
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR|0x01);
	find = i2c_do_read_byte(1);
	i2c_do_stop();
	if (test||find!=0xff) find = 0;
	return find;
}
/*----------------------------------------------------------------------------*/
unsigned char qmc5883_init(void) {
	unsigned char test;
	test = 0;
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR);
	test |= i2c_do_send_byte(0x0b); /* set/reset period FBR register */
	test |= i2c_do_send_byte(0x01); /* recommended value */
	i2c_do_stop();
	return test;
}
/*----------------------------------------------------------------------------*/
unsigned char qmc5883_conf(void) {
	unsigned char test;
	test = 0;
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR);
	test |= i2c_do_send_byte(0x09); /* mode register */
	test |= i2c_do_send_byte(QMC5883_INIT_STATE);
	i2c_do_stop();
	return test;
}
/*----------------------------------------------------------------------------*/
unsigned char qmc5883_done(void) {
	unsigned char test, done;
	test = 0;
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR);
	test |= i2c_do_send_byte(0x06); /* status register */
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR|0x01);
	done = i2c_do_read_byte(1);
	i2c_do_stop();
	if (test) done = 0;
	return done&0x01; /* ready bit */
}
/*----------------------------------------------------------------------------*/
unsigned char qmc5883_data(magdata_t* save) {
	unsigned char *curr;
	unsigned char loop, test;
	curr = (unsigned char*)save;
	test = 0;
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR);
	test |= i2c_do_send_byte(0x00);
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR|0x01);
	for (loop=0;loop<5;loop++)
		curr[loop] = (unsigned char)i2c_do_read_byte(0);
	curr[loop] = i2c_do_read_byte(1);
	i2c_do_stop();
	return test;
}
/*----------------------------------------------------------------------------*/
unsigned char qmc5883_conf2(void) {
	unsigned char test;
	test = 0;
	i2c_do_start();
	test |= i2c_do_send_byte(QMC5883_I2C_ADDR);
	test |= i2c_do_send_byte(0x0a);
	test |= i2c_do_send_byte(0x00);
	i2c_do_stop();
	return test;
}
/*----------------------------------------------------------------------------*/
void qmc5883_head(magdata_t *calc) {
	float temp;
	temp = atan2((float)calc->yaxis,(float)calc->xaxis);
	temp = rad2deg(temp);
	calc->xhead = (int)temp;
	/* rangeL 0-360 */
	if (calc->xhead<0) calc->xhead += 360;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1QMC5883_H__ */
/*----------------------------------------------------------------------------*/
