/*----------------------------------------------------------------------------*/
#ifndef __MY1SIM808GPS_H__
#define __MY1SIM808GPS_H__
/*----------------------------------------------------------------------------*/
/**
 * sim808gps.h
 * - sim808 interface for gps info
 * - requires sim808base.h
 * - data:(9)
 * mode (cgpsinf=0)
 * lng,lat,alt
 * utc (yyyymmddHHMMSS.sssyyy),
 * time-to-first-fix(secs.. always 0?),
 * num(satellite-count),
 * vel,dir
**/
/*----------------------------------------------------------------------------*/
#include "sim808base.h"
/*----------------------------------------------------------------------------*/
#define sim808gps_init() atcmd_puts("AT+CGPSPWR=1\r\n")
#define sim808gps_data() atcmd_puts("AT+CGPSINF=0\r\n")
#define sim808gps_opts() atcmd_puts("AT+CGPSINF=?\r\n");
#define sim808gps_fixd() atcmd_puts("AT+CGPSSTATUS?\r\n");
/*----------------------------------------------------------------------------*/
typedef struct _sim808gps_inf_t {
	char *plng, *plat, *palt, *putc;
	char *pfix, *pnum, *pvel, *pdir;
	char *p_ok, *pchk, *pstr;
	unsigned char temp, loop;
	/* external buffer */
	char *buff;
	unsigned char size;
} sim808gps_inf_t;
/*----------------------------------------------------------------------------*/
#ifndef SIM808GPS_INF_TYPE
#define SIM808GPS_INF_TYPE __xdata
#endif
/*----------------------------------------------------------------------------*/
SIM808GPS_INF_TYPE sim808gps_inf_t gpsstr;
/*----------------------------------------------------------------------------*/
#define sim808gps_inf_init(pb,sz) gpsstr.buff = pb; gpsstr.size = sz
/*----------------------------------------------------------------------------*/
void sim808gps_wait_data(void) {
	gpsstr.temp = atcmd_read_wait(gpsstr.buff,gpsstr.size);
	gpsstr.p_ok = 0x0;
	if (gpsstr.temp) {
		gpsstr.pchk = sim808_trim_ok(gpsstr.buff,gpsstr.temp);
		if (gpsstr.pchk) {
			gpsstr.pchk = sim808_trim_stat(gpsstr.pchk);
			if (gpsstr.pchk) {
				gpsstr.pstr = gpsstr.pchk; gpsstr.loop = 0;
				while (gpsstr.pstr[0]) {
					if (gpsstr.pstr[0]==',') {
						gpsstr.loop++;
						switch (gpsstr.loop) {
							case 1: gpsstr.plng = &gpsstr.pstr[1]; break;
							case 2: gpsstr.plat = &gpsstr.pstr[1]; break;
							case 3: gpsstr.palt = &gpsstr.pstr[1]; break;
							case 4: gpsstr.putc = &gpsstr.pstr[1]; break;
							case 5: gpsstr.pfix = &gpsstr.pstr[1]; break;
							case 6: gpsstr.pnum = &gpsstr.pstr[1]; break;
							case 7: gpsstr.pvel = &gpsstr.pstr[1]; break;
							case 8: gpsstr.pdir = &gpsstr.pstr[1]; break;
						}
					}
					else if (gpsstr.pstr[0]=='\r'||gpsstr.pstr[0]=='\n') {
						gpsstr.pstr[0] = 0x0; break;
					}
					gpsstr.pstr++;
				}
				if (gpsstr.loop==8) gpsstr.p_ok = gpsstr.pchk;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1SIM808GPS_H__ */
/*----------------------------------------------------------------------------*/
