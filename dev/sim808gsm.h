/*----------------------------------------------------------------------------*/
#ifndef __MY1SIM808GSM_H__
#define __MY1SIM808GSM_H__
/*----------------------------------------------------------------------------*/
/**
 * sim808gsm.h
 * - sim808 interface for gsm (basic call/sms)
 * - requires sim808base.h
 * AT+CGMI 	Get manufacturer info
 * AT+CGMM 	Get manufacturer model
 * AT+CGSN 	Get IMEI (unique worldwide) 
**/
/*----------------------------------------------------------------------------*/
#include "sim808base.h"
/*----------------------------------------------------------------------------*/
#define sim808gsm_stat() atcmd_puts("AT+CREG?\r\n")
#define sim808gsm_init() atcmd_puts("AT+CREG=1\r\n")
#define sim808gsm_cpin() atcmd_puts("AT+CPIN?\r\n")
#define sim808gsm_conf_text() atcmd_puts("AT+CMGF=1\r\n");
#define sim808gsm_signalq() atcmd_puts("AT+CSQ\r\n")
/*----------------------------------------------------------------------------*/
#define SIM808GSM_TEXT_ERROR 0x01
/*----------------------------------------------------------------------------*/
unsigned char sim808gsm_text(char* pnum, char* ptxt) {
	unsigned char wait, find;
	atcmd_puts("AT+CMGS=\"");
	atcmd_puts(pnum);
	atcmd_puts("\"\r\n");
	wait = 0; find = 0;
	do {
		if (atcmd_timeout(ATCMD_WAIT_DELAY))
			break;
		wait = atcmd_read();
		if (wait=='>') break;
	} while (1);
	if (wait!='>') return SIM808GSM_TEXT_ERROR;
	atcmd_puts(ptxt);
	atcmd_send(0x1a); /* ctrl+z */
	return ATCMD_OK;
}
/*----------------------------------------------------------------------------*/
void sim808gsm_call(char* pnum) {
	atcmd_puts("ATD");
	atcmd_puts(pnum);
	atcmd_puts(";\r\n");
}
/*----------------------------------------------------------------------------*/
void sim808gsm_hang(void) {
	atcmd_puts("ATH\r\n");
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1SIM808GSM_H__ */
/*----------------------------------------------------------------------------*/
