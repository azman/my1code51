/*----------------------------------------------------------------------------*/
#ifndef __TEXTLCD_H__
#define __TEXTLCD_H__
/*----------------------------------------------------------------------------*/
/**
 * textlcd.h
 *   - library for text lcd using HD44780U controller/driver
 *   - by default uses 4-bit interface
 *     = use #define _LCD_8BIT_INTERFACE_ to use 8-bit interface
 *   - default interface pins:
 *     = [4bit] DB4-DB7 => P0.4-P0.7, E => P0.2, R/W => P0.1, RS => P0.0
 *     = [8bit] DB0-DB7 => P0, E => P2.7, R/W => P2.6, RS => P2.5
 *     = use #define _LCD_PINS_DEFINED_ to use other pins
TextLCD pins: <1> Vss,Vcc,Vee | RS,R/W,E | D0-D7 | A,K
**/
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
/*----------------------------------------------------------------------------*/
#define LCD_8BITS_FLAG 0x10
#define LCD_2LINE_FLAG 0x08
/* default font is 5X8 dots */
#define LCD_5X10D_FLAG 0x04
/*----------------------------------------------------------------------------*/
#ifndef _LCD_8BIT_INTERFACE_
#define LCD_8BIT_OPT 0x00
#ifndef _LCD_PINS_DEFINED_
my1sfr(LCD_DATA,0x80);
my1sbit(LCD_BUSY,0x87);
my1sbit(LCD_ENB,0x82);
my1sbit(LCD_RNW,0x81);
my1sbit(LCD_DNC,0x80);
#endif
#else
#define LCD_8BIT_OPT LCD_8BITS_FLAG
#ifndef _LCD_PINS_DEFINED_
my1sfr(LCD_DATA,0x80);
my1sbit(LCD_BUSY,0x87);
my1sbit(LCD_ENB,0xA7);
my1sbit(LCD_RNW,0xA6);
my1sbit(LCD_DNC,0xA5);
#endif
#endif
/*----------------------------------------------------------------------------*/
/** for stc 1ms requires ~335 */
#define LCD_WAIT_1MS 335
#define LCD_WAIT_40MS (LCD_WAIT_1MS*40)
#define LCD_WAIT_4P1MS (LCD_WAIT_1MS*5)
#define LCD_WAIT_100US (LCD_WAIT_1MS/10)
#define LCD_WAIT_EXEC8 (LCD_WAIT_1MS*3)
#define LCD_WAIT_EXEC4 (LCD_WAIT_1MS*5)
/*----------------------------------------------------------------------------*/
/* nxp does not need this! needed for stc... so much faster! :o */
/** PWmin(LCD_ENB) = 230n (from HD44780U datasheet) */
#ifndef LCD_PULSE_DELAY
#define LCD_PULSE_DELAY 24
#endif
/*----------------------------------------------------------------------------*/
/** lcd init reset sequence */
#define LCD_CMD_INITSEQ 0x30
/** lcd init bit select */
#define LCD_CMD_BIT_OPT (0x20|LCD_8BIT_OPT)
/** configure 2-lines, 5x8 dots font */
#define LCD_CMD_CONF_L2F5X7 (LCD_CMD_BIT_OPT|LCD_2LINE_FLAG)
/** display off */
#define LCD_CMD_CONF_DISP0 ((unsigned char)0x08)
/** display on, cursor off, blinking off */
#define LCD_CMD_CONF_D1C0B0 0x0C
/** addr increment, shift diplay off - entry mode set? */
#define LCD_CMD_CONF_CIS0 0x06
/** clear diplay, ram addr 0 */
#define LCD_CMD_CLEAR 0x01
/** lcd max char per row */
#define LCD_MAX_CHAR 16
/** lcd line cursor positions */
#define LCD_POS_LINE1 0x80
#define LCD_POS_LINE2 0xC0
/*----------------------------------------------------------------------------*/
/* useful to differentiate type */
typedef unsigned char lcdbyte_t;
/*----------------------------------------------------------------------------*/
void lcd_pulse_e(void) {
	unsigned char loop = LCD_PULSE_DELAY;
	LCD_ENB = 1;
	while (loop>0) loop--;
	LCD_ENB = 0;
}
/*----------------------------------------------------------------------------*/
void lcd_busy_wait(void) { /** wait while lcd is busy! */
	unsigned char done = 0;
	/** command, read */
	LCD_DNC = 0; LCD_RNW = 1;
	LCD_BUSY = 0; /** ease up lcd pull up? NEED 1 as input!! */
	while (!done) {
		lcd_pulse_e();
		if (!LCD_BUSY) done = 1;
#ifndef _LCD_8BIT_INTERFACE_
		lcd_pulse_e();
#endif
	}
}
/*----------------------------------------------------------------------------*/
void lcd_init_byte(lcdbyte_t command, unsigned int delay) {
	while(delay>0) delay--;
#ifdef _LCD_8BIT_INTERFACE_
	LCD_DATA = command;
#else
	LCD_DATA = command & 0xF0;
#endif
	/** command, write */
	LCD_DNC = 0; LCD_RNW = 0;
	lcd_pulse_e();
}
/*----------------------------------------------------------------------------*/
#define lcd_init_nibb(c,d) { lcd_init_byte(c,d); lcd_init_byte(c<<4,d); }
/*----------------------------------------------------------------------------*/
void lcd_command(lcdbyte_t command) {
	lcd_busy_wait();
#ifdef _LCD_8BIT_INTERFACE_
	LCD_DATA = command;
#else
	LCD_DATA = command & 0xF0;
	/** command, write */
	LCD_DNC = 0; LCD_RNW = 0;
	lcd_pulse_e();
	lcd_busy_wait();
	LCD_DATA = (command<<4) & 0xF0;
#endif
	/** command, write */
	LCD_DNC = 0; LCD_RNW = 0;
	lcd_pulse_e();
}
/*----------------------------------------------------------------------------*/
void lcd_data(lcdbyte_t asciidat) {
	lcd_busy_wait();
#ifdef _LCD_8BIT_INTERFACE_
	LCD_DATA = asciidat;
#else
	LCD_DATA = asciidat & 0xF0;
	/** data, write */
	LCD_DNC = 1; LCD_RNW = 0;
	lcd_pulse_e();
	lcd_busy_wait();
	LCD_DATA = (asciidat<<4) & 0xF0;
#endif
	/** data, write */
	LCD_DNC = 1; LCD_RNW = 0;
	lcd_pulse_e();
}
/*----------------------------------------------------------------------------*/
void lcd_init(void) {
	LCD_ENB = 0;
	/** wait >40ms Vcc=2.7V @ >15ms Vcc=4.5V (from HD44780U datasheet) */
	lcd_init_byte(LCD_CMD_INITSEQ,LCD_WAIT_40MS);
	/** wait >4.1ms (from HD44780U datasheet) */
	lcd_init_byte(LCD_CMD_INITSEQ,LCD_WAIT_4P1MS);
	/** wait >100ums (from HD44780U datasheet) */
	lcd_init_byte(LCD_CMD_INITSEQ,LCD_WAIT_100US);
	/** reset sequence */
#ifdef _LCD_8BIT_INTERFACE_
	lcd_init_byte(LCD_CMD_CONF_L2F5X7,LCD_WAIT_EXEC8); /* function set */
	lcd_init_byte(LCD_CMD_CONF_DISP0,LCD_WAIT_EXEC8); /* display off */
	lcd_init_byte(LCD_CMD_CLEAR,LCD_WAIT_EXEC8); /* display clear */
	lcd_init_byte(LCD_CMD_CONF_CIS0,LCD_WAIT_EXEC8); /* entry mode */
#else
	lcd_init_byte(LCD_CMD_BIT_OPT,LCD_WAIT_EXEC4);
	lcd_init_nibb(LCD_CMD_CONF_L2F5X7,LCD_WAIT_EXEC4);
	lcd_init_nibb(LCD_CMD_CONF_DISP0,LCD_WAIT_EXEC4);
	lcd_init_nibb(LCD_CMD_CLEAR,LCD_WAIT_EXEC4);
	lcd_init_nibb(LCD_CMD_CONF_CIS0,LCD_WAIT_EXEC4);
#endif
	/** busy flag now SHOULD REALLY be usable! */
	lcd_command(LCD_CMD_CONF_D1C0B0);
}
/*----------------------------------------------------------------------------*/
void lcd_puts(char* mesg) {
	while(*mesg) {
		lcd_data(*mesg);
		mesg++;
	}
}
/*----------------------------------------------------------------------------*/
#define lcd_print(text) lcd_puts(text)
#define lcd_goto_line1() lcd_command(LCD_POS_LINE1)
#define lcd_goto_line2() lcd_command(LCD_POS_LINE2)
/*----------------------------------------------------------------------------*/
#endif /** __TEXTLCD_H__ */
/*----------------------------------------------------------------------------*/
