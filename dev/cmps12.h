/*----------------------------------------------------------------------------*/
#ifndef __MY1CMPS12_H__
#define __MY1CMPS12_H__
/*----------------------------------------------------------------------------*/
/**
 *  cmps12.h - library for CMPS12 module
 *  - requires i2c_core
 */
/*----------------------------------------------------------------------------*/
#include "i2c_core.h"
/*----------------------------------------------------------------------------*/
#ifndef CMPS12_I2C_ADDR
#define CMPS12_I2C_ADDR 0x60
#endif
#define CMPS12_I2C_AUSE (unsigned char)(CMPS12_I2C_ADDR<<1)
/*----------------------------------------------------------------------------*/
#define CMPS12_REG_VERSION 0x00
#define CMPS12_REG_HEAD08 0x01
#define CMPS12_REG_HEAD16 0x02
/*----------------------------------------------------------------------------*/
unsigned char cmps12_getversion(void) {
	unsigned char sver;
	i2c_do_start();
	i2c_do_send_byte(CMPS12_I2C_AUSE);
	i2c_do_send_byte(CMPS12_REG_VERSION);
	i2c_do_start(); /* send a restart for read */
	i2c_do_send_byte(CMPS12_I2C_AUSE|0x01); /* activate read bit */
	sver = i2c_do_read_byte(1);
	i2c_do_stop();
	return sver;
}
/*----------------------------------------------------------------------------*/
unsigned char cmps12_gethead08(void) {
	unsigned char head;
	i2c_do_start();
	i2c_do_send_byte(CMPS12_I2C_AUSE);
	i2c_do_send_byte(CMPS12_REG_HEAD08);
	i2c_do_start();
	i2c_do_send_byte(CMPS12_I2C_AUSE|0x01);
	head = i2c_do_read_byte(1);
	i2c_do_stop();
	return head;
}
/*----------------------------------------------------------------------------*/
unsigned int cmps12_gethead16(void) {
	unsigned int head;
	i2c_do_start();
	i2c_do_send_byte(CMPS12_I2C_AUSE);
	i2c_do_send_byte(CMPS12_REG_HEAD16);
	i2c_do_start();
	i2c_do_send_byte(CMPS12_I2C_AUSE|0x01);
	head = (unsigned char)i2c_do_read_byte(0)<<8; /* upper byte first! */
	head |= i2c_do_read_byte(1);
	i2c_do_stop();
	return head;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1CMPS12_H__ */
/*----------------------------------------------------------------------------*/
