/*----------------------------------------------------------------------------*/
#ifndef __MY1SIM808BASE_H__
#define __MY1SIM808BASE_H__
/*----------------------------------------------------------------------------*/
/**
 * sim808base.h
 * - sim808 uart interface
 * - requires atcmd.h
**/
/*----------------------------------------------------------------------------*/
#ifndef SIM808_WAIT_TIME
#define SIM808_WAIT_TIME 1000
#endif
/*----------------------------------------------------------------------------*/
#define ATCMD_WAIT_DELAY SIM808_WAIT_TIME
/*----------------------------------------------------------------------------*/
#include "atcmd.h"
/*----------------------------------------------------------------------------*/
#define SIM808_OK ATCMD_OK
#define SIM808_TIMEOUT ATCMD_TIMEOUT
/*----------------------------------------------------------------------------*/
/* uart functions / atcmd macros */
#define sim808_init atcmd_init
#define sim808_send atcmd_send
#define sim808_read atcmd_read
#define sim808_puts atcmd_puts
#define sim808_peek atcmd_peek
#define sim808_purge atcmd_purge
/*----------------------------------------------------------------------------*/
/* atcmd macros */
#define sim808_find atcmd_find
#define sim808_wait_ok atcmd_wait_ok
#define sim808_recv_ok atcmd_recv_ok
#define sim808_echo_off atcmd_echo_off
#define sim808_found atcmd_found
#define sim808_ok atcmd_ok
/*----------------------------------------------------------------------------*/
/* atcmd functions */
#define sim808_timeout() atcmd_timeout(SIM808_WAIT_TIME)
#define sim808_read_wait atcmd_read_wait
#define sim808_wait_str atcmd_wait_str
/*----------------------------------------------------------------------------*/
#define sim808_buff_ok(pbuf,ccnt) \
	(ccnt>=6&&pbuf[ccnt-1]=='\n'&&pbuf[ccnt-2]=='\r'&&pbuf[ccnt-3]=='K'&& \
	pbuf[ccnt-4]=='O'&&pbuf[ccnt-5]=='\n'&&pbuf[ccnt-6]=='\r')?1:0
#define sim808_trim_endl(pbuf,ccnt) \
	while (pbuf[ccnt-1]==0x0d||buff[ccnt-1]==0x0a) { \
		ccnt--; pbuf[ccnt] = 0x0; }
/*----------------------------------------------------------------------------*/
/** 'silent-mode' macros */
#define sim808_find_wait() do { sim808_find(); } while (!sim808_found())
#define sim808_ok2pass() if (!sim808_recv_ok()) hang()
/*----------------------------------------------------------------------------*/
/* sim808-specific at command */
#define sim808_minf() atcmd_puts("AT+CGMI\r\n")
#define sim808_mmod() atcmd_puts("AT+CGMM\r\n")
#define sim808_imei() atcmd_puts("AT+CGSN\r\n")
/*----------------------------------------------------------------------------*/
char* sim808_trim_ok(char* buff, unsigned char size) {
	/* size should be output of atcmd_read_wait */
	if (!sim808_buff_ok(buff,size)) return 0x0;
	size -= 6;
	buff[size] = 0x0;
	if (size>2)
		sim808_trim_endl(buff,size);
	if (size>2)
		while (buff[0]==0x0d||buff[0]==0x0a) buff++;
	return buff;
}
/*----------------------------------------------------------------------------*/
char* sim808_trim_stat(char* buff) {
	char *pstr, find;
	if (buff[0]!='+') return 0x0;
	buff++; pstr = 0x0; find = 0;
	while (buff[0]) {
		if (find) {
			if (buff[0]==' ') {
				pstr = &buff[1];
				find = 0;
			}
		}
		/* trim at first newline */
		if (buff[0]=='\r'||buff[0]=='\n') {
			buff[0] = 0x0;
			break;
		}
		else if (buff[0]==':')
			if (!pstr) find = buff[0];
		buff++;
	}
	return pstr;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1SIM808BASE_H__ */
/*----------------------------------------------------------------------------*/
