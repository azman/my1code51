/*----------------------------------------------------------------------------*/
#ifndef __MY1ESP01_AP_H__
#define __MY1ESP01_AP_H__
/*----------------------------------------------------------------------------*/
/**
 * esp01_ap.h
 * - extension for esp01.h
 * - provides useful function(s) for access-point mode
**/
/*----------------------------------------------------------------------------*/
#include "esp01.h"
/*----------------------------------------------------------------------------*/
#define SSID_BUFFLEN 16
/*----------------------------------------------------------------------------*/
void esp01_ap_ssid(char* buff) {
	buff[0] = 0x0;
	esp01_getcurrssid();
	if (esp01_task_ok()) {
		if (esp01_seek_ssid()) {
			espd.pbuf = &espd.buff[espd.test+1];
			espd.loop = 0;
			while (espd.loop<SSID_BUFFLEN&&espd.pbuf[espd.loop]) {
				if (espd.pbuf[espd.loop]=='"')
					break;
				buff[espd.loop] = espd.pbuf[espd.loop++];
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
/* espd.stat flag */
#define ESP01AP_CONN_OK 0x04
#define ESP01AP_LINK_OK 0x08
#define ESP01AP_TAGD_OK 0x10
#define ESP01AP_DCNT_OK 0x20
#define ESP01AP_DATA_OK 0x40
#define ESP01AP_CCRC_OK 0x80
/*----------------------------------------------------------------------------*/
#define ESP01AP_REQ0_OK (ESP01AP_LINK_OK|ESP01AP_TAGD_OK)
#define ESP01AP_REQ1_OK (ESP01AP_DCNT_OK|ESP01AP_DATA_OK)
#define ESP01AP_REQ2_OK (ESP01AP_CCRC_OK|ESP01AP_REQ1_OK)
#define ESP01AP_REQD_OK (ESP01AP_REQ2_OK|ESP01AP_REQ0_OK)
/*----------------------------------------------------------------------------*/
#define ESP01AP_PORT "1337"
#define ESP01AP_SSID_SETERR 0x10
/*----------------------------------------------------------------------------*/
#define ESP01AP_MULTICONN "AT+CIPMUX=1\r\n"
#define ESP01AP_INIT0 "AT+CIPSERVER=1,"
#define ESP01AP_INIT1 "\r\n"
#define ESP01AP_INIT ESP01AP_INIT0 ESP01AP_PORT ESP01AP_INIT1
/*----------------------------------------------------------------------------*/
/* specially crafted for esp01_ap_scan_req! */
#define esp01_buff_connect(buf,cnt) \
	(buf[cnt-2]=='\r'&&buf[cnt-3]=='T'&&buf[cnt-4]=='C'&& \
	buf[cnt-5]=='E'&&buf[cnt-6]=='N'&&buf[cnt-7]=='N'&&buf[cnt-8]=='O'&& \
	buf[cnt-9]=='C')?1:0
/*----------------------------------------------------------------------------*/
#define IPDTAG_SIZE 7
__code char IPDTAG[] = "\r\n+IPD,";
/*----------------------------------------------------------------------------*/
#define esp01_ap_setssid_error() (espd.stat&ESP01AP_SSID_SETERR)
/*----------------------------------------------------------------------------*/
#define esp01_ap_multiconn() esp01_puts(ESP01AP_MULTICONN)
#define esp01_ap_serve() esp01_puts(ESP01AP_INIT)
/*----------------------------------------------------------------------------*/
void esp01_ap_ssid_set(void) {
	esp01_task(ESP01CMD_CWSAP);
	if (!esp01_task_ok()||!esp01_buff_ok())
		espd.stat |= ESP01AP_SSID_SETERR;
}
/*----------------------------------------------------------------------------*/
#define esp01_ap_scan_prep() espd.stat=ESP01_STAT_OK; espd.loop=0
#define esp01_ap_conn_ok() (espd.stat&ESP01AP_CONN_OK)
#define esp01_ap_data_ok() (espd.stat==ESP01AP_REQD_OK)
/*----------------------------------------------------------------------------*/
void esp01_ap_scan_req(void) {
	if (esp01_peek()) {
		espd.temp = esp01_read();
		espd.buff[espd.loop++] = espd.temp;
		if (espd.temp=='\n') {
			if (espd.loop>=9) {
				if (esp01_buff_connect(espd.buff,espd.loop))
					espd.stat |= ESP01AP_CONN_OK;
			}
			espd.loop = 0;
		}
	}
}
/*----------------------------------------------------------------------------*/
#include "utils_lite.h"
#include "crc16.h"
/*----------------------------------------------------------------------------*/
void esp01_ap_read_req(void) {
	espd.stat = ESP01_STAT_OK; /* just in case */
	do {
		/* espd.test will hold channel number (1 digit/ascii char) */
		if (espd.buff[1]==','&&espd.buff[0]>=0x30&&espd.buff[0]<=0x39) {
			espd.stat |= ESP01AP_LINK_OK;
			espd.test = espd.buff[0];
		}
		else espd.test = 0x0;
		espd.loop = 0;
		while (espd.loop<IPDTAG_SIZE) {
			if (esp01_timeout()) {
				espd.stat |= ESP01_TASK_TIMEOUT;
				break;
			}
			espd.buff[espd.loop] = esp01_read();
			if (espd.buff[espd.loop]!=IPDTAG[espd.loop])
				break;
			espd.loop++;
		}
		if (esp01_task_timeout()) break;
		if (espd.loop!=IPDTAG_SIZE) break;
		espd.stat |= ESP01AP_TAGD_OK;
		/* get link channel & data count */
		espd.fill = 0; espd.loop = 0; espd.temp = 0;
		while (espd.loop<espd.size) {
			if (esp01_timeout()) {
				espd.stat |= ESP01_TASK_TIMEOUT;
				break;
			}
			espd.buff[espd.loop] = esp01_read();
			if (espd.buff[espd.loop]==':') {
				espd.buff[espd.loop] = 0;
				if ((espd.loop-espd.temp)<4) {
					cstr_2uint16(&espd.buff[espd.temp],&espd.ui16);
					if (espd.ui16>0)
						espd.fill = espd.loop;
				}
				break;
			}
			else if (espd.buff[espd.loop]==',')
				espd.temp = espd.loop+1;
			espd.loop++;
		}
		if (esp01_task_timeout()) break;
		if (espd.temp>=2&&(espd.buff[espd.temp-2]!=espd.test))
			break; /* enforce same channel? */
		if (!espd.fill) break;
		espd.stat |= ESP01AP_DCNT_OK;
		/* read data */
		for (espd.loop=0;espd.loop<espd.ui16;espd.loop++) {
			if (esp01_timeout()) {
				espd.stat |= ESP01_TASK_TIMEOUT;
				break;
			}
			espd.buff[espd.loop] = esp01_read();
		}
		if (esp01_task_timeout()) break;
		if (espd.loop!=espd.ui16) break;
		espd.stat |= ESP01AP_DATA_OK;
		crc16_calc(espd.buff,espd.loop-2);
		if (ccrc==((espd.buff[espd.loop-1]<<8)|espd.buff[espd.loop-2])) {
			espd.fill = espd.loop;
			espd.stat |= ESP01AP_CCRC_OK;
		}
	} while (0);
	if (esp01_task_timeout())
		esp01_purge();
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1ESP01_AP_H__ */
/*----------------------------------------------------------------------------*/
