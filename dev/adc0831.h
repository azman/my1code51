/*----------------------------------------------------------------------------*/
#ifndef __ADC0831_H__
#define __ADC0831_H__
/*----------------------------------------------------------------------------*/
/**
 * adc0831.h
 * - library for adc0831 8-bit serial Analog-to-Digital Converter
**/
/*----------------------------------------------------------------------------*/
#include "my1mcu51.h"
/*----------------------------------------------------------------------------*/
/** => P3.5-P3.7 */
my1sbit(ADC_CSN,0xB5);
my1sbit(ADC_CLK,0xB6);
my1sbit(ADC_DAT,0xB7);
/*----------------------------------------------------------------------------*/
/* assume using default Vref */
#define VREF 5.0
/*----------------------------------------------------------------------------*/
/* useful to differentiate type */
typedef unsigned char adcbyte_t;
/*----------------------------------------------------------------------------*/
void adc_init(void)
{
	ADC_CSN = 1; ADC_CLK = 0; ADC_DAT = 1;
}
/*----------------------------------------------------------------------------*/
adcbyte_t adc_get_data(void)
{
	adcbyte_t adcd = 0x0, adct = 8;
	/** dumb=0 == NOP ? cannot find Keil/SDCC cross solution */
	ADC_CSN = 0; NOP; NOP;
	/** need t(su)=350ns delay : no problemo */
	/** f(clk) is between 10kHz (0.1ms) to 400kHz (2.5us) */
	ADC_CLK = 1; NOP; ADC_CLK = 0; NOP;
	/** first clk to setup mux? data after 2 clks */
	do {
		ADC_CLK = 1; NOP; ADC_CLK = 0; NOP;
		/** t(dp) output delay (typ:650ns,max:1500ns) */
		adcd = adcd << 1;
		adcd = adcd | ADC_DAT; /** read on +ve edge */
		adct--;
	} while(adct>0);
	/** extra clk ( do we need this? ) */
	ADC_CLK = 1; NOP; ADC_CLK = 0; NOP;
	ADC_CSN = 1;
	return adcd;
}
/*----------------------------------------------------------------------------*/
#endif /** __ADC0831_H__ */
/*----------------------------------------------------------------------------*/
