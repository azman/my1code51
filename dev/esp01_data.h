/*----------------------------------------------------------------------------*/
#ifndef __MY1ESP01_DATA_H__
#define __MY1ESP01_DATA_H__
/*----------------------------------------------------------------------------*/
/**
 * esp01_data.h
 * - extension library for esp01
 */
/*----------------------------------------------------------------------------*/
#include "esp01.h"
/*----------------------------------------------------------------------------*/
/**
 * tcp packet:
 * > 0000 | 2 | size (excluding final 16-bit crc)
 * > 0002 | 2 | uuid
 * > 0004 | 2 | type
 * > 0006 | n | data (n=size-6)
 * > size | 2 | crc16
 **/
/*----------------------------------------------------------------------------*/
#define TCPD_DATA_MINSIZE 6
#define TCPD_TYPE_TX 0x1000
#define TCPD_TYPE_RX 0x2000
#define TCPD_TYPE_OK 0x4000
#define TCPD_TYPE_QQ 0x8000
/* to be used by application */
#define TCPD_TYPE_APPMASK 0x0FFF
/*----------------------------------------------------------------------------*/
/* limit this so packet size is 32? */
#define ESP01_DATASIZE 26
/*----------------------------------------------------------------------------*/
/* using this as pointer only? */
typedef struct _my1esp01_data_t {
	unsigned int size;
	unsigned int uuid;
	unsigned int type;
	unsigned char data[ESP01_DATASIZE];
} my1esp01_data_t;
/*----------------------------------------------------------------------------*/
#define ESP01_DATA_POSTED 0x10
#define ESP01_DATA_CLOSED 0x20
/*----------------------------------------------------------------------------*/
#define esp01_data_posted() (espd.stat&ESP01_DATA_POSTED)
#define esp01_data_closed() (espd.stat&ESP01_DATA_CLOSED)
/*----------------------------------------------------------------------------*/
void esp01_data_send(unsigned char* dat4, unsigned char size) {
	espd.stat = ESP01_STAT_OK;
	esp01_puts("AT+CIPSEND=");
	/* call this after esp01_ap_read_req, espd.test is ch. num (ascii) */
	esp01_send(espd.test);
	esp01_puts(",");
	uint8_2cstr(espd.tbuf,size);
	esp01_puts(espd.tbuf);
	esp01_puts("\r\n");
	esp01_wait_ok();
	esp01_wait();
	if (espd.buff[espd.fill-2]=='>'&&espd.buff[espd.fill-1]==' ') {
		for (espd.loop=0;espd.loop<size;espd.loop++)
			esp01_send(dat4[espd.loop]);
		/* not waiting for reply */
		espd.stat |= ESP01_DATA_POSTED;
	}
}
/*----------------------------------------------------------------------------*/
void esp01_data_done(void) {
	esp01_puts(ESP01CMD_SOCKENDS"=");
	esp01_send(espd.test);
	esp01_puts("\r\n");
	/* not waiting for reply */
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1ESP01_DATA_H__ */
/*----------------------------------------------------------------------------*/
