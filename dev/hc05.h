/*----------------------------------------------------------------------------*/
#ifndef __HC05_H__
#define __HC05_H__
/*----------------------------------------------------------------------------*/
/**
 * hc05.h
 * - library for hc05 bluetooth module
 *   = AT mode need 38400 baudrate
 *     # push button on powerup (or KEYES pin pull up!)
 *   = LED blinks slower than normal mode
 * - requires atcmd
 *   = atcmd by default uses uart2stc
 *   = define __ATCMD_ON_UART__ to use uart
**/
/*----------------------------------------------------------------------------*/
#define HC05_WAIT_DELAY 2000
#define ATCMD_WAIT_DELAY HC05_WAIT_DELAY
#include "atcmd.h"
/*----------------------------------------------------------------------------*/
#ifndef BTNAME
#define BTNAME "my1bluez51"
#endif
#ifndef BTPASS
#define BTPASS "5105"
#endif
#ifndef ATMODE_BAUD
#define ATMODE_BAUD BRT_BAUD38400
#endif
/*----------------------------------------------------------------------------*/
#define HC05CMD_AT "AT\r\n"
#define HC05CMD_ATVERS "AT+VERSION?\r\n"
#define HC05CMD_ATNAME "AT+NAME="
#define HC05CMD_ATPASS "AT+PSWD="
#define HC05CMD_ATENDS "\r\n"
#define HC05CMD_ATNAME_DEF HC05CMD_ATNAME BTNAME HC05CMD_ATENDS
#define HC05CMD_ATPASS_DEF HC05CMD_ATPASS "\"" BTPASS "\"" HC05CMD_ATENDS
/*----------------------------------------------------------------------------*/
#define HC05_OK ATCMD_OK
#define HC05_TIMEOUT ATCMD_TIMEOUT
/*----------------------------------------------------------------------------*/
#define hc05_init atcmd_init
#define hc05_send atcmd_send
#define hc05_read atcmd_read
#define hc05_puts atcmd_puts
#define hc05_peek atcmd_peek
/*----------------------------------------------------------------------------*/
#define hc05_find atcmd_find
#define hc05_wait_ok atcmd_wait_ok
#define hc05_buff_ok atcmd_buff_ok
#define hc05_wait_send atcmd_wait_send
/*----------------------------------------------------------------------------*/
#define hc05_wait(pbuf,size) atcmd_read_wait(pbuf,size)
#define hc05_getvers(pbuf,size) atcmd_wait_send(pbuf,size,HC05CMD_ATVERS)
#define hc05_setname(pbuf,size) atcmd_wait_send(pbuf,size,HC05CMD_ATNAME_DEF)
#define hc05_setpin(pbuf,size) atcmd_wait_send(pbuf,size,HC05CMD_ATPASS_DEF)
#define hc05_purge() while (!atcmd_timeout(HC05_WAIT_DELAY)) atcmd_read()
/*----------------------------------------------------------------------------*/
void hc05_init_atmode(void) {
#ifdef __ATCMD_ON_UART__
	SCON = 0x50;
	brt_serial1(); /* use brt instead of timer 1 */
#else
	S2CON = 0x50;
	brt_smod0();
#endif
	brt_load((unsigned char)ATMODE_BAUD);
	brt_mode1t();
	brt_enable();
}
/*----------------------------------------------------------------------------*/
#endif /* __HC05_H__ */
/*----------------------------------------------------------------------------*/
